﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BuscoSalon.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NucleoBuscoSalon.Contratos;
using NucleoBuscoSalon.Implementaciones;
using NucleoBuscoSalon.Models;

namespace BuscoSalon.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReservacionesController : ControllerBase
    {

        [Authorize]
        [HttpPost("agregarreservacion")]
        public ActionResult<Resultado> AgregarReservacion([FromBody] Reservacion reservacion)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.CrearReservacion(reservacion);
            return resultado;
        }

        [Authorize]
        [HttpPut("autorizarreservacion")]
        public ActionResult<Resultado> AutorizarReservacion([FromBody] Reservacion reservacion)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.AutorizarReservacion(reservacion.NoReservacion);
            return resultado;
        }

        [Authorize]
        [HttpPut("calificarreservacion")]
        public ActionResult<Resultado> CalificarReservacion([FromBody] Reservacion reservacion)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.CalificarReservacion(reservacion.NoReservacion, reservacion.Estatus, reservacion.SocioNombre);
            return resultado;
        }

        [Authorize]
        [HttpPut("cancelarreservacion")]
        public ActionResult<Resultado> CancelarReservacion([FromBody] Reservacion reservacion)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.CancelarReservacion(reservacion.NoReservacion);
            return resultado;
        }

        [Authorize]
        [HttpPut("eliminarreservacion")]
        public ActionResult<Resultado> ElimincarReservacion([FromBody] Reservacion reservacion)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.EliminarReservacion(reservacion.NoReservacion);
            return resultado;
        }

        [Authorize]
        [HttpGet("consultarmisreservaciones/{id}")]
        public ActionResult<List<Reservacion>> ConsultarMisReservaciones(long id)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.ConsultarReservaciones(id, 5);
            return resultado;
        }

        [Authorize]
        [HttpGet("consultarporautorizar/{id}")]
        public ActionResult<List<Reservacion>> ConsultarPorAutorizar(long id)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.ConsultarReservaciones(id, 1);
            return resultado;
        }

        [Authorize]
        [HttpGet("consultarporcalificar/{id}")]
        public ActionResult<List<Reservacion>> ConsultarPorCalificar(long id)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.ConsultarReservaciones(id, 6);
            return resultado;
        }

        [Authorize]
        [HttpGet("consultarporinmueble/{id}")]
        public ActionResult<List<Reservacion>> ConsultarPorInmueble(long id)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.ConsultarReservaciones(id, 2);
            return resultado;
        }

        [Authorize]
        [HttpGet("consultarporsocio/{id}")]
        public ActionResult<List<Reservacion>> ConsultarPorSocio(long id)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.ConsultarReservaciones(id, 3);
            return resultado;
        }

        [Authorize]
        [HttpGet("consultarporid/{id}")]
        public ActionResult<Reservacion> ConsultarPorID(long id)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.ConsultarReservaciones(id, 4);
            return (resultado.Count > 0 ? resultado[0]: new Reservacion());
        }

    }
}
