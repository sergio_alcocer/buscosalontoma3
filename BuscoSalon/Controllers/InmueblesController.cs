﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BuscoSalon.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NucleoBuscoSalon.Contratos;
using NucleoBuscoSalon.Implementaciones;
using NucleoBuscoSalon.Models;

namespace BuscoSalon.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InmueblesController : ControllerBase
    {
        [Authorize]
        [HttpGet]
        [Route("lista")]
        public IActionResult Lista()
        {
            return Ok(new List<MCliente>()
            {
                new MCliente()
                {
                    Nombre="Hola",
                    Apellido="Mundo",
                    Edad=35,
                    FechaDeNacimiento=DateTime.Now
                }
            });
        }

        // [Authorize]
        [HttpGet("consultarestadosporpais/{id}")]
        public ActionResult<IEnumerable<Estado>> ConsultarEstadosPorPais(long ID)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var estados = o_implementaciones.ConsultarEstadosPorPais(ID);
            return estados;
        }

        // [Authorize]
        [HttpGet("consultarciudadesporestado/{id}")]
        public ActionResult<IEnumerable<Ciudad>> ConsultarCiudadesPorEstado(long ID)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var ciudad = o_implementaciones.ConsultarCiudadesPorEstado(ID);
            return ciudad;
        }

        // [Authorize]
        [HttpGet("consultarcoloniasporcp/{codigoPostal}")]
        public ActionResult<IEnumerable<Colonia>> ConsultarColoniasPorCP(string codigoPostal)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var colonias = o_implementaciones.ConsultarColoniasPorCP(codigoPostal);
            return colonias;
        }

        // [Authorize]
        [HttpGet("consultarcoloniasporciudad/{id}")]
        public ActionResult<IEnumerable<Colonia>> ConsultarColonias(long ID)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var colonias = o_implementaciones.ConsultarColoniasPorCiudad(ID);
            return colonias;
        }

        // [Authorize]
        [HttpGet("consultartiposambientes")]
        public ActionResult<IEnumerable<Ambiente>> ConsultarTiposAmbientes()
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var tiposambientes = o_implementaciones.ConsultarTiposAmbientes();
            return tiposambientes;
        }

        [Authorize]
        [HttpGet("consultarinmuebles/{filtroBusqueda}/{idSocio}")]
        public ActionResult<IEnumerable<Inmueble>> ConsultarInmuebles(string filtroBusqueda, long idSocio)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var inmuebles = o_implementaciones.ConsultarInmuebles(filtroBusqueda, idSocio);
            return inmuebles;
        }

        [Authorize]
        [HttpGet("consultarinmueble/{idInmueble}")]
        public ActionResult<Inmueble> ConsultarInmueblePorID(long idInmueble)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var inmueble = o_implementaciones.ConsultarInmueblePorID(idInmueble);
            return inmueble;
        }

        [HttpPost("agregarinmueble")]
        public ActionResult<Resultado> AgregarInmueble([FromBody] Inmueble inmueble)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.CrearInmueble(inmueble);
            return resultado;
        }

        [Authorize]
        [HttpPut("modificarinmueble")]
        public ActionResult<Resultado> ModificarInmueble([FromBody] Inmueble inmueble)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.ModificarInmueble(inmueble);
            return resultado;
        }

        [Authorize]
        [HttpDelete("eliminarinmueble/{idInmueble}")]
        public ActionResult<Resultado> EliminarInmueble(long idInmueble)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.EliminarInmueble(idInmueble);
            return resultado;
        }

        [HttpPost("buscarinmuebles")]
        public ActionResult<IEnumerable<Inmueble>> BuscarInmuebles([FromBody] Parametro_Busqueda parametro)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.BuscarInmuebles(parametro);
            return resultado;
        }

        [HttpGet("consultarpaquetes/{idInmueble}")]
        public ActionResult<IEnumerable<Paquete>> ConsultarPaquetes(long idInmueble)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var inmuebles = o_implementaciones.ConsultarPaquetes(idInmueble);
            return inmuebles;
        }

        [Authorize]
        [HttpGet("consultarpaquete/{idPaquete}")]
        public ActionResult<Paquete> ConsultarPaquetePorID(long idPaquete)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var inmueble = o_implementaciones.ConsultarPaquetePorID(idPaquete);
            return inmueble;
        }

        [HttpPost("agregarpaquete")]
        public ActionResult<Resultado> AgregarPaquete([FromBody] Paquete paquete)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.CrearPaquete(paquete);
            return resultado;
        }

        [Authorize]
        [HttpPut("modificarpaquete")]
        public ActionResult<Resultado> ModificarPaquete([FromBody] Paquete paquete)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.ModificarPaquete(paquete);
            return resultado;
        }

        [Authorize]
        [HttpDelete("eliminarpaquete/{idPaquete}")]
        public ActionResult<Resultado> EliminarPaquete(long idPaquete)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.EliminarPaquete(idPaquete);
            return resultado;
        }

        [Authorize]
        [HttpPost("agregarimagen")]
        public ActionResult<Resultado> AgregarImagen([FromBody] Imagen imagen)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.CrearImagen(imagen);
            return resultado;
        }

        [HttpGet("consultarimagenes/{idInmueble}")]
        public ActionResult<IEnumerable<Imagen>> ConsultarImagenes(long idInmueble)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var inmuebles = o_implementaciones.ConsultarImagenes(idInmueble);
            return inmuebles;
        }

        [Authorize]
        [HttpDelete("eliminarimagen/{idImagen}")]
        public ActionResult<Resultado> EliminarImagen(long idImagen)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.EliminarImagen(idImagen);
            return resultado;
        }

        [HttpGet("consultarcalificaciones/{idInmueble}")]
        public ActionResult<IEnumerable<Calificacion>> ConsultarCalificaciones(long idInmueble)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var calificaciones = o_implementaciones.ObtenerCalificacionesInmueble(idInmueble);
            return calificaciones;
        }

        [HttpGet("consultarfechasinmueble/{idInmueble}")]
        public ActionResult<IEnumerable<Reservacion>> ConsultarFechasInmuebles(long idInmueble)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var reservaciones = o_implementaciones.ConsultarFechasInmuebles(idInmueble);
            return reservaciones;
        }

        [Authorize]
        [HttpGet("validarnuevoinmuebleporidsocio/{idSocio}")]
        public ActionResult<Resultado> ValidarNuevoInmueblePorIDSocio(long idSocio)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var inmueble = o_implementaciones.ValidarNuevoInmueblePorIDSocio(idSocio);
            return inmueble;
        }

        [HttpGet("consultarimagenminuatura/{idInmueble}")]
        public ActionResult<Imagen> ConsultarImagenMiniatura(long idInmueble)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var inmuebles = o_implementaciones.ConsultarImagenMiniatura(idInmueble);
            return inmuebles;
        }
    }
}
