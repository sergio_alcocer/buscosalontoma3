﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NucleoBuscoSalon.Contratos;
using NucleoBuscoSalon.Implementaciones;
using Microsoft.AspNetCore.Authorization;

namespace BuscoSalon.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        [Authorize]
        [HttpGet("consultarusuarios/{filtroBusqueda}")]
        public ActionResult<IEnumerable<Usuario>> ConsultarUsuarios(string filtroBusqueda)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var usuarios = o_implementaciones.ConsultarUsuarios(filtroBusqueda);
            return usuarios;
        }

        [Authorize]
        [HttpGet("consultarusuario/{idUsuario}")]
        public ActionResult<Usuario> ConsultarUsuarioPorID(long idUsuario)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var usuario = o_implementaciones.ConsultarUsuarioPorID(idUsuario);
            return usuario;
        }

        [HttpGet("existenickname/{NickName}")]
        public ActionResult<IEnumerable<Usuario>> consultarnickname(string NickName)
        {
            ///var token=Guid.NewGuid
            Implementaciones o_implementaciones = new Implementaciones();
            var usuario = o_implementaciones.existenickname(NickName);
            return usuario;
        }
        [Authorize]
        [HttpGet("existenicknameusuario/{NickName}/{Id}")]
        public ActionResult<IEnumerable<Usuario>> ExisteNicknameUsuario(string NickName,Int64 Id)
        {
            Usuario p_usuario = new Usuario();
            p_usuario.ID = Id;
            p_usuario.Nick = NickName;

            Implementaciones o_implementaciones = new Implementaciones();
            var usuario = o_implementaciones.existenicknameusuario(p_usuario);
            return usuario;
        }

        [HttpGet("existeemail/{Email}")]
        public ActionResult<IEnumerable<Usuario>> existeemail(string Email)
        {
            ///var token=Guid.NewGuid
            Implementaciones o_implementaciones = new Implementaciones();
            var usuario = o_implementaciones.existeemail(Email);
            return usuario;
        }
        [Authorize]
        [HttpGet("existeemailusuario/{Email}/{Id}")]
        public ActionResult<IEnumerable<Usuario>> ExisteEmailUsuario(string Email, Int64 Id)
        {
            Usuario p_usuario = new Usuario();
            p_usuario.ID = Id;
            p_usuario.Email = Email;

            Implementaciones o_implementaciones = new Implementaciones();
            var usuario = o_implementaciones.existenicknameusuario(p_usuario);
            return usuario;
        }

        [HttpGet("consultaremail/{Email}")]
        public ActionResult<string> consultaremail(string Email)
        {
            ///var token=Guid.NewGuid
            Implementaciones o_implementaciones = new Implementaciones();
            var usuario = o_implementaciones.consultaremail(Email);
            return usuario;
        }

        [HttpPost("agregarusuario")]
        public ActionResult<Resultado> AgregarUsuario([FromBody] Usuario usuario)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.CrearUsuario(usuario);
            return resultado;
        }

        [Authorize]
        [HttpPut("modificarusuario")]
        public ActionResult<Resultado> ModificarUsuario([FromBody] Usuario usuario)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.ModificarUsuario(usuario);
            return resultado;
        }

        [Authorize]
        [HttpPut("modificarusuarioperfil")]
        public ActionResult<Resultado> ModificarUsuarioPerfil([FromBody] Usuario usuario)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.ModificarUsuarioPerfil(usuario);
            return resultado;
        }        

        [Authorize]
        [HttpDelete("eliminarusuario/{idUsuario}")]
        public ActionResult<Resultado> EliminarUsuario(long idUsuario)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.EliminarUsuario(idUsuario);
            return resultado;
        }

        [Authorize]
        [HttpPut("restablecerpassword")]
        public ActionResult<Resultado> RestablecerPassword([FromBody] Usuario usuario)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.RestablecerPasswordUsuario(usuario.ID);
            return resultado;
        }

        [Authorize]
        [HttpPost("actualizapasswordusuario")]
        public ActionResult<Resultado> ActualizaPasswordUsuario([FromBody] ActualizaPassword actualiza)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.ActualizaPasswordUsuario(actualiza);
            return resultado;
        }        

        [HttpGet("CodigoRecuperacion/{Email}")]
        public ActionResult<Resultado> CodigoRecuperacion(string Email)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var Respuesta = o_implementaciones.CodigoRecuperacion(Email);
            return Respuesta;
        }

        [HttpPost("actualizapasspublico")]
        public ActionResult<Resultado> actualizapasspublico([FromBody] Usuario usuario)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.actualizapasspublico(usuario);
            return resultado;
        }
    }
}
