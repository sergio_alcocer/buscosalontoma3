﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BuscoSalon.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NucleoBuscoSalon.Contratos;
using NucleoBuscoSalon.Implementaciones;
using NucleoBuscoSalon.Models;

namespace BuscoSalon.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParametrosController : Controller
    {
        [Authorize]
        [HttpPut("modificarparametros")]
        public ActionResult<Resultado> ModificarParametros([FromBody] Parametros p_parametros)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.ModificarParametros(p_parametros);
            return resultado;
        }

        //[Authorize]
        [HttpGet("consultarparametros")]
        public ActionResult<Parametros> ConsultarParametros()
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.ConsultarParametros();
            return resultado;
        }

    }
}
