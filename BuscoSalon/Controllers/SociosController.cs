﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BuscoSalon.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NucleoBuscoSalon.Contratos;
using NucleoBuscoSalon.Implementaciones;

namespace BuscoSalon.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SociosController : ControllerBase
    {
        [Authorize]
        [HttpGet]
        [Route("lista")]
        public IActionResult Lista()
        {
            return Ok(new List<MCliente>()
            {
                new MCliente()
                {
                    Nombre="Hola",
                    Apellido="Mundo",
                    Edad=35,
                    FechaDeNacimiento=DateTime.Now
                }
            });
        }

        [Authorize]
        [HttpGet("consultarsociosinternos/{filtroBusqueda}")]
        public ActionResult<IEnumerable<Socio>> ConsultarSociosInternos(string filtroBusqueda)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var socios = o_implementaciones.ConsultarSocios(filtroBusqueda, 2);
            return socios;
        }

        [Authorize]
        [HttpGet("consultarsociosexterno/{filtroBusqueda}")]
        public ActionResult<IEnumerable<Socio>> ConsultarSociosExternos(string filtroBusqueda)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var socios = o_implementaciones.ConsultarSocios(filtroBusqueda, 1);
            return socios;
        }

        [Authorize]
        [HttpGet("ConsultarSociosSinActivar/{filtroBusqueda}")]
        public ActionResult<IEnumerable<Socio>> ConsultarSociosSinActivar(string filtroBusqueda)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var socios = o_implementaciones.ConsultarSociosSinActivar(filtroBusqueda, 2);
            return socios;
        }

        [Authorize]
        [HttpGet("consultarsocio/{idSocio}")]
        public ActionResult<Socio> ConsultarSocioPorID(long idSocio)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var socio = o_implementaciones.ConsultarSocioPorID(idSocio);
            return socio;
        }

        [HttpGet("consultarnickname/{NickName}")]
        public ActionResult<IEnumerable<Socio>> consultarnickname(string NickName)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var socio = o_implementaciones.consultarnicknameSocio(NickName);
            return socio;
        }

        [HttpPut("agregarsocio")]
        public ActionResult<Resultado> AgregarSocio([FromBody] Socio socio)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.CrearSocio(socio);
            return resultado;
        }

        [Authorize]
        [HttpPut("modificarsocio")]
        public ActionResult<Resultado> ModificarSocio([FromBody] Socio socio)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.ModificarSocio(socio);
            return resultado;
        }
        [Authorize]
        [HttpPut("modificarsocioperfil")]
        public ActionResult<Resultado> ModificarSocioPerfil([FromBody] Socio socio)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.ModificarSocioPerfil(socio);
            return resultado;
        }


        [Authorize]
        [HttpPut("activarSocio")]
        public ActionResult<Resultado> ActivarSocio([FromBody] Socio socio)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.ActivarSocio(socio);
            return resultado;
        }

        [Authorize]
        [HttpDelete("eliminarsocio/{idSocio}")]
        public ActionResult<Resultado> EliminarSocio(long idSocio)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.EliminarSocio(idSocio);
            return resultado;
        }

        [Authorize]
        [HttpPut("restablecerpassword")]
        public ActionResult<Resultado> RestablecerPassword([FromBody] Socio socio)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.RestablecerPasswordSocio(socio.ID);
            return resultado;
        }
        [Authorize]
        [HttpPost("actualizapasswordsocio")]
        public ActionResult<Resultado> actualizapasswordsocio([FromBody] ActualizaPassword actualiza)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.ActualizaPasswordSocio(actualiza);
            return resultado;
        }


        [HttpPost]
        [Route("ActualizaPasswordToken")]
        public ActionResult<Resultado> ActualizaPasswordToken(Recuperacion Recupera)
        {
            Implementaciones o_implementaciones = new Implementaciones();
            var resultado = o_implementaciones.ActualizaPasswordToken(Recupera);
            return resultado;
        }

        [Authorize]
        [HttpGet("existenicknamesocio/{NickName}/{Id}")]
        public ActionResult<IEnumerable<Socio>> ExisteNicknameSocio(string NickName, Int64 Id)
        {
            Socio p_socio = new Socio();
            p_socio.ID = Id;
            p_socio.Nick = NickName;
            Implementaciones o_implementaciones = new Implementaciones();
            var socio = o_implementaciones.existenicknamesocio(p_socio);
            return socio;
        }
        [Authorize]
        [HttpGet("existeemailsocio/{Email}/{Id}")]
        public ActionResult<IEnumerable<Socio>> ExisteEmailSocio(string Email, Int64 Id)
        {
            Socio p_socio = new Socio();
            p_socio.ID = Id;
            p_socio.Email = Email;
            Implementaciones o_implementaciones = new Implementaciones();
            var socio = o_implementaciones.existenicknamesocio(p_socio);
            return socio;
        }
    }
}
