﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using NucleoBuscoSalon.Models;
using NucleoBuscoSalon.Contratos;
using BuscoSalon.ViewModel;

using Microsoft.EntityFrameworkCore;
using AutoMapper;
using System.IO;




namespace BuscoSalon.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnuncioController : ControllerBase
    {
        private IAnuncioRepositorio AnuncioRepositorio;
        private ISociosRepositorio SocioRepositorio;
        private IParametrosRepositorio ParametrosRepositorio;
        private readonly IMapper Mapper;

        public AnuncioController(ISociosRepositorio SocioRepositorio, IAnuncioRepositorio AnuncioRepositorio, IParametrosRepositorio ParametrosRepositorio, IMapper Mapper)
        {
            this.SocioRepositorio = SocioRepositorio;
            this.AnuncioRepositorio = AnuncioRepositorio;
            this.ParametrosRepositorio = ParametrosRepositorio;
            this.Mapper = Mapper;

        }

        [Route("obtenerParametros"), HttpGet]
        public ParametrosViewModel obtenerParametros()
        {
            ParametrosViewModel parametrosViewModel = new ParametrosViewModel();

            try
            {
                var parametros = ParametrosRepositorio.Parametros.FirstOrDefault();
                parametrosViewModel = Mapper.Map<ParametrosViewModel>(parametros);

            }
            catch (Exception ex) { 
            
            }

            return parametrosViewModel;

        }


        [Route("obtenerSocios"), HttpGet]
        public List<ComboViewModel> obtenerSocios()
        {
            List<ComboViewModel> comboViewModels = new List<ComboViewModel>();

            try {

                 var buscarSocios = SocioRepositorio.Socios
                               .Include(x=> x.Tipos_Socio)
                               .Where(x=> x.Tipos_Socio.Tipo_Nombre == "SOCIO INTERNO")
                               .OrderBy(x => x.Socio_Nombre);

                comboViewModels = Mapper.Map<List<ComboViewModel>>(buscarSocios.ToList());

            }catch(Exception ex)
            {

            }

            return comboViewModels;
        }

        [Route("obtenerImagen"), HttpGet]
        public ActionResult obtenerCadenaImagen(int anuncio_id) {
            Resultado resultado = new Resultado();
            AnuncioViewModel anuncioViewModel;

            try {

                if (anuncio_id > 0)
                {

                    var buscarAnuncio = AnuncioRepositorio.Anuncios
                                         .Include(x => x.Socios)
                                         .Where(x => x.Anuncio_ID == anuncio_id);

                    anuncioViewModel = Mapper.Map<AnuncioViewModel>(buscarAnuncio.FirstOrDefault());

                    var folderName = Path.Combine("Recursos", "imagenes");
                    var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
                    var fullPath = Path.Combine(pathToSave, anuncioViewModel.archivo);

                    byte[] byteData = System.IO.File.ReadAllBytes(fullPath);
                    string imreBase64Data = Convert.ToBase64String(byteData);
                    string imgDataURL = string.Format("data:image/png;base64,{0}", imreBase64Data);

                    resultado.MensajeError = "ok";
                    resultado.Descripcion = imgDataURL;
                }
                else {
                    resultado.MensajeError = "no_imagen";
                    resultado.Descripcion = "";
                }

            }
            catch(Exception ex)
            {
                resultado.MensajeError = ex.Message;

            }
            return Ok(resultado);
        }

        [Route("activarImagen/{anuncio_id}"),HttpGet]
        public ActionResult activarImagen(int anuncio_id )
        {
            Resultado resultado = new Resultado();

            try
            {
                AnuncioRepositorio.activarImagen(anuncio_id);
                resultado.MensajeError = "ok";
            }
            catch (Exception ex) {
                resultado.MensajeError = ex.Message;
            }

            return Ok(resultado);
        }

        [Route("obtenerImagenesVigentes"),HttpGet]
        public ActionResult obtenerImagenesVigentes()
        {
            List<AnuncioViewModel> listaAnunciaViewModel = new List<AnuncioViewModel>();

            var filtroAnuncios = AnuncioRepositorio.Anuncios
                           .Include(x => x.Socios)
                           .Where(x => x.Estatus == 1 && DateTime.Now >= x.Fecha_Inicio && DateTime.Now <= x.Fecha_Final)
                           .OrderBy(x => x.Fecha_Final);

            listaAnunciaViewModel = Mapper.Map<List<AnuncioViewModel>>(filtroAnuncios.ToList());

            return Ok(listaAnunciaViewModel);
        }

        [Route("obtenerAnunciosAplicar"), HttpGet]
        public ActionResult obtenerAnunciosAplicar(string search, int pagina = 1)
        {

            int renglonesTomar = Constantes.NUMERO_RENGLONES;
            int start = 0;

            List<AnuncioViewModel> listaAnunciaViewModel = new List<AnuncioViewModel>();

            var filtroAnuncios = AnuncioRepositorio.Anuncios
                                 .Include(x => x.Socios)
                                 .Where(x=> x.Estatus == 2)
                                 .Where(x => string.IsNullOrWhiteSpace(search) ||
                                  x.Socios.Socio_Nombre.Contains(search) ||
                                  x.Socios.Socio_Paterno.Contains(search) ||
                                  x.Socios.Socio_Materno.Contains(search))
                                 .OrderBy(x => x.Socios.Socio_Nombre)
                                 .ThenBy(x => x.Socios.Socio_Paterno);
            start = (pagina * renglonesTomar) - renglonesTomar;

            var anunciosVisualizar = filtroAnuncios
               .Skip(start)
             .Take(renglonesTomar);

            listaAnunciaViewModel = Mapper.Map<List<AnuncioViewModel>>(filtroAnuncios.ToList());

            return Ok(new
            {
                data = listaAnunciaViewModel,
                total = filtroAnuncios.Count(),
                rows = renglonesTomar

            });

        }

        [Route("obtenerAnuncios"), HttpGet]
        public ActionResult obtenerAnuncios(string search, int pagina = 1) {

            int renglonesTomar = Constantes.NUMERO_RENGLONES;
            int start = 0;

            List<AnuncioViewModel> listaAnunciaViewModel = new List<AnuncioViewModel>();

            var filtroAnuncios = AnuncioRepositorio.Anuncios
                                 .Include(x => x.Socios)
                                 .Where(x => string.IsNullOrWhiteSpace(search) ||
                                  x.Socios.Socio_Nombre.Contains(search) ||
                                  x.Socios.Socio_Paterno.Contains(search) ||
                                  x.Socios.Socio_Materno.Contains(search))
                                 .OrderBy(x => x.Socios.Socio_Nombre)
                                 .ThenBy(x => x.Socios.Socio_Paterno);
            start = (pagina * renglonesTomar) - renglonesTomar;

            var anunciosVisualizar = filtroAnuncios
               .Skip(start)
             .Take(renglonesTomar);

            listaAnunciaViewModel = Mapper.Map<List<AnuncioViewModel>>(filtroAnuncios.ToList());

            return Ok(new
            {
                data = listaAnunciaViewModel,
                total = filtroAnuncios.Count(),
                rows = renglonesTomar

            });

        }

        [Route("guardar"), HttpPost]
        public ActionResult guardarAnunciar(AnuncioViewModel anuncioViewModel)
        {
            Resultado resultado = new Resultado();
            Anuncios anuncios = new Anuncios();
            ParametrosViewModel parametrosViewModel;
            string nombreImagen;
            

            try {


                var parametros = ParametrosRepositorio.Parametros.FirstOrDefault();
                parametrosViewModel = Mapper.Map<ParametrosViewModel>(parametros);

                anuncioViewModel.fecha_final = obtenerFechaFormatada(anuncioViewModel.fecha_final);
                anuncioViewModel.fecha_inicio = obtenerFechaFormatada(anuncioViewModel.fecha_inicio);
                anuncios = Mapper.Map<Anuncios>(anuncioViewModel);


                if (anuncios.Anuncio_ID == 0)
                {
                    nombreImagen = guardarImagen(anuncioViewModel);
                    anuncios.Archivo = nombreImagen;
                    anuncios.Url = parametrosViewModel.Url_Imagen + nombreImagen;
                    AnuncioRepositorio.guardar(anuncios);

                }
                else {

                    if (anuncioViewModel.edito_foto)
                    {
                        nombreImagen= guardarImagen(anuncioViewModel);
                        anuncios.Archivo = nombreImagen;
                        anuncios.Url = parametrosViewModel.Url_Imagen + nombreImagen;

                    }
                    AnuncioRepositorio.editar(anuncios, anuncioViewModel.edito_foto);
                }


                resultado.MensajeError = "ok";

            }
            catch(Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }

            return Ok(resultado);
        }

        private string guardarImagen(AnuncioViewModel anuncioViewModel)
        {
            string uniqueFileName = null;
            string cadena = "";
            int posicion;

            if(anuncioViewModel.anuncio_id == 0)
             uniqueFileName = Guid.NewGuid().ToString() + "_Image.png";
            else
              uniqueFileName = Guid.NewGuid().ToString() + "_" + anuncioViewModel.anuncio_id.ToString()  + "_Image.png";

            posicion = anuncioViewModel.file.IndexOf(",");
            cadena = anuncioViewModel.file.Substring(posicion + 1, anuncioViewModel.file.Length - (posicion + 1));

            var folderName = Path.Combine("Recursos", "imagenes");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);


            if (anuncioViewModel.anuncio_id != 0) {
                var fullPathEliminar = Path.Combine(pathToSave, anuncioViewModel.archivo);

                if (System.IO.File.Exists(fullPathEliminar))
                {
                    System.IO.File.Delete(fullPathEliminar);
                }

            }

            var fullPath = Path.Combine(pathToSave, uniqueFileName);
            byte[] bytes = Convert.FromBase64String(cadena);
            System.IO.File.WriteAllBytes(fullPath, bytes);

            return uniqueFileName;

        }

        private string obtenerFechaFormatada(string fecha)
        {
            string respuesta = "";
            int posicion;

            posicion = fecha.IndexOf("T");
            respuesta = fecha.Substring(0, posicion);
            return respuesta;
        }

    }
}
