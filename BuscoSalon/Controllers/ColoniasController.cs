﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NucleoBuscoSalon.Models;
using NucleoBuscoSalon.Contratos;
using BuscoSalon.ViewModel;

using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace BuscoSalon.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ColoniasController : ControllerBase
    {
        private IColoniaRepositorio ColoniasRepositorio;
        private ICiudadRepositorio CiudadRepositorio;
        private readonly IMapper Mapper;

        public ColoniasController( IColoniaRepositorio ColoniasRepositorio, ICiudadRepositorio CiudadRepositorio, IMapper Mapper) {
            this.ColoniasRepositorio = ColoniasRepositorio;
            this.CiudadRepositorio = CiudadRepositorio;
            this.Mapper = Mapper;
        }


        [  Route("guardar"),  HttpPost]
        public ActionResult guardarColonia([FromBody] ColoniaViewModel coloniaViewModel) {

            Resultado resultado = new Resultado();
            Colonias colonias = new Colonias();

            try {

                colonias = Mapper.Map<Colonias>(coloniaViewModel);

                if (colonias.Colonia_ID == 0)
                {
                    // guardar
                    ColoniasRepositorio.guardar(colonias);
                }
                else {
                    ColoniasRepositorio.editar(colonias);
                }

                resultado.MensajeError = "ok";

            }catch(Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }

            return Ok(resultado);
        }

        [Route("obtenerTipoColonia"), HttpGet]
        public List<ComboSimpleViewModel> obtenerTipoColonia() {
            List<ComboSimpleViewModel> ComboSimpleViewModel = new List<ComboSimpleViewModel>();

            try
            {

                List<string> buscarTipoColonia = ColoniasRepositorio.Colonia
                    .OrderBy(x=> x.Tipo)
                    .Select(x => x.Tipo).Distinct().ToList();


                foreach (var item in buscarTipoColonia) {
                    ComboSimpleViewModel.Add(new ComboSimpleViewModel(item));
                }

            }
            catch (Exception ex) { 
            
            }

            return ComboSimpleViewModel;
        
        }


        [Route("obtenerCiudad"), HttpGet]
        public List<ComboViewModel> obtenerCiudades() {

            List<ComboViewModel> comboViewModels = new List<ComboViewModel>();

            try
            {

                var buscarCiudades = CiudadRepositorio.Ciudades
                                    .Where(x => x.Ciudad_Estatus == 1)
                                    .OrderBy(x => x.Ciudad_Nombre);

                comboViewModels = Mapper.Map<List<ComboViewModel>>(buscarCiudades.ToList());


            }
            catch (Exception ex) { 
            
            }


            return comboViewModels;

        }

        [Route("obtenerColonias"), HttpGet]
        public ActionResult obtenerColonias(string search, int pagina = 1) {

            int renglonesTomar = Constantes.NUMERO_RENGLONES;
            int start = 0;
            List<ColoniaViewModel> listaColoniaViewModel = new List<ColoniaViewModel>();

            var filtroColonias = ColoniasRepositorio.Colonia
                 .Include(x => x.Ciudades)
                 .Where(x => string.IsNullOrWhiteSpace(search) ||
                     x.Nombre.Contains(search) ||
                     x.Ciudades.Ciudad_Nombre.Contains(search) ||
                     x.Tipo.Contains(search))
                 .OrderBy(x => x.Ciudades.Ciudad_Nombre)
                 .ThenBy(x => x.Nombre);

            start = (pagina * renglonesTomar) - renglonesTomar;

            var coloniasVisualizar = filtroColonias
              .Skip(start)
              .Take(renglonesTomar);

            listaColoniaViewModel = Mapper.Map<List<ColoniaViewModel>>(coloniasVisualizar.ToList());

            return Ok(
                 new {
                     data = listaColoniaViewModel,
                     total = filtroColonias.Count(),
                     rows = renglonesTomar
                 }
                );
        }

    }
}
