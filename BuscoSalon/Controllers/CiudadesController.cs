﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NucleoBuscoSalon.Models;
using NucleoBuscoSalon.Contratos;
using BuscoSalon.ViewModel;

using Microsoft.EntityFrameworkCore;
using AutoMapper;


namespace BuscoSalon.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CiudadesController : ControllerBase
    {
        private ICiudadRepositorio CiudadRepositorio;
        private IEstadosRepositorio EstadosRepositorio;
        private readonly IMapper Mapper;

        public CiudadesController(ICiudadRepositorio CiudadRepositorio, IEstadosRepositorio EstadosRepositorio, IMapper Mapper) {

            this.CiudadRepositorio = CiudadRepositorio;
            this.EstadosRepositorio = EstadosRepositorio;
            this.Mapper = Mapper;
        }


        [Route("guardar"), HttpPost]
        public ActionResult guardarCiudad([FromBody] CiudadViewModel ciudadViewModel) {
            Resultado resultado = new Resultado();
            Ciudades ciudades = new Ciudades();
            try
            {
                ciudades = Mapper.Map<Ciudades>(ciudadViewModel);


                if (ciudades.Ciudad_ID == 0)
                {

                    CiudadRepositorio.guardar(ciudades);
                }
                else {
                    CiudadRepositorio.editar(ciudades);
                }


                resultado.MensajeError = "ok";
            }
            catch (Exception ex) {
                resultado.MensajeError = ex.Message;
            }

            return Ok(resultado);
        }

        [Route("obtenerEstados"), HttpGet]
        public List<ComboViewModel> obtenerEstados()
        {
            List<ComboViewModel> comboViewModels = new List<ComboViewModel>();

            try
            {
                var buscarEstados = EstadosRepositorio.Estados
                                .Where(x => x.Estado_Estatus == 1)
                                .OrderBy(x => x.Estado_Nombre);

                comboViewModels = Mapper.Map<List<ComboViewModel>>(buscarEstados.ToList());

            }
            catch (Exception ex) { 
            
            }

            return comboViewModels;
        }

        [Route("obtenerCiudades"), HttpGet]
        public ActionResult obtenerCiudades(string search, int pagina = 1) {

            int renglonesTomar = Constantes.NUMERO_RENGLONES;
            int start = 0;
            List<CiudadViewModel> listaCiudadesViewModel = new List<CiudadViewModel>();

            var filtroCiudades = CiudadRepositorio.Ciudades
                .Include(x => x.Estados)
                .Where(x => string.IsNullOrWhiteSpace(search) ||
                   x.Ciudad_Nombre.Contains(search) ||
                   x.Estados.Estado_Nombre.Contains(search))
                .OrderBy(x => x.Estados.Estado_Nombre)
                .ThenBy(x => x.Ciudad_Nombre);

            start = (pagina * renglonesTomar) - renglonesTomar;

            var ciudadesVisualizar = filtroCiudades
                .Skip(start)
              .Take(renglonesTomar);

            listaCiudadesViewModel = Mapper.Map<List<CiudadViewModel>>(ciudadesVisualizar.ToList());

            return Ok(new
            {
                data = listaCiudadesViewModel,
                total = filtroCiudades.Count(),
                rows = renglonesTomar

            });
        }
    }
}
