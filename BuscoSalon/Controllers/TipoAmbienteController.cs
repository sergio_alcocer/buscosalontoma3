﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NucleoBuscoSalon.Models;
using NucleoBuscoSalon.Contratos;
using BuscoSalon.ViewModel;
using AutoMapper;

namespace BuscoSalon.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoAmbienteController : ControllerBase
    {

        private ITipoAmbienteRepositorio TipoAmbienteRepositorio;
        private readonly IMapper Mapper;

        public TipoAmbienteController(ITipoAmbienteRepositorio TipoAmbienteRepositorio, IMapper Mapper) {
            this.TipoAmbienteRepositorio = TipoAmbienteRepositorio;
            this.Mapper = Mapper;
        }


        [HttpPost]
        public IActionResult guardarTipoAmbiente([FromBody] TipoAmbienViewModel tipoAmbienViewModel) {
            Resultado resultado = new Resultado();
            Tipos_Ambientes tipos_Ambientes;
            try
            {
                tipos_Ambientes = Mapper.Map<Tipos_Ambientes>(tipoAmbienViewModel);

                if (tipos_Ambientes.Ambiente_ID == 0)
                {
                    // guardar

                    TipoAmbienteRepositorio.guadar(tipos_Ambientes);
                    resultado.MensajeError = "ok";
                }
                else {

                    // editar
                    TipoAmbienteRepositorio.editar(tipos_Ambientes);
                    resultado.MensajeError = "ok";
                }

            }
            catch (Exception ex) {

                resultado.MensajeError = ex.Message;
            }

            return Ok(resultado);
        }


        [HttpGet]
        public IActionResult obtenerTiposAmbiente(string search, int pagina = 1) {

            int renglonesTomar = Constantes.NUMERO_RENGLONES;
            int start = 0;
            List<TipoAmbienViewModel> listaTipoViewModel = new List<TipoAmbienViewModel>();


            var filtroAmbientes = TipoAmbienteRepositorio.Tipos_Ambientes
                                     .Where(x =>
                                           string.IsNullOrWhiteSpace(search) ||
                                           x.Nombre.Contains(search))
                                     .OrderBy(x => x.Nombre);

            start = (pagina * renglonesTomar) - renglonesTomar;

            var tiposAmbienteVisualizar = filtroAmbientes
                .Skip(start)
                .Take(renglonesTomar);

            listaTipoViewModel = Mapper.Map<List<TipoAmbienViewModel>>(tiposAmbienteVisualizar).ToList();

            return Ok(new
            {
                data = listaTipoViewModel,
                total = filtroAmbientes.Count(),
                rows = renglonesTomar
            });

        }
    }
}
