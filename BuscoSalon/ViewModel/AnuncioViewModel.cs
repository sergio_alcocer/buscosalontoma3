﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BuscoSalon.ViewModel
{
    public class AnuncioViewModel
    {
        public string file { get; set; }
        public string fecha_final { get; set; }
        public string fecha_inicio { get; set; }
        public string socio_id { get; set; }
        public string nombre_socio { get; set; }
        public int anuncio_id { get; set; }
        public int dias { get; set; }
        public decimal costo_total { get; set; }
        public string estatus { get; set; }
        public string url { get; set; }
        public string archivo { get; set; }
        public bool edito_foto { get; set; }
        public string costo_formato { get; set; }
    }
}
