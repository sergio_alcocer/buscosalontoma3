﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BuscoSalon.ViewModel
{
    public class ComboViewModel
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
