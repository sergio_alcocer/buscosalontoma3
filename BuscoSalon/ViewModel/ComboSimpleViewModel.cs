﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BuscoSalon.ViewModel
{
    public class ComboSimpleViewModel
    {
        public string name { get; set; }

        public ComboSimpleViewModel(string name)
        {
            this.name = name;
        }
    }
}
