﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BuscoSalon.ViewModel
{
    public class ParametrosViewModel
    {
        public string Url_Imagen { get; set; }
        public  decimal Precio { get; set; }
    }
}
