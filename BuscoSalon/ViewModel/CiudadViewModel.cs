﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BuscoSalon.ViewModel
{
    public class CiudadViewModel
    {

        public int Ciudad_ID { get; set; }
        public string Estado_ID { get; set; }
        public string Ciudad_Nombre { get; set; }
        public string Ciudad_Estatus { get; set; }
        public string Nombre_Estado { get; set; }

    }
}
