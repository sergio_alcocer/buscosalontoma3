﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BuscoSalon.ViewModel
{
    public class ColoniaViewModel
    {
        public int Colonia_ID { get; set; }
        public string Ciudad_ID { get; set; }
        public string Tipo { get; set; }
        public string Nombre { get; set; }
        public string Codigo_Postal { get; set; }
        public string Estatus { get; set; }
        public string Nombre_Ciudad { get; set; }

    }
}
