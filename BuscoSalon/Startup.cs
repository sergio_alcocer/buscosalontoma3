using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using static BuscoSalon.Constantes;
using Microsoft.OpenApi.Models;
using NucleoBuscoSalon.Models;
using NucleoBuscoSalon.Contratos;
using NucleoBuscoSalon.Implementaciones;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.FileProviders;

namespace BuscoSalon
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddAntiforgery(options =>
            {
                options.HeaderName = "X-XSRF-TOKEN";
                options.SuppressXFrameOptionsHeader = false;
            });

            //Autenticacion
            services.AddAuthentication(options =>
            {
                //AuthenticationScheme = "Bearer"
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(jwtBearerOptions =>
            {
                //Esto no es necesario poner
                jwtBearerOptions.Events = new JwtBearerEvents()
                {
                    OnTokenValidated = context =>
                    {
                        List<Claim> cl = new List<Claim>(((ClaimsIdentity)context.Principal.Identity).Claims);
                        string strUsuario = cl.Where(c => c.Type == JWT_CLAIM_USUARIO).First().Value;

                        if (string.IsNullOrWhiteSpace(strUsuario))
                        {
                            context.Fail("Unauthorized");
                        }

                        return Task.CompletedTask;
                    }
                };

                //Esta es la referencia al RFC
                //https://tools.ietf.org/html/rfc7519#page-9
                jwtBearerOptions.TokenValidationParameters = new TokenValidationParameters()
                {
                    SaveSigninToken = true,
                    ValidateActor = true,
                    ValidateIssuer = true, //Issuer: Emisor
                    ValidateAudience = true, //Audience: Son los destinatarios del token
                    ValidateLifetime = true, //Lifetime: Tiempo de vida del token
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Configuration["ApiAuth:Issuer"],
                    ValidAudience = Configuration["ApiAuth:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["ApiAuth:SecretKey"]))
                };
            });

            //CORS
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.WithOrigins(new[]
                    {
                        //"http://localhost:4200",
                        //"http://localhost:5000",
                        //"http://buscosalon.com/apibuscosalon",
                        //"http://buscosalon.com",
                        "https://buscosalon.com/apibuscosalon",
                        "https://buscosalon.com"
                    })
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });

            services.AddCors(c =>
            {
                c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin());
            });


            //Esto no viene en lo del video
            //Mapeo de entidades db <-> model
            //services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            //// Base de datos MariaDB
            //string strCnnSQL = Configuration["connectionString"].ToString();
            //services.AddDbContextPool<curso_angularContext>(
            //options => options.UseMySql(strCnnSQL,
            //          mysqlOptions =>
            //          {
            //              mysqlOptions.ServerVersion(new Version(10, 3, 13), ServerType.MariaDb);
            //          }));

            //services.AddScoped<IUnitOfWork, UnitOfWork>();
            //services.AddScoped<IClientesRepository, ClientesRepository>();

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

           

           // services.AddControllers()
        //.AddJsonOptions(options =>
        //{
        //    options.JsonSerializerOptions.PropertyNamingPolicy = null;
        //});
        // esta configuración hace que las serialización la mande como es modelo es decir
        // si la propiedad del modelo es Nombre al momento de serializar la inforamación la mando como Nombre
        // ya que otra manera la mandaria en minúsculas
        // pero la quitamos,porque se llava trabajo mandando las cosas en minusculas


            string connectionString = Configuration["ConnectionStrings:DefaultConnection"];

            services.AddDbContext<DataContex>(options =>
              options.UseSqlServer(connectionString));

            services.AddScoped<ITipoAmbienteRepositorio, TipoAmbienteRepositorio>();
            services.AddScoped<IColoniaRepositorio, ColoniaRepositorio>();
            services.AddScoped<ICiudadRepositorio, CiudadesRepositorio>();
            services.AddScoped<IEstadosRepositorio, EstadoRepositorio>();
            services.AddScoped<ISociosRepositorio, SocioRepositorio>();
            services.AddScoped<IParametrosRepositorio, ParametrosRepositorio>();
            services.AddScoped<IAnuncioRepositorio, AnuncioRepositorio>();

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo { Title = "Busco Salon API", Version = "v1" });
            });

            //LOGs - Serilog RollingLog
            Log.Logger = new LoggerConfiguration()
               .ReadFrom.Configuration(Configuration)
               .CreateLogger();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env,
                              ILoggerFactory loggerFactory, IAntiforgery antiforgery)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            //https://docs.microsoft.com/en-us/aspnet/core/migration/22-to-30?view=aspnetcore-3.0&tabs=visual-studio
            /*For most apps, calls to UseAuthentication, UseAuthorization, and UseCors must appear between 
             * the calls to UseRouting and UseEndpoints to be effective.*/
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseSwagger();
            app.UseSwaggerUI(option =>
            {
                option.SwaggerEndpoint("../swagger/v1/swagger.json", "Busco Salon API");

            });

            //Esta no estaba
            //app.Use(next => context =>
            //{
            //    string path = context.Request.Path.Value;

            //    if (path != null && path.ToLower().Contains("/api"))
            //    {
            //        var tokens = antiforgery.GetAndStoreTokens(context);
            //        context.Response.Cookies.Append("XSRF-TOKEN", tokens.RequestToken,
            //            new CookieOptions()
            //            {
            //                HttpOnly = false
            //            });
            //    }

            //    return next(context);
            //});

            app.UseCors("CorsPolicy");
            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"Recursos")),
                RequestPath = new PathString("/Recursos")
            });

            loggerFactory.AddSerilog();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
