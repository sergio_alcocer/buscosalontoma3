﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using NucleoBuscoSalon.Models;
using BuscoSalon.ViewModel;
using System.Globalization;

namespace BuscoSalon.Mappers
{
    public class AnuncioProfiles : Profile
    {

        public AnuncioProfiles() {



            CreateMap<Anuncios, AnuncioViewModel>()
                .ForMember(
                   dest => dest.anuncio_id,
                   opt => opt.MapFrom(src => src.Anuncio_ID)
                )
                .ForMember(
                   dest => dest.socio_id,
                   opt => opt.MapFrom(src => src.Socio_ID.ToString())
                )
                .ForMember(
                    dest => dest.url,
                    opt=> opt.MapFrom(src => src.Url)
                )
                .ForMember(
                  dest => dest.estatus,
                  opt => opt.ConvertUsing(new EstatusStringConverter(), src => src.Estatus)
                )
                .ForMember(
                   dest => dest.fecha_final,
                   opt => opt.MapFrom(src => src.Fecha_Final.ToString("dd/MM/yyyy"))
                 )
                .ForMember(
                   dest => dest.fecha_inicio,
                   opt => opt.MapFrom(src => src.Fecha_Inicio.ToString("dd/MM/yyyy"))
                   )
                .ForMember(
                  dest => dest.dias,
                  opt => opt.MapFrom(src => src.Dias)
                )
                .ForMember(
                  dest => dest.costo_total,
                  opt => opt.MapFrom(src => src.Total_Costo)
                )
                .ForMember(
                   dest => dest.costo_formato,
                   opt => opt.MapFrom(src => src.Total_Costo.ToString("c"))
                )
                .ForMember(
                    dest => dest.archivo ,
                    opt => opt.MapFrom(src=> src.Archivo)
                 )
                .ForMember(
                  dest => dest.nombre_socio,
                  opt => opt.MapFrom(src => src.Socios.Socio_Nombre + " " + src.Socios.Socio_Paterno + " " + src.Socios.Socio_Materno)
                );
             

            CreateMap<AnuncioViewModel, Anuncios>()
                .ForMember(
                  dest => dest.Anuncio_ID,
                  opt => opt.MapFrom(src => src.anuncio_id)
                )
                .ForMember(
                   dest => dest.Socio_ID,
                   opt => opt.MapFrom(src => int.Parse(src.socio_id))
                 )
                .ForMember(
                   dest => dest.Estatus,
                   opt => opt.ConvertUsing(new EstatusConverter(), src => src.estatus)
                )
                .ForMember(
                  dest => dest.Dias,
                  opt=> opt.MapFrom(src => src.dias)
                )
                .ForMember(
                   dest => dest.Total_Costo,
                   opt=> opt.MapFrom(src => src.costo_total)
                )
                .ForMember(
                   dest => dest.Fecha_Inicio,
                   opt => opt.MapFrom(src => DateTime.ParseExact(src.fecha_inicio, "yyyy-MM-dd", CultureInfo.InvariantCulture))
                )
                .ForMember(
                   dest => dest.Fecha_Final,
                   opt => opt.MapFrom(src => DateTime.ParseExact(src.fecha_final, "yyyy-MM-dd", CultureInfo.InvariantCulture))
                );
        }




    }


    public class EstatusConverter : IValueConverter<string, int>
    {
        int IValueConverter<string, int>.Convert(string sourceMember, ResolutionContext context)
        {
            int respuesta = 0;
            switch (sourceMember.ToLower())
            {
                case "inactivo":
                    respuesta= 0;
                    break;
                case "activo":
                    respuesta = 1;
                    break;
                case "pendiente por activar":
                    respuesta = 2;
                    break;
                case "suspendido":
                    respuesta = 3;
                    break;
                case "eliminado":
                    respuesta = 4;
                    break;
            }

            return respuesta;
        }
    }

    public class EstatusStringConverter : IValueConverter<int, string>
    {
        

        string IValueConverter<int, string>.Convert(int sourceMember, ResolutionContext context)
        {
            string respuesta = "";

              switch(sourceMember)
            {
                case 0:
                    respuesta = "INACTIVO";
                    break;
                case 1:
                    respuesta = "ACTIVO";
                    break;
                case 2:
                    respuesta = "PENDIENTE POR ACTIVAR";
                    break;
                case 3:
                    respuesta = "SUSPENDIDO";
                    break;
                case 4:
                    respuesta = "ELIMINADO";
                    break;
            }

            return respuesta;
        }

    }
}
