﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using NucleoBuscoSalon.Models;
using BuscoSalon.ViewModel;
using System.Globalization;

namespace BuscoSalon.Mappers
{
    public class ParametrosProfiles : Profile
    {

        public ParametrosProfiles() {

            CreateMap<Parametros, ParametrosViewModel>();
        }
    }
}
