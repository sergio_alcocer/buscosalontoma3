﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using NucleoBuscoSalon.Models;
using BuscoSalon.ViewModel;

namespace BuscoSalon.Mappers
{
    public class TipoAmbienteProfile : Profile
    {

        public TipoAmbienteProfile() {

            CreateMap<Tipos_Ambientes, TipoAmbienViewModel>()
                .ForMember(
                    dest => dest.Estatus,
                    opt => opt.MapFrom(src => src.Estatus == 0 ? "INACTIVO" : "ACTIVO")
                );

            CreateMap<TipoAmbienViewModel, Tipos_Ambientes>()
                .ForMember(
                   dest => dest.Estatus,
                   opt => opt.MapFrom(src => src.Estatus == "INACTIVO" ? 0 : 1)
                );

        }
    }
}
