﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using NucleoBuscoSalon.Models;
using BuscoSalon.ViewModel;

namespace BuscoSalon.Mappers
{
    public class ColoniasProfiles : Profile
    {

        public ColoniasProfiles() {

            CreateMap<Colonias, ColoniaViewModel>()
                .ForMember(
                   dest => dest.Estatus,
                   opt => opt.MapFrom(src => src.Estatus == 0 ? "INACTIVO" : "ACTIVO")
                )
                .ForMember(
                  dest => dest.Ciudad_ID,
                  opt => opt.MapFrom(src => src.Ciudad_ID.ToString())
                )
                .ForMember(
                   dest => dest.Nombre_Ciudad,
                   opt => opt.MapFrom(src => src.Ciudades.Ciudad_Nombre)
                );

            CreateMap<ColoniaViewModel, Colonias>()
                .ForMember(
                   dest => dest.Estatus,
                   opt => opt.MapFrom(src => src.Estatus == "INACTIVO" ? 0 : 1)
                )
                .ForMember(
                   dest => dest.Ciudad_ID,
                   opt => opt.MapFrom(src => int.Parse(src.Ciudad_ID))
                );
        }
    }
}
