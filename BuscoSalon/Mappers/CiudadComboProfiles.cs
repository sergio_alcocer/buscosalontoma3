﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using NucleoBuscoSalon.Models;
using BuscoSalon.ViewModel;


namespace BuscoSalon.Mappers
{
    public class CiudadComboProfiles : Profile
    {

        public CiudadComboProfiles() {

            CreateMap<Ciudades, ComboViewModel>()
                .ForMember(
                    dest => dest.id,
                    opt => opt.MapFrom(src => src.Ciudad_ID)
                )
                .ForMember(
                    dest => dest.name,
                    opt => opt.MapFrom(src => src.Ciudad_Nombre)
                );

            CreateMap<Estados, ComboViewModel>()
                .ForMember(
                 dest => dest.id,
                 opt => opt.MapFrom(src => src.Estado_ID)
                )
                .ForMember(
                   dest => dest.name,
                   opt => opt.MapFrom(src => src.Estado_Nombre)
                );

            CreateMap<Socios, ComboViewModel>()
                .ForMember(
                   dest => dest.id,
                   opt => opt.MapFrom(src => src.Socio_ID)
                )
                .ForMember(
                    dest => dest.name,
                    opt => opt.MapFrom(src => src.Socio_Nombre + " " + src.Socio_Paterno + " " + src.Socio_Materno)
                );

        }
    }
}
