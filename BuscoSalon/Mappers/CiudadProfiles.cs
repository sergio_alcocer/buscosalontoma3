﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using NucleoBuscoSalon.Models;
using BuscoSalon.ViewModel;

namespace BuscoSalon.Mappers
{
    public class CiudadProfiles : Profile
    {
        public CiudadProfiles() {

            CreateMap<Ciudades, CiudadViewModel>()
                .ForMember(
                    dest => dest.Ciudad_Estatus,
                     opt => opt.MapFrom(src => src.Ciudad_Estatus == 0 ? "INACTIVO" : "ACTIVO")
                 )
                .ForMember(
                  dest => dest.Estado_ID,
                  opt => opt.MapFrom(src => src.Estado_ID.ToString())
                )
                .ForMember(
                  dest => dest.Nombre_Estado,
                  opt => opt.MapFrom(src => src.Estados.Estado_Nombre)
                );

            CreateMap<CiudadViewModel, Ciudades>()
                .ForMember(
                   dest => dest.Ciudad_Estatus,
                   opt => opt.MapFrom(src => src.Ciudad_Estatus == "INACTIVO" ? 0 : 1)
                )
                .ForMember(
                   dest => dest.Estado_ID,
                   opt => opt.MapFrom(src => int.Parse(src.Estado_ID))
                );

        }

    }
}
