import { Component, Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Usuario } from '../modelos/Usuario';
import { Resultado } from '../modelos/Resultado';
import { DataService } from '../services/data.service';
import { map } from "rxjs/operators";
import { FormGroup, AbstractControl } from '@angular/forms';
import { GlobalConstants } from '../constantes/global-constants';

@Injectable({
  providedIn: 'root',
})
export class UsuariosService {

  url = GlobalConstants.apiURL + "/api/usuarios";

  constructor(private httpClient: HttpClient, private dataService: DataService) {
  }

  ConsultarUsuarios(filtroBusqueda: string): Observable<any> {
    const path = `${this.url}/consultarusuarios/${filtroBusqueda}`;
    return this.dataService.get<Usuario[]>(path);
  }

  ConsultarUsuarioPorID(id: number) {
    const path = `${this.url}/consultarusuario/${id}`;
    return this.dataService.get<Usuario>(path);
  }

  AgregarUsuario(usuario: Usuario): Observable<any> {
    const data: Usuario = {
      nick: usuario.nick,
      password: usuario.password,
      nombre: usuario.nombre,
      email: usuario.email,
      tipo: Number(usuario.tipo),
      estatus: (usuario.estatus ? 1 : 0)
    };
    const path = `${this.url}/agregarusuario`;
    return this.dataService.post<Resultado>(path, data);
  }

  ModificarUsuario(usuario: Usuario): Observable<any> {
    const data: Usuario = {
      id: usuario.id,
      nick: usuario.nick,
      password: '',
      nombre: usuario.nombre,
      email: usuario.email,
      tipo: Number(usuario.tipo),
      estatus: (usuario.estatus ? 1 : 0)
    }
    const path = `${this.url}/modificarusuario`;
    return this.dataService.put<Resultado>(path, data);
  }

  EliminarUsuario(id: number): Observable<any> {
    const path = `${this.url}/eliminarusuario/${id}`;
    return this.dataService.delete<Resultado>(path);
  }

  RestablecerPassword(id: number): Observable<any> {
    const data = new Usuario();
    data.id = id as number;
    const path = `${this.url}/restablecerpassword`;
    return this.dataService.put<Resultado>(path, data);
  }
  passwordMatchValidator(pass: string, confirm: string) {        
    return (formGroup: FormGroup) => {
      const passwordControl = formGroup.controls[pass];
      const confirmPasswordControl = formGroup.controls[confirm];

      if (!passwordControl || !confirmPasswordControl) {
        return null;
      }

      if (
        confirmPasswordControl.errors &&
        !confirmPasswordControl.errors.passwordMismatch
      ) {
        return null;
      }

      if (passwordControl.value !== confirmPasswordControl.value) {
        confirmPasswordControl.setErrors({ passwordMismatch: true });
      } else {
        confirmPasswordControl.setErrors(null);
      }
    };
  }

  ValidaNickname(control: AbstractControl) {
    return this.ExisteNickName(control.value).pipe(
      map(res => {
        return res ? null : { nickExist: true };
      })
    );
  }

  ValidaEmail(control: AbstractControl) {
    return this.ExisteEmail(control.value).pipe(
      map(res => {
        return res ? null : { emailExist: true };
      })
    );
  }

  //Fake API call -- You can have this in another service
  ExisteNickName(username: string): Observable<boolean> {
    const path = `${this.url}/existenickname/${username}`;
    return this.httpClient.get(path).pipe(
      map((usernameList: Array<any>) =>
        usernameList.filter(user => user.nick === username)
      ),
      map(users => !users.length)
    );
  }

  ExisteEmail(email: string): Observable<boolean> {
    const path = `${this.url}/existeemail/${email}`;
    return this.httpClient.get(path).pipe(
      map((usernameList: Array<any>) =>
        usernameList.filter(user => user.email === email)
      ),
      map(users => !users.length)
    );
  }
}
