import { Component, Inject, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { UsuariosService } from './cat-usuarios.service';
import { Usuario } from '../modelos/Usuario';
import { Subscription } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ErrorStateMatcher1 } from '../error-state-matcher1';
import { ToasterService } from "../Repositorios/toast.service";
import { NgxSpinnerService } from "ngx-spinner";

interface Tipos {
  value: number;
  viewValue: string;
}
@Component({
  selector: 'app-usuarios',
  templateUrl: './cat-usuarios.component.html',
  styleUrls: ['./cat-usuarios.component.css']
})
export class UsuariosComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['nick', 'nombre', 'tipo', 'estatus', 'editar', 'actualizar', 'eliminar'];
  usuarios: any;
  formularioUsuarios: FormGroup;
  matcher = new ErrorStateMatcher1();
  usuarioactivo: Usuario;
  estadoformulario: number;
  mostrarProcesoExitoso: boolean;
  msjProcesoExitoso: string;
  mostrarAlertaValidacion: boolean;
  mostrarError: boolean;
  msjError: string;
  mostrarEliminarUsuario: boolean;
  filtroBusqueda: string;
  msjAlertaValidacion: string;
  selectedValue: string;
  breakpoint:number;

  subRef$: Subscription;
  confirmationDialogService: any;

  tipos: Tipos[] = [
    { value: 1, viewValue: 'Administrador' },
    { value: 2, viewValue: 'Coordinador' }
  ];

  constructor(private usuarioService: UsuariosService, private formBuilder: FormBuilder, public servicioToast: ToasterService, private spinner: NgxSpinnerService) {
    this.consultarusuarios();
    this.limpiarMensajes();
    this.limpiarUsuario();
    this.estadoformulario = 1;
    this.filtroBusqueda = '';
    this.msjAlertaValidacion = '';
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit(): void {
    this.breakpoint = (window.innerWidth <= 600) ? 1 : 2;
    this.formularioUsuarios = this.formBuilder.group({
      nick: ['', [Validators.required, Validators.minLength(4)], this.usuarioService.ValidaNickname.bind(this.usuarioService)
      ],
      nombre: ['', [Validators.required, Validators.minLength(4)]],
      pass: ['', [Validators.required, Validators.minLength(4)]],
      confirm: ['', Validators.required],
      email: ['', Validators.required, this.usuarioService.ValidaEmail.bind(this.usuarioService)],
      tipo: [''],
      estatus: ['']
    },
      {
        validator: this.usuarioService.passwordMatchValidator(
          "pass",
          "confirm"
        )
      }
    );
  }

  consultarusuarios() {
    let filtro: string;
    if (this.filtroBusqueda === undefined || this.filtroBusqueda.trim() === '') {
      filtro = '---sindatos---';
    } else {
      filtro = this.filtroBusqueda;
    }
    this.spinner.show();
    this.subRef$ = this.usuarioService.ConsultarUsuarios(filtro).subscribe(resultado => {
      this.usuarios = new MatTableDataSource<Usuario>(resultado.body);
      this.usuarios.paginator = this.paginator;
      this.spinner.hide();
    }, error => {
      console.warn(error._message);
      this.spinner.hide();
    });
  }

  consultarusuarioporid(id: number) {
    this.limpiarMensajes();
    this.limpiarUsuario();
    this.spinner.show();
    this.subRef$ = this.usuarioService.ConsultarUsuarioPorID(id).subscribe(resultado => {
      this.usuarioactivo = resultado.body;
      this.estadoformulario = 3;
      this.spinner.hide();
    }, error => {
      console.warn(error._message);
      this.spinner.hide();
    });
  }

  agregarusuario() {
    this.limpiarMensajes();
    this.limpiarUsuario();
    this.usuarioactivo.estatus = 1;
    this.estadoformulario = 2;
  }

  regresaralistado() {
    this.limpiarUsuario();
    this.consultarusuarios();
    this.estadoformulario = 1;
  }

  limpiarUsuario() {
    this.usuarioactivo = {
      id: 0 as number,
      nick: '' as string,
      password: '' as string,
      nombre: '' as string,
      email: '' as string,
      tipo: 0 as number,
      realizo: 0 as number,
      creacion: undefined as Date,
      estatus: 0 as number
    };
  }

  limpiarMensajes() {
    this.mostrarAlertaValidacion = false;
    this.mostrarProcesoExitoso = false;
    this.msjProcesoExitoso = '';
    this.mostrarError = false;
    this.msjError = '';
    this.mostrarEliminarUsuario = false;
  }

  guardarusuario() {
    this.limpiarMensajes();
    this.spinner.show();
    if (this.usuarioactivo.id === 0) {
      this.subRef$ = this.usuarioService.AgregarUsuario(this.usuarioactivo)
        .subscribe(resultado => {
          if (resultado.body.id === 1) {
            this.servicioToast.Success("Busca Salón", "Alta de usuario exitosa!");
            this.spinner.hide();
            this.regresaralistado();
          } else {
            this.servicioToast.Warning("Busca Salón", resultado.body.mensajeError);
            this.spinner.hide();
          }
        }, error => {
          console.warn(error._message);
          this.spinner.hide();
        });
    } else {
      this.subRef$ = this.usuarioService.ModificarUsuario(this.usuarioactivo)
        .subscribe(resultado => {
          if (resultado.body.id === 1) {
            this.servicioToast.Success("Busca Salón", "Actualización de usuario exitosa!.");
            this.spinner.hide();
            this.regresaralistado();
          } else {
            this.servicioToast.Warning("Busca Salón", resultado.body.mensajeError);
            this.spinner.hide();
          }
        }, error => {
          console.warn(error._message);
          this.spinner.hide();
        });
    }
  }

  eliminarusuario(id: number) {
    this.limpiarMensajes();
    let isOk = confirm('¿Esta seguro que desea eliminar el usuario?');
    if (isOk) {
      this.spinner.show();
      this.subRef$ = this.usuarioService.EliminarUsuario(id)
        .subscribe(resultado => {
          if (resultado.body.id === 1) {
            this.servicioToast.Success("Busca Salón", "Eliminación de usuario exitosa!.");
            this.spinner.hide();
            this.consultarusuarios();
          } else {
            this.servicioToast.Warning("Busca Salón", resultado.body.mensajeError);
            this.spinner.hide();
          }
        }, error => {
          this.spinner.hide();
          console.warn(error._message);
        });
    }
  }

  restablecerpassword(id: number) {
    this.limpiarMensajes();
    this.spinner.show();
    this.subRef$ = this.usuarioService.RestablecerPassword(id)
      .subscribe(resultado => {
        if (resultado.body.id === 1) {
          this.servicioToast.Success("Busca Salón", "Contraseña restablecida a \'qwerty\' de forma exitosa!.");
          this.spinner.hide();
          this.consultarusuarios();
        } else {
          this.servicioToast.Warning("Busca Salón", resultado.body.mensajeError);
          this.spinner.hide();
        }
      }, error => {
        this.spinner.hide();
        console.warn(error._message);
      });
  }

  buscarUsuario(event: any) {
    if (event.keyCode === 13) {
      this.consultarusuarios();
    }
  }
  hasError(nombreControl: string, validacion: string) {
    const control = this.formularioUsuarios.get(nombreControl);
    return control.hasError(validacion);
  }
  ngOnDestroy() {
    if (this.subRef$) { this.subRef$.unsubscribe(); }
  }
  onResize(event) {
    this.breakpoint = (event.target.innerWidth <= 600) ? 1 : 2;
  }
}
