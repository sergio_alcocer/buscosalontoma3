export class Calificacion{
    noCalificacion : number;
    noReservacion: number;
    socioID	: number;
    socioNombre	: string
    inmuebleID: number;
    inmuebleNombre :	string
    valorCalificacion: number;
    comentario:	string
    fecha: Date
}