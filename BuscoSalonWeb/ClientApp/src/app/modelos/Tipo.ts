export class Tipo {
    public id: number;
    public descripcion: string;
    public constructor(id: number, descripcion: string){
        this.id = id;
        this.descripcion = descripcion;
    }
}
