export class Colonia {
    public coloniaID: number;
    public nombreColonia: string;
    public ciudadID: number;
    public nombreCiudad: string;
    public estadoID: number;
    public nombreEstado: string;
}
