export class Parametros {
    public Parametro_ID: number;
    public Precio: number;
    public Url_Imagen: string;
    public Correos_CC: string;
    public MensajeReservacion: string;
}