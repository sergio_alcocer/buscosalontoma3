export class Ambiente {
    id?: number;
    nombre?: string;
    constructor(public p_id?: number, public p_nombre?: string) {
        this.id = p_id;
        this.nombre = p_nombre;
    }
}
