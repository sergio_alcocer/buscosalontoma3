
export class Anuncios {

    public anuncio_id: number;
    public socio_id: string;
    public nombre_socio: string;
    public estatus: string;
    public url: string;
    public fecha_final: string;
    public fecha_inicio: string;
    public dias: number;
    public costo_total: number;
    public archivo: string;
    public costo_formato: string;
}