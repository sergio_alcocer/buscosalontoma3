// tslint:disable-next-line: class-name
export class Parametro_Busqueda {
    public fitroEstado: string;
    public filtroGeneral: string;
    public tipoAmbiente: string;
    public precioMaximo: string;
    public incluirMesas: string;
    public incluirMeseros: string;
    public incluirManteles: string;
    public orderBy: string;
    public pagina: string;
}
