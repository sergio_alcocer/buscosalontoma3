export class Reservacion {
    noReservacion: number;
    socioID: number;
    socioNombre: string;
    socioEmail: string;
    inmuebleID: number;
    inmuebleNombre: string;
    paqueteID: number;
    paqueteNombre: string;
    fechaReservacionInicia?: Date;
    fechaReservacionTermina?: Date;
    costo?: number;
    estatus: number;    
}