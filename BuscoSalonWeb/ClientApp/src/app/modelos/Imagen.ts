export class Imagen {
    public id: number;
    public inmuebleID: number;
    public nombre: String;
    public src: String;
    public estatus: number;
  }