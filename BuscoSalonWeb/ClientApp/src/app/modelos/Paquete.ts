export class Paquete {
    id?: number;
    nombre?: string;
    inmuebleID?: number;
    inmuebleNombre?: string;
    costo?: number;
    incluyeMeseros?: number;
    incluyeMesas?: number;
    incluyeManteleria?: number;
    otrosDatos?: string;
    estatus?: number;
}
