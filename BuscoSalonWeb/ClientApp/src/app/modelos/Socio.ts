export class Socio {
    id?: number;
    tipo?: number;
    nick?: string;
    password?: string;
    nombre?: string;
    paterno?: string;
    materno?: string;
    sexo?: number;
    email?: string;
    acepta_Pago_Tarjeta?: number;
    cuenta_Clabe?: string;
    numero_lugares?: number;
    fecha_alta?: Date;
    estatus?: number;
    token?: string;
    noReferencia?: string;
    noSalones?: number;
    whatsApp?: string;
}
