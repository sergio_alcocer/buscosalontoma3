
export class Colonia {

    public colonia_ID: number;
    public ciudad_ID: string;
    public tipo: string;
    public nombre: string;
    public codigo_Postal: string;
    public estatus: string;
    public nombre_Ciudad: string;
}