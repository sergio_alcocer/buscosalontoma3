export class Usuario {
    id?: number;
    nick?: string;
    password?: string;
    nombre?: string;
    email?: string;
    tipo?: number;
    creacion?: Date;
    realizo?: number;
    estatus?: number;
    token?: string;
}
