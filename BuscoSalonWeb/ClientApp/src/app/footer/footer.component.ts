import { Component, OnDestroy } from '@angular/core';
import { from, Subscription } from 'rxjs';
import { SecurityService } from '../services/security.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnDestroy {
  IsAuthenticated = false;
  private subsAuth$: Subscription;

  constructor(private securityService: SecurityService, private router: Router) {
    this.IsAuthenticated = this.securityService.IsAuthorized;

    // Nos suscribimos al observador de los eventos de auth.
    this.subsAuth$ = this.securityService.authChallenge$.subscribe(
      (isAuth) => {
        this.IsAuthenticated = isAuth;
      });
  }

  ngOnDestroy() {
    if (this.subsAuth$) {
      this.subsAuth$.unsubscribe();
    }
  }
  LogOut() {
    this.securityService.LogOff();
    this.router.navigate(['/']);
  }

}
