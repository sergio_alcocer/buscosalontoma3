import { Component, Inject, OnInit, OnDestroy,AfterViewInit,ViewChild } from '@angular/core';
import { ErrorStateMatcher1 } from '../error-state-matcher1';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { SociosInternosService } from './cat-socios-internos.service';
import { Socio } from '../modelos/Socio';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { ToasterService } from "../Repositorios/toast.service";
import { NgxSpinnerService } from "ngx-spinner";


@Component({
  selector: 'app-socios-internos',
  templateUrl: './cat-socios-internos.component.html',
  styleUrls: ['./cat-socios-internos.component.css']
})
export class SociosInternosComponent implements OnInit, OnDestroy,AfterViewInit  {
  displayedColumns: string[] = ['nick', 'nombre', 'paterno', 'materno', 'estatus','editar','actualizar','eliminar'];
  socios: any;
  formularioSocios: FormGroup;
  matcher = new ErrorStateMatcher1();
  socioactivo: Socio;
  estadoformulario: number;
  mostrarProcesoExitoso: boolean;
  msjProcesoExitoso: string;
  mostrarAlertaValidacion: boolean;
  msjAlertaValidacion: string;
  mostrarError: boolean;
  msjError: string;
  mostrarEliminarSocio: boolean;
  filtroBusqueda: string;
  breakpoint:number;

  subRef$: Subscription;
  confirmationDialogService: any;

  get clabe() {
    return this.formularioSocios.get('clabe');
  }

  constructor(private sociointernoService: SociosInternosService, private formBuilder: FormBuilder,public servicioToast: ToasterService,private spinner: NgxSpinnerService) {
    this.consultarsocios();
    this.limpiarMensajes();
    this.limpiarSocio();
    this.estadoformulario = 1;
    this.filtroBusqueda = '';    
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngAfterViewInit() {
    
  }

  ngOnInit() {
    this.breakpoint = (window.innerWidth <= 600) ? 1 : 2;
    this.formularioSocios = this.formBuilder.group({
      nick: ['', [Validators.required, Validators.minLength(4)], this.sociointernoService.ValidaNickname.bind(this.sociointernoService)
      ],
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      paterno: ['', [Validators.required, Validators.minLength(4)]],
      email: ['', Validators.required,this.sociointernoService.ValidaEmail.bind(this.sociointernoService)],
      materno: [''],
      PagoConT: [''],
      clabe: [''],
      whatsapp: ['']
    });

    this.formularioSocios.get('PagoConT').valueChanges
      .subscribe(chechedValue => {
        const clabe = this.formularioSocios.get('clabe');

        if (chechedValue) {
          clabe.setValidators(Validators.required);
        } else {
          clabe.clearValidators();
        }
        clabe.updateValueAndValidity();
      }
      );
  }

  consultarsocios() {
    let filtro: string;
    if (this.filtroBusqueda === undefined || this.filtroBusqueda.trim() === '') {
      filtro = '---sindatos---';
    } else {
      filtro = this.filtroBusqueda;
    }
    this.spinner.show();
    this.subRef$ =  this.sociointernoService.ConsultarSocios(filtro).subscribe(resultado => {
      this.socios = new MatTableDataSource<Socio>(resultado.body);
      this.socios.paginator = this.paginator;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
        console.warn(error._message);
    });
  }

  consultarsocioporid(id: number) {
    this.limpiarMensajes();
    this.limpiarSocio();
    this.spinner.show();
    this.subRef$ = this.sociointernoService.ConsultarSocioPorID(id).subscribe(resultado => {
      this.socioactivo = resultado.body;
      this.estadoformulario = 3;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
        console.warn(error._message);
    });
  }

  agregarsocio() {
    this.limpiarMensajes();
    this.limpiarSocio();
    this.socioactivo.estatus = 1;
    this.estadoformulario = 2;
  }

  regresaralistado() {
    this.limpiarSocio();
    this.consultarsocios();
    this.estadoformulario = 1;
  }

  limpiarSocio() {
    this.socioactivo = {
      id: 0 as number,
      nick: '' as string,
      password: '' as string,
      nombre: '' as string,
      paterno: '' as string,
      materno: '' as string,
      sexo: 0 as number,
      email: '' as string,
      tipo: 0 as number,
      acepta_Pago_Tarjeta: 0 as number,
      cuenta_Clabe: '' as string,
      fecha_alta: undefined as Date,
      estatus: 0 as number,
      whatsApp: '' as string
    };
  }

  limpiarMensajes() {
    this.mostrarAlertaValidacion = false;
    this.msjAlertaValidacion = '';
    this.mostrarProcesoExitoso = false;
    this.msjProcesoExitoso = '';
    this.mostrarError = false;
    this.msjError = '';
    this.mostrarEliminarSocio = false;
  }

  guardarsocio() {
    this.limpiarMensajes();
    this.spinner.show();
    if (this.socioactivo.id === 0) {
      this.socioactivo.tipo = 2;
      this.socioactivo.password = 'qwerty';
      this.socioactivo.numero_lugares = 1;
      this.socioactivo.noSalones = 1;
      this.socioactivo.estatus = 3;
      this.subRef$ =  this.sociointernoService.AgregarSocio(this.socioactivo)
      .subscribe(resultado => {
        if (resultado.body.id === 1) {
          this.servicioToast.Success("Busca Salón", "Alta de socio exitosa!.");     
          this.spinner.hide();
          this.regresaralistado();
         } else {
          this.servicioToast.Warning("Busca Salón",resultado.body.mensajeError);
          this.spinner.hide();
         }
      }, error => {
        this.spinner.hide();
          console.warn(error._message);
      });
    } else {
      this.subRef$ =  this.sociointernoService.ModificarSocio(this.socioactivo)
      .subscribe(resultado => {
        if (resultado.body.id === 1) {
          this.servicioToast.Success("Busca Salón", "Actualización de socio exitosa!.");
          this.spinner.hide();
          this.regresaralistado();
         } else {
          this.servicioToast.Warning("Busca Salón",resultado.body.mensajeError);
          this.spinner.hide();
         }
      }, error => {
        this.spinner.hide();
          console.warn(error._message);
      });
    }
  }

  eliminarsocio(id: number) {
    this.limpiarMensajes();
    let isOk = confirm('¿Esta seguro que desea eliminar el socio?');
    if(isOk) {
      this.spinner.show();
    this.subRef$ =  this.sociointernoService.EliminarSocio(id)
      .subscribe(resultado => {
        if (resultado.body.id === 1) {
          this.servicioToast.Success("Busca Salón", "Eliminación de socio exitosa!.");
          this.spinner.hide();
          this.consultarsocios();
         } else {
          this.servicioToast.Warning("Busca Salón",resultado.body.mensajeError);
          this.spinner.hide();
         }
      }, error => {
        this.spinner.hide();
          console.warn(error._message);
      });
    }
  }

  restablecerpassword(id: number) {
    this.limpiarMensajes();
    this.spinner.show();
    this.subRef$ =  this.sociointernoService.RestablecerPassword(id)
      .subscribe(resultado => {
        if (resultado.body.id === 1) {
          this.servicioToast.Success("Busca Salón", "Contraseña restablecida a \'qwerty\' de forma exitosa!.");
          this.spinner.hide();
          this.consultarsocios();
         } else {
          this.servicioToast.Warning("Busca Salón",resultado.body.mensajeError);
          this.spinner.hide();
         }
      }, error => {
        this.spinner.hide();
          console.warn(error._message);
      });
  }

  buscarSocio(event: any) {
    if (event.keyCode === 13) {
       this.consultarsocios();
    }
 }
 hasError(nombreControl: string, validacion: string) {
  const control = this.formularioSocios.get(nombreControl);
  return control.hasError(validacion);
}
 ngOnDestroy() {
  if (this.subRef$) { this.subRef$.unsubscribe(); }
}
onResize(event) {
  this.breakpoint = (event.target.innerWidth <= 600) ? 1 : 2;
}
}
