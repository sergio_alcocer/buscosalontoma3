import { Component, Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Socio } from '../modelos/Socio';
import { Resultado } from '../modelos/Resultado';
import { DataService } from '../services/data.service';
import { FormGroup, AbstractControl } from '@angular/forms';
import { map } from "rxjs/operators";
import { GlobalConstants } from '../constantes/global-constants';

@Injectable({
  providedIn: 'root',
})
export class SociosInternosService {

  url = GlobalConstants.apiURL + "/api/socios";
  url2 = GlobalConstants.apiURL + "/api/usuarios";

  constructor(private httpClient: HttpClient, private dataService: DataService) {
  }

  ConsultarSocios(filtroBusqueda: string): Observable<any> {
    const path = `${this.url}/consultarsociosinternos/${filtroBusqueda}`;
    return this.dataService.get<Socio[]>(path);
  }

  ConsultarSocioPorID(id: number) {
    const path = `${this.url}/consultarsocio/${id}`;
    return this.dataService.get<Socio>(path);
  }

  AgregarSocio(socio: Socio): Observable<any> {
    const data: Socio = {
      id: socio.id,
      nick: socio.nick,
      password: socio.password,
      nombre: socio.nombre,
      paterno: socio.paterno,
      materno: socio.materno,
      sexo:  Number(socio.sexo),
      email: socio.email,
      tipo: socio.tipo,
      acepta_Pago_Tarjeta: (socio.acepta_Pago_Tarjeta ? 1 : 0),
      cuenta_Clabe: socio.cuenta_Clabe,
      estatus: 3,
      numero_lugares: socio.numero_lugares,
      noSalones: socio.noSalones,
      whatsApp: socio.whatsApp
    };
    const path = `${this.url}/agregarsocio`;
    return this.dataService.put<Resultado>(path, data);
  }

  ModificarSocio(socio: Socio): Observable<any> {
    const data: Socio = {
      id: socio.id,
      nick: socio.nick,
      password: socio.password,
      nombre: socio.nombre,
      paterno: socio.paterno,
      materno: socio.materno,
      sexo:  Number(socio.sexo),
      email: socio.email,
      tipo: socio.tipo,
      acepta_Pago_Tarjeta: (socio.acepta_Pago_Tarjeta ? 1 : 0),
      cuenta_Clabe: socio.cuenta_Clabe,
      estatus: (socio.estatus ? 1 : 0),
      whatsApp: socio.whatsApp
    }
    const path = `${this.url}/modificarsocio`;
    return this.dataService.put<Resultado>(path, data);
  }

  EliminarSocio(id: number): Observable<any> {
    const path = `${this.url}/eliminarsocio/${id}`;
    return this.dataService.delete<Resultado>(path);
  }

  RestablecerPassword(id: number): Observable<any> {
    const data = new Socio();
    data.id = id as number;
    const path = `${this.url}/restablecerpassword`;
    return this.dataService.put<Resultado>(path, data);
  }
  

  passwordMatchValidator(pass: string, confirm: string) {        
    return (formGroup: FormGroup) => {
      const passwordControl = formGroup.controls[pass];
      const confirmPasswordControl = formGroup.controls[confirm];

      if (!passwordControl || !confirmPasswordControl) {
        return null;
      }

      if (
        confirmPasswordControl.errors &&
        !confirmPasswordControl.errors.passwordMismatch
      ) {
        return null;
      }

      if (passwordControl.value !== confirmPasswordControl.value) {
        confirmPasswordControl.setErrors({ passwordMismatch: true });
      } else {
        confirmPasswordControl.setErrors(null);
      }
    };
  }

  ValidaNickname(control: AbstractControl) {
    return this.ExisteNickName(control.value).pipe(
      map(res => {
        return res ? null : { nickExist: true };
      })
    );
  }

  ValidaEmail(control: AbstractControl) {
    return this.ExisteEmail(control.value).pipe(
      map(res => {
        return res ? null : { emailExist: true };
      })
    );
  }

  //Fake API call -- You can have this in another service
  ExisteNickName(username: string): Observable<boolean> {
    const path = `${this.url2}/existenickname/${username}`;
    return this.httpClient.get(path).pipe(
      map((usernameList: Array<any>) =>
        usernameList.filter(user => user.nick === username)
      ),
      map(users => !users.length)
    );
  }

  ExisteEmail(email: string): Observable<boolean> {
    const path = `${this.url2}/existeemail/${email}`;
    return this.httpClient.get(path).pipe(
      map((usernameList: Array<any>) =>
        usernameList.filter(user => user.email === email)
      ),
      map(users => !users.length)
    );
  }
}
