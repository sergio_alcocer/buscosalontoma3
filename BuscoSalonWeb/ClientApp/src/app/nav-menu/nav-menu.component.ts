import { Component,OnInit } from '@angular/core';
import { SecurityService } from '../services/security.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {
  isExpanded = false;
  IsAuthenticated=false;
  Role=0;

  constructor(
    private secutiryService: SecurityService,
    private router: Router
  ) { }

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }

  LogOut() {
    this.secutiryService.LogOff();
    this.router.navigate(['/login']);
  }

  ngOnInit(): void {
    this.IsAuthenticated= this.secutiryService.IsAuthorized;
    this.Role = this.secutiryService.Role;
  }
}
