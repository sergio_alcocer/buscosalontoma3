import { Component, Inject, OnInit, OnDestroy, NgZone, ViewChild } from '@angular/core';
import { ErrorStateMatcher1 } from '../error-state-matcher1';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Parametros } from "../modelos/Parametros";
import {ParametrosService} from "./parametros.service";
import {CdkTextareaAutosize} from '@angular/cdk/text-field';
import {take} from 'rxjs/operators';
import { ToasterService } from "../Repositorios/toast.service";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
    selector: 'app-parametros',
    templateUrl: './parametros.component.html',
    styleUrls: ['./parametros.component.css']
  })

  export class ParametrosComponent implements OnInit, OnDestroy  {
    formularioParametros: FormGroup;
    matcher = new ErrorStateMatcher1();
    parametrosActivos: any;
    mostrarProcesoExitoso: boolean;
    msjProcesoExitoso: string;
    mostrarAlertaValidacion: boolean;
    msjAlertaValidacion: string;
    mostrarError: boolean;
    msjError: string;
  
    subRef$: Subscription;
    confirmationDialogService: any;
    load: boolean;
  
  
    constructor(private parametrosService: ParametrosService, private formBuilder: FormBuilder,private _ngZone: NgZone,public servicioToast: ToasterService,private spinner: NgxSpinnerService) {
      this.ConsultarParametros();
      this.limpiarMensajes();
      this.limpiarParametros();
      this.formularioParametros = this.formBuilder.group({
        url_Imagen: ['', [Validators.required, Validators.minLength(4)]],
        precio: ['', [Validators.required]],
        correos_CC: ['', [Validators.required]],
        mensajereservacion: ['', [Validators.required]],
      });
      
    }
  
    @ViewChild('autosize') autosize: CdkTextareaAutosize;

    ngOnInit(): void {

    }
  
    triggerResize() {
      // Wait for changes to be applied, then trigger textarea resize.
      this._ngZone.onStable.pipe(take(1))
          .subscribe(() => this.autosize.resizeToFitContent(true));
    }
  
    ConsultarParametros() {
      this.spinner.show();
      this.subRef$ =  this.parametrosService.ConsultarParametros().subscribe(resultado => {
        this.parametrosActivos = resultado.body;
        this.load = true;
        console.log(this.parametrosActivos);
        this.spinner.hide();
      }, error => {
          console.warn(error._message);
          this.spinner.hide();
      });
    }
  
    limpiarMensajes() {
      this.mostrarAlertaValidacion = false;
      this.msjAlertaValidacion = '';
      this.mostrarProcesoExitoso = false;
      this.msjProcesoExitoso = '';
      this.mostrarError = false;
      this.msjError = '';
    }

    limpiarParametros() {
      this.parametrosActivos = {
        precio: 0 as number,
        Url_Imagen: '' as string,
        Correos_CC: '' as string,
        MensajeReservacion: '' as string
      };
    }
  
  
    guardarparametros() {
      this.limpiarMensajes();
      this.spinner.show();
        this.subRef$ =  this.parametrosService.ModificarParametros(this.parametrosActivos)
        .subscribe(resultado => {
          if (resultado.body.id === 1) {
            this.servicioToast.Success("Busca Salón", "Actualización de parametros exitosa!.");
            this.ConsultarParametros();
           } else {
            this.servicioToast.Warning("Busca Salón",resultado.body.mensajeError);
           }
           this.spinner.hide();
        }, error => {
          this.spinner.hide();
            console.warn(error._message);
        });
    }
  
   hasError(nombreControl: string, validacion: string) {
    const control = this.formularioParametros.get(nombreControl);
    return control.hasError(validacion);
  }
   ngOnDestroy() {
    if (this.subRef$) { this.subRef$.unsubscribe(); }
  }
}
  