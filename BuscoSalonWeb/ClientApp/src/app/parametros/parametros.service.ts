import { Component, Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Parametros } from "../modelos/Parametros";
import { Resultado } from '../modelos/Resultado';
import { DataService } from '../services/data.service';
import { GlobalConstants } from '../constantes/global-constants';

@Injectable({
    providedIn: 'root',
  })
  export class ParametrosService {
    url = GlobalConstants.apiURL + "/api/Parametros";
  
    constructor(private httpClient: HttpClient, private dataService: DataService) {
    }
  
    ConsultarParametros(): Observable<any> {
      const path = `${this.url}/consultarparametros`;
      return this.dataService.get<Parametros[]>(path);
    }

    ModificarParametros(parametro: any): Observable<any> {
      const data: any = {
        precio: parametro.precio,
        url_Imagen: parametro.url_Imagen,
        correos_CC: parametro.correos_CC,
        mensajeReservacion: parametro.mensajeReservacion
      }
      const path = `${this.url}/modificarparametros`;
      return this.dataService.put<Resultado>(path, data);
    }

  }
  