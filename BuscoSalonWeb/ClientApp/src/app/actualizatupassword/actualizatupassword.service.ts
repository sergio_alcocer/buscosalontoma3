import { Component, Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Usuario } from '../modelos/Usuario';
import { Resultado } from '../modelos/Resultado';
import { DataService } from '../services/data.service';
import { FormGroup, AbstractControl } from "@angular/forms";
import { map } from "rxjs/operators";
import { GlobalConstants } from '../constantes/global-constants';
import { ActualizaPassword } from '../modelos/ActualizaPassword';

@Injectable({
    providedIn: 'root',
  })
  export class ActualizatuPasswordService {
    urlUsuarios = GlobalConstants.apiURL + "/api/usuarios";
    urlSocios = GlobalConstants.apiURL + "/api/Socios";

    constructor(private httpClient: HttpClient, private dataService: DataService) {
    }
      ActualizaPassSocio(Actualiza: ActualizaPassword): Observable<any> {
        const data: ActualizaPassword = {
          actualpassword: Actualiza.actualpassword,
          nuevopassword:Actualiza.nuevopassword,
          id:Actualiza.id
        };
        const path = `${this.urlSocios}/actualizapasswordsocio`;
        return this.dataService.post<Resultado>(path, data);
      }
      ActualizaPassUsuario(Actualiza: ActualizaPassword): Observable<any> {
        const data: ActualizaPassword = {
          actualpassword: Actualiza.actualpassword,
          nuevopassword:Actualiza.nuevopassword,
          id:Actualiza.id
        };
        const path = `${this.urlUsuarios}/actualizapasswordusuario`;
        return this.dataService.post<Resultado>(path, data);
      }


      passwordMatchValidator(pass: string, confirm: string) {        
        return (formGroup: FormGroup) => {
          const passwordControl = formGroup.controls[pass];
          const confirmPasswordControl = formGroup.controls[confirm];
    
          if (!passwordControl || !confirmPasswordControl) {
            return null;
          }
    
          if (
            confirmPasswordControl.errors &&
            !confirmPasswordControl.errors.passwordMismatch
          ) {
            return null;
          }
    
          if (passwordControl.value !== confirmPasswordControl.value) {
            confirmPasswordControl.setErrors({ passwordMismatch: true });
          } else {
            confirmPasswordControl.setErrors(null);
          }
        };
      }
  }