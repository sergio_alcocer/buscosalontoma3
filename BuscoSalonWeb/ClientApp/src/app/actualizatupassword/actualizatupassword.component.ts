import { Component, OnInit, OnDestroy } from '@angular/core';
import { ErrorStateMatcher1 } from '../error-state-matcher1';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { environment } from '../../environments/environment';
import { ActualizatuPasswordService } from './actualizatupassword.service';
import { Usuario } from '../modelos/Usuario';
import { ActivatedRoute } from '@angular/router';
import { Recuperacion } from '../modelos/Recuperacion';
import { ToasterService } from "../Repositorios/toast.service";
import { NgxSpinnerService } from "ngx-spinner";
import { ActualizaPassword } from '../modelos/ActualizaPassword';
import { SecurityService } from '../services/security.service';

@Component({
  selector: 'app-actualizatupassword',
  templateUrl: './actualizatupassword.component.html',
  styles: []
})
export class actualizatupassword implements OnInit, OnDestroy {
  matcher = new ErrorStateMatcher1();
  formActualizaPass: FormGroup;
  subRef$: Subscription;
  mostrarProcesoExitoso: boolean;
  msjProcesoExitoso: string;
  mostrarAlertaValidacion: boolean;
  msjAlertaValidacion: string;
  mostrarError: boolean;
  msjError: string;

  constructor(
    formBuilder: FormBuilder,
    private router: Router,
    public dialog: MatDialog,
    private ActualizatuPasswordService: ActualizatuPasswordService,
    private route: ActivatedRoute,
    public servicioToast: ToasterService,
    private Sesion: SecurityService,
    private spinner: NgxSpinnerService) {

    this.formActualizaPass = formBuilder.group({
        actualpass:['', [Validators.required, Validators.minLength(6)]],
        nuevapass: ['', [Validators.required, Validators.minLength(6)]],
        confirm: ['', Validators.required]
    },
    {
        validator: this.ActualizatuPasswordService.passwordMatchValidator(
          "nuevapass",
          "confirm"
        )
      }      
    );
  }
  ngOnInit() { 
  }

  Confirmar() {
    const Actualiza: ActualizaPassword = {
        actualpassword:this.formActualizaPass.value.actualpass,
        nuevopassword: this.formActualizaPass.value.nuevapass,
        id:this.Sesion.Id       
      };
      
    this.spinner.show();
    if(this.Sesion.Role< 3) {
      this.subRef$ =  this.ActualizatuPasswordService.ActualizaPassUsuario(Actualiza)
      .subscribe(resultado => {
        if (resultado.body.id === 1) {
          this.servicioToast.Success("Busca Salón", "Su contraseña ha sido actualizada.");
          this.router.navigate(['/perfil']);
         } else {
          this.servicioToast.Warning("Busca Salón","La contraseña actual no es correcta");
         }
         this.spinner.hide();
      }, error => {
          console.warn(error._message);
          this.spinner.hide();
      });
    }
    else{
        this.subRef$ =  this.ActualizatuPasswordService.ActualizaPassSocio(Actualiza)
        .subscribe(resultado => {
          if (resultado.body.id === 1) {
            this.servicioToast.Success("Busca Salón", "Su contraseña ha sido actualizada.");
            this.router.navigate(['/perfil']);
           } else {
            this.servicioToast.Warning("Busca Salón","La contraseña actual no es correcta");
           }
           this.spinner.hide();
        }, error => {
            console.warn(error._message);
            this.spinner.hide();
        });
    }

    }
  cancelar() {
    this.router.navigate(['/perfil']);
  }

  hasError(nombreControl: string, validacion: string) {
    const control = this.formActualizaPass.get(nombreControl);
    return control.hasError(validacion);
  }

  ngOnDestroy() {
    if (this.subRef$) {
      this.subRef$.unsubscribe();
    }
  }
}
