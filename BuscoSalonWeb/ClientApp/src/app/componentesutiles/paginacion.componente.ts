import { Component,Input } from "@angular/core";
import { Paginacion } from "../modelos/configurar-modelos-repositorio"; 
import { CatalogoServicio } from "../Repositorios/catologosServicios";


@Component({
    selector: "paginacion-componente",
    templateUrl: "paginacion.componente.html"
  })

  export class PaginacionComponente {
   
    page:number
    maxSize = 20;


    @Input()
    paginacionObject: Paginacion;

    constructor(private catalogoServicio: CatalogoServicio) {

    }

    get current(): number {
        return this.paginacionObject.currentPage;
      }

      
    
      get paginas(): number[] {
    
        if (this.paginacionObject.rows > 0) {
          return Array(Math.ceil(this.paginacionObject.totalElementos / this.paginacionObject.rows))
            .fill(0).map((x, i) => i + 1);
        } else {
          return [];
        }
      }
    
      get totalItmens(): number {
        return this.paginacionObject.totalElementos ;
      }

      get currentPage(): number{
        return this.paginacionObject.currentPage;
      }

      get elementoPorPagina(): number{
        return this.paginacionObject.rows;
      }

      cambioPagina(nuevaPagina: number) {
        this.paginacionObject.currentPage = nuevaPagina;
        this.catalogoServicio.enviarEventoPaginacion(nuevaPagina);
      }

      
      pageChanged(event: any): void{
        this.page = event.page
        this.paginacionObject.currentPage = this.page;
        this.catalogoServicio.enviarEventoPaginacion(this.page);
      }

  }
