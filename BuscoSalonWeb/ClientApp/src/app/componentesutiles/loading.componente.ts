import { Component, Input } from '@angular/core';


@Component({
  selector: 'loading-show',
  templateUrl: 'loading.componente.html'
})

export class LoadingShowComponent {

  @Input()
  showModal :boolean
}
