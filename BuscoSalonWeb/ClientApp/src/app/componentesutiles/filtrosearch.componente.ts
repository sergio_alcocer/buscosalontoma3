import { Component, Input } from "@angular/core";
import { Paginacion,Filtro } from "../modelos/configurar-modelos-repositorio"; 
import { CatalogoServicio } from "../Repositorios/catologosServicios";

@Component({
    selector: "filtro-search-componente",
    templateUrl: "filtrosearch.componente.html"
  })

  export class FiltroSearchComponente{

    @Input()
    paginacionObject: Paginacion;
  
    @Input()
    filtroObject: Filtro;  

    constructor(private catalogoServicio: CatalogoServicio) {

    }

  

    onKeypressEvent(event: any) {
        console.log(event.target.value);
    }

    onKey(event: any) { // without type info
        console.log(event.target.value);
        let cadena: string;
        cadena = event.target.value.replace(/\s/g, "");
        this.filtroObject.search = cadena;
        this.paginacionObject.pagina = 1;
        this.paginacionObject.currentPage = 1;
        this.catalogoServicio.enviarEventoFiltroSearch(cadena);
       
      } 

      onEnter(cadena: string){

        
        this.filtroObject.search = cadena;
        this.paginacionObject.pagina = 1;
        this.paginacionObject.currentPage = 1;
        this.catalogoServicio.enviarEventoFiltroSearch(cadena);
        
      }
  }