import { Component, Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { DataService } from '../services/data.service';
import { TipoAmbiente } from '../modelos/tipo_ambiente_model';
import { Parametro_Busqueda } from '../modelos/Parametro_Busqueda';
import { Inmueble } from '../modelos/Inmueble';
import { GlobalConstants } from '../constantes/global-constants';

@Injectable({
  providedIn: 'root',
})
export class BuscadorService {
  url = GlobalConstants.apiURL + "/api/inmuebles";

  constructor(private httpClient: HttpClient, private dataService: DataService) {
  }

  ConsultarTiposAmbientes(): Observable<any> {
    const path = `${this.url}/consultartiposambientes`;
    return this.dataService.get<TipoAmbiente[]>(path);
  }

  BuscarInmuebles(p_parametros: Parametro_Busqueda): Observable<any> {
    const parametros: Parametro_Busqueda = {
      fitroEstado : p_parametros.fitroEstado,
      filtroGeneral: p_parametros.filtroGeneral,
      tipoAmbiente : p_parametros.tipoAmbiente,
      precioMaximo : p_parametros.precioMaximo,
      incluirMesas : p_parametros.incluirMesas,
      incluirMeseros : p_parametros.incluirMeseros,
      incluirManteles : p_parametros.incluirManteles,
      orderBy : p_parametros.orderBy,
      pagina : p_parametros.pagina
    };
    const path = `${this.url}/buscarinmuebles`;
    return this.dataService.post<Inmueble[]>(path, parametros);
  }

}
