import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { BuscadorService } from './buscador.service';
import { Subscription } from 'rxjs';
import { Ambiente } from '../modelos/Ambiente';
import { Parametro_Busqueda } from '../modelos/Parametro_Busqueda';
import { Inmueble } from '../modelos/Inmueble';
import { SecurityService } from '../services/security.service';
import { NgxSpinnerService } from "ngx-spinner";
import { ReservacionPendienteService } from '../services/reservacion-pendiente.service';
import { AlmacenamientoService } from '../services/almacenamiento.service';

@Component({
  selector: 'app-buscador-listado',
  templateUrl: './buscador-listado.component.html',
  styleUrls: ['./buscador-listado.component.css']
})
export class BuscadorListadoComponent implements OnInit, OnDestroy {

    @Input()
    buscarEstado: string;
    @Input()
    buscarCiudad: string;
    @Input()
    buscarAmbiente: string;
    IsAuthenticated=false;
    Role=0;
    inicial: boolean = true;
    inmuebleSeleccionado: Inmueble;
    listaAmbientes: Ambiente[] = [];
    listaInmuebles: Inmueble[] = [];
    subRef$: Subscription;
    precioMaximo: string;
    incluirMesas: boolean;
    incluirMeseros: boolean;
    incluirManteles: boolean;
    orderBy: string;
    pagina: number;

    constructor(private buscadorService: BuscadorService, private secutiryService: SecurityService,private spinner: NgxSpinnerService, private reservacionpendiente: ReservacionPendienteService, public almacenamientoService: AlmacenamientoService) {
        this.consultartiposambientes();
    }

    consultartiposambientes() {
      this.spinner.show();
      this.subRef$ =  this.buscadorService.ConsultarTiposAmbientes().subscribe(resultado => {
        this.listaAmbientes = resultado.body;
        this.spinner.hide();
      }, error => {
          console.warn(error._message);
          this.spinner.hide();
      });
    }

    buscarinmuebles() {
      this.inicial = true;
      this.precioMaximo = '';
      this.incluirMesas = false;
      this.incluirMeseros = false;
      this.incluirManteles = false;
      this.orderBy = '0';
      this.pagina= 1;
      const parametros: Parametro_Busqueda = {
        fitroEstado : this.buscarEstado,
        filtroGeneral: this.buscarCiudad,
        tipoAmbiente : this.buscarAmbiente,
        precioMaximo : String(this.precioMaximo),
        incluirMesas : (this.incluirMesas?'1':'0'),
        incluirMeseros : (this.incluirMeseros?'1':'0'),
        incluirManteles : (this.incluirManteles?'1':'0'),
        orderBy : this.orderBy,
        pagina : String(this.pagina)
      };
      this.spinner.show();
      this.subRef$ =  this.buscadorService.BuscarInmuebles(parametros).subscribe(resultado => {
        this.listaInmuebles = resultado.body;
        this.spinner.hide();
      }, error => {
          console.warn(error._message);
          this.spinner.hide();
      });
    }

    filtrar(){
      this.inicial = true;
      if(this.precioMaximo==null)this.precioMaximo = '';
      const parametros: Parametro_Busqueda = {
        fitroEstado : this.buscarEstado,
        filtroGeneral: this.buscarCiudad,
        tipoAmbiente : this.buscarAmbiente,
        precioMaximo : String(this.precioMaximo),
        incluirMesas : (this.incluirMesas?'1':'0'),
        incluirMeseros : (this.incluirMeseros?'1':'0'),
        incluirManteles : (this.incluirManteles?'1':'0'),
        orderBy : this.orderBy,
        pagina : String(this.pagina)
      };
      this.spinner.show();
      this.subRef$ =  this.buscadorService.BuscarInmuebles(parametros).subscribe(resultado => {
        this.listaInmuebles = resultado.body;
        this.spinner.hide();
      }, error => {
          console.warn(error._message);
          this.spinner.hide();
      });
    }

    anterior(){
      this.pagina = this.pagina-1;
      this.inicial = true;
      if(this.precioMaximo==null)this.precioMaximo = '';
      const parametros: Parametro_Busqueda = {
        fitroEstado : this.buscarEstado,
        filtroGeneral: this.buscarCiudad,
        tipoAmbiente : this.buscarAmbiente,
        precioMaximo : String(this.precioMaximo),
        incluirMesas : (this.incluirMesas?'1':'0'),
        incluirMeseros : (this.incluirMeseros?'1':'0'),
        incluirManteles : (this.incluirManteles?'1':'0'),
        orderBy : this.orderBy,
        pagina : String(this.pagina)
      };
      this.spinner.show();
      this.subRef$ =  this.buscadorService.BuscarInmuebles(parametros).subscribe(resultado => {
        this.listaInmuebles = resultado.body;
        this.spinner.hide();
      }, error => {
          console.warn(error._message);
          this.spinner.hide();
      });
    }

    siguiente(){
      this.pagina = this.pagina+1;
      this.inicial = true;
      if(this.precioMaximo==null)this.precioMaximo = '';
      const parametros: Parametro_Busqueda = {
        fitroEstado : this.buscarEstado,
        filtroGeneral: this.buscarCiudad,
        tipoAmbiente : this.buscarAmbiente,
        precioMaximo : String(this.precioMaximo),
        incluirMesas : (this.incluirMesas?'1':'0'),
        incluirMeseros : (this.incluirMeseros?'1':'0'),
        incluirManteles : (this.incluirManteles?'1':'0'),
        orderBy : this.orderBy,
        pagina : String(this.pagina)
      };
      this.spinner.show();
      this.subRef$ =  this.buscadorService.BuscarInmuebles(parametros).subscribe(resultado => {
        this.listaInmuebles = resultado.body;
        this.spinner.hide();
      }, error => {
          console.warn(error._message);
          this.spinner.hide();
      });
    }

    ngOnInit(): void {
        console.log(this.buscadorService);
        this.IsAuthenticated= this.secutiryService.IsAuthorized;
        this.Role = this.secutiryService.Role;
        this.precioMaximo = '';
        this.incluirMesas = false;
        this.incluirMeseros = false;
        this.incluirManteles = false;
        this.orderBy = '0';
        this.pagina = 1;
        this.buscarinmuebles();
        if(this.reservacionpendiente.inmuebleSeleccionado.id>0){
          this.almacenamientoService.inmuebleSeleccionado = this.reservacionpendiente.inmuebleSeleccionado;
          this.reservacionpendiente.reset();
          this.inicial = false;
        }
      }

    ngOnDestroy() {
        if (this.subRef$) { this.subRef$.unsubscribe(); }
    }
}
