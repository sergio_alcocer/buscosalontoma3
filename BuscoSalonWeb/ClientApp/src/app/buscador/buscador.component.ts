import { Component, OnInit, OnDestroy } from '@angular/core';
import { BuscadorService } from './buscador.service';
import { Subscription } from 'rxjs';
import { Ambiente } from '../modelos/Ambiente';
import { SecurityService } from '../services/security.service';
import { NgxSpinnerService } from "ngx-spinner";
import { ReservacionPendienteService } from '../services/reservacion-pendiente.service';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css']
})
export class BuscadorComponent implements OnInit, OnDestroy{

    listaAmbientes: Ambiente[] = [];
    buscarEstado: string;
    buscarCiudad: string;
    buscarAmbiente: string;
    inicial: boolean;
    subRef$: Subscription;

    constructor(private buscadorService: BuscadorService,private spinner: NgxSpinnerService, private reservacionpendiente:ReservacionPendienteService){
        this.consultartiposambientes();
        this.buscarEstado = '';
        this.buscarCiudad = '';
        this.buscarAmbiente = '0';
        this.inicial = true;
    }

    consultartiposambientes() {
      this.spinner.show();
      this.subRef$ =  this.buscadorService.ConsultarTiposAmbientes().subscribe(resultado => {
        this.listaAmbientes = resultado.body;
        this.spinner.hide();
      }, error => {
          console.warn(error._message);
          this.spinner.hide();
      });
    }

    ejecutarBuscar() {
      this.inicial = false;
    }

    ngOnInit(): void {
        console.log(this.buscadorService);
        if(this.reservacionpendiente.inmuebleSeleccionado.id > 0){
          this.ejecutarBuscar();
        }
      }

    ngOnDestroy() {
        if (this.subRef$) { this.subRef$.unsubscribe(); }
      }

}
