import { Component, Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Usuario } from '../modelos/Usuario';
import { Resultado } from '../modelos/Resultado';
import { DataService } from '../services/data.service';
import { FormGroup, AbstractControl } from "@angular/forms";
import { map } from "rxjs/operators";
import { GlobalConstants } from '../constantes/global-constants';

@Injectable({
    providedIn: 'root',
  })
  export class RecuperacionService {
    url = GlobalConstants.apiURL + "/api/usuarios";

    constructor(private httpClient: HttpClient, private dataService: DataService) {
    }
      Recuperar(Email: number) {
        const path = `${this.url}/CodigoRecuperacion/${Email}`;
        return this.dataService.get<Resultado>(path);
      }


    
      validaEmailExist(control: AbstractControl) {
        return this.checkEmail(control.value).pipe(
          map(res => {
            return res ? true : { usernameTaken: false };
          })
        );
      }
    
      //Fake API call -- You can have this in another service
      checkEmail(Email: string): Observable<boolean> {
        const path = `${this.url}/consultaremail/${Email}`;
        return this.httpClient.get(path).pipe(
          map((emailresp: string) =>
              emailresp === Email
          ),
          map(users => !users)
        );
      }
  }