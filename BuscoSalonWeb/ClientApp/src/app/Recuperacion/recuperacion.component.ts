import { Component, OnInit, OnDestroy } from '@angular/core';
import { ErrorStateMatcher1 } from '../error-state-matcher1';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { environment } from '../../environments/environment';
import { RecuperacionService } from './recuperacion.service';
import { Usuario } from '../modelos/Usuario';
import { ToasterService } from "../Repositorios/toast.service";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-recuperacion',
  templateUrl: './recuperacion.component.html',
  styles: []
})
export class RecuperacionComponent implements OnInit, OnDestroy {
  cargando = false;
  matcher = new ErrorStateMatcher1();
  formRecuperacion: FormGroup;
  maxDate: Date;
  idDirTmp = 0;
  idTelTmp = 0;
  subRef$: Subscription;
  usuarios: any;
  usuarioactivo: Usuario;
  mostrarProcesoExitoso: boolean;
  msjProcesoExitoso: string;
  mostrarAlertaValidacion: boolean;
  msjAlertaValidacion: string;
  mostrarError: boolean;
  msjError: string;

  constructor(
    formBuilder: FormBuilder,
    private router: Router,
    public dialog: MatDialog,
    private RecuperacionService: RecuperacionService,
    public servicioToast: ToasterService,
    private spinner: NgxSpinnerService) {

    this.formRecuperacion = formBuilder.group({
      //email: ['', Validators.required,this.RecuperacionService.validaEmailExist.bind(this.RecuperacionService)]
      email: ['', Validators.required]
    }
    );


    this.maxDate = new Date();
  }
  ngOnInit() { }

  RecuperarContrasena() {
    this.cargando = true;
    this.spinner.show();
    this.subRef$ = this.RecuperacionService.Recuperar(this.formRecuperacion.value.email)
      .subscribe(resultado => {
        if (resultado.body.id === 1) {
          this.servicioToast.Success("Busca Salón", "Se ha enviado en correo de recuparcion a su correo.");
          this.router.navigate(['/login']);
        } else {
          this.servicioToast.Warning("Busca Salón", resultado.body.mensajeError);
        }
        this.spinner.hide();
      }, error => {
        this.spinner.hide();
        this.cargando = false;
        console.warn(error._message);
      });
  }
  cancelar() {
    this.router.navigate(['/login']);
  }

  hasError(nombreControl: string, validacion: string) {
    const control = this.formRecuperacion.get(nombreControl);
    return control.hasError(validacion);
  }

  ngOnDestroy() {
    if (this.subRef$) {
      this.subRef$.unsubscribe();
    }
  }
}
