import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { ActivaSociosService } from './activaSocios.service';
import { Socio } from '../modelos/Socio';
import {MatTableDataSource} from '@angular/material/table';
import {SelectionModel} from '@angular/cdk/collections';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToasterService } from "../Repositorios/toast.service";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-socios-internos',
  templateUrl: './activaSocios.component.html',
  styleUrls: ['./activaSocios.component.css']
})
export class ActivaSociosComponent implements OnInit, OnDestroy {

  displayedColumns: string[] = ['Nick', 'Nombre', 'Paterno', 'Materno', 'Activar'];
  selection = new SelectionModel<Socio>(true, []);
  dataSource: any;

  formularioSocios: FormGroup;
 
  socios: any;
  socioactivo: Socio;
  estadoformulario: number;
  mostrarProcesoExitoso: boolean;
  msjProcesoExitoso: string;
  mostrarAlertaValidacion: boolean;
  msjAlertaValidacion: string;
  mostrarError: boolean;
  msjError: string;
  mostrarEliminarSocio: boolean;
  filtroBusqueda: string;
  breakpoint:number;

  subRef$: Subscription;
  confirmationDialogService: any;

  constructor(private sociointernoService: ActivaSociosService,private formBuilder: FormBuilder,public servicioToast: ToasterService,private spinner: NgxSpinnerService) {
    this.consultarsocios();
    this.limpiarMensajes();
    this.limpiarSocio();
    this.estadoformulario = 1;
    this.filtroBusqueda = '';
  }

  ngOnInit(): void {
    this.formularioSocios = this.formBuilder.group({
      nick: ['', [Validators.required, Validators.minLength(4)]],
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      paterno: ['', [Validators.required, Validators.minLength(4)]],
      email: ['', Validators.required],
      materno: [''],
      noreferencia: ['', [Validators.required, Validators.minLength(4)]],
      whatsapp: ['', Validators.required],
    });
    this.breakpoint = (window.innerWidth <= 600) ? 1 : 2;
  }

  consultarsocios() {
    let filtro: string;
    this.spinner.show();
    if (this.filtroBusqueda === undefined || this.filtroBusqueda.trim() === '') {
      filtro = '---sindatos---';
    } else {
      filtro = this.filtroBusqueda;
    }
    this.subRef$ =  this.sociointernoService.ConsultarSocios(filtro).subscribe(resultado => {
      this.socios = resultado.body;
      this.dataSource = new MatTableDataSource<Socio>(resultado.body);
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
        console.warn(error._message);
    });
  }

  consultarsocioporid(id: number) {
    this.spinner.show();
    this.limpiarMensajes();
    this.limpiarSocio();
    this.subRef$ = this.sociointernoService.ConsultarSocioPorID(id).subscribe(resultado => {
      this.socioactivo = resultado.body;
      this.estadoformulario = 3;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
        console.warn(error._message);
    });
  }

  ActivarSocio() {
    this.spinner.show();
      this.subRef$ =  this.sociointernoService.ActivarSocio(this.socioactivo)
      .subscribe(resultado => {
        if (resultado.body.id === 1) {
          this.spinner.hide();
          this.servicioToast.Success("Busca Salón", "Activacion de socio correcta.");          
          this.regresaralistado();
         } else {
          this.spinner.hide();
          this.servicioToast.Warning("Busca Salón",resultado.body.mensajeError);
         }
      }, error => {
          this.spinner.hide();
          console.warn(error._message);
      });
  }
  regresaralistado() {
    this.limpiarSocio();
    this.consultarsocios();
    this.estadoformulario = 1;
  }

  limpiarSocio() {
    this.socioactivo = {
      id: 0 as number,
      nick: '' as string,
      password: '' as string,
      nombre: '' as string,
      paterno: '' as string,
      materno: '' as string,
      sexo: 0 as number,
      email: '' as string,
      tipo: 0 as number,
      acepta_Pago_Tarjeta: 0 as number,
      cuenta_Clabe: '' as string,
      fecha_alta: undefined as Date,
      estatus: 0 as number,
      whatsApp: '' as string
    };
  }

  limpiarMensajes() {
    this.mostrarAlertaValidacion = false;
    this.msjAlertaValidacion = '';
    this.mostrarProcesoExitoso = false;
    this.msjProcesoExitoso = '';
    this.mostrarError = false;
    this.msjError = '';
    this.mostrarEliminarSocio = false;
  }


  buscarSocio(event: any) {
    if (event.keyCode === 13) {
       this.consultarsocios();
    }
 }

 hasError(nombreControl: string, validacion: string) {
  const control = this.formularioSocios.get(nombreControl);
  return control.hasError(validacion);
}
 ngOnDestroy() {
  if (this.subRef$) { this.subRef$.unsubscribe(); }
}


  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource!=undefined?this.dataSource.data.length:0;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Socio): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }
  onResize(event) {
    this.breakpoint = (event.target.innerWidth <= 600) ? 1 : 2;
  }

}