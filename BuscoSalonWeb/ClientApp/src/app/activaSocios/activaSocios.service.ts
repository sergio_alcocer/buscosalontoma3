import { Component, Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Socio } from '../modelos/Socio';
import { Resultado } from '../modelos/Resultado';
import { DataService } from '../services/data.service';
import { GlobalConstants } from '../constantes/global-constants';

@Injectable({
  providedIn: 'root',
})

export class ActivaSociosService {
  
  url = GlobalConstants.apiURL + "/api/socios";

  constructor(private httpClient: HttpClient, private dataService: DataService) {
  }

  ConsultarSocios(filtroBusqueda: string): Observable<any> {
    const path = `${this.url}/ConsultarSociosSinActivar/${filtroBusqueda}`;
    return this.dataService.get<Socio[]>(path);
  }

  ConsultarSocioPorID(id: number) {
    const path = `${this.url}/consultarsocio/${id}`;
    return this.dataService.get<Socio>(path);
  }

  ActivarSocio(socio: Socio): Observable<any> {
    const data: Socio = {
      id:socio.id,
      nick: socio.nick,
      password: socio.password,
      nombre: socio.nombre,
      paterno: socio.paterno,
      materno: socio.materno,
      token: "",
      noReferencia: socio.noReferencia,
      whatsApp: socio.whatsApp
    };
    const path = `${this.url}/activarsocio`;
    return this.dataService.put<Resultado>(path, socio);
  }

  AgregarSocio(socio: Socio): Observable<any> {
    const data: Socio = {
      nick: socio.nick,
      password: socio.password,
      nombre: socio.nombre,
      paterno: socio.paterno,
      materno: socio.materno,
      sexo:  Number(socio.sexo),
      email: socio.email,
      tipo: 1,
      acepta_Pago_Tarjeta: (socio.acepta_Pago_Tarjeta ? 1 : 0),
      cuenta_Clabe: socio.cuenta_Clabe,
      whatsApp: socio.whatsApp,
      estatus: (socio.estatus ? 1 : 0)
    };
    const path = `${this.url}/agregarsocio`;
    return this.dataService.post<Resultado>(path, data);
  }

  ModificarSocio(socio: Socio): Observable<any> {
    const data: Socio = {
      id: socio.id,
      nick: socio.nick,
      password: socio.password,
      nombre: socio.nombre,
      paterno: socio.paterno,
      materno: socio.materno,
      sexo:  Number(socio.sexo),
      email: socio.email,
      tipo: 1,
      acepta_Pago_Tarjeta: (socio.acepta_Pago_Tarjeta ? 1 : 0),
      cuenta_Clabe: socio.cuenta_Clabe,
      whatsApp: socio.whatsApp,
      estatus: 1
    }
    const path = `${this.url}/activarsocio`;
    return this.dataService.put<Resultado>(path, data);
  }

  EliminarSocio(id: number): Observable<any> {
    const path = `${this.url}/eliminarsocio/${id}`;
    return this.dataService.delete<Resultado>(path);
  }

  RestablecerPassword(id: number): Observable<any> {
    const data = new Socio();
    data.id = id as number;
    const path = `${this.url}/restablecerpassword`;
    return this.dataService.put<Resultado>(path, data);
  }
}
