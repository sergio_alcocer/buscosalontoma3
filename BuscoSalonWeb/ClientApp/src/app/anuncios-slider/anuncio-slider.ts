import { Component } from "@angular/core";
import { RepositorioAnuncioAplicar } from "../Repositorios/repositorioAplicarAnuncio";

@Component({
    selector: "anuncio-slider",
    templateUrl: "anuncio-slider.html",
    styleUrls:['./anuncio-slider.css']

  })

  export class SliderAnuncio {

   myInterval = 1500;
   activeSlideIndex = 0;

     constructor(private respositorioAnuncio: RepositorioAnuncioAplicar){
         
        this.respositorioAnuncio.obtenerAnunciosVigentes();
     }

     get listaAnuncio(){
        return this.respositorioAnuncio.anunciosVigente;
     }

  }