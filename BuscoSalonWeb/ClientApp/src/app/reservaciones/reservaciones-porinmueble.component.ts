import { OnInit, OnDestroy, Component } from '@angular/core';
import { ReservacionesService } from './reservaciones.service';
import { Subscription } from 'rxjs';
import { Reservacion } from '../modelos/Reservacion';
import { SecurityService } from '../services/security.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-reservaciones-porinmueble',
  templateUrl: './reservaciones-porinmueble.component.html',
  styleUrls: ['./reservaciones-porinmueble.component.css']
})
export class ReservacionesPorInmuebleComponent implements OnInit, OnDestroy {

  subRef$: Subscription;
  reservaciones: Reservacion[] = [];
  reservacionSeleccionada: Reservacion;
  inicial: boolean;

  constructor(public reservacionesService: ReservacionesService, private securityService: SecurityService, private spinner: NgxSpinnerService) {

  }

  ngOnInit(): void {
    this.securityService.Id;
    this.inicial = true;
    this.consultarreservaciones();
  }

  consultarreservaciones() {
    this.spinner.show();
    this.reservacionesService.ConsultarReservacionesPorInmueble(this.securityService.Id)
      .subscribe(resultado => {
        this.reservaciones = resultado.body;
        this.spinner.hide();
      }, error => {
        console.warn(error._message);
        this.spinner.hide();
      });
  }

  verreservacion(reservacion: Reservacion) {
    this.reservacionSeleccionada = reservacion;
    this.inicial = false;
  }

  ngOnDestroy() {
    if (this.subRef$) { this.subRef$.unsubscribe(); }
  }
}