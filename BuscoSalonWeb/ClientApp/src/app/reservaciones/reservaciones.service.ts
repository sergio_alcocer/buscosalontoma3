import { Component, Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Resultado } from '../modelos/Resultado';
import { DataService } from '../services/data.service';
import { Reservacion } from '../modelos/Reservacion';
import { GlobalConstants } from '../constantes/global-constants';

@Injectable({
  providedIn: 'root',
})
export class ReservacionesService {
  url = GlobalConstants.apiURL + "/api/reservaciones";

  constructor(private httpClient: HttpClient, private dataService: DataService) {
  }
  
  AgregarReservacion(reservacion: Reservacion): Observable<any> {
    const data: Reservacion = {
      noReservacion: 0,      
      socioID: Number(reservacion.socioID),
      socioNombre: reservacion.socioNombre,
      socioEmail: reservacion.socioEmail,
      inmuebleID: Number(reservacion.inmuebleID),
      inmuebleNombre: '',
      paqueteID: Number(reservacion.paqueteID),
      paqueteNombre: reservacion.paqueteNombre,
      fechaReservacionInicia: reservacion.fechaReservacionInicia as Date,
      fechaReservacionTermina: reservacion.fechaReservacionTermina as Date,
      costo: Number(reservacion.costo),
      estatus: Number(reservacion.estatus)
    };
    const path = `${this.url}/agregarreservacion`;
    return this.dataService.post<Resultado>(path, data);
  }

  ConsultarReservacionesPorCalificar(id: number) {
    const path = `${this.url}/consultarporcalificar/${id}`;
    return this.dataService.get<Reservacion[]>(path);
  }

  ConsultarReservacionesPorAutorizar(id: number) {
    const path = `${this.url}/consultarporautorizar/${id}`;
    return this.dataService.get<Reservacion[]>(path);
  }

  ConsultarReservacionesPorInmueble(id: number) {
    const path = `${this.url}/consultarporinmueble/${id}`;
    return this.dataService.get<Reservacion[]>(path);
  }

  ConsultarMisReservaciones(id: number) {
    const path = `${this.url}/consultarmisreservaciones/${id}`;
    return this.dataService.get<Reservacion[]>(path);
  }

  CalificarReservacion(reservacion: Reservacion) {
    const data: Reservacion = {
      noReservacion: Number(reservacion.noReservacion),   
      socioID: Number(reservacion.socioID),
      socioNombre: reservacion.socioNombre,
      socioEmail: reservacion.socioEmail,
      inmuebleID: Number(reservacion.inmuebleID),
      inmuebleNombre: '',
      paqueteID: Number(reservacion.paqueteID),
      paqueteNombre: '',
      fechaReservacionInicia: reservacion.fechaReservacionInicia as Date,
      fechaReservacionTermina: reservacion.fechaReservacionTermina as Date,
      costo: Number(reservacion.costo),
      estatus: Number(reservacion.estatus)
    };
    const path = `${this.url}/calificarreservacion`;
    return this.dataService.put<Resultado>(path, reservacion);
  }

  AutorizarReservacion(reservacion: Reservacion) {
    const data: Reservacion = {
      noReservacion: Number(reservacion.noReservacion),      
      socioID: Number(reservacion.socioID),
      socioNombre: '',
      socioEmail: reservacion.socioEmail,
      inmuebleID: Number(reservacion.inmuebleID),
      inmuebleNombre: '',
      paqueteID: Number(reservacion.paqueteID),
      paqueteNombre: '',
      fechaReservacionInicia: reservacion.fechaReservacionInicia as Date,
      fechaReservacionTermina: reservacion.fechaReservacionTermina as Date,
      costo: Number(reservacion.costo),
      estatus: Number(reservacion.estatus)
    };
    const path = `${this.url}/autorizarreservacion`;
    return this.dataService.put<Resultado>(path, reservacion);
  }

  RechazarReservacion(reservacion: Reservacion) {
    const data: Reservacion = {
      noReservacion: reservacion.noReservacion,      
      socioID: Number(reservacion.socioID),
      socioNombre: '',
      socioEmail: reservacion.socioEmail,
      inmuebleID: Number(reservacion.inmuebleID),
      inmuebleNombre: '',
      paqueteID: Number(reservacion.paqueteID),
      paqueteNombre: '',
      fechaReservacionInicia: reservacion.fechaReservacionInicia as Date,
      fechaReservacionTermina: reservacion.fechaReservacionTermina as Date,
      costo: Number(reservacion.costo),
      estatus: Number(reservacion.estatus)
    };
    const path = `${this.url}/cancelarreservacion`;
    return this.dataService.put<Resultado>(path, reservacion);
  }

  EliminarReservacion(reservacion: Reservacion) {
    const data: Reservacion = {
      noReservacion: reservacion.noReservacion,      
      socioID: Number(reservacion.socioID),
      socioNombre: '',
      socioEmail: reservacion.socioEmail,
      inmuebleID: Number(reservacion.inmuebleID),
      inmuebleNombre: '',
      paqueteID: Number(reservacion.paqueteID),
      paqueteNombre: '',
      fechaReservacionInicia: reservacion.fechaReservacionInicia as Date,
      fechaReservacionTermina: reservacion.fechaReservacionTermina as Date,
      costo: Number(reservacion.costo),
      estatus: Number(reservacion.estatus)
    };
    const path = `${this.url}/eliminarreservacion`;
    return this.dataService.put<Resultado>(path, reservacion);
  }

}
