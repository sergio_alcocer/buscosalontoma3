import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { SecurityService } from '../services/security.service';
import { ReservacionesService } from './reservaciones.service';
import { InmueblesService } from '../cat-inmuebles/cat-inmuebles.service';
import { Subscription } from 'rxjs';
import { Reservacion } from '../modelos/Reservacion';
import { Inmueble } from '../modelos/Inmueble';
import { Paquete } from '../modelos/Paquete';
import { ToasterService } from "../Repositorios/toast.service";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
    selector: 'app-reservacion',
    templateUrl: './reservacion.component.html',
    styleUrls: ['./reservacion.component.css']
})
export class ReservacionComponent implements OnInit, OnDestroy {

    @Input()
    reservacion: Reservacion;
    @Input()
    modo: number; // 1 - Por Autorizar, 2 - Ver
    @Output()
    regresarPantalla= new EventEmitter<boolean>();
    IsAuthenticated = false;
    Role = 0;
    Id = 0;
    inicial: boolean;
    subRef$: Subscription;
    inmueble: Inmueble;
    paquete: Paquete;
    comentarios: string;
    calificacion: number;
    
    constructor(private inmuebleService: InmueblesService, private reservacionesService: ReservacionesService, private secutiryService: SecurityService,public servicioToast: ToasterService,private spinner: NgxSpinnerService) {
        this.limpiarInmueble();
    }

    ngOnInit(): void {
        this.IsAuthenticated= this.secutiryService.IsAuthorized;
        this.Role = this.secutiryService.Role;
        this.Id = this.secutiryService.Id;
        this.consultarInmueble();
        this.consultarPaquete();
        this.comentarios = '';
        this.calificacion = 0;
    }
    
    ngOnDestroy() {
    
    }

    regresar(){
        this.regresarPantalla.emit(true);
    }
    
    calificar(){
        this.reservacion.socioNombre = this.comentarios;
        this.reservacion.estatus = Number(this.calificacion);
        this.spinner.show();
        this.reservacionesService.CalificarReservacion(this.reservacion)
            .subscribe(resultado => {
                this.servicioToast.Success("Busca Salón", "Calificación exitosa");
                this.spinner.hide();
                this.regresar();
            }, error => {
                this.spinner.hide();
            console.warn(error._message);
        });
    }
    
    autorizar(){
        this.spinner.show();
        this.reservacionesService.AutorizarReservacion(this.reservacion)
            .subscribe(resultado => {
                this.servicioToast.Success("Busca Salón", "Autorización exitosa");
                this.regresar();
                this.spinner.hide();
            }, error => {
                this.spinner.hide();
            console.warn(error._message);
        });
    }

    cancelar(){
        this.spinner.show();
        this.reservacionesService.RechazarReservacion(this.reservacion)
            .subscribe(resultado => {
                this.servicioToast.Success("Busca Salón", "Rechazo exitoso");
                this.spinner.hide();
                this.regresar();
            }, error => {
                this.spinner.hide();
            console.warn(error._message);
        });
    }

    consultarPaquete(){
        if(this.reservacion.paqueteID == 0){
            this.paquete = {
                id: Number(0),
                nombre: 'Básico'
            };
        }else{
            this.spinner.show();
            this.inmuebleService.ConsultarPaquetePorID(this.reservacion.paqueteID)
            .subscribe(resultado => {
                this.paquete = resultado.body;
                this.spinner.hide();
            }, error => {
                this.spinner.hide();
                console.warn(error._message);
            });
        }
    }

    consultarInmueble(){
        this.spinner.show();
        this.inmuebleService.ConsultarInmueblePorID(this.reservacion.inmuebleID)
            .subscribe(resultado => {
                this.inmueble = resultado.body;
                this.spinner.hide();
            }, error => {
                this.spinner.hide();
                console.warn(error._message);
            });
    }

    limpiarInmueble() {
        this.inmueble = {
            id: 0 as number,
            socioID: Number(this.Id),
            descripcionsocio: '' as string,
            ciudadID: 0 as number,
            descripcionCiudad: '' as string,
            nombre: '' as string,
            direccion: '' as string,
            estadoID: 0 as number,
            coloniaID: 0 as number,
            descripcionColonia: '' as string,
            referencias: '' as string,
            ambienteID: 0 as number,
            descripcionAmbiente: '' as string,
            capacidad: 0 as number,
            costo: 0 as number,
            usuario: '' as string,
            creacion: undefined as Date,
            estatus: 0 as number,
            calificacionPromedio: 0 as number,
            localizacion: '' as string,
            correoElectronico: '' as string,
            telefono: '' as string,
            latitud: 0,
            longitud: 0
        };
    }
}