import { Component, OnInit } from '@angular/core';
import { SecurityService } from '../services/security.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{

  IsAuthenticated=false;
  Role=0;

  constructor(private secutiryService: SecurityService) {}
  
  ngOnInit(): void {
    this.IsAuthenticated= this.secutiryService.IsAuthorized;
    this.Role = this.secutiryService.Role;
  }
  
}
