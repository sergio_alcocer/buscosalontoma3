import { Component, OnInit, OnDestroy } from '@angular/core';
import { ErrorStateMatcher1 } from '../error-state-matcher1';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { environment } from '../../environments/environment';
import { RegistrationService } from './registration.service';
import { Usuario } from '../modelos/Usuario';
import { MatRadioButton, MatRadioChange } from '@angular/material/radio';
import { Socio } from '../modelos/Socio';
import { ToasterService } from "../Repositorios/toast.service";
import { NgxSpinnerService } from "ngx-spinner";

interface Sexo {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-registro',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit, OnDestroy {
  cargando = false;
  matcher = new ErrorStateMatcher1();
  formAgregarCliente: FormGroup;
  maxDate: Date;
  subRef$: Subscription;
  usuarios: any;
  usuarioactivo: Usuario;
  mostrarProcesoExitoso: boolean;
  msjProcesoExitoso: string;
  mostrarAlertaValidacion: boolean;
  msjAlertaValidacion: string;
  mostrarError: boolean;
  msjError: string;
  breakpoint:number;
  Generos: Sexo[] = [
    { value: '1', viewValue: 'Masculino' },
    { value: '2', viewValue: 'Femenino' },
    { value: '3', viewValue: 'Otro' }
  ];
  MuestraPago = true;
  PagoTarjeta = false;
  reservaSalon = false;
  PInmuebles = false;
  PAnuncios = false;

  get clabe() {
    return this.formAgregarCliente.get('clabe');
  }

  get NoSalones() {
    return this.formAgregarCliente.get('NoSalones');
  }

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public dialog: MatDialog,
    private RegistrationService: RegistrationService,
    public servicioToast: ToasterService,
    private spinner: NgxSpinnerService) {
    this.maxDate = new Date();
  }
  private clabeValidators = [
    Validators.maxLength(250),
    Validators.minLength(5)
  ];

  ngOnInit() {
    this.breakpoint = (window.innerWidth <= 600) ? 1 : 2;
    this.formAgregarCliente = this.formBuilder.group({
      nick: ['', [Validators.required, Validators.minLength(4)], this.RegistrationService.ValidaNickname.bind(this.RegistrationService)
      ],
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      paterno: ['', [Validators.required, Validators.minLength(4)]],
      email: ['', Validators.required,this.RegistrationService.ValidaEmail.bind(this.RegistrationService)],
      pass: ['', [Validators.required, Validators.minLength(6)]],
      confirm: ['', Validators.required],
      materno: [''],
      sexo: [''],
      PagoConT: [''],
      clabe: [''],
      NoSalones: [''],
      reservaS: [''],
      inmuebles: [''],
      Publicidad: [''],
      whatsApp: ['']
    },
      {
        validator: this.RegistrationService.passwordMatchValidator(
          "pass",
          "confirm"
        )
      }      

    );

    this.formAgregarCliente.get('PagoConT').valueChanges
      .subscribe(chechedValue => {
        const clabe = this.formAgregarCliente.get('clabe');

        if (chechedValue) {
          clabe.setValidators(Validators.required);
        } else {
          clabe.clearValidators();
        }
        clabe.updateValueAndValidity();
      }
      );

      this.formAgregarCliente.get('inmuebles').valueChanges
      .subscribe(chechedValue => {
        const NoSalones = this.formAgregarCliente.get('NoSalones');

        if (chechedValue) {
          NoSalones.setValidators(Validators.required);
        } else {
          NoSalones.clearValidators();
        }
        NoSalones.updateValueAndValidity();
      }
      );
    }

  AgregarUsuario() {

    if (this.PInmuebles || this.PAnuncios || this.reservaSalon) {
      const NuevoSocio: Socio = {
        id: 0,
        nick: this.formAgregarCliente.value.nick,
        nombre: this.formAgregarCliente.value.nombre,
        paterno: this.formAgregarCliente.value.paterno,
        materno: this.formAgregarCliente.value.materno,
        sexo: this.formAgregarCliente.value.sexo != undefined ? Number(this.formAgregarCliente.value.sexo) : 0,
        email: this.formAgregarCliente.value.email,
        password: this.formAgregarCliente.value.pass,
        tipo: this.PInmuebles || this.PAnuncios ? 2 : 1,
        estatus: this.PInmuebles || this.PAnuncios ? 3 : 1,
        acepta_Pago_Tarjeta: this.formAgregarCliente.value.PagoConT == false ? 0 : 1,
        cuenta_Clabe: this.formAgregarCliente.value.clabe,
        noSalones:this.formAgregarCliente.value.NoSalones==""?0:this.formAgregarCliente.value.NoSalones,
        whatsApp:this.formAgregarCliente.value.whatsApp,
      };

      this.cargando = true;
      this.spinner.show();
      this.subRef$ = this.RegistrationService.AgregarSocio(NuevoSocio)
        .subscribe(resultado => {
          if (resultado.body.id === 1) {
            this.servicioToast.Success("Busca Salón", "Alta de usuario exitosa!.");
            this.router.navigate(['/login']);
            this.spinner.hide();
          } else {
            this.servicioToast.Warning("Busca Salón",resultado.body.mensajeError);
            this.spinner.hide();
          }
        }, error => {
          this.cargando = false;
          console.log(error.message);
          this.spinner.hide();
        });
    }
    else{
      this.servicioToast.Info("Busca Salón","Seleccione por lo menos alguna opcion de lo que desea hacer.");
    }
  }

  CambiaAccion(accion: MatRadioButton) {
    if (accion.value == 3) {
      this.MuestraPago = false;
    }
    else {
      this.MuestraPago = true;
    }
  }

  cancelar() {
    this.router.navigate(['/login']);
  }

  hasError(nombreControl: string, validacion: string) {
    const control = this.formAgregarCliente.get(nombreControl);
    return control.hasError(validacion);
  }

  ngOnDestroy() {
    if (this.subRef$) {
      this.subRef$.unsubscribe();
    }
  }
  onResize(event) {
    this.breakpoint = (event.target.innerWidth <= 600) ? 1 : 2;
  }
}
