import { Component, Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Usuario } from '../modelos/Usuario';
import { Socio } from '../modelos/Socio';
import { Resultado } from '../modelos/Resultado';
import { DataService } from '../services/data.service';
import { FormGroup, AbstractControl } from "@angular/forms";
import { map } from "rxjs/operators";
import { GlobalConstants } from '../constantes/global-constants';

@Injectable({
    providedIn: 'root',
  })
  export class RegistrationService {
    url = GlobalConstants.apiURL + "/api/usuarios";
    urlSocio = GlobalConstants.apiURL + "/api/Socios";

    constructor(private httpClient: HttpClient, private dataService: DataService) {
    }

    /*
    AgregarUsuario(usuario: Usuario): Observable<any> {
        const data: Usuario = {
          nick: usuario.nick,
          password: usuario.password,
          nombre: usuario.nombre,
          email: usuario.email,
          tipo: 3,
          estatus: 1
        };
        const path = `${this.url}/agregarusuario`;
        return this.dataService.post<Resultado>(path, data);
      }
      */

      AgregarSocio(socio: Socio): Observable<any> {
        const data: Socio = {
          //id:0,
          nick: socio.nick,
          password: socio.password,
          nombre: socio.nombre,
          paterno: socio.paterno,
          materno: socio.materno,
          sexo: socio.sexo,
          email: socio.email,
          //1 reserva -- 3
          //2 reserva y publica --4          
          tipo: socio.tipo,
          estatus: socio.estatus,
          acepta_Pago_Tarjeta: socio.acepta_Pago_Tarjeta,
          cuenta_Clabe: socio.cuenta_Clabe,
          numero_lugares:0,
          noSalones:socio.noSalones,
          whatsApp: socio.whatsApp
          /*          
          fecha_alta:undefined
          */
          
        };
        const path = `${this.urlSocio}/agregarsocio`;
        return this.dataService.put<Resultado>(path, data);
      }

      passwordMatchValidator(pass: string, confirm: string) {        
        return (formGroup: FormGroup) => {
          const passwordControl = formGroup.controls[pass];
          const confirmPasswordControl = formGroup.controls[confirm];
    
          if (!passwordControl || !confirmPasswordControl) {
            return null;
          }
    
          if (
            confirmPasswordControl.errors &&
            !confirmPasswordControl.errors.passwordMismatch
          ) {
            return null;
          }
    
          if (passwordControl.value !== confirmPasswordControl.value) {
            confirmPasswordControl.setErrors({ passwordMismatch: true });
          } else {
            confirmPasswordControl.setErrors(null);
          }
        };
      }
    
      ValidaNickname(control: AbstractControl) {
        return this.ExisteNickName(control.value).pipe(
          map(res => {
            return res ? null : { nickExist: true };
          })
        );
      }

      ValidaEmail(control: AbstractControl) {
        return this.ExisteEmail(control.value).pipe(
          map(res => {
            return res ? null : { emailExist: true };
          })
        );
      }
    
      //Fake API call -- You can have this in another service
      ExisteNickName(username: string): Observable<boolean> {
        const path = `${this.url}/existenickname/${username}`;
        return this.httpClient.get(path).pipe(
          map((usernameList: Array<any>) =>
            usernameList.filter(user => user.nick === username)
          ),
          map(users => !users.length)
        );
      }

      ExisteEmail(email: string): Observable<boolean> {
        const path = `${this.url}/existeemail/${email}`;
        return this.httpClient.get(path).pipe(
          map((usernameList: Array<any>) =>
            usernameList.filter(user => user.email === email)
          ),
          map(users => !users.length)
        );
      }
  }