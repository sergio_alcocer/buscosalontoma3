import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Inmueble } from '../modelos/Inmueble';
import { CarruselComponent } from '../carrusel/carrusel.component';
import { MatDialog } from '@angular/material/dialog';
import { BuscadorResultadoDetalleComponent } from '../buscador-resultado-detalle/buscador-resultado-detalle.component';
import { AlmacenamientoService } from '../services/almacenamiento.service';
import { InmueblesService } from '../cat-inmuebles/cat-inmuebles.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-buscador-resultado',
  templateUrl: './buscador-resultado.component.html',
  styleUrls: ['./buscador-resultado.component.css']
})
export class BuscadorResultadoComponent implements OnInit, OnDestroy {

    @Input()
    inmueble: Inmueble;
    @Output()
    inmuebleregresar = new EventEmitter<boolean>();
    subRef$: Subscription;

    constructor(public dialog: MatDialog, public almacenamientoService: AlmacenamientoService, private inmuebleService: InmueblesService) {}

    ngOnInit(): void {
      this.cargarimagenminuatura();
    }

    ngOnDestroy() {

    }

    openDialog(): void {
      const dialogRef = this.dialog.open(CarruselComponent, {
        width: '60%',
        data: this.inmueble
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
      });
    }

    verdetalle(){
      this.almacenamientoService.inmuebleSeleccionado = this.inmueble;
      this.inmuebleregresar.emit(true);
    }
    
    cargarimagenminuatura(){
      this.subRef$ =  this.inmuebleService.ConsultarImagenMinuatura(this.inmueble.id).subscribe(resultado => {
        this.inmueble.src = resultado.body.src;        
      }, error => {
          console.warn(error._message);
      });
    }

}
