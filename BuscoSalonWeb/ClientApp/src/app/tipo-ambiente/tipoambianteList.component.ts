import { Component,OnDestroy } from "@angular/core";
import { RepositorioTipoAmbiente } from "../Repositorios/repositorioTipoAmbiente";
import { CatalogoServicio } from "../Repositorios/catologosServicios";
import { TipoAmbiente } from "../modelos/tipo_ambiente_model";
import { Filtro,Paginacion } from "../modelos/configurar-modelos-repositorio";
import { Subscription } from 'rxjs';
import { NgxSpinnerService } from "ngx-spinner";

@Component({

    selector:"tipo-ambiente-list-componente",
    templateUrl :"tipoambianteList.component.html",
    styleUrls: ['./tipoambienteEstilo.css']
})

export class TipoAmbienteList  implements OnDestroy {

  eventoPaginacionSubscription: Subscription;
  eventoFitroSearchSubsripction: Subscription;
  eventoRegresarSubscription: Subscription;
  eventoAceptarSubscription: Subscription;
  eventosLoading: Subscription;

    visualizarMode: boolean = true;
    visualizarLoading: boolean = false;

   constructor(private repositorioTipoAmbiente:RepositorioTipoAmbiente, private servicioCatalogo: CatalogoServicio,private spinner: NgxSpinnerService){

       this.eventoPaginacionSubscription = this.servicioCatalogo.obtenerEventoPaginacion().subscribe
      (x => {
      this.cambioPagina(x);
      });

      this.eventoFitroSearchSubsripction = this.servicioCatalogo.obtnerEventoFiltroSearch().subscribe
      (x => {
      this.filtrarInformacion(x);
      });

      this.eventoAceptarSubscription = this.servicioCatalogo.obtenerEventoAceptar().subscribe(x => {
        this.guardar();
      });

      this.eventoRegresarSubscription = this.servicioCatalogo.obtenerEventoRegresar().subscribe(x => {
        this.regresar();
      });

      this.eventosLoading = this.servicioCatalogo.obtenerEventoLoading().subscribe(
        x => {
          this.eliminarLoadding();
        }
      );

       
      this.iniciarPeticiones();
     
   }

   ngOnInit() {

    this.visualizarMode = true;

  }

    // Zona de gets y sets

    get tipoAmbienteEntidad(): TipoAmbiente {
      return this.repositorioTipoAmbiente.tipoAmbiente;
    }
     
    get tiposAmbiente(): TipoAmbiente[]{
        return this.repositorioTipoAmbiente.tiposAmbientes;
    }

    get paginacion(): Paginacion {
        return this.repositorioTipoAmbiente.paginacionObject;
      }

    get visualizar(): boolean {
        return this.visualizarMode;
    }

    set visualizar(nuevoValor: boolean) {
        this.visualizarMode = nuevoValor;
      }

      get filtros(): Filtro {
        return this.repositorioTipoAmbiente.filtroObject;
      }

      get loading(): boolean {
        return this.visualizarLoading;
      }
    // Métodos
    
    cambioPagina(nuevaPagina: number) {
        this.repositorioTipoAmbiente.paginacionObject.pagina = nuevaPagina;
        this.visualizarLoading = true;
        this.repositorioTipoAmbiente.obtenerTipoAmbientes();
      }


      nuevoElemento(){
         this.repositorioTipoAmbiente.tipoAmbiente = new TipoAmbiente();
         this.repositorioTipoAmbiente.tipoAmbiente.ambiente_ID = 0;
         this.visualizarMode = false;
      }

      filtrarInformacion(cadena: string) {
        this.repositorioTipoAmbiente.filtroObject.search = cadena;
        this.visualizarLoading = true;
        this.repositorioTipoAmbiente.obtenerTipoAmbientes();
      }

      guardar() {
        this.iniciarPeticiones();
        this.visualizar = true
      }

      regresar() {
        this.visualizarMode = true;
      }

      seleccionarElemento(pTipoAmbiente: TipoAmbiente) {
        this.repositorioTipoAmbiente.tipoAmbiente = pTipoAmbiente;
        this.visualizarMode = false;
      }

      iniciarPeticiones(){
        this.repositorioTipoAmbiente.paginacionObject.pagina = 1;
        this.repositorioTipoAmbiente.paginacionObject.currentPage = 1;
        this.repositorioTipoAmbiente.filtroObject.search = "";
        this.visualizarLoading = true;
        this.repositorioTipoAmbiente.obtenerTipoAmbientes();
      }
      eliminarLoadding(){
        this.visualizarLoading = false;
      }

      ngOnDestroy(){
        this.eventoFitroSearchSubsripction.unsubscribe();
        this.eventoPaginacionSubscription.unsubscribe();
         this.eventoRegresarSubscription.unsubscribe();
        this.eventoAceptarSubscription.unsubscribe();
        this.eventosLoading.unsubscribe();
      }
} 