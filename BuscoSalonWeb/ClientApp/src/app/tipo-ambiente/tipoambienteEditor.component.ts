import { Component,Input } from "@angular/core";
import { TipoAmbiente } from "../modelos/tipo_ambiente_model";
import { RepositorioTipoAmbiente } from "../Repositorios/repositorioTipoAmbiente";
import { CatalogoServicio } from "../Repositorios/catologosServicios";
import { FormBuilder,FormGroup, Validators } from "@angular/forms";
import { ToasterService } from "../Repositorios/toast.service";

@Component({
    selector: "tipoambiente-editor",
    templateUrl: "tipoambienteEditor.component.html"
  
  })

  export class EditorTipoAmbienteComponente {

    @Input()
    tipoAmbienteObject: TipoAmbiente;
    @Input()
    visualizar: boolean;
    visualizarLoading: boolean = false;

    tipoAmbienteForm: FormGroup;

    constructor(private repositorioTipoAmbiente:RepositorioTipoAmbiente, private servicioCatalogo: CatalogoServicio,
       public servicioToast: ToasterService,
        public formBuilder: FormBuilder){

            this.tipoAmbienteForm = this.formBuilder.group({
                Ambiente_ID:[0],
                Nombre: ['', Validators.compose([Validators.pattern('^[^"$%&|\'<>#]*$'),Validators.required])],
               Estatus:['',Validators.required],
            });

    }
    
    
    ngOnInit() {
         this.llenarFormulario();
     }

   

     // get y set
     
     get loading(): boolean {
        return this.visualizarLoading;
      }


     // métodos


     llenarFormulario(){
       
         if(this.tipoAmbienteObject.ambiente_ID != 0){

            this.tipoAmbienteForm.patchValue({
              Ambiente_ID: this.tipoAmbienteObject.ambiente_ID,
              Nombre: this.tipoAmbienteObject.nombre,
              Estatus: this.tipoAmbienteObject.estatus
            });

         }

      }

     guardar() {
        
        if(!this.tipoAmbienteForm.valid){
             this.servicioToast.Warning("Busca Salón","Se requiere uno valores");
        }else{
            let datos = this.tipoAmbienteForm.value;
            this.visualizarLoading = true;
            this.repositorioTipoAmbiente.guardar(datos)
            .subscribe(response => {
                this.visualizarLoading = false;

                if(response.mensajeError == "ok"){
                     this.servicioCatalogo.enviarEventoAceptar();
                     this.servicioToast.Success("Busca Salón", "Proceso Correcto");
                }else{
                  this.servicioToast.Warning("Busca Salón",response.mensajeError);
                }

            },(error) => {
              this.visualizarLoading = false;
              this.servicioToast.Warning("Busca Salón","Error de Conectividad");
            });

        }

      }

     regresar() {
        this.servicioCatalogo.enviarEventoRegresar();
      }

  }