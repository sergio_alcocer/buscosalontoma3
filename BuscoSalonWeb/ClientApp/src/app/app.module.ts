import { BrowserModule } from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule} from '@angular/forms';
import { ModuloRepositorio } from './Repositorios/modulo.repositorio';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { UsuariosComponent } from './cat-usuarios/cat-usuarios.component';
import { UsuariosService } from './cat-usuarios/cat-usuarios.service';
import { PerfilComponent } from './perfil/perfil.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FooterComponent } from './footer/footer.component';
import { JwtInterceptor } from './services/auth/jwt-interceptor';
import { AuthGuard } from './services/auth/auth-guard';
import { MaterialModule } from './modules/material.module';
import { RegistrationComponent} from './registro/registration.component';
import { SociosInternosComponent } from './cat-socios-internos/cat-socios-internos.component';
import { SociosInternosService } from './cat-socios-internos/cat-socios-internos.service';
import { ActivaSociosComponent } from './activaSocios/activaSocios.component';
import { ActivaSociosService } from './activaSocios/activaSocios.service';
import { RecuperacionComponent} from './Recuperacion/recuperacion.component';
import { InmueblesComponent } from './cat-inmuebles/cat-inmuebles.component';
import { InmueblesService } from './cat-inmuebles/cat-inmuebles.service';
import {nuevopassword} from './nuevopassword/nuevopassword.component';
import { TipoAmbienteList } from './tipo-ambiente/tipoambianteList.component';
import { FiltroSearchComponente } from './componentesutiles/filtrosearch.componente';
import { PaginacionComponente } from './componentesutiles/paginacion.componente';
import {EditorTipoAmbienteComponente } from './tipo-ambiente/tipoambienteEditor.component';
import { ShowErrorsComponent } from './componentesutiles/show-errors.component';
import { LoadingShowComponent } from './componentesutiles/loading.componente';
import { BuscadorComponent } from './buscador/buscador.component';
import { BuscadorListadoComponent } from './buscador/buscador-listado.component';
import { BuscadorResultadoComponent } from './buscador-resultado/buscador-resultado.component';
import { EditorColonia } from './colonias/colonia.component';
import { ColoniasList } from './colonias/coloniaList.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { BuscadorFiltroComponent } from './buscador-filtro/buscador-filtro.component';
import { InmueblesPaquetesComponent } from './cat-inmuebles/cat-inmuebles-paquetes.component';
import { CiudadList } from './ciudades/ciudadesList.component';
import { EditarCiudad  } from './ciudades/ciudadesEditor.component';
import { InmueblesImagenesComponent } from './cat-inmuebles/cat-inmuebles-imagenes.component';
import {ImageUploadModule} from 'angular2-image-upload';
import { CarruselComponent } from './carrusel/carrusel.component';
import { EditarAnuncio } from './anuncios/anuncioEditor.componente';
import { AnuncioList } from './anuncios/anuncioList.componente';
import { BsDatepickerModule,BsLocaleService  } from 'ngx-bootstrap/datepicker';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
defineLocale('es', esLocale);
import { CarouselModule } from 'ngx-bootstrap/carousel';
import {  SliderAnuncio} from './anuncios-slider/anuncio-slider';
import { AplicarAnuncio } from './aplicar_anuncios/anuncio-aplicar';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BuscadorResultadoDetalleComponent } from './buscador-resultado-detalle/buscador-resultado-detalle.component';
import { BuscadorResultadoDetalleReservacionComponent } from './buscador-resultado-detalle/buscador-resultado-detalle-reservacion.component';
import { AlmacenamientoService } from './services/almacenamiento.service';
import { ReservacionesPorAutorizarComponent } from './reservaciones/reservaciones-porautorizar.component';
import { ReservacionComponent } from './reservaciones/reservacion.component';
import { MisReservacionesComponent } from './reservaciones/misreservaciones.component';
import { ReservacionesPorInmuebleComponent } from './reservaciones/reservaciones-porinmueble.component';
import { ReservacionesPorCalificarComponent } from './reservaciones/reservaciones-porcalificar.component';
import { GoogleMapsModule } from '@angular/google-maps';
import { InmueblesSeguimientoComponent } from './cat-inmuebles/cat-inmuebles-seguimiento.component';
import{ParametrosComponent} from './parametros/parametros.component';
import{actualizatupassword} from './actualizatupassword/actualizatupassword.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { from } from 'rxjs';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    UsuariosComponent,
    SociosInternosComponent,
    PerfilComponent,
    actualizatupassword,
    ActivaSociosComponent,
    InmueblesComponent,
    LoginComponent,
    FooterComponent,
    RegistrationComponent,
    RecuperacionComponent,
    nuevopassword,
    TipoAmbienteList,
    FiltroSearchComponente,
    PaginacionComponente,
    EditorTipoAmbienteComponente,
    ShowErrorsComponent,
    LoadingShowComponent,
    BuscadorComponent,
    BuscadorListadoComponent,
    ColoniasList,
    EditorColonia,
    BuscadorResultadoComponent,
    CiudadList,
    EditarCiudad,
    BuscadorFiltroComponent,
    InmueblesPaquetesComponent,
    InmueblesImagenesComponent,
    CarruselComponent,
    EditarAnuncio,
    AnuncioList,
    SliderAnuncio,
    AplicarAnuncio,
    ParametrosComponent,
    BuscadorResultadoDetalleComponent,
    BuscadorResultadoDetalleReservacionComponent,
    ReservacionesPorAutorizarComponent,
    ReservacionComponent,
    MisReservacionesComponent,
    ReservacionesPorInmuebleComponent,
    ReservacionesPorCalificarComponent,
    InmueblesSeguimientoComponent,
    ReservacionesPorCalificarComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    NgxSpinnerModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    ImageUploadModule.forRoot(),
    RouterModule.forRoot([
      { path: '', redirectTo:'/buscador', pathMatch: 'full' },
      { path: 'counter', component: CounterComponent },
      { path: 'fetch-data', component: FetchDataComponent},
      { path: 'usuarios', component: UsuariosComponent, canActivate: [AuthGuard] },
      { path: 'perfil', component: PerfilComponent, canActivate: [AuthGuard] },
      { path: 'actualizatupassword', component: actualizatupassword, canActivate: [AuthGuard] },      
      { path: 'socios', component: SociosInternosComponent, canActivate: [AuthGuard] },
      { path: 'activasocios', component: ActivaSociosComponent, canActivate: [AuthGuard] },
      { path: 'parametros', component: ParametrosComponent, canActivate: [AuthGuard] },
      { path: 'inmuebles', component: InmueblesComponent, canActivate: [AuthGuard] },
      { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
      { path: 'login', component: LoginComponent },
      { path: 'registro', component: RegistrationComponent},
      { path: 'recuperacion', component: RecuperacionComponent},
      { path: 'nuevopassword/:token', component: nuevopassword},
      { path: 'tipo-ambiente', component: TipoAmbienteList },
      { path: 'buscador', component: BuscadorComponent },
      { path: 'colonia', component: ColoniasList },
      { path: 'buscador-listado', component: BuscadorListadoComponent },
      { path: 'ciudad', component: CiudadList},
      { path: 'buscador-filtro', component: BuscadorFiltroComponent },
      { path: 'inmuebles-paquetes', component: InmueblesPaquetesComponent, canActivate: [AuthGuard] },
      { path: 'inmuebles-imagenes', component: InmueblesImagenesComponent, canActivate: [AuthGuard] },
      { path: 'carrusel', component: CarruselComponent },
      { path: 'anuncio', component: AnuncioList},
      { path: 'anuncio-aplicar', component: AplicarAnuncio},
      { path: 'reservaciones-porautorizar', component: ReservacionesPorAutorizarComponent},
      { path: 'misreservaciones', component: MisReservacionesComponent},
      { path: 'reservaciones-porinmueble', component: ReservacionesPorInmuebleComponent},
	    { path: 'reservaciones-porcalificar', component: ReservacionesPorCalificarComponent},
      { path: 'inmuebles-seguimiento', component: InmueblesSeguimientoComponent},
      { path: 'reservaciones-porcalificar', component: ReservacionesPorCalificarComponent}
    ]),
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    MatToolbarModule,
    MaterialModule,
    ModuloRepositorio,
    PaginationModule.forRoot(),
    BsDatepickerModule.forRoot(),
    CarouselModule.forRoot(),
    ModalModule.forRoot(),
    GoogleMapsModule,
    ModalModule.forRoot()
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [UsuariosService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }, SociosInternosService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }, ActivaSociosService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }, InmueblesService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor( private bsLocaleService: BsLocaleService){
    this.bsLocaleService.use('es');//fecha en español, datepicker
  }
 }
