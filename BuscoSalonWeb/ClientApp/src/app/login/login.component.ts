import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ILogin } from '../modelos/ilogin';
import { IResponse } from '../modelos/iresponse';
import { Subscription } from 'rxjs';
import { DataService } from '../services/data.service';
import { SecurityService } from '../services/security.service';
import { ErrorStateMatcher1 } from '../error-state-matcher1';
import { GlobalConstants } from '../constantes/global-constants';
import { ToasterService } from "../Repositorios/toast.service";
import { NgxSpinnerService } from "ngx-spinner";
import { ReservacionPendienteService } from '../services/reservacion-pendiente.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  formLogin: FormGroup;
  subRef$:Subscription;
  matcher = new ErrorStateMatcher1();
  scrHeight: any;
  scrWidth: any;

  // HostListener: https://angular.io/api/core/HostListener
  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;
    console.log(this.scrHeight, this.scrWidth);
  }

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private dataService: DataService,
    private securityService: SecurityService,
    public servicioToast: ToasterService,
    private spinner: NgxSpinnerService, 
    private reservacionpendiente: ReservacionPendienteService
  ) { 
    this.securityService.LogOff();

    this.formLogin = formBuilder.group({
      usuario:['',Validators.required],
      password:['',Validators.required]
    });
  }

  ngOnInit(): void {
  }

  Registro(){
    this.router.navigate(['/home']);
    }

  Login(){
    const usuarioLogin: ILogin = {
      usuario: this.formLogin.value.usuario,
      password: this.formLogin.value.password,
    };

    const url = GlobalConstants.apiURL + "/api/identidad/login";
    this.spinner.show();
    this.subRef$ = this.dataService.post<any>(url,usuarioLogin)
    .subscribe(res =>{
      this.servicioToast.Success("Busca Salón", "Acceso Correcto");
      const token=res.body.response;
      this.securityService.SetAuthData(token);
      if(this.reservacionpendiente.inmuebleSeleccionado.id == 0){
        this.router.navigate(['/home']);
      }else{
        this.router.navigate(['/'+this.reservacionpendiente.url]);
      }
      this.spinner.hide();
    }, err => {
      this.servicioToast.Warning("Busca Salón","Usuario o Contraseña Invalida");
      console.log('Error en el login', err);
      this.spinner.hide();
    });
  }

  hasError(nombreControl: string, validacion: string) {
    const control = this.formLogin.get(nombreControl);
    return control.hasError(validacion);
  }

  ngOnDestroy(){
    if(this.subRef$){
      this.subRef$.unsubscribe();
    }
  }
}
