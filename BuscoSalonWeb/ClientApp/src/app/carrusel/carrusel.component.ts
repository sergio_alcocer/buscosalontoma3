import { Component, Input, OnInit, Inject } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Imagen } from '../modelos/Imagen';
import { InmueblesService } from '../cat-inmuebles/cat-inmuebles.service';
import { Subscription } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Inmueble } from '../modelos/Inmueble';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-carrusel',
  templateUrl: './carrusel.component.html',
  styleUrls: ['./carrusel.component.css']
})
export class CarruselComponent implements OnInit {

  inmuebleID: number;
  subRef$: Subscription;
  imagenesbd: Imagen[] = [];

  // tslint:disable-next-line: max-line-length
  constructor(private inmuebleService: InmueblesService, public dialogRef: MatDialogRef<CarruselComponent>, @Inject(MAT_DIALOG_DATA) public data: Inmueble,private spinner: NgxSpinnerService) {
    this.inmuebleID = data.id;
  }

  ngOnInit() {
    this.consultarimagenes();
  }

  consultarimagenes() {
    this.spinner.show();
    this.subRef$ =  this.inmuebleService.ConsultarImagenes(this.inmuebleID).subscribe(resultado => {
      this.imagenesbd = resultado.body;
      this.spinner.hide();
    }, error => {
        console.warn(error._message);
        this.spinner.hide();
    });
  }

}
