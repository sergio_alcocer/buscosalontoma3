import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { SecurityService } from '../services/security.service';
import { Inmueble } from '../modelos/Inmueble';
import { InmueblesService } from '../cat-inmuebles/cat-inmuebles.service';
import { Subscription } from 'rxjs';
import { ReservacionesService } from '../reservaciones/reservaciones.service';
import { Reservacion } from '../modelos/Reservacion';
import { Router } from '@angular/router'; 
import { ToasterService } from "../Repositorios/toast.service";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
    selector: 'app-buscador-resultado-detalle-reservacion',
    templateUrl: './buscador-resultado-detalle-reservacion.component.html',
    styleUrls: ['./buscador-resultado-detalle-reservacion.component.css']
  })
  export class BuscadorResultadoDetalleReservacionComponent implements OnInit, OnDestroy {

    @Input()
    inmueble: Inmueble;
    @Input()
    paqueteId: number;
    @Input()
    paqueteNombre: string;
    @Input()
    paquetePrecio: number;
    @Input()
    fecha: string;
    @Output()
    regresarPantalla= new EventEmitter<boolean>();
    IsAuthenticated = false;
    Role = 0;
    Id = 0;
    inicial: boolean;
    subRef$: Subscription;

    constructor(private inmuebleService: InmueblesService, private reservacionService: ReservacionesService, private secutiryService: SecurityService, private router: Router,public servicioToast: ToasterService,private spinner: NgxSpinnerService) {
      this.inicial = true;
    }

    ngOnInit(): void {
        this.IsAuthenticated= this.secutiryService.IsAuthorized;
        this.Role = this.secutiryService.Role;
        this.Id = this.secutiryService.Id;
    }
  
    ngOnDestroy() {
  
    }

    regresar(){
      this.regresarPantalla.emit(true);
    }

    terminarReservacion(){
      const data: Reservacion = {
        noReservacion: 0,
        socioID: Number(this.Id),
        socioNombre: '',
        socioEmail: '',
        inmuebleID: this.inmueble.id,
        inmuebleNombre: '',
        paqueteID: this.paqueteId,
        paqueteNombre: '',
        fechaReservacionInicia: new Date(this.fecha),
        fechaReservacionTermina: new Date(this.fecha),
        costo:  Number(this.paquetePrecio),
        estatus: 1
      }
      this.spinner.show();
      this.subRef$ =  this.reservacionService.AgregarReservacion(data)
      .subscribe(resultado => {
        if (resultado.body.id === 1) {
          this.servicioToast.Success("Busca Salón", "Se ha realizado el proceso exitosamente, queda pendiente la autorización");
          this.router.navigate(['/home']);
          this.spinner.hide();
        } else {
          this.servicioToast.Warning("Busca Salón",resultado.body.mensajeError);
          this.spinner.hide();
        }
      }, error => {
        this.spinner.hide();
          console.warn(error._message);
      });
    }

  }