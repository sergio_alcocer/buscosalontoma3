import { Component, OnInit, OnDestroy, Input, Inject, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { Inmueble } from '../modelos/Inmueble';
import { CarruselComponent } from '../carrusel/carrusel.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { InmueblesService } from '../cat-inmuebles/cat-inmuebles.service';
import { Subscription } from 'rxjs';
import { Imagen } from '../modelos/Imagen';
import { SecurityService } from '../services/security.service';
import { AlmacenamientoService } from '../services/almacenamiento.service';
import { MatDatepickerInputEvent, MatCalendarCellCssClasses } from '@angular/material/datepicker';
import { ToasterService } from "../Repositorios/toast.service";
import { NgxSpinnerService } from "ngx-spinner";
import { ActivatedRoute, Router } from '@angular/router';
import { ReservacionPendienteService } from '../services/reservacion-pendiente.service';
import { ParametrosService } from '../parametros/parametros.service';
import { SociosInternosService } from '../cat-socios-internos/cat-socios-internos.service';

@Component({
    selector: 'app-buscador-resultado-detalle',
    templateUrl: './buscador-resultado-detalle.component.html',
    styleUrls: ['./buscador-resultado-detalle.component.css'],
    encapsulation: ViewEncapsulation.None,
  })
  export class BuscadorResultadoDetalleComponent implements OnInit, OnDestroy {
  
      @Output() regresarPantalla = new EventEmitter<boolean>();
      inmueble: Inmueble;
      subRef$: Subscription;
      paquetes: any;
      calificaciones: any;
      mostrarpaquetes: boolean;
      mostrarcalificaciones: boolean;
      imagenesbd: Imagen[] = [];
      paqueteIdElegido: number;
      paqueteNombreElegido: string;
      precioElegido: number;
      fecha: string;
      IsAuthenticated=false;
      Role=0;
      inicial: boolean;
      center: google.maps.LatLngLiteral
      fechas: any;
      zoom = 12;
      sitioweburl: string;
      waurl: string;
      mensajeReservacion: string;
      ocultar: boolean;
  
      constructor(public dialog: MatDialog, private inmuebleService: InmueblesService, private secutiryService: SecurityService, public almacenamientoService: AlmacenamientoService,public servicioToast: ToasterService, private spinner: NgxSpinnerService, private router: Router, private route: ActivatedRoute, private reservacionpendiente: ReservacionPendienteService, private parametrosService: ParametrosService) {
        this.mostrarpaquetes = false;
        this.inicial = true;
        this.ocultar = false;
        this.inmueble = almacenamientoService.inmuebleSeleccionado;
      }
  
      ngOnInit(): void {
        this.IsAuthenticated= this.secutiryService.IsAuthorized;
        this.Role = this.secutiryService.Role;
        this.consultarpaquetes();
        this.consultarcalificaciones();
        this.consultarimagenes();
        this.consultarfechasinmueble();
        this.consultarMensajeReservacion();
        this.precioElegido = this.inmueble.costo;  
        this.fecha = '';
        this.paqueteIdElegido = 0;
        this.paqueteNombreElegido = 'Básico';

        if(this.inmueble.latitud != 0 && this.inmueble.longitud != 0 ){
          this.center = { lat: this.inmueble.latitud, lng: this.inmueble.longitud}
        }else{
          this.center = {lat:20.67675, lng:-101.35628 }
        }

        this.sitioweburl = '';
        if (!/^http[s]?:\/\//.test(this.inmueble.sitioWeb)) {
          this.sitioweburl += 'http://';
        }        
        this.sitioweburl += this.inmueble.sitioWeb;
      }
  
      ngOnDestroy() {
  
      }
  
      regresar(){
        this.regresarPantalla.emit(true);
      }

      openDialog(): void {
        const dialogRef = this.dialog.open(CarruselComponent, {
          width: '60%',
          data: this.inmueble
        });
  
        dialogRef.afterClosed().subscribe(result => {
          console.log('The dialog was closed');
        });
      }
  
      consultarpaquetes() {
        this.spinner.show();
        this.subRef$ =  this.inmuebleService.ConsultarPaquetes(this.inmueble.id).subscribe(resultado => {
          this.paquetes = resultado.body;
          this.mostrarpaquetes = (this.paquetes.length>0 ? true : false);
          this.spinner.hide();
        }, error => {
            console.warn(error._message);
            this.spinner.hide();
        });
      }
  
      consultarcalificaciones() {
        this.spinner.show();
        this.subRef$ =  this.inmuebleService.ConsultarCalificaciones(this.inmueble.id).subscribe(resultado => {
          this.calificaciones = resultado.body;
          this.calificaciones.forEach(calificacion => {
            calificacion.fecha = new Date(calificacion.fecha);
            this.spinner.hide();
          });
          this.mostrarcalificaciones = (this.calificaciones.length > 0 ? true : false);
        }, error => {
            console.warn(error._message);
            this.spinner.hide();
        });
      }

      consultarimagenes() {
        this.spinner.show();
        this.subRef$ =  this.inmuebleService.ConsultarImagenes(this.inmueble.id).subscribe(resultado => {
          this.imagenesbd = resultado.body;
          this.spinner.hide();
        }, error => {
            console.warn(error._message);
            this.spinner.hide();
        });
      }

      consultarfechasinmueble() {
        this.spinner.show();
          this.subRef$ =  this.inmuebleService.ConsultarFechasInmuebles(this.inmueble.id).subscribe(resultado => {
          this.fechas = resultado.body;
          this.spinner.hide();
          }, error => {
              console.warn(error._message);
              this.spinner.hide();
          });
        }

      consultarMensajeReservacion() {
        this.spinner.show();
          this.subRef$ =  this.parametrosService.ConsultarParametros().subscribe(resultado => {
          this.mensajeReservacion = resultado.body.mensajeReservacion;
          this.spinner.hide();
          }, error => {
              console.warn(error._message);
              this.spinner.hide();
          });
        }
    
      elegiblesChange(id: number){
        this.paqueteIdElegido = id;
        if(id == 0) {
          this.precioElegido=this.inmueble.costo;
          this.paqueteNombreElegido = 'Básico';          
        } else {
          this.paquetes.forEach(element => {
            if(id == element.id){
              this.precioElegido = element.costo;
              this.paqueteNombreElegido = element.nombre;
            }
          });
        }
      }

      fechaElegidaChange(){
        this.servicioToast.Info("Busca Salón", this.fecha);
      }

      ejecutarReservar() {
        if(this.fecha == ''){
          this.servicioToast.Success("Busca Salón", "Debe seleccionar la fecha para la reservación del inmueble.");
        }else{
          this.inicial = false;
        }
      }
      
      dateClass = (d: Date): MatCalendarCellCssClasses => {
        let clase : string;
        clase = '';
        var _existeEvento = this.fechas.filter(function (x) {
            return ( new Date(x.fechaReservacionInicia).getDate() === d.getDate() &&  new Date(x.fechaReservacionInicia).getMonth() === d.getMonth() && new Date(x.fechaReservacionInicia).getFullYear() === d.getFullYear());
        })[0];

        if(_existeEvento){
            if(_existeEvento.estatus === 2) clase = 'datePickerFechaNoDisponible';
            if(_existeEvento.estatus === 4) clase = 'datePickerFechaNoDisponible';
        }
        return clase;
    }
  
      addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
          //this.events.push(`${type}: ${event.value}`);
      }
  
      addFilter = (d: Date | null): boolean => {
        let bloquear : boolean;
        bloquear = true;
        var _existeEvento = this.fechas.filter(function (x) {
            return ( new Date(x.fechaReservacionInicia).getDate() === d.getDate() &&  new Date(x.fechaReservacionInicia).getMonth() === d.getMonth() && new Date(x.fechaReservacionInicia).getFullYear() === d.getFullYear());
        })[0];

        if(_existeEvento){
            if(_existeEvento.estatus === 1) bloquear = true;
            if(_existeEvento.estatus === 2) bloquear = false;
            if(_existeEvento.estatus === 4) bloquear = false;
        }
        return bloquear;
      }

      redireccionarLogin(){
        this.reservacionpendiente.url = 'buscador'
        this.reservacionpendiente.fecha = new Date(this.fecha);
        this.reservacionpendiente.paquete = this.paqueteIdElegido;
        this.reservacionpendiente.inmuebleSeleccionado = this.inmueble;
        this.router.navigate(['/login']);
      }

      rediccionarWhatsApp(){
        if(this.inmueble.whatsAppSocio!=''){
          
          let mensajeEnviar = this.mensajeReservacion.replace('#NombreSalon#', this.inmueble.nombre);
          this.waurl = 'https://api.whatsapp.com/send?phone=+52'+this.inmueble.whatsAppSocio+'&text=' + mensajeEnviar;
          window.open(this.waurl)
        }        
      }

  }
  