import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalConstants } from '../constantes/global-constants';
import { Filtro,Paginacion } from '../modelos/configurar-modelos-repositorio';
import { Ciudad } from '../modelos/ciudad-model';
import { Combo } from '../modelos/Combo';
import { CatalogoServicio } from "../Repositorios/catologosServicios";
import { Observable } from 'rxjs';
import { Resultado } from '../modelos/Resultado';

const ciudadUrl = GlobalConstants.apiURL + "/api/Ciudades";


@Injectable()
export class RepositorioCiudad{

    public  ciudadesList: Ciudad[];
    public ciudadModel: Ciudad;
    public estadosCombo: Combo[];
    public paginacionObject = new Paginacion();
    public filtroObject = new Filtro();

    constructor(private http: HttpClient,private catalogoServicio: CatalogoServicio){

    }


    obtenerEstados(){

        let url = ciudadUrl + "/obtenerEstados";
        this.http.get<Combo[]>(url).subscribe(res=> {
            this.estadosCombo = res;
            this.catalogoServicio.enviarEventoLoading(false);
            this.catalogoServicio.enviarEventoTerminoPeticion();
        }, (error) => {
            this.catalogoServicio.enviarEventoLoading(false);
            this.catalogoServicio.enviarEventoTerminoPeticion();
        });
    }


    obtenerCiudades(){
        let url = ciudadUrl + "/obtenerCiudades?pagina=" + this.paginacionObject.pagina;

        if(this.filtroObject.search){
            url +=  "&search=" + this.filtroObject.search
        }

        this.http.get(url)
        .subscribe(( res : any)=>{ 
           this.catalogoServicio.enviarEventoLoading(false);
           this.ciudadesList = res.data;
           this.paginacionObject.totalElementos = res.total;
           this.paginacionObject.rows = res.rows;
       },(error) => {
        this.catalogoServicio.enviarEventoLoading(false);
       });

    }

    guardar(data?: any): Observable<any>{

        let url = ciudadUrl + "/guardar";
        return this.http.post<Resultado>(url,data);
    }

}