import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalConstants } from '../constantes/global-constants';
import { forkJoin, Observable } from 'rxjs';
import { Combo } from '../modelos/Combo';
import { CatalogoServicio } from "../Repositorios/catologosServicios";
import { Resultado } from '../modelos/Resultado';
import { Filtro,Paginacion } from '../modelos/configurar-modelos-repositorio';
import {  mergeMap, tap } from 'rxjs/operators';
import { Anuncios } from "../modelos/anuncios-model";
import { ParametrosApp } from "../modelos/parametros-model";

const anuncioUrl = GlobalConstants.apiURL + "/api/Anuncio";

@Injectable()
export class RepositorioAnuncio{


     public anunciosList:Anuncios[];
     public anunciosModel: Anuncios;
     public parametrosApp: ParametrosApp;
     public  imagenCadena: string;
     public sociosCombo: Combo[];
     public paginacionObject = new Paginacion();
     public filtroObject = new Filtro();

     constructor(private http: HttpClient,private catalogoServicio: CatalogoServicio){

     }


     public obtenerSociosEImagen(anuncio_id: number){
         
          let urlImagen = anuncioUrl + "/obtenerImagen?anuncio_id=" + anuncio_id;
          let urlSocios = anuncioUrl + "/obtenerSocios";
           let urlParametros = anuncioUrl + "/obtenerParametros";

          this.http.get<Resultado>(urlImagen)
            .pipe(
                 tap(res=> {

                       if(res.mensajeError== "ok")
                           this.imagenCadena = res.descripcion
                        else
                          this.imagenCadena = "";   
                 }),
               
                 
                 mergeMap( anuncio => {
                     const tiposocio = this.http.get<Combo[]>(urlSocios);
                     const parametro =  this.http.get<ParametrosApp>(urlParametros);

                       return forkJoin([tiposocio,parametro]);
                    })
            )
            .subscribe(res=>{
                this.sociosCombo = res[0];
                this.parametrosApp = res[1];
                this.catalogoServicio.enviarEventoTerminoPeticion();
                this.catalogoServicio.enviarEventoLoading(false);
            },(error) => {
               this.catalogoServicio.enviarEventoLoading(false);
            });
     }


     public obtenerAnuncios(){

          let url = anuncioUrl + "/obtenerAnuncios?pagina=1" + this.paginacionObject.pagina;
          if(this.filtroObject.search){
               url +=  "&search=" + this.filtroObject.search
           }
          
           this.http.get(url)
            .subscribe( (res: any) => {

               this.catalogoServicio.enviarEventoLoading(false);
               this.anunciosList = res.data;
               this.paginacionObject.totalElementos = res.total;
               this.paginacionObject.rows = res.rows;

            },(error) => {
               this.catalogoServicio.enviarEventoLoading(false);
            });
   
     }

     guardar(data?: any) :Observable<any>{
         let url = anuncioUrl + "/guardar";
          return this.http.post<Resultado>(url,data);
     }
}