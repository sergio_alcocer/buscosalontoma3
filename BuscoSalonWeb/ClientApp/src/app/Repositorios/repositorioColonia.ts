import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalConstants } from '../constantes/global-constants';
import { Filtro,Paginacion } from '../modelos/configurar-modelos-repositorio';
import { Colonia } from '../modelos/colonia_model';
import { Combo } from '../modelos/Combo';
import { ComboSimple } from '../modelos/ComboSimple';
import { CatalogoServicio } from "../Repositorios/catologosServicios";
import { Observable } from 'rxjs';
import { Resultado } from '../modelos/Resultado';
import {  mergeMap,  tap } from 'rxjs/operators'

const coloniaUrl = GlobalConstants.apiURL + "/api/Colonias";

@Injectable()
export class RepositorioColonias{

    public  coloniaList: Colonia[];
    public coloniaModel: Colonia;   
    public ciudadesCombo: Combo[];
    public tipoColonia: ComboSimple[];
    public paginacionObject = new Paginacion();
    public filtroObject = new Filtro();

    constructor(private http: HttpClient,private catalogoServicio: CatalogoServicio){

    }

     
    
   obtenerTodasPeticiones(){
   
    let urlCiudad = coloniaUrl + "/obtenerCiudad";
    let urlTipoColonia = coloniaUrl + "/obtenerTipoColonia";
    this.http.get<Combo[]>(urlCiudad)

    .pipe(
        tap(res => {console.log('First result', res)
        this.ciudadesCombo = res; 
       }),
       mergeMap( tipoColonia => this.http.get<ComboSimple[]>(urlTipoColonia))
     ).subscribe(res=> {
         
         this.tipoColonia = res;
         this.catalogoServicio.enviarEventoTerminoPeticion();
         this.catalogoServicio.enviarEventoLoading(false);
     }, (error) => {
        this.catalogoServicio.enviarEventoLoading(false);
     })    
   


   }

   

   obtenerTipoColonia(){
    let url = coloniaUrl + "/obtenerTipoColonia";

    this.http.get<ComboSimple[]>(url).subscribe(res=> {
        this.tipoColonia = res;
        this.catalogoServicio.enviarEventoLoading(false);
    }, (error) => {
        this.catalogoServicio.enviarEventoLoading(false);
    });



   }

    obtenerCiudadCombo(){
        let url = coloniaUrl + "/obtenerCiudad";

        this.http.get<Combo[]>(url).subscribe(res=> {
            this.ciudadesCombo = res;
            this.catalogoServicio.enviarEventoLoading(false);
        }, (error) => {
            this.catalogoServicio.enviarEventoLoading(false);
        });

    }

    obtenerColonias(){

        let url = coloniaUrl + "/obtenerColonias?pagina=" + this.paginacionObject.pagina;

        if(this.filtroObject.search){
            url +=  "&search=" + this.filtroObject.search
        }

        this.http.get(url)
        .subscribe(( res : any)=>{ 
           this.catalogoServicio.enviarEventoLoading(false);
           this.coloniaList = res.data;
           this.paginacionObject.totalElementos = res.total;
           this.paginacionObject.rows = res.rows;
       },(error) => {
        this.catalogoServicio.enviarEventoLoading(false);
       });
    }

    guardar(data?: any): Observable<any>{
        let url = coloniaUrl + "/guardar";
        return this.http.post<Resultado>(url,data);
    }

}