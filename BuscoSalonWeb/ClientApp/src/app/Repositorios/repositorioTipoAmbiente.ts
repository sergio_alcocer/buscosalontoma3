import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TipoAmbiente } from '../modelos/tipo_ambiente_model';
import { GlobalConstants } from '../constantes/global-constants';
import { Filtro,Paginacion } from '../modelos/configurar-modelos-repositorio';
import { Observable } from 'rxjs';
import { Resultado } from '../modelos/Resultado';
import { CatalogoServicio } from "../Repositorios/catologosServicios";
const tipoAmbienteUrl = GlobalConstants.apiURL + "/api/TipoAmbiente";

@Injectable()
export class RepositorioTipoAmbiente{

    public tiposAmbientes: TipoAmbiente[];
    public tipoAmbiente: TipoAmbiente;
    public paginacionObject = new Paginacion();
    public filtroObject = new Filtro();

   constructor(private http: HttpClient, private catalogoServicio: CatalogoServicio){
      this.paginacionObject.pagina = 1;
      //this.obtenerTipoAmbientes();
   }


  obtenerTipoAmbientes(){

    let url  = tipoAmbienteUrl + "?pagina=" + this.paginacionObject.pagina;

    if(this.filtroObject.search){
        url +=  "&search=" + this.filtroObject.search
    }

    this.http.get(url)
         .subscribe(( res : any)=>{ 
           this.catalogoServicio.enviarEventoLoading(false);
            this.tiposAmbientes = res.data;
            this.paginacionObject.totalElementos = res.total;
        this.paginacionObject.rows = res.rows;
        },(error)=>{
          this.catalogoServicio.enviarEventoLoading(false);
        });
  }

  guardar(data?: any):Observable<any>{

    return this.http.post<Resultado>(tipoAmbienteUrl, data);
  }


}