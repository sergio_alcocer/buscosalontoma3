import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class CatalogoServicio {

    private subjectPaginacion = new Subject<any>();
    private subjectFiltroSearch = new Subject<any>();
    private subjectRegreso = new Subject<any>();
    private subjectAceptar = new Subject<any>();
    private subjectLoanding = new Subject<any>();
    private subjectTerminoPeticion = new Subject();

    // métodos
    enviarEventoPaginacion(pagina: number) {
        this.subjectPaginacion.next(pagina);
      }
    
      obtenerEventoPaginacion(): Observable<any> {
        return this.subjectPaginacion.asObservable();
      }
    
      enviarEventoFiltroSearch(search: string) {
        this.subjectFiltroSearch.next(search)
      }
      obtnerEventoFiltroSearch(): Observable<any> {
        return this.subjectFiltroSearch.asObservable();
      }
    
      enviarEventoRegresar() {
        this.subjectRegreso.next();
      }
    
      obtenerEventoRegresar(): Observable<any> {
        return this.subjectRegreso.asObservable();
      }
    
      enviarEventoAceptar() {
        this.subjectAceptar.next();
      }
    
      obtenerEventoAceptar(): Observable<any> {
       return this.subjectAceptar.asObservable();
      }

      enviarEventoLoading(estatus: boolean){
        this.subjectLoanding.next(estatus);
      }

      obtenerEventoLoading(): Observable<any> {
       return this.subjectLoanding.asObservable();
      }

      enviarEventoTerminoPeticion(){
        this.subjectTerminoPeticion.next();
      }

      obtenerEventoTerminoPeticion(){
         return this.subjectTerminoPeticion.asObservable();
      }
}