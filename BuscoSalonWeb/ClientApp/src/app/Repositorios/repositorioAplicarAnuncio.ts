import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalConstants } from '../constantes/global-constants';
import { Observable } from 'rxjs';
import { Combo } from '../modelos/Combo';
import { CatalogoServicio } from "../Repositorios/catologosServicios";
import { Resultado } from '../modelos/Resultado';
import { Filtro,Paginacion } from '../modelos/configurar-modelos-repositorio';
import {  mergeMap, tap } from 'rxjs/operators';
import { Anuncios } from "../modelos/anuncios-model";

const anuncioUrl = GlobalConstants.apiURL + "/api/Anuncio";

@Injectable()
export class RepositorioAnuncioAplicar {

     
     public anunciosVigente: Anuncios[];
     public anunciosList:Anuncios[];
     public anunciosModel: Anuncios;
     public  imagenCadena: string;
     public sociosCombo: Combo[];
     public paginacionObject = new Paginacion();
     public filtroObject = new Filtro();

     constructor(private http: HttpClient,private catalogoServicio: CatalogoServicio){

     }




     public obtenerAnunciosVigentes(){
          let url = anuncioUrl + "/obtenerImagenesVigentes";
         
          this.http.get<Anuncios[]>(url).subscribe(res=> {
               this.anunciosVigente = res;
          },(error) => {

          });

     }


     public obtenerAnuncios(){

          let url = anuncioUrl + "/obtenerAnunciosAplicar?pagina=1" + this.paginacionObject.pagina;
          if(this.filtroObject.search){
               url +=  "&search=" + this.filtroObject.search
           }
          
           this.http.get(url)
            .subscribe( (res: any) => {

               this.catalogoServicio.enviarEventoLoading(false);
               this.anunciosList = res.data;
               this.paginacionObject.totalElementos = res.total;
               this.paginacionObject.rows = res.rows;

            },(error) => {
               this.catalogoServicio.enviarEventoLoading(false);
            });
   
     }

     activarAnuncio(id_anuncio: number) :Observable<any>{
         let url = anuncioUrl + "/activarImagen/" + id_anuncio.toString();
          return this.http.get<Resultado>(url);
     }
}