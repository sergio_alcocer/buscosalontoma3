import { NgModule } from "@angular/core";
import {RepositorioTipoAmbiente } from "./repositorioTipoAmbiente";
import { CatalogoServicio } from "./catologosServicios";
import { ToasterService } from "./toast.service";
import { RepositorioColonias } from './repositorioColonia';
import { RepositorioCiudad } from './repositorioCiudad';
import {RepositorioAnuncio } from './repositorioAnuncio';
import {RepositorioAnuncioAplicar } from './repositorioAplicarAnuncio';




@NgModule({
    providers:[RepositorioTipoAmbiente,CatalogoServicio,ToasterService,RepositorioColonias,RepositorioCiudad,RepositorioAnuncio,RepositorioAnuncioAplicar]
})

export class ModuloRepositorio{}