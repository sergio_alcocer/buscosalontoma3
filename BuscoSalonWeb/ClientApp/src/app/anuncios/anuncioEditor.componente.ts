import { Component,Input,OnDestroy,ChangeDetectorRef } from "@angular/core";
import { FormBuilder,FormGroup, Validators } from "@angular/forms";
import { ToasterService } from "../Repositorios/toast.service";
import { RepositorioAnuncio } from '../Repositorios/repositorioAnuncio';
import { Subscription } from 'rxjs';
import { CatalogoServicio } from "../Repositorios/catologosServicios";
import { Combo } from "../modelos/Combo";
import { Anuncios  } from "../modelos/anuncios-model";
import { StorageService } from '../services/storage.service';
import { ParametrosApp } from "../modelos/parametros-model";
import { ThrowStmt } from "@angular/compiler";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
    selector: "anuncio-editor",
    templateUrl: "anuncioEidtor.componente.html"

  })

  export class EditarAnuncio implements OnDestroy {

    @Input()
    anuncioObject: Anuncios;
    @Input()
    visualizar: boolean;
    visualizarLoading: boolean = false;
   
    dataInicial: Date = null;
    dataFinal: Date = null;
    Dias: number = 0;
    usuarioID: number = 0;
    anchoImagen: number = 0;
    altoImagen: number = 0;
    imagenCadena;
    anuncioForm: FormGroup;
    eventoTerminarPeticionSubscription: Subscription;

    constructor(public formBuilder: FormBuilder,private cd: ChangeDetectorRef,public servicioToast: ToasterService,
      public repositorioAnuncio:RepositorioAnuncio,private servicioCatalogo: CatalogoServicio,
      private storeService: StorageService,private spinner: NgxSpinnerService){
          
          this.anuncioForm = this.formBuilder.group({
               anuncio_id:[0],
               socio_id:['',Validators.compose([Validators.required])],
               file:[null, Validators.required],
               fecha_final: ['',Validators.required],
               fecha_inicio: ['',Validators.required],
               dias:[0],
               costo_total:[0],
               estatus: ['PENDIENTE POR ACTIVAR',Validators.compose([Validators.required])],
               archivo:[''],
               edito_foto:[false]

          });

          this.eventoTerminarPeticionSubscription = this.servicioCatalogo.obtenerEventoTerminoPeticion().subscribe
          (x=> {
             this.terminarPeticion();
          });

    }


    ngOnInit() {
      this.visualizarLoading = true;
      this.repositorioAnuncio.obtenerSociosEImagen(this.anuncioObject.anuncio_id); 
    }

    // gets y Sets
      get listadoSocios(): Combo[] {
        return this.repositorioAnuncio.sociosCombo;
      }

      get loading(): boolean {
        return this.visualizarLoading;
      }

      get parametros(): ParametrosApp{
        return this.repositorioAnuncio.parametrosApp;
      }

    // métodos
    onFileChange(event){
       const  reader = new FileReader();

       if(event.target.files && event.target.files.length){
            const [file] = event.target.files;
            reader.readAsDataURL(file);

            reader.onload = () =>{

               this.anuncioForm.patchValue({
                  file: reader.result,
                  edito_foto:true
               });

               const img = new Image();
                img.src = reader.result as string;
                img.onload = () => {
                  this.altoImagen = img.naturalHeight;
                  this.anchoImagen = img.naturalWidth;
                  console.log('Width and Height', this.anchoImagen, this.altoImagen);
                };
                      
               this.imagenCadena = reader.result;
               this.cd.markForCheck();
            };
       }


    }

    terminarPeticion(){
       
      setTimeout(() => {
        this.llenarFormulario();
        this.visualizarLoading = false;
      },150);
    }

    llenarFormulario(){
        
         if(this.anuncioObject.anuncio_id !=0){

              // edición de un anuncio
              this.imagenCadena = this.repositorioAnuncio.imagenCadena;

              if(this.imagenCadena.length > 0){
                const img = new Image();
                img.src = this.imagenCadena as string;
                img.onload = () => {
                  this.altoImagen = img.naturalHeight;
                  this.anchoImagen = img.naturalWidth;
                  console.log('Width and Height', this.anchoImagen, this.altoImagen);
                };
              } 

              this.dataFinal = this.obtenerFecha(this.anuncioObject.fecha_final)
              this.dataInicial = this.obtenerFecha(this.anuncioObject.fecha_inicio)
              this.anuncioForm.patchValue({
                anuncio_id: this.anuncioObject.anuncio_id,
                file: this.imagenCadena,
                socio_id: this.anuncioObject.socio_id,
                estatus: this.anuncioObject.estatus,
                archivo: this.anuncioObject.archivo,
                fecha_final: this.dataFinal,
                fecha_inicio: this.dataInicial,
                dias: this.anuncioObject.dias,
                costo_total: this.anuncioObject.costo_total
              })
         }else{
           
          // nuevo anuncio
          if (this.storeService.retrieve('Id') !== '') {
              this.usuarioID =  this.storeService.retrieve('Id');

              this.anuncioForm.patchValue({
                socio_id: this.usuarioID.toString()
              });
          }

         }

    }


    onValueChangeInicial(value: Date): void {
      this.dataInicial = value;
      this.calcularPrecio();
    }

    onValueChangeFinal(value: Date): void {
      this.dataFinal = value;
      this.calcularPrecio();
    }


    borrarPrecio(){

      this.anuncioForm.patchValue({
        dias: 0,
        costo_total: 0,
      });
      this.Dias = 0;
    }

    calcularPrecio(){

      if(this.dataInicial !== null && this.dataFinal !== null){
           
        var dias = this.days_between(this.dataInicial, this.dataFinal);

        if(dias>=0){
            
          this.Dias = dias + 1;
          var costo_total = this.Dias * this.parametros.precio
          this.anuncioForm.patchValue({
            dias: this.Dias,
            costo_total: costo_total,
          });

        }else{
            this.borrarPrecio();
        }
      }else{
          this.borrarPrecio();
      }

    }

    days_between(date1, date2) {

      // The number of milliseconds in one day
      const ONE_DAY = 1000 * 60 * 60 * 24;
  
      // Calculate the difference in milliseconds
      const differenceMs = date2 - date1;
  
      // Convert back to days and return
      return Math.round(differenceMs / ONE_DAY);
  
  }

    guardar(){
   
       if(!this.anuncioForm.valid){
        this.servicioToast.Warning("Busca Salón","Se requiere uno valores");
       }else{

          if(this.anchoImagen < 600 || this.anchoImagen > 1200){
            this.servicioToast.Warning("Busca Salón", "El Ancho de la imagen debe estar entre 600 y 1200 pixeles ");
            return;
          }  

          if( this.altoImagen < 300 || this.altoImagen > 500){
            this.servicioToast.Warning("Busca Salón", "La Altura de la imagen debe estar entre 300 y 500 pixeles");
            return;
          }  

         this.Dias = this.anuncioForm.get("dias").value;    
       
         if(this.Dias <= 0){
           this.servicioToast.Warning("Busca Salón", "Selección de fechas incorrectas ");
           return;
         }

        let fechaActual =this.obtenerFechaActualConHoraCero();

         if(this.dataInicial < fechaActual){
           this.servicioToast.Warning("Busca Salón", "La fecha de inicio no puede ser menor a la actual ");
           return;
         }

          let datos = this.anuncioForm.value;
          this.visualizarLoading = true;

          this.spinner.show();
           this.repositorioAnuncio.guardar(datos).subscribe(response =>{
            this.visualizarLoading = false;
             if(response.mensajeError == "ok"){
               this.servicioCatalogo.enviarEventoAceptar();
               this.spinner.hide();
               this.servicioToast.Success("Busca Salón", "Proceso Correcto");
             }else{
              this.spinner.hide();
               this.servicioToast.Warning("Busca Salón",response.mensajeError);

             }
             

           }, (error) =>{
            this.spinner.hide();
            this.visualizarLoading = false;
            this.servicioToast.Warning("Busca Salón","Error de Conectividad");
           });

       }
    }

    regresar() {
      this.servicioCatalogo.enviarEventoRegresar();
    }

    // zona de destrucción
      ngOnDestroy() {
        this.eventoTerminarPeticionSubscription.unsubscribe();
      }
    

    obtenerFecha(strFecha: String) : Date {
    
        let strDay = strFecha.substring(0,2);
        let strMonth = strFecha.substring(3,5);
        let strYear = strFecha.substring(6,10)


        return new Date( parseInt(strYear),parseInt(strMonth), parseInt(strDay) );
    }

    obtenerFechaActualConHoraCero(): Date {
      var today = new Date();
      var myToday = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);
      return myToday;
    }

  }