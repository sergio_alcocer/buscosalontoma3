import { Component,Input,OnDestroy } from "@angular/core";
import { RepositorioAnuncio } from "../Repositorios/repositorioAnuncio";
import { CatalogoServicio } from "../Repositorios/catologosServicios";
import { Anuncios } from "../modelos/anuncios-model";
import { Filtro,Paginacion } from "../modelos/configurar-modelos-repositorio";
import { Subscription } from 'rxjs';

@Component({
    selector: "anuncio-list-componente",
    templateUrl: "anuncioList.componente.html",
    styleUrls: ['./anuncioEstilo.css']

  })

  export class AnuncioList implements OnDestroy {
      
    eventoPaginacionSubscription: Subscription;
    eventoFitroSearchSubsripction: Subscription;
    eventoRegresarSubscription: Subscription;
    eventoAceptarSubscription: Subscription;
    eventosLoading: Subscription;

    visualizarMode: boolean = true;
    visualizarLoading: boolean = false;


    constructor(private repositorioAnuncio:RepositorioAnuncio,private servicioCatalogo: CatalogoServicio){

      this.eventoPaginacionSubscription = this.servicioCatalogo.obtenerEventoPaginacion().subscribe
      (x => {
      this.cambioPagina(x);
      });

      this.eventoFitroSearchSubsripction = this.servicioCatalogo.obtnerEventoFiltroSearch().subscribe
      (x => {
      this.filtrarInformacion(x);
      });
  
       
       this.eventosLoading = this.servicioCatalogo.obtenerEventoLoading().subscribe(
         x => {
           this.eliminarLoadding();
         }
       );

       this.eventoRegresarSubscription = this.servicioCatalogo.obtenerEventoRegresar().subscribe(x => {
        this.regresar();
      });

      this.eventoAceptarSubscription = this.servicioCatalogo.obtenerEventoAceptar().subscribe(x => {
        this.guardar();
      });


      this.iniciarPeticiones();

    }

    ngOnInit() {
      this.visualizarMode = true;
    }


    // Zona de gets y sets

    get anuncioEntidad(): Anuncios{
      return this.repositorioAnuncio.anunciosModel;
    }

    get  anuncioLista(): Anuncios[]{
      return this.repositorioAnuncio.anunciosList;
    }

    get paginacion(): Paginacion {
      return this.repositorioAnuncio.paginacionObject;
    }

    
    get visualizar(): boolean {
      return this.visualizarMode;
    }

   set visualizar(nuevoValor: boolean) {
      this.visualizarMode = nuevoValor;
    }

    get filtros(): Filtro {
      return this.repositorioAnuncio.filtroObject;
    }

    get loading(): boolean {
      return this.visualizarLoading;
    }

    // métodos
    cambioPagina(nuevaPagina: number) {
      this.repositorioAnuncio.paginacionObject.pagina = nuevaPagina;
      this.visualizarLoading = true;
      this.repositorioAnuncio.obtenerAnuncios();
    }

    filtrarInformacion(cadena: string) {
      this.repositorioAnuncio.filtroObject.search = cadena;
      this.visualizarLoading = true;
      this.repositorioAnuncio.obtenerAnuncios();
    }

    iniciarPeticiones(){
      this.repositorioAnuncio.paginacionObject.pagina = 1;
      this.repositorioAnuncio.paginacionObject.currentPage = 1;
      this.repositorioAnuncio.filtroObject.search = "";
      this.visualizarLoading = true;
      this.repositorioAnuncio.obtenerAnuncios();
    }

    eliminarLoadding(){
      this.visualizarLoading = false;
    }

    nuevoElemento(){
      this.repositorioAnuncio.anunciosModel = new Anuncios();
      this.repositorioAnuncio.anunciosModel.anuncio_id = 0;
      this.visualizarMode = false;
   }

    regresar() {
    this.visualizarMode = true;
    }

    seleccionarElemento(pAnuncios: Anuncios) {
      this.repositorioAnuncio.anunciosModel = pAnuncios;
      this.visualizarMode = false;
    }

    guardar() {
      this.iniciarPeticiones();
      this.visualizar = true
    }

     /// zona de destrucción
     ngOnDestroy() {
      this.eventoFitroSearchSubsripction.unsubscribe();
      this.eventoPaginacionSubscription.unsubscribe();
      this.eventoRegresarSubscription.unsubscribe();
      this.eventoAceptarSubscription.unsubscribe();
      this.eventosLoading.unsubscribe();
     }


  }