import { Component, OnInit, OnDestroy } from '@angular/core';
import { ErrorStateMatcher1 } from '../error-state-matcher1';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { environment } from '../../environments/environment';
import { NuevoPasswordService } from './nuevopassword.service';
import { Usuario } from '../modelos/Usuario';
import { ActivatedRoute } from '@angular/router';
import { Recuperacion } from '../modelos/Recuperacion';
import { ToasterService } from "../Repositorios/toast.service";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-nuevopassword',
  templateUrl: './nuevopassword.component.html',
  styles: []
})
export class nuevopassword implements OnInit, OnDestroy {
  cargando = false;
  matcher = new ErrorStateMatcher1();
  formNuevoPass: FormGroup;
  subRef$: Subscription;
  mostrarProcesoExitoso: boolean;
  msjProcesoExitoso: string;
  mostrarAlertaValidacion: boolean;
  msjAlertaValidacion: string;
  mostrarError: boolean;
  msjError: string;
  token: string;

  constructor(
    formBuilder: FormBuilder,
    private router: Router,
    public dialog: MatDialog,
    private NuevoPasswordService: NuevoPasswordService,
    private route: ActivatedRoute,
    public servicioToast: ToasterService,
    private spinner: NgxSpinnerService) {

    this.formNuevoPass = formBuilder.group({
        pass: ['', [Validators.required, Validators.minLength(6)]],
        confirm: ['', Validators.required]
    },
    {
        validator: this.NuevoPasswordService.passwordMatchValidator(
          "pass",
          "confirm"
        )
      }      
    );
  }
  ngOnInit() { 
    this.token = this.route.snapshot.paramMap.get('token');
  }

  Confirmar() {
    const Recupera: Recuperacion = {
        password: this.formNuevoPass.value.pass,
        token:this.token
      };
      
    this.cargando = true;
    this.spinner.show();
      this.subRef$ =  this.NuevoPasswordService.ActualizaPass(Recupera)
      .subscribe(resultado => {
        if (resultado.body.id === 1) {
          this.servicioToast.Success("Busca Salón", "Su contraseña ha sido actualizada.");
          this.router.navigate(['/login']);
         } else {
          this.servicioToast.Warning("Busca Salón",resultado.body.mensajeError);
         }
         this.spinner.hide();
      }, error => {
          this.cargando = false;
          console.warn(error._message);
          this.spinner.hide();
      });
    }
  cancelar() {
    this.router.navigate(['/login']);
  }

  hasError(nombreControl: string, validacion: string) {
    const control = this.formNuevoPass.get(nombreControl);
    return control.hasError(validacion);
  }

  ngOnDestroy() {
    if (this.subRef$) {
      this.subRef$.unsubscribe();
    }
  }
}
