import { Component, Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Usuario } from '../modelos/Usuario';
import { Resultado } from '../modelos/Resultado';
import { DataService } from '../services/data.service';
import { FormGroup, AbstractControl } from "@angular/forms";
import { map } from "rxjs/operators";
import { Recuperacion } from '../modelos/Recuperacion';
import { GlobalConstants } from '../constantes/global-constants';

@Injectable({
    providedIn: 'root',
  })
  export class NuevoPasswordService {
    url = GlobalConstants.apiURL + "/api/socios";

    constructor(private httpClient: HttpClient, private dataService: DataService) {
    }
      ActualizaPass(Recupera: Recuperacion): Observable<any> {
        const data: Recuperacion = {
          password: Recupera.password,
          token:Recupera.token
        };
        const path = `${this.url}/ActualizaPasswordToken`;
        return this.dataService.post<Resultado>(path, data);
      }


      passwordMatchValidator(pass: string, confirm: string) {        
        return (formGroup: FormGroup) => {
          const passwordControl = formGroup.controls[pass];
          const confirmPasswordControl = formGroup.controls[confirm];
    
          if (!passwordControl || !confirmPasswordControl) {
            return null;
          }
    
          if (
            confirmPasswordControl.errors &&
            !confirmPasswordControl.errors.passwordMismatch
          ) {
            return null;
          }
    
          if (passwordControl.value !== confirmPasswordControl.value) {
            confirmPasswordControl.setErrors({ passwordMismatch: true });
          } else {
            confirmPasswordControl.setErrors(null);
          }
        };
      }
  }