import { Component,OnDestroy } from "@angular/core";
import { RepositorioCiudad } from "../Repositorios/repositorioCiudad";
import { CatalogoServicio } from "../Repositorios/catologosServicios";
import { Ciudad } from "../modelos/ciudad-model";
import { Filtro,Paginacion } from "../modelos/configurar-modelos-repositorio";
import { Subscription } from 'rxjs';


@Component({

    selector:"ciudad-list-componente",
    templateUrl:"ciudadesList.component.html",
    styleUrls: ['./ciudadEstilo.css']
  })


  export class CiudadList implements OnDestroy{

    
    eventoPaginacionSubscription: Subscription;
    eventoFitroSearchSubsripction: Subscription;
    eventoRegresarSubscription: Subscription;
    eventoAceptarSubscription: Subscription;
    eventosLoading: Subscription;

    visualizarMode: boolean = true;
    visualizarLoading: boolean = false;

    constructor(private repositorioCiudad: RepositorioCiudad,private servicioCatalogo: CatalogoServicio){

        this.eventoPaginacionSubscription = this.servicioCatalogo.obtenerEventoPaginacion().subscribe
        (x => {
        this.cambioPagina(x);
        });
  
        this.eventoFitroSearchSubsripction = this.servicioCatalogo.obtnerEventoFiltroSearch().subscribe
        (x => {
        this.filtrarInformacion(x);
        });
    
         
         this.eventosLoading = this.servicioCatalogo.obtenerEventoLoading().subscribe(
           x => {
             this.eliminarLoadding();
           }
         );
  
         this.eventoRegresarSubscription = this.servicioCatalogo.obtenerEventoRegresar().subscribe(x => {
          this.regresar();
        });
  
        this.eventoAceptarSubscription = this.servicioCatalogo.obtenerEventoAceptar().subscribe(x => {
          this.guardar();
        });
  


        this.iniciarPeticiones();
    }


    ngOnInit() {
        this.visualizarMode = true;
      }


      // Zona de gets y sets


      get ciudadEntidad(): Ciudad{

        return this.repositorioCiudad.ciudadModel;
      }

      get ciudadLista() : Ciudad[]{
          return this.repositorioCiudad.ciudadesList;
      }

      get paginacion(): Paginacion {
        return this.repositorioCiudad.paginacionObject;
      }
  
      get visualizar(): boolean {
        return this.visualizarMode;
     }

     set visualizar(nuevoValor: boolean) {
        this.visualizarMode = nuevoValor;
      }
  
      get filtros(): Filtro {
        return this.repositorioCiudad.filtroObject;
      }
  
      get loading(): boolean {
        return this.visualizarLoading;
      }

       // métodillos
       cambioPagina(nuevaPagina: number) {
        this.repositorioCiudad.paginacionObject.pagina = nuevaPagina;
        this.visualizarLoading = true;
        this.repositorioCiudad.obtenerCiudades();
      }
  
      filtrarInformacion(cadena: string) {
        this.repositorioCiudad.filtroObject.search = cadena;
        this.visualizarLoading = true;
        this.repositorioCiudad.obtenerCiudades();
      }

      iniciarPeticiones(){
        this.repositorioCiudad.paginacionObject.pagina = 1;
        this.repositorioCiudad.paginacionObject.currentPage = 1;
        this.repositorioCiudad.filtroObject.search = "";
        this.visualizarLoading = true;
        this.repositorioCiudad.obtenerCiudades();
      }

      eliminarLoadding(){
        this.visualizarLoading = false;
      }

      nuevoElemento(){
        this.repositorioCiudad.ciudadModel = new Ciudad();
        this.repositorioCiudad.ciudadModel.ciudad_ID = 0;
        this.visualizarMode = false;
     }

     regresar() {
        this.visualizarMode = true;
      }

      seleccionarElemento(pCiudad: Ciudad) {
        this.repositorioCiudad.ciudadModel = pCiudad;
        this.visualizarMode = false;
      }

      guardar() {
        this.iniciarPeticiones();
        this.visualizar = true
      }
      
      // cuando termina el ciclo de vida del componente

    ngOnDestroy() {
      this.eventoFitroSearchSubsripction.unsubscribe();
      this.eventoPaginacionSubscription.unsubscribe();
      this.eventoRegresarSubscription.unsubscribe();
      this.eventoAceptarSubscription.unsubscribe();
      this.eventosLoading.unsubscribe();
    }


  }