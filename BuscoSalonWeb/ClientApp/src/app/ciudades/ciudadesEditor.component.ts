import { Component,Input,OnDestroy } from "@angular/core";
import { RepositorioCiudad } from "../Repositorios/repositorioCiudad";
import { Ciudad } from "../modelos/ciudad-model";
import { Combo } from "../modelos/Combo";
import { CatalogoServicio } from "../Repositorios/catologosServicios";
import { FormBuilder,FormGroup, Validators } from "@angular/forms";
import { ToasterService } from "../Repositorios/toast.service";
import { Subscription } from 'rxjs';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
    selector: "ciudad-editor",
    templateUrl: "ciudadesEditor.component.html"

  })


  export class EditarCiudad implements OnDestroy {

    @Input()
    ciudadObject : Ciudad;
    @Input()
    visualizar: boolean;
    visualizarLoading: boolean = false;

    ciudadForm: FormGroup;
    eventoTerminarPeticionSubscription: Subscription;

    constructor(private repositorioCiudad:RepositorioCiudad, private servicioCatalogo: CatalogoServicio,
        public servicioToast: ToasterService,public formBuilder: FormBuilder,private spinner: NgxSpinnerService){

            this.ciudadForm = this.formBuilder.group({
                Ciudad_ID:[0],
                Estado_ID:['',Validators.compose([Validators.required])],
                Ciudad_Nombre:['',Validators.compose([Validators.pattern('^[^"$%&|\'<>#]*$'),Validators.required])],
                Ciudad_Estatus:['',Validators.compose([Validators.required])]

            });

         
            this.eventoTerminarPeticionSubscription = this.servicioCatalogo.obtenerEventoTerminoPeticion().subscribe
            (x=> {
               this.terminarPeticion();
            });
        
         }

     ngOnInit() {
        this.visualizarLoading = true
        this.repositorioCiudad.obtenerEstados();
     }

    // get y set
     
    get listadoEstados(): Combo[]{
         return this.repositorioCiudad.estadosCombo;
    } 

    // métodos
    get loading(): boolean {
        return this.visualizarLoading;
      }
   
    guardar() {
     
      if(!this.ciudadForm.valid){
        this.servicioToast.Warning("Busca Salón","Se requiere uno valores");
      }else{
          let datos = this.ciudadForm.value;
          
          this.repositorioCiudad.guardar(datos).subscribe(response => {
            if(response.mensajeError == "ok"){
              this.servicioCatalogo.enviarEventoAceptar();
              this.servicioToast.Success("Busca Salón", "Proceso Correcto");
          }else{
            this.servicioToast.Warning("Busca Salón",response.mensajeError);
          }

          }, (error) => {
            this.visualizarLoading = false;
            this.servicioToast.Warning("Busca Salón","Error de Conectividad");
          });

      }

    }

    llenarFormulario(){
         
        if(this.ciudadObject.ciudad_ID != 0){

           this.ciudadForm.patchValue({
            Ciudad_ID: this.ciudadObject.ciudad_ID,
            Estado_ID: this.ciudadObject.estado_ID,
            Ciudad_Nombre: this.ciudadObject.ciudad_Nombre,
            Ciudad_Estatus: this.ciudadObject.ciudad_Estatus
           });

        }
    }
    regresar() {
        this.servicioCatalogo.enviarEventoRegresar();
    }

    terminarPeticion(){

        setTimeout(() => {
            this.llenarFormulario();
            this.visualizarLoading = false;
          },100);

    }     


    ngOnDestroy(){
        this.eventoTerminarPeticionSubscription.unsubscribe();
    }
  }