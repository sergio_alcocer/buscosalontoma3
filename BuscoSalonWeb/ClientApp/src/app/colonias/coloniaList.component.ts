import { Component,OnDestroy } from "@angular/core";
import { RepositorioColonias } from "../Repositorios/repositorioColonia";
import { CatalogoServicio } from "../Repositorios/catologosServicios";
import { Colonia } from "../modelos/colonia_model";
import { Filtro,Paginacion } from "../modelos/configurar-modelos-repositorio";
import { Subscription } from 'rxjs';
@Component({

  selector:"colonia-list-componente",
  templateUrl:"coloniaList.component.html",
  styleUrls: ['./coloniaEstilo.css']
})

export class ColoniasList implements OnDestroy {


    eventoPaginacionSubscription: Subscription;
    eventoFitroSearchSubsripction: Subscription;
    eventoRegresarSubscription: Subscription;
    eventoAceptarSubscription: Subscription;
    eventosLoading: Subscription;

    visualizarMode: boolean = true;
    visualizarLoading: boolean = false;

    constructor(private repositorioColonias: RepositorioColonias,private servicioCatalogo: CatalogoServicio){


      this.eventoPaginacionSubscription = this.servicioCatalogo.obtenerEventoPaginacion().subscribe
      (x => {
      this.cambioPagina(x);
      });

      this.eventoFitroSearchSubsripction = this.servicioCatalogo.obtnerEventoFiltroSearch().subscribe
      (x => {
      this.filtrarInformacion(x);
      });
  
       
       this.eventosLoading = this.servicioCatalogo.obtenerEventoLoading().subscribe(
         x => {
           this.eliminarLoadding();
         }
       );

       this.eventoRegresarSubscription = this.servicioCatalogo.obtenerEventoRegresar().subscribe(x => {
        this.regresar();
      });

      this.eventoAceptarSubscription = this.servicioCatalogo.obtenerEventoAceptar().subscribe(x => {
        this.guardar();
      });


      this.iniciarPeticiones();

    }

    ngOnInit() {
      this.visualizarMode = true;
    }

    // Zona de gets y sets
    get coloniaEntidad(): Colonia {
      return this.repositorioColonias.coloniaModel;
    }

    get coloniaLista(): Colonia[]{
      return this.repositorioColonias.coloniaList;
   }
    get paginacion(): Paginacion {
      return this.repositorioColonias.paginacionObject;
    }

    get visualizar(): boolean {
      return this.visualizarMode;
  }

  set visualizar(nuevoValor: boolean) {
      this.visualizarMode = nuevoValor;
    }

    get filtros(): Filtro {
      return this.repositorioColonias.filtroObject;
    }

    get loading(): boolean {
      return this.visualizarLoading;
    }

    // Métodos
    cambioPagina(nuevaPagina: number) {
      this.repositorioColonias.paginacionObject.pagina = nuevaPagina;
      this.visualizarLoading = true;
      this.repositorioColonias.obtenerColonias();
    }

    filtrarInformacion(cadena: string) {
      this.repositorioColonias.filtroObject.search = cadena;
      this.visualizarLoading = true;
      this.repositorioColonias.obtenerColonias();
    }

    iniciarPeticiones(){
      this.repositorioColonias.paginacionObject.pagina = 1;
      this.repositorioColonias.paginacionObject.currentPage = 1;
      this.repositorioColonias.filtroObject.search = "";
      this.visualizarLoading = true;
      this.repositorioColonias.obtenerColonias();
    }

    eliminarLoadding(){
      this.visualizarLoading = false;
    }

    nuevoElemento(){
      this.repositorioColonias.coloniaModel = new Colonia();
      this.repositorioColonias.coloniaModel.colonia_ID = 0;
      this.visualizarMode = false;
   }

   regresar() {
    this.visualizarMode = true;
  }

  seleccionarElemento(pColonia: Colonia) {
    this.repositorioColonias.coloniaModel = pColonia;
    this.visualizarMode = false;
  }

  guardar() {
    this.iniciarPeticiones();
    this.visualizar = true
  }

  ngOnDestroy() {

    this.eventoFitroSearchSubsripction.unsubscribe();
    this.eventoPaginacionSubscription.unsubscribe();
    this.eventoRegresarSubscription.unsubscribe();
    this.eventoAceptarSubscription.unsubscribe();
    this.eventosLoading.unsubscribe();
  }


}