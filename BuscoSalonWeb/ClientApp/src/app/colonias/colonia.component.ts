import { Component,Input,OnDestroy } from "@angular/core";
import { RepositorioColonias } from "../Repositorios/repositorioColonia";
import { Colonia } from "../modelos/colonia_model";
import { Combo } from "../modelos/Combo";
import { ComboSimple } from "../modelos/ComboSimple";
import { CatalogoServicio } from "../Repositorios/catologosServicios";
import { FormBuilder,FormGroup, Validators } from "@angular/forms";
import { ToasterService } from "../Repositorios/toast.service";
import { Subscription } from 'rxjs';



@Component({
    selector: "colonia-editor",
    templateUrl: "colonia.component.html"
  
  })

  export class EditorColonia  implements OnDestroy {

    @Input()
    coloniaObject : Colonia;
    @Input()
    visualizar: boolean;
    visualizarLoading: boolean = false;

    coloniaForm: FormGroup;

    eventoTerminarPeticionSubscription: Subscription;

    constructor(private repositorioColonias:RepositorioColonias, private servicioCatalogo: CatalogoServicio,
        public servicioToast: ToasterService,
         public formBuilder: FormBuilder){

          // this.obtenerCatalogos();

            this.coloniaForm = this.formBuilder.group({
              Colonia_ID:[0],
              Ciudad_ID:['',Validators.compose([Validators.required])],
              Tipo:['',Validators.compose([Validators.required])],
              Nombre: ['',Validators.compose([Validators.pattern('^[^"$%&|\'<>#]*$'),Validators.required])],
              Codigo_Postal: ['',Validators.compose([ Validators.minLength(3),  Validators.maxLength(5),  Validators.required,Validators.pattern('^[0-9]+$')])],
              Estatus: ['',Validators.compose([Validators.required])]
            });


            this.eventoTerminarPeticionSubscription = this.servicioCatalogo.obtenerEventoTerminoPeticion().subscribe
            (x=> {
               this.terminarPeticion();
            });

         }


         ngOnInit() {
         // this.llenarFormulario();
          this.visualizarLoading = true;
           this.repositorioColonias.obtenerTodasPeticiones();
         }    

         // get y set

         get listadoCiudad(): Combo[]{
           return this.repositorioColonias.ciudadesCombo;
         }
   
         get listadoTipoColonia(): ComboSimple[]{
           return this.repositorioColonias.tipoColonia;
         }

         // métodos

         get loading(): boolean {
          return this.visualizarLoading;
        }
    

         obtenerCatalogos(){
          this.repositorioColonias.obtenerCiudadCombo();
          this.repositorioColonias.obtenerTipoColonia();
         }

         llenarFormulario(){

           if(this.coloniaObject.colonia_ID !=0){

            this.coloniaForm.patchValue({
              Colonia_ID: this.coloniaObject.colonia_ID,
              Ciudad_ID: this.coloniaObject.ciudad_ID,
              Tipo: this.coloniaObject.tipo,
              Nombre: this.coloniaObject.nombre,
              Codigo_Postal: this.coloniaObject.codigo_Postal ,
              Estatus: this.coloniaObject.estatus
            });

           }

         }

         guardar() {
            
          if(!this.coloniaForm.valid){
            this.servicioToast.Warning("Busca Salón","Se requiere uno valores");
          }else{
              let datos = this.coloniaForm.value;
              
              this.repositorioColonias.guardar(datos)
              .subscribe(response => {
                this.visualizarLoading = false;

                if(response.mensajeError == "ok"){
                    this.servicioCatalogo.enviarEventoAceptar();
                    this.servicioToast.Success("Busca Salón", "Proceso Correcto");
                }else{
                  this.servicioToast.Warning("Busca Salón",response.mensajeError);
                }

            },(error) => {
              this.visualizarLoading = false;
              this.servicioToast.Warning("Busca Salón","Error de Conectividad");
            });

          }


         }

         regresar() {
          this.servicioCatalogo.enviarEventoRegresar();
        }

        terminarPeticion(){

          setTimeout(() => {
            this.llenarFormulario();
            this.visualizarLoading = false;
          },150);
          
        }

        ngOnDestroy() {
          this.eventoTerminarPeticionSubscription.unsubscribe();
        }

  }