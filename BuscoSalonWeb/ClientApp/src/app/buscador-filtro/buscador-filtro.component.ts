import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Parametro_Busqueda } from '../modelos/Parametro_Busqueda';
import { InmueblesService } from '../cat-inmuebles/cat-inmuebles.service';

@Component({
  selector: 'app-buscador-filtro',
  templateUrl: './buscador-filtro.component.html',
  styleUrls: ['./buscador-filtro.component.css']
})
export class BuscadorFiltroComponent implements OnInit, OnDestroy {

    constructor(private inmuebleService: InmueblesService) {
    }

    ngOnInit(): void {

    }

    ngOnDestroy() {

    }

}
