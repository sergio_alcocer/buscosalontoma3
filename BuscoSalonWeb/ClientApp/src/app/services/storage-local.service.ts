import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageLocalService {
  private storage: any;

  constructor() {
    //Para si queremos datos que persistan, si sale del navegador o paga su pc etc
    this.storage = localStorage;
  }

  public retrieve(key: string): any {
    const item = this.storage.getItem(key);

    if (item && item !== 'undefined') {
      return JSON.parse(item);
    }

    return;
  }

  public store(key: string, value: any) {
    this.storage.setItem(key, JSON.stringify(value));
  }
}
