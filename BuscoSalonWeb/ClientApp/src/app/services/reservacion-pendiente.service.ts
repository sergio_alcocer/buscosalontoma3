import { Injectable } from '@angular/core';
import { Inmueble } from '../modelos/Inmueble';

@Injectable({
    providedIn: 'root'
})
export class ReservacionPendienteService {

    inmuebleSeleccionado: Inmueble = {id : 0};
    fecha: Date;
    paquete: number;
    url: string;

    constructor(){

    }

    public set(inmueble: Inmueble, fecha: Date, paquete: number, url: string){
        this.inmuebleSeleccionado = inmueble;
        this.fecha = fecha;
        this.paquete = paquete;
        this.url = url;
    }

    public reset(){
        this.inmuebleSeleccionado = {id : 0};
        this.fecha = undefined;
        this.paquete = 0;
        this.url = '';
    }


}
