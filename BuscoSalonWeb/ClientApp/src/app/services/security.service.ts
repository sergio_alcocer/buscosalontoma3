import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {
  IsAuthorized: any;
  Role: any;
  Id: any;

  private authSource = new Subject<boolean>();
  //Es un obsevable para cuando haya un cambio
  authChallenge$ = this.authSource.asObservable();

  constructor(
    private storeService: StorageService) {

    if (this.storeService.retrieve('IsAuthorized') !== '') {
      this.IsAuthorized = this.storeService.retrieve('IsAuthorized');
      //El next sirve para avisarle a todos los componentes que lo usan que esta loggueado OK
      this.authSource.next(this.IsAuthorized);
    }

    if (this.storeService.retrieve('Role') !== '') {
      this.Role = this.storeService.retrieve('Role');
      //El next sirve para avisarle a todos los componentes que lo usan que esta loggueado OK
      this.authSource.next(this.Role);
    }

    if (this.storeService.retrieve('Id') !== '') {
      this.Id = this.storeService.retrieve('Id');
      //El next sirve para avisarle a todos los componentes que lo usan que esta loggueado OK
      this.authSource.next(this.Id);
    }
  }

  public GetToken(): any {
    return this.storeService.retrieve('authData');
  }

  public ResetAuthData() {
    this.storeService.store('authData', '');
    this.IsAuthorized = false;
    this.Role=0;
    this.storeService.store('IsAuthorized', false);
    this.storeService.store('Role', '');
    this.storeService.store('Id', '');
  }

  public SetAuthData(token: any) {
    this.storeService.store('authData', token.token);
    this.IsAuthorized = true;
    this.Role = token.tipo;
    this.Id = token.id;
    this.storeService.store('IsAuthorized', true);
    this.storeService.store('Role', token.tipo);
    this.storeService.store('Id', token.id);
    
    this.authSource.next(true);
  }

  public LogOff() {
    this.ResetAuthData();

    this.authSource.next(false);
  }
}
