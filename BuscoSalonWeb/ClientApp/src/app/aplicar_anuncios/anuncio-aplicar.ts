import { Component,Input,OnDestroy,TemplateRef  } from "@angular/core";
import { RepositorioAnuncioAplicar } from "../Repositorios/repositorioAplicarAnuncio";
import { CatalogoServicio } from "../Repositorios/catologosServicios";
import { Anuncios } from "../modelos/anuncios-model";
import { Filtro,Paginacion } from "../modelos/configurar-modelos-repositorio";
import { Subscription } from 'rxjs';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ToasterService } from "../Repositorios/toast.service";

@Component({
    selector: "anuncio-aplicar",
    templateUrl: "anuncio-aplicar.html",
    styleUrls: ['./anuncio-aplicar.css']

  })

  export class AplicarAnuncio implements OnDestroy{

    eventoPaginacionSubscription: Subscription;
    eventoFitroSearchSubsripction: Subscription;
    eventosLoading: Subscription;
    modalRef: BsModalRef;
    message: string;
    visualizarLoading: boolean = false;
    anuncio_id_selected : number = 0;

    constructor(private repositorioAnuncio:RepositorioAnuncioAplicar,private servicioCatalogo: CatalogoServicio,
      private modalService: BsModalService,public servicioToast: ToasterService){
      this.eventoPaginacionSubscription = this.servicioCatalogo.obtenerEventoPaginacion().subscribe
      (x => {
      this.cambioPagina(x);
      });

      this.eventoFitroSearchSubsripction = this.servicioCatalogo.obtnerEventoFiltroSearch().subscribe
      (x => {
      this.filtrarInformacion(x);
      });
  
       
       this.eventosLoading = this.servicioCatalogo.obtenerEventoLoading().subscribe(
         x => {
           this.eliminarLoadding();
         }
       );

     

    

        this.iniciarPeticiones();
    }
    // Zona de gets y sets

     get  anuncioLista(): Anuncios[]{
        return this.repositorioAnuncio.anunciosList;
      }
  
      get paginacion(): Paginacion {
        return this.repositorioAnuncio.paginacionObject;
      }

      get filtros(): Filtro {
        return this.repositorioAnuncio.filtroObject;
      }
  
      get loading(): boolean {
        return this.visualizarLoading;
      }

    ngOnInit() {

    }

     // métodos
     cambioPagina(nuevaPagina: number) {
        this.repositorioAnuncio.paginacionObject.pagina = nuevaPagina;
        this.visualizarLoading = true;
        this.repositorioAnuncio.obtenerAnuncios();
      }
  
      filtrarInformacion(cadena: string) {
        this.repositorioAnuncio.filtroObject.search = cadena;
        this.visualizarLoading = true;
        this.repositorioAnuncio.obtenerAnuncios();
      }
  
      iniciarPeticiones(){
        this.repositorioAnuncio.paginacionObject.pagina = 1;
        this.repositorioAnuncio.paginacionObject.currentPage = 1;
        this.repositorioAnuncio.filtroObject.search = "";
        this.visualizarLoading = true;
        this.repositorioAnuncio.obtenerAnuncios();
      }
  
      eliminarLoadding(){
        this.visualizarLoading = false;
      }
  
     
      seleccionarElemento(pAnuncios: Anuncios,template: TemplateRef<any>) {
        this.repositorioAnuncio.anunciosModel = pAnuncios;
        this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
        this.anuncio_id_selected = pAnuncios.anuncio_id;
   
      
      }
    /// zona de destrucción
    ngOnDestroy() {
        this.eventoFitroSearchSubsripction.unsubscribe();
        this.eventoPaginacionSubscription.unsubscribe();
        this.eventosLoading.unsubscribe();
    }


    activarImagen(anuncio_id: number){
     
        this.visualizarLoading = true;
        this.repositorioAnuncio.activarAnuncio(anuncio_id).subscribe(response=> {
         this.visualizarLoading = false;
         if(response.mensajeError == "ok"){
          this.servicioToast.Success("Busca Salón", "Proceso Correcto");
          this.iniciarPeticiones();
         }else{
          this.servicioToast.Warning("Busca Salón",response.mensajeError);
         }

        },(error) => {
          this.visualizarLoading = false;
          this.servicioToast.Warning("Busca Salón","Error de Conectividad");
        });

    }

    // modales
    confirm(): void {
      this.message = 'Confirmed!';
      this.modalRef.hide();
      this.activarImagen(this.anuncio_id_selected);
    }
   
    decline(): void {
      this.message = 'Declined!';
      this.modalRef.hide();
    }
  }