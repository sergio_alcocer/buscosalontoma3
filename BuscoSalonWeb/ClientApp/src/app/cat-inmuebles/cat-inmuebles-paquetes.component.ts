import { Component, Inject, OnInit, OnDestroy, Input, EventEmitter, Output } from '@angular/core';
import { InmueblesService } from './cat-inmuebles.service';
import { Subscription } from 'rxjs';
import { Paquete } from '../modelos/Paquete';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-inmuebles-paquetes',
  templateUrl: './cat-inmuebles-paquetes.component.html',
  styleUrls: ['./cat-inmuebles-paquetes.component.css']
})
export class InmueblesPaquetesComponent implements OnInit, OnDestroy {

  @Input() inmuebleID: number;
  @Input() nombreInmueble: string;
  @Output() regresarPantalla = new EventEmitter<boolean>();
  options: FormGroup;
  txtnombre = new FormControl('auto');
  txtcosto = new FormControl('auto');
  chkincluyemesas = new FormControl('auto');
  chkincluyemeseros = new FormControl('auto');
  chkincluyemanteleria = new FormControl('auto');
  txtotrosdatos = new FormControl('auto');
  estadoformulario: number;
  paquetes: any;
  paqueteactivo: Paquete;
  subRef$: Subscription;

  constructor(private inmuebleService: InmueblesService, fb: FormBuilder, private spinner: NgxSpinnerService) {
    this.estadoformulario = 1;
    this.limpiarPaquete();
    this.options = fb.group({
      txtnombre: this.txtnombre,
      txtcosto: this.txtcosto,
      chkincluyemesas: this.chkincluyemesas,
      chkincluyemeseros: this.chkincluyemeseros,
      chkincluyemanteleria: this.chkincluyemanteleria,
      txtotrosdatos: this.txtotrosdatos,
    });
  }

  ngOnInit(): void {
    console.log(this.inmuebleService);
    this.consultarpaquetes();
  }

  regresar() {
    this.regresarPantalla.emit(true);
  }

  consultarpaquetes() {
    this.spinner.show();
    this.subRef$ = this.inmuebleService.ConsultarPaquetes(this.inmuebleID).subscribe(resultado => {
      this.paquetes = resultado.body;
      this.spinner.hide();
    }, error => {
      console.warn(error._message);
      this.spinner.hide();
    });
  }

  consultarpaqueteporid(id: number) {
    this.limpiarPaquete();
    this.spinner.show();
    this.subRef$ = this.inmuebleService.ConsultarPaquetePorID(id).subscribe(resultado => {
      this.paqueteactivo = resultado.body;
      this.estadoformulario = 3;
      this.spinner.hide();
    }, error => {
      console.warn(error._message);
      this.spinner.hide();
    });
  }

  agregarpaquete() {
    this.limpiarPaquete();
    this.paqueteactivo.estatus = 1;
    this.estadoformulario = 2;
  }

  regresaralistado() {
    this.limpiarPaquete();
    this.consultarpaquetes();
    this.estadoformulario = 1;
  }

  limpiarPaquete() {
    this.paqueteactivo = {
      id: 0 as number,
      nombre: '' as string,
      inmuebleID: this.inmuebleID,
      inmuebleNombre: '' as string,
      costo: 0 as number,
      incluyeMeseros: 0 as number,
      incluyeMesas: 0 as number,
      incluyeManteleria: 0 as number,
      otrosDatos: '' as string,
      estatus: 0 as number
    };
  }

  guardarpaquete() {
    this.spinner.show();
    if (this.paqueteactivo.id === 0) {
      this.subRef$ = this.inmuebleService.AgregarPaquete(this.paqueteactivo)
        .subscribe(resultado => {
          if (resultado.body.id === 1) {
            this.regresaralistado();
            this.spinner.hide();
          }
        }, error => {
          console.warn(error._message);
          this.spinner.hide();
        });
    } else {
      this.subRef$ = this.inmuebleService.ModificarPaquete(this.paqueteactivo)
        .subscribe(resultado => {
          if (resultado.body.id === 1) {
            this.spinner.hide();
            this.regresaralistado();
          }
        }, error => {
          this.spinner.hide();
          console.warn(error._message);
        });
    }
  }

  eliminarpaquete(id: number) {
    const isOk = confirm('¿Esta seguro que desea eliminar el paquete?');
    if (isOk) {
      this.spinner.show();
      this.subRef$ = this.inmuebleService.EliminarPaquete(id)
        .subscribe(resultado => {
          if (resultado.body.id === 1) {
            this.spinner.hide();
            this.consultarpaquetes();
          }
        }, error => {
          console.warn(error._message);
          this.spinner.hide();
        });
    }
  }

  ngOnDestroy() {
    if (this.subRef$) { this.subRef$.unsubscribe(); }
  }
}
