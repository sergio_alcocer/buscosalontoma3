import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { InmueblesService } from './cat-inmuebles.service';
import { Subscription } from 'rxjs';
import { Inmueble } from '../modelos/Inmueble';
import { Ambiente } from '../modelos/Ambiente';
import { TipoAmbienteList } from '../tipo-ambiente/tipoambianteList.component';
import { Estado } from '../modelos/Estado';
import { Ciudad } from '../modelos/Ciudad';
import { Colonia } from '../modelos/Colonia';
import { SecurityService } from '../services/security.service';
import { ToasterService } from "../Repositorios/toast.service";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-inmuebles',
  templateUrl: './cat-inmuebles.component.html',
  styleUrls: ['./cat-inmuebles.component.css']
})
export class InmueblesComponent implements OnInit, OnDestroy {

  inmuebles: any;
  inmuebleactivo: Inmueble;
  estadoformulario: number;
  mostrarProcesoExitoso: boolean;
  msjProcesoExitoso: string;
  mostrarAlertaValidacion: boolean;
  msjAlertaValidacion: string;
  mostrarError: boolean;
  msjError: string;
  mostrarEliminarInmueble: boolean;
  filtroBusqueda: string;
  codigoPostal: string;
  listaAmbientes: Ambiente[] = [];
  listaEstados: Estado[] = [];
  listaCiudades: Ciudad[] = [];
  listaColonias: Colonia[] = [];
  subRef$: Subscription;
  confirmationDialogService: any;
  visualizarinmuebles: boolean;
  visualizarpaquetes: boolean;
  visualizarimagenes: boolean;
  visualizarseguimiento: boolean;
  agregarNuevoInmueble: boolean;
  inmuebleID: number;
  nombreInmueble: string;
  IsAuthenticated = false;
  Role = 0;
  Id = 0;

  constructor(private inmuebleService: InmueblesService, private secutiryService: SecurityService,public servicioToast: ToasterService,private spinner: NgxSpinnerService) {
    this.limpiarMensajes();
    this.limpiarInmueble();
    this.estadoformulario = 1;
    this.filtroBusqueda = '';
    this.visualizarinmuebles = true;
    this.visualizarpaquetes = false;
    this.visualizarimagenes = false;
    this.visualizarseguimiento = false;
    this.consultartiposambientes();
    this.consultarestados();
  }

  ngOnInit(): void {
    this.IsAuthenticated= this.secutiryService.IsAuthorized;
    this.Role = this.secutiryService.Role;
    this.Id = this.secutiryService.Id;
    this.consultarinmuebles();
    this.validaragregarinmueble();
  }

  set visualizarlistainmuebles(nuevoValor: boolean) {
    this.visualizarinmuebles = nuevoValor;
  }

  consultarinmuebles() {
    let filtro: string;
    let idSocio: number;
    if (this.filtroBusqueda === undefined || this.filtroBusqueda.trim() === '') {
      filtro = '---sindatos---';
    } else {
      filtro = this.filtroBusqueda;
    }
    idSocio = (this.Role == 4 ? Number(this.Id) : 0);
    this.spinner.show();
    this.subRef$ =  this.inmuebleService.ConsultarInmuebles(filtro, idSocio).subscribe(resultado => {
      this.inmuebles = resultado.body;
      this.spinner.hide();
    }, error => {
        console.warn(error._message);
        this.spinner.hide();
    });
  }

  validaragregarinmueble() {
    let idSocio: number;
    idSocio = Number(this.Id);
    this.spinner.show();
    this.subRef$ =  this.inmuebleService.ValidarAgregarInmueble(idSocio).subscribe(resultado => {
      if (resultado.body.id === 1) {
        this.agregarNuevoInmueble = true;
        this.spinner.hide();
      } else {
        this.agregarNuevoInmueble = false;
        this.spinner.hide();
      }
    }, error => {
        console.warn(error._message);
        this.spinner.hide();
    });
  }

  consultarColoniaPorCP(cp: string) {
    this.spinner.show();
    this.subRef$ =  this.inmuebleService.ConsultarColoniasPorCP(cp).subscribe(resultado => {
      this.listaColonias = resultado.body;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
        console.warn(error._message);
    });
  }

  consultarinmuebleporid(id: number) {
    this.limpiarMensajes();
    this.limpiarInmueble();
    this.spinner.show();
    this.subRef$ = this.inmuebleService.ConsultarInmueblePorID(id).subscribe(resultado => {
      this.inmuebleactivo = resultado.body;
      this.spinner.hide();
      this.consultarciudades(this.inmuebleactivo.estadoID);
      this.consultarcolonias(this.inmuebleactivo.ciudadID);
      this.estadoformulario = 3;
    }, error => {
      this.spinner.hide();
        console.warn(error._message);
    });
  }

  agregarinmueble() {
    let idSocio: number;
    idSocio = Number(this.Id);
    this.spinner.show();
    this.subRef$ =  this.inmuebleService.ValidarAgregarInmueble(idSocio).subscribe(resultado => {
      if (resultado.body.id === 1) {
        this.agregarNuevoInmueble = true;
        this.limpiarMensajes();
        this.limpiarInmueble();
        this.inmuebleactivo.estatus = 1;
        this.estadoformulario = 2;
        this.spinner.hide();
      } else {
        this.agregarNuevoInmueble = false;
        this.spinner.hide();
        this.servicioToast.Warning("Busca Salón", "Ha llegado a su máximo de inmuebles para registrar.");
      }
    }, error => {
        console.warn(error._message);
        this.spinner.hide();
    });
  }

  regresaralistado() {
    this.limpiarInmueble();
    this.consultarinmuebles();
    this.estadoformulario = 1;
  }

  limpiarInmueble() {
    this.inmuebleactivo = {
      id: 0 as number,
      socioID: Number(this.Id),
      descripcionsocio: '' as string,
      ciudadID: 0 as number,
      descripcionCiudad: '' as string,
      nombre: '' as string,
      direccion: '' as string,
      estadoID: 0 as number,
      coloniaID: 0 as number,
      descripcionColonia: '' as string,
      referencias: '' as string,
      ambienteID: 0 as number,
      descripcionAmbiente: '' as string,
      capacidad: 0 as number,
      costo: 0 as number,
      usuario: '' as string,
      creacion: undefined as Date,
      estatus: 0 as number,
      calificacionPromedio: 0 as number,
      localizacion: '' as string,
      correoElectronico: '' as string,
      telefono: '' as string,
      whatsapp: '' as string,
      facebook: '' as string,
      sitioWeb: '' as string,
      latitud: 0,
      longitud: 0
    };
  }

  limpiarMensajes() {
    this.mostrarAlertaValidacion = false;
    this.msjAlertaValidacion = '';
    this.mostrarProcesoExitoso = false;
    this.msjProcesoExitoso = '';
    this.mostrarError = false;
    this.msjError = '';
    this.mostrarEliminarInmueble = false;
  }

  guardarinmueble() {    
    this.limpiarMensajes();
    if(isNaN(this.inmuebleactivo.latitud) || this.inmuebleactivo.latitud == null){
      this.servicioToast.Warning("Busca Salón", "Se requiere un número para la latitud");
      return;
    }

    if(isNaN(this.inmuebleactivo.longitud) || this.inmuebleactivo.longitud == null){
      this.servicioToast.Warning("Busca Salón", "Se requiere un número para la longitud");
      return;
    }

    this.inmuebleactivo.socioID = Number(this.Id);
    if(this.inmuebleactivo.descripcionColonia === null)
      this.inmuebleactivo.descripcionColonia = '';
    this.limpiarMensajes();
    this.spinner.show();
    if (this.inmuebleactivo.id === 0) {
      this.subRef$ =  this.inmuebleService.AgregarInmueble(this.inmuebleactivo)
      .subscribe(resultado => {
        if (resultado.body.id === 1) {
          this.servicioToast.Success("Busca Salón", "Alta de inmueble exitosa!.");
          this.spinner.hide();
          this.regresaralistado();
        } else {
          this.spinner.hide();
          this.servicioToast.Warning("Busca Salón", resultado.body.mensajeError);
        }
      }, error => {
        this.spinner.hide();
          console.warn(error._message);
      });
    } else {
      this.subRef$ =  this.inmuebleService.ModificarInmueble(this.inmuebleactivo)
      .subscribe(resultado => {
        if (resultado.body.id === 1) {
          this.servicioToast.Success("Busca Salón", "Actualización de inmueble exitosa!.");
          this.spinner.hide();
          this.regresaralistado();
        } else {
          this.spinner.hide();
          this.servicioToast.Warning("Busca Salón", resultado.body.mensajeError);
        }
      }, error => {
        this.spinner.hide();
          console.warn(error._message);
      });
    }
  }

  eliminarinmueble(id: number) {
    this.limpiarMensajes();
    const isOk = confirm('¿Esta seguro que desea eliminar el inmueble?');
    if (isOk) {
      this.spinner.show();
      this.subRef$ = this.inmuebleService.EliminarInmueble(id)
        .subscribe(resultado => {
          if (resultado.body.id === 1) {
            this.servicioToast.Success("Busca Salón", "Eliminación de inmueble exitosa!.");
            this.spinner.hide();
            this.consultarinmuebles();
          } else {
            this.spinner.hide();
            this.servicioToast.Warning("Busca Salón", resultado.body.mensajeError);
          }
        }, error => {
          this.spinner.hide();
          console.warn(error._message);
        });
    }
  }

  buscarInmueble(event: any) {
    if (event.keyCode === 13) {
      this.consultarinmuebles();
    }
}

  buscarColoniaCP(event: any) {
    if (event.keyCode === 13) {
      this.consultarColoniaPorCP(event.target.value);
    }
}

consultartiposambientes() {
  this.subRef$ =  this.inmuebleService.ConsultarTiposAmbientes().subscribe(resultado => {
    this.listaAmbientes = resultado.body;
  }, error => {
      console.warn(error._message);
  });
}

consultarestados() {
  this.spinner.show();
  this.subRef$ =  this.inmuebleService.ConsultarEstados().subscribe(resultado => {
    this.listaEstados = resultado.body;
    this.spinner.hide();
  }, error => {
      console.warn(error._message);
      this.spinner.hide();
  });
}

consultarciudades(ID: number) {
  const id: number = Number(ID);
  this.spinner.show();
  this.subRef$ =  this.inmuebleService.ConsultarCiudades(id).subscribe(resultado => {
    this.listaCiudades = resultado.body;
    this.spinner.hide();
  }, error => {
      console.warn(error._message);
      this.spinner.hide();
  });
}

consultarcolonias(ID: number) {
  this.spinner.show();
  this.subRef$ =  this.inmuebleService.ConsultarColonias(ID).subscribe(resultado => {
    this.listaColonias = resultado.body;
    this.spinner.hide();
  }, error => {
      console.warn(error._message);
      this.spinner.hide();
  });
}

  ngOnDestroy() {
  if (this.subRef$) { this.subRef$.unsubscribe(); }
  }

  verpaquetes(inmuebleID: number, nombreInmueble: string) {
    this.inmuebleID = inmuebleID;
    this.nombreInmueble = nombreInmueble;
    this.visualizarinmuebles = false;
    this.visualizarpaquetes = true;
    this.visualizarimagenes = false;
    this.visualizarseguimiento = false;
  }

  verimagenes(inmuebleID: number, nombreInmueble: string) {
    this.inmuebleID = inmuebleID;
    this.nombreInmueble = nombreInmueble;
    this.visualizarinmuebles = false;
    this.visualizarpaquetes = false;
    this.visualizarimagenes = true;
    this.visualizarseguimiento = false;
  }

  seleccioncolonia(coloniaID: number) {
    const id: number = Number(coloniaID);
    let coloniaDes: string;
    coloniaDes = '';
    if (id !== 0) {
      const colonia = this.listaColonias.find((i) => i.coloniaID === id);
      coloniaDes = colonia.nombreColonia;
    }
    this.inmuebleactivo.descripcionColonia = coloniaDes;
  }

  verseguimiento(inmuebleID: number, nombreInmueble: string) {
    this.inmuebleID = inmuebleID;
    this.nombreInmueble = nombreInmueble;
    this.visualizarinmuebles = false;
    this.visualizarpaquetes = false;
    this.visualizarimagenes = false;
    this.visualizarseguimiento = true;
  }

}
