import { Component, Inject, OnInit, OnDestroy, Input, EventEmitter, Output, ViewEncapsulation } from '@angular/core';
import { InmueblesService } from './cat-inmuebles.service';
import { Subscription } from 'rxjs';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import { MatCalendarCellCssClasses, MatDatepickerInputEvent } from '@angular/material/datepicker';
import { Reservacion } from '../modelos/Reservacion';
import { SecurityService } from '../services/security.service';
import { ReservacionesService } from '../reservaciones/reservaciones.service';
import { Router } from '@angular/router';
import { ToasterService } from "../Repositorios/toast.service";
import { NgxSpinnerService } from "ngx-spinner";


@Component({
    selector: 'app-inmuebles-seguimiento',
    templateUrl: './cat-inmuebles-seguimiento.component.html',
    styleUrls: ['./cat-inmuebles-seguimiento.component.css'],
    encapsulation: ViewEncapsulation.None,
})
export class InmueblesSeguimientoComponent implements OnInit, OnDestroy {

    @Input() inmuebleID: number;
    @Input() nombreInmueble: string;
    @Output() regresarPantalla = new EventEmitter<boolean>();
    mostrarcalificaciones: boolean;
    calificaciones: any;
    fechas: any;
    subRef$: Subscription;
    events: string[] = [];
    nombreSocioReservo: string;
    nombrePaqueteReservo: string;
    noReservacion: number;
    Id = 0;
    modo: number; // 1.- Ver 2.- Autoreservación
    fecha: Date;

    constructor(private inmuebleService: InmueblesService, private reservacionService: ReservacionesService, private secutiryService: SecurityService, private router: Router,public servicioToast: ToasterService,private spinner: NgxSpinnerService) {
    }

    ngOnInit(): void {
        console.log(this.inmuebleService);
        this.consultarcalificaciones();
        this.consultarfechasinmueble();
        this.limpiarinformacionreservacionseleccionada();
        this.Id = this.secutiryService.Id;
        this.modo = 1;
        this.fecha = undefined;
    }

    dateClass = (d: Date): MatCalendarCellCssClasses => {
        let clase : string;
        clase = '';
        var _existeEvento = this.fechas.filter(function (x) {
            return ( new Date(x.fechaReservacionInicia).getDate() === d.getDate() &&  new Date(x.fechaReservacionInicia).getMonth() === d.getMonth() && new Date(x.fechaReservacionInicia).getFullYear() === d.getFullYear());
        })[0];

        if(_existeEvento){
            if(_existeEvento.estatus === 1) clase = 'datePickerFechaNoDisponible';
            if(_existeEvento.estatus === 2) clase = 'datePickerFechaAutorizada';
            if(_existeEvento.estatus === 4) clase = 'datePickerFechaCalificada';
        }
        return clase;
    }

    consultarcalificaciones() {
        this.spinner.show();
        this.subRef$ =  this.inmuebleService.ConsultarCalificaciones(this.inmuebleID).subscribe(resultado => {
        this.calificaciones = resultado.body;
        this.calificaciones.forEach(calificacion => {
            calificacion.fecha = new Date(calificacion.fecha);
        });
        this.mostrarcalificaciones = (this.calificaciones.length > 0 ? true : false);
        this.spinner.hide();
        }, error => {
            console.warn(error._message);
            this.spinner.hide();
        });
    }

    consultarfechasinmueble() {
        this.spinner.show();
        this.subRef$ =  this.inmuebleService.ConsultarFechasInmuebles(this.inmuebleID).subscribe(resultado => {
        this.fechas = resultado.body;
        this.spinner.hide();
        }, error => {
            console.warn(error._message);
            this.spinner.hide();
        });
    }
    
    limpiarinformacionreservacionseleccionada(){
        this.nombreSocioReservo = '';
        this.nombrePaqueteReservo = '';
        this.noReservacion = 0;
    }

    crearAutoReservacion(fechaReservacion: Date){
        const data: Reservacion = {
            noReservacion: 0,
            socioID: Number(this.Id),
            socioNombre: this.nombreSocioReservo,
            socioEmail: '',
            inmuebleID: this.inmuebleID,
            inmuebleNombre: '',
            paqueteID: 0,
            paqueteNombre: this.nombrePaqueteReservo,
            fechaReservacionInicia: fechaReservacion,
            fechaReservacionTermina: fechaReservacion,
            costo:  0,
            estatus: 4
        }
        this.spinner.show();
        this.subRef$ =  this.reservacionService.AgregarReservacion(data)
        .subscribe(resultado => {
            if (resultado.body.id === 1) {
                this.servicioToast.Success("Busca Salón", "Se ha realizado el proceso exitosamente.");
                this.spinner.hide();
                this.modo = 1;
                this.fecha = undefined;
                this.limpiarinformacionreservacionseleccionada();
                this.consultarfechasinmueble();
            } else {
                this.spinner.hide();
                this.servicioToast.Warning("Busca Salón","resultado.body.mensajeError");
            }
        }, error => {
            this.spinner.hide();
            console.warn(error._message);
        });
    }

    regresar() {
        this.regresarPantalla.emit(true);
    }

    ngOnDestroy() {
        if (this.subRef$) { this.subRef$.unsubscribe(); }
    }

    addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
        this.limpiarinformacionreservacionseleccionada();
        const d = new Date(event.value);
        let clase : string;
        clase = '';
        var _existeEvento = this.fechas.filter(function (x) {
            return ( new Date(x.fechaReservacionInicia).getDate() === d.getDate() &&  new Date(x.fechaReservacionInicia).getMonth() === d.getMonth() && new Date(x.fechaReservacionInicia).getFullYear() === d.getFullYear());
        })[0];

        if(_existeEvento){
            this.nombreSocioReservo = _existeEvento.socioNombre;
            this.nombrePaqueteReservo = _existeEvento.paqueteNombre;
            this.noReservacion = _existeEvento.noReservacion;
            this.modo = 3;
        }else{
            if(confirm('¿Desea realizar una autoreservación para el día: ' + d.getDate() + '/' + d.getMonth() + '/' + d.getFullYear() )){
                this.fecha = d;
                this.modo = 2;
            }
        }
    }

    cancelar(){
        this.limpiarinformacionreservacionseleccionada();
        this.modo = 1;
    }

    autoreservar(){
        this.crearAutoReservacion(this.fecha);
    }

    eliminar(){
        const isOk = confirm('¿Esta seguro que desea eliminar la reservación?');
        if(isOk) {
            this.spinner.show();
            const data: Reservacion = {
                noReservacion: Number(this.noReservacion),
                socioID: 0,
                socioNombre: '',
                socioEmail: '',
                inmuebleID: 0,
                inmuebleNombre: '',
                paqueteID: 0,
                paqueteNombre: '',
                fechaReservacionInicia: undefined,
                fechaReservacionTermina: undefined,
                costo:  0,
                estatus: 4
            }
            this.reservacionService.EliminarReservacion(data)
                .subscribe(resultado => {
                    this.servicioToast.Success("Busca Salón", "Eliminación exitosa");
                    this.spinner.hide();
                    this.modo = 1;
                    this.fecha = undefined;
                    this.limpiarinformacionreservacionseleccionada();
                    this.consultarfechasinmueble();
                }, error => {
                    this.spinner.hide();
                console.warn(error._message);
            });
        }
    }
}