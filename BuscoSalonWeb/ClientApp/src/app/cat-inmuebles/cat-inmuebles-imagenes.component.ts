import { Component, Inject, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { InmueblesService } from './cat-inmuebles.service';
import { Subscription } from 'rxjs';
import { Imagen } from '../modelos/Imagen';
import { isNumeric } from 'rxjs/internal-compatibility';
import { ToasterService } from "../Repositorios/toast.service";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-inmuebles-imagenes',
  templateUrl: './cat-inmuebles-imagenes.component.html',
  styleUrls: ['./cat-inmuebles-imagenes.component.css']
})
export class InmueblesImagenesComponent implements OnInit, OnDestroy {

  @Input()inmuebleID: number;
  @Input() nombreInmueble: string;
  @Output() regresarPantalla = new EventEmitter<boolean>();
  subRef$: Subscription;
  imagen: Imagen;
  imagenes: Imagen[] = [];
  imagenesbd: Imagen[] = [];
  inicial: boolean = false;

  constructor(private inmuebleService: InmueblesService,public servicioToast: ToasterService,private spinner: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.imagen = null;
    this.consultarimagenes();
  }

  regresar() {
    this.regresarPantalla.emit(true);
  }

  cargarimagenes(event) {
    this.imagen = new Imagen;
    this.imagen.id = 0;
    this.imagen.src = event.src;
    this.imagen.nombre = event.file.name;
    this.imagen.estatus = 1;
    this.imagen.inmuebleID = Number(this.inmuebleID);
    this.imagenes.push(this.imagen);
  }

  quitarimagen(event) {
    this.imagen = new Imagen;
    this.imagen.src = event.src;
    this.imagen.nombre = event.file.name;
    this.imagenes = this.imagenes.filter((i) => i.src !== this.imagen.src);
  }

  guardarimagenes() {
    this.spinner.show();
    this.imagenes.forEach(element => {
      this.subRef$ =  this.inmuebleService.AgregarImagen(element)
      .subscribe(resultado => {
        if (resultado.body.id === 1) {
          this.imagenes = [];
          this.spinner.hide();
          this.consultarimagenes();
        }
      }, error => {
          console.warn(error._message);
          this.spinner.hide();
      });
    });
  }

  consultarimagenes() {
    this.spinner.show();
    this.subRef$ =  this.inmuebleService.ConsultarImagenes(this.inmuebleID).subscribe(resultado => {
      this.imagenesbd = resultado.body;
      this.spinner.hide();
    }, error => {
        console.warn(error._message);
        this.spinner.hide();
    });
  }

  eliminarimagen(id: number) {
    const isOk = confirm('¿Esta seguro que desea eliminar la imagen?');
    if(isOk) {
      this.spinner.show();
    this.subRef$ =  this.inmuebleService.EliminarImagen(id)
      .subscribe(resultado => {
        if (resultado.body.id === 1) {
          this.servicioToast.Success("Busca Salón", "Se ha eliminado la imagen de forma exitosa");
          this.spinner.hide();
          this.consultarimagenes();
        }
      }, error => {
        this.spinner.hide();
          console.warn(error._message);
      });
    }
  }

  ngOnDestroy() {
    if (this.subRef$) { this.subRef$.unsubscribe(); }
  }
}
