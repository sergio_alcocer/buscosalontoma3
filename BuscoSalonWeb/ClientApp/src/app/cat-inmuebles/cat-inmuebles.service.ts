import { Component, Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Inmueble } from '../modelos/Inmueble';
import { Resultado } from '../modelos/Resultado';
import { DataService } from '../services/data.service';
import { TipoAmbiente } from '../modelos/tipo_ambiente_model';
import { Estado } from '../modelos/Estado';
import { Ciudad } from '../modelos/Ciudad';
import { Colonia } from '../modelos/Colonia';
import { Paquete } from '../modelos/Paquete';
import { Imagen } from '../modelos/Imagen';
import { Reservacion } from '../modelos/Reservacion';
import { GlobalConstants } from '../constantes/global-constants';

@Injectable({
  providedIn: 'root',
})
export class InmueblesService {
  url = GlobalConstants.apiURL + "/api/inmuebles";

  constructor(private httpClient: HttpClient, private dataService: DataService) {
  }

  ConsultarInmuebles(filtroBusqueda: string, idSocio: number): Observable<any> {
    const path = `${this.url}/consultarinmuebles/${filtroBusqueda}/${idSocio}`;
    return this.dataService.get<Inmueble[]>(path);
  }

  ConsultarInmueblePorID(id: number) {
    const path = `${this.url}/consultarinmueble/${id}`;
    return this.dataService.get<Inmueble>(path);
  }

  AgregarInmueble(inmueble: Inmueble): Observable<any> {
    const data: Inmueble = {
      socioID: Number(inmueble.socioID),
      ciudadID: Number(inmueble.ciudadID),
      nombre: inmueble.nombre,
      direccion: inmueble.direccion,
      coloniaID: Number(inmueble.coloniaID),
      descripcionColonia: inmueble.descripcionColonia,
      referencias: inmueble.referencias,
      ambienteID:  Number(inmueble.ambienteID),
      capacidad:  Number(inmueble.capacidad),
      costo:  Number(inmueble.costo),
      horario: inmueble.horario,
      usuario: '',
      estatus: (inmueble.estatus ? 1 : 0),
      localizacion: inmueble.localizacion,
      telefono: inmueble.telefono,
      latitud: inmueble.latitud,
      longitud: inmueble.longitud,
      correoElectronico: inmueble.correoElectronico,
      whatsapp: inmueble.whatsapp,
      facebook: inmueble.facebook,
      sitioWeb: inmueble.sitioWeb
    };
    const path = `${this.url}/agregarinmueble`;
    return this.dataService.post<Resultado>(path, data);
  }

  ModificarInmueble(inmueble: Inmueble): Observable<any> {
    const data: Inmueble = {
      id: Number(inmueble.id),
      socioID: Number(inmueble.socioID),
      ciudadID: Number(inmueble.ciudadID),
      nombre: inmueble.nombre,
      direccion: inmueble.direccion,
      coloniaID: Number(inmueble.coloniaID),
      descripcionColonia: inmueble.descripcionColonia,
      referencias: inmueble.referencias,
      ambienteID:  Number(inmueble.ambienteID),
      capacidad:  Number(inmueble.capacidad),
      costo:  Number(inmueble.costo),
      horario: inmueble.horario,
      usuario: '',
      estatus: (inmueble.estatus ? 1 : 0),
      localizacion: inmueble.localizacion,
      telefono: inmueble.telefono,
      latitud: inmueble.latitud,
      longitud: inmueble.longitud,
      correoElectronico: inmueble.correoElectronico,
      whatsapp: inmueble.whatsapp,
      facebook: inmueble.facebook,
      sitioWeb: inmueble.sitioWeb
    };
    const path = `${this.url}/modificarinmueble`;
    return this.dataService.put<Resultado>(path, data);
  }

  EliminarInmueble(id: number): Observable<any> {
    const path = `${this.url}/eliminarinmueble/${id}`;
    return this.dataService.delete<Resultado>(path);
  }

  ConsultarColoniasPorCP(codigoPostal: string): Observable<any> {
    const path = `${this.url}/consultarcoloniasporcp/${codigoPostal}`;
    return this.dataService.get<Inmueble[]>(path);
  }

  ConsultarTiposAmbientes(): Observable<any> {
    const path = `${this.url}/consultartiposambientes`;
    return this.dataService.get<TipoAmbiente[]>(path);
  }

  ConsultarEstados(): Observable<any> {
    const path = `${this.url}/consultarestadosporpais/1`;
    return this.dataService.get<Estado[]>(path);
  }

  ConsultarCiudades(id: number): Observable<any> {
    const path = `${this.url}/consultarciudadesporestado/${id}`;
    return this.dataService.get<Ciudad[]>(path);
  }

  ConsultarColonias(id: number): Observable<any> {
    const path = `${this.url}/consultarcoloniasporciudad/${id}`;
    return this.dataService.get<Colonia[]>(path);
  }

  ConsultarPaquetes(idInmueble: number): Observable<any> {
    const path = `${this.url}/consultarpaquetes/${idInmueble}`;
    return this.dataService.get<Paquete[]>(path);
  }

  ConsultarPaquetePorID(id: number) {
    const path = `${this.url}/consultarpaquete/${id}`;
    return this.dataService.get<Paquete>(path);
  }

  AgregarPaquete(paquete: Paquete): Observable<any> {
    const data: Paquete = {
      inmuebleID: Number(paquete.inmuebleID),
      nombre: paquete.nombre,
      costo: Number(paquete.costo),
      incluyeMesas: Number(paquete.incluyeMesas),
      incluyeMeseros: Number(paquete.incluyeMeseros),
      incluyeManteleria: Number(paquete.incluyeManteleria),
      otrosDatos: paquete.otrosDatos,
      estatus: 1
    };
    const path = `${this.url}/agregarpaquete`;
    return this.dataService.post<Resultado>(path, data);
  }

  ModificarPaquete(paquete: Paquete): Observable<any> {
    const data: Paquete = {
      id: Number(paquete.id),
      inmuebleID: Number(paquete.inmuebleID),
      nombre: paquete.nombre,
      costo: Number(paquete.costo),
      incluyeMesas: Number(paquete.incluyeMesas),
      incluyeMeseros: Number(paquete.incluyeMeseros),
      incluyeManteleria: Number(paquete.incluyeManteleria),
      otrosDatos: paquete.otrosDatos,
      estatus: 1
    };
    const path = `${this.url}/modificarpaquete`;
    return this.dataService.put<Resultado>(path, data);
  }

  EliminarPaquete(id: number): Observable<any> {
    const path = `${this.url}/eliminarpaquete/${id}`;
    return this.dataService.delete<Resultado>(path);
  }

  AgregarImagen(imagen: Imagen): Observable<any> {
    const data: Imagen = {
      id: Number(imagen.id),
      inmuebleID: Number(imagen.inmuebleID),
      nombre: imagen.nombre,
      src: imagen.src,
      estatus: Number(imagen.estatus)
    };
    const path = `${this.url}/agregarimagen`;
    return this.dataService.post<Resultado>(path, data);
  }

  EliminarImagen(id: number): Observable<any> {
    const path = `${this.url}/eliminarimagen/${id}`;
    return this.dataService.delete<Resultado>(path);
  }

  ConsultarImagenes(idInmueble: number): Observable<any> {
    const path = `${this.url}/consultarimagenes/${idInmueble}`;
    return this.dataService.get<Imagen[]>(path);
  }

  ConsultarCalificaciones(idInmueble: number): Observable<any> {
    const path = `${this.url}/consultarcalificaciones/${idInmueble}`;
    return this.dataService.get<Paquete[]>(path);
  }

  ConsultarFechasInmuebles(idInmueble: number): Observable<any> {
    const path = `${this.url}/consultarfechasinmueble/${idInmueble}`;
    return this.dataService.get<Reservacion[]>(path);
  }

  ValidarAgregarInmueble(idSocio: number): Observable<any> {
    const path = `${this.url}/validarnuevoinmuebleporidsocio/${idSocio}`;
    return this.dataService.get<Resultado>(path);
  }

  ConsultarImagenMinuatura(idInmueble: number): Observable<any> {
    const path = `${this.url}/consultarimagenminuatura/${idInmueble}`;
    return this.dataService.get<Imagen>(path);
  }

}