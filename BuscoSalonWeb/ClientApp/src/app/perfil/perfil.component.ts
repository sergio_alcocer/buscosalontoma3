import { Component, Inject, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { PerfilService } from './perfil.service';
import { Usuario } from '../modelos/Usuario';
import { Subscription } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ErrorStateMatcher1 } from '../error-state-matcher1';
import { SecurityService } from '../services/security.service';
import { Socio } from '../modelos/Socio';
import { ToasterService } from "../Repositorios/toast.service";
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from '@angular/router';


interface Tipos {
  value: number;
  viewValue: string;
}
@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})

export class PerfilComponent implements OnInit, OnDestroy {
  usuarios: any;
  formularioPerfil: FormGroup;
  matcher = new ErrorStateMatcher1();
  usuarioactivo: Socio;
  estadoformulario: number;
  mostrarProcesoExitoso: boolean;
  msjProcesoExitoso: string;
  mostrarAlertaValidacion: boolean;
  mostrarError: boolean;
  msjError: string;
  mostrarEliminarUsuario: boolean;
  filtroBusqueda: string;
  msjAlertaValidacion: string;
  selectedValue: string;
  EsSocio: boolean;
  breakpoint:number;

  subRef$: Subscription;
  confirmationDialogService: any;

  constructor(private usuarioService: PerfilService, private formBuilder: FormBuilder, private Sesion: SecurityService, public servicioToast: ToasterService, private spinner: NgxSpinnerService,private router: Router) {
    this.limpiarMensajes();
    this.limpiarUsuario();
    this.estadoformulario = 1;
    this.filtroBusqueda = '';
    this.msjAlertaValidacion = '';
    this.EsSocio=false;
  }

  tipos: Tipos[] = [
    { value: 1, viewValue: 'Administrador' },
    { value: 2, viewValue: 'Coordinador' }
  ];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit(): void {
    this.consultarusuarioporid(this.Sesion.Id, this.Sesion.Role);
    this.breakpoint = (window.innerWidth <= 600) ? 1 : 2;
    this.formularioPerfil = this.formBuilder.group({
      nick: ['', [Validators.required, Validators.minLength(4)], this.usuarioService.ValidaNickname.bind(this.usuarioService)
      ],
      nombre: ['', [Validators.required, Validators.minLength(4)]],
      pass: ['', [Validators.required, Validators.minLength(4)]],
      email: ['', Validators.required, this.usuarioService.ValidaEmail.bind(this.usuarioService)],
      paterno: ['', [Validators.required, Validators.minLength(4)]],
      materno: [''],
      estatus: [''],
      clabe: [''],
      NoSalones: [''],
      noReferencia: [''],
      whatsApp: ['']
    }
    );
  }

  consultarusuarioporid(id: number, rol: number) {
    this.limpiarMensajes();
    this.limpiarUsuario();
    this.spinner.show();
    if (rol < 3) {
      this.subRef$ = this.usuarioService.ConsultarUsuarioPorID(id).subscribe(resultado => {
        this.usuarioactivo.id = resultado.body.id;
        this.usuarioactivo.tipo = resultado.body.tipo;
        this.usuarioactivo.nick = resultado.body.nick;
        this.usuarioactivo.password = resultado.body.password;
        this.usuarioactivo.nombre = resultado.body.nombre;
        this.usuarioactivo.paterno = '';
        this.usuarioactivo.materno = '';
        this.usuarioactivo.sexo = 0;
        this.usuarioactivo.email = resultado.body.email;
        this.usuarioactivo.acepta_Pago_Tarjeta = 0;
        this.usuarioactivo.cuenta_Clabe = '';
        this.usuarioactivo.numero_lugares = 0;
        this.usuarioactivo.fecha_alta = undefined;
        this.usuarioactivo.estatus = resultado.body.estatus;
        this.usuarioactivo.token = '';
        this.usuarioactivo.noReferencia = '';
        this.usuarioactivo.noSalones = 0;
        this.estadoformulario = 3;
        this.EsSocio = false;
        this.spinner.hide();
      }, error => {
        this.spinner.hide();
        console.log(error._message);
      });
    }
    else {
      this.subRef$ = this.usuarioService.ConsultarSocioPorID(id).subscribe(resultado => {
        this.usuarioactivo = resultado.body;
        this.estadoformulario = 3;
        this.EsSocio = true;
        this.spinner.hide();
      }, error => {
        console.log(error._message);
        this.spinner.hide();
      });
    }
  }

  limpiarUsuario() {
    this.usuarioactivo = {
      id: 0 as number,
      tipo: 0 as number,
      nick: '' as string,
      password: '' as string,
      nombre: '' as string,
      paterno: '' as string,
      materno: '' as string,
      sexo: 0 as number,
      email: '' as string,
      acepta_Pago_Tarjeta: 0 as number,
      cuenta_Clabe: '' as string,
      numero_lugares: 0 as number,
      fecha_alta: undefined as Date,
      estatus: 0 as number,
      token: '' as string,
      noReferencia: '' as string,
      noSalones: 0 as number,
      whatsApp: '' as string
    };
  }

  limpiarMensajes() {
    this.mostrarAlertaValidacion = false;
    this.mostrarProcesoExitoso = false;
    this.msjProcesoExitoso = '';
    this.mostrarError = false;
    this.msjError = '';
    this.mostrarEliminarUsuario = false;
  }

  ModificarPerfil() {
    this.spinner.show();
    this.limpiarMensajes();
    if(this.Sesion.Role< 3) {
      this.subRef$ = this.usuarioService.ModificarUsuarioPerfil(this.usuarioactivo)
        .subscribe(resultado => {
          if (resultado.body.id === 1) {
            this.servicioToast.Success("Busca Salón", "Su perfil ha sido actualizado!.");
            this.spinner.hide();
            this.router.navigate(['/perfil']);
          } else {
            this.spinner.hide();
            this.servicioToast.Warning("Busca Salón", resultado.body.mensajeError);
          }
        }, error => {
          this.spinner.hide();
          console.warn(error._message);
        });
      }
      else{
        this.subRef$ = this.usuarioService.ModificarSocioPerfil(this.usuarioactivo)
        .subscribe(resultado => {
          if (resultado.body.id === 1) {
            this.servicioToast.Success("Busca Salón", "Su perfil ha sido actualizado!.");
            this.spinner.hide();
            this.router.navigate(['/perfil']);
          } else {
            this.spinner.hide();
            this.servicioToast.Warning("Busca Salón", resultado.body.mensajeError);
          }
        }, error => {
          this.spinner.hide();
          console.warn(error._message);
        });
      }
  }

  hasError(nombreControl: string, validacion: string) {    
    const control = this.formularioPerfil.get(nombreControl);
    return control.hasError(validacion);
  }
  ngOnDestroy() {
    if (this.subRef$) { this.subRef$.unsubscribe(); }
  }
  onResize(event) {
    this.breakpoint = (event.target.innerWidth <= 600) ? 1 : 2;
  }
}
