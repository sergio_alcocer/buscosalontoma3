import { Component, Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Usuario } from '../modelos/Usuario';
import { Resultado } from '../modelos/Resultado';
import { DataService } from '../services/data.service';
import { map } from "rxjs/operators";
import { FormGroup, AbstractControl } from '@angular/forms';
import { GlobalConstants } from '../constantes/global-constants';
import { Socio } from '../modelos/Socio';
import { SecurityService } from '../services/security.service';

@Injectable({
  providedIn: 'root',
})
export class PerfilService {
  urlUsuarios = GlobalConstants.apiURL + "/api/usuarios";
  urlSocios = GlobalConstants.apiURL + "/api/Socios";

  constructor(private httpClient: HttpClient, private dataService: DataService, private Sesion: SecurityService) {
  }

  ConsultarUsuarioPorID(id: number) {
    const path = `${this.urlUsuarios}/consultarusuario/${id}`;
    return this.dataService.get<Usuario>(path);
  }
  ConsultarSocioPorID(id: number) {
    const path = `${this.urlSocios}/consultarsocio/${id}`;
    return this.dataService.get<Socio>(path);
  }


  ModificarUsuarioPerfil(usuario: Usuario): Observable<any> {
    const data: Usuario = {
      id: usuario.id,
      nick: usuario.nick,
      password: '',
      nombre: usuario.nombre,
      email: usuario.email,
      tipo: Number(usuario.tipo),
      estatus: (usuario.estatus ? 1 : 0)
    }
    const path = `${this.urlUsuarios}/modificarusuarioperfil`;
    return this.dataService.put<Resultado>(path, data);
  }

  ModificarSocioPerfil(socio: Socio): Observable<any> {
    const data: Socio = {
      id: socio.id,
      nick: socio.nick,
      password: '',
      nombre: socio.nombre,
      paterno: socio.paterno,
      materno: socio.materno,
      email: socio.email,
      cuenta_Clabe: socio.cuenta_Clabe,
      noSalones: socio.noSalones,
      whatsApp: socio.whatsApp
    }
    const path = `${this.urlSocios}/modificarsocioperfil`;
    return this.dataService.put<Resultado>(path, data);
  }

  ValidaNickname(control: AbstractControl) {
    return this.ExisteNickName(control.value).pipe(
      map(res => {
        return res ? null : { nickExist: true };
      })
    );
  }

  ValidaEmail(control: AbstractControl) {
    return this.ExisteEmail(control.value).pipe(
      map(res => {
        return res ? null : { emailExist: true };
      })
    );
  }

  //Fake API call -- You can have this in another service
  ExisteNickName(username: string): Observable<boolean> {
    if (this.Sesion.Role < 3) {
      const path = `${this.urlUsuarios}/existenicknameusuario/${username}/${this.Sesion.Id}`;
      return this.httpClient.get(path).pipe(
        map((usernameList: Array<any>) =>
          usernameList.filter(user => user.nick === username)
        ),
        map(users => !users.length)
      );
    }
    else {
      const path = `${this.urlSocios}/existenicknamesocio/${username}/${this.Sesion.Id}`;
      return this.httpClient.get(path).pipe(
        map((usernameList: Array<any>) =>
          usernameList.filter(user => user.nick === username)
        ),
        map(users => !users.length)
      );
    }
  }

  ExisteEmail(email: string): Observable<boolean> {
    if (this.Sesion.Role < 3) {
      const path = `${this.urlUsuarios}/existeemailusuario/${email}/${this.Sesion.Id}`;
      return this.httpClient.get(path).pipe(
        map((usernameList: Array<any>) =>
          usernameList.filter(user => user.email === email)
        ),
        map(users => !users.length)
      );
    }
    else{
      const path = `${this.urlSocios}/existeemailsocio/${email}/${this.Sesion.Id}`;
      return this.httpClient.get(path).pipe(
        map((usernameList: Array<any>) =>
          usernameList.filter(user => user.email === email)
        ),
        map(users => !users.length)
      );
    }
  }
}
  