﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NucleoBuscoSalon.Models
{
    [Table("Ciudades")]
    public class Ciudades
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "bigint")]
        public int Ciudad_ID { get; set; }
        [Column(TypeName = "bigint")]
        public int Estado_ID { get; set; }
        public  string Ciudad_Nombre { get; set; }
        public int Ciudad_Estatus { get; set; }

        [ForeignKey(nameof(Estado_ID))]
        public Estados Estados { get; set; }
    }
}
