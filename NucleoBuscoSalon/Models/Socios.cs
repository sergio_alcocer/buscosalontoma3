﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NucleoBuscoSalon.Models
{
    [Table("Socios")]
    public class Socios
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "bigint")]
        public int Socio_ID { get; set; }

        [ForeignKey("Tipo_ID")]
        public int Tipos_Socio_ID { get; set; }
        public string Socio_Nombre { get; set; }
        public string Socio_Paterno { get; set; }
        public string Socio_Materno { get; set; }
        public int Socio_Estatus { get; set; }
        public string Socio_WhatsApp { get; set; }

        [ForeignKey(nameof(Tipos_Socio_ID))]
        public Tipos_Socio Tipos_Socio { get; set; }
    }
}
