﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace NucleoBuscoSalon.Models
{
    [Table("Colonias")]
    public class Colonias
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "bigint")]
        public int Colonia_ID { get; set; }
        [Column(TypeName = "bigint")]
        public int Ciudad_ID { get; set; }
        public string Tipo { get; set; }
        public string Nombre { get; set; }
        public string Codigo_Postal { get; set; }
        public int Estatus { get; set; }

        [ForeignKey(nameof(Ciudad_ID))]
        public Ciudades Ciudades { get; set; }
    }
}
