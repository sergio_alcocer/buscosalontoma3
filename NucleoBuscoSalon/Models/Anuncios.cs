﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NucleoBuscoSalon.Models
{
    [Table("Anuncios")]
    public   class Anuncios
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "bigint")]
        public int Anuncio_ID { get; set; }
        [Column(TypeName = "bigint")]
        public int Socio_ID { get; set; }
        public string Url { get; set; }
        public string Archivo { get; set; }
        public DateTime Fecha_Inicio { get; set; }
        public DateTime Fecha_Final { get; set; }
        public int Estatus { get; set; }
        public int Dias { get; set; }
        public Decimal Total_Costo { get; set; }

        [ForeignKey(nameof(Socio_ID))]
        public Socios Socios { get; set; }
    }
}
