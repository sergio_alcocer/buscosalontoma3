﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NucleoBuscoSalon.Models
{
    [Table("Paises")]
    public class Paises
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "bigint")]
        public int Pais_ID { get; set; }
        public string Pais_Nombre { get; set; }
        public int Pais_Estatus { get; set; }
    }
}
