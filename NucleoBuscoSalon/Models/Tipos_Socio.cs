﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NucleoBuscoSalon.Models
{
    [Table("Tipos_Socio")]
    public class Tipos_Socio 
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int   Tipo_ID { get; set; }
        public string Tipo_Nombre { get; set; }
        public int Tipo_Estatus { get; set; }
    }
}
