﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NucleoBuscoSalon.Models
{
    [Table("Estados")]
    public class Estados
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(TypeName = "bigint")]
        public int Estado_ID { get; set; }
        [Column(TypeName = "bigint")]
        public int Pais_ID { get; set; }
        public string Estado_Nombre { get; set; }
        public int Estado_Estatus { get; set; }

    }
}
