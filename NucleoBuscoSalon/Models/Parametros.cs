﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NucleoBuscoSalon.Models
{
    [Table("Parametros")]
    public class Parametros
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Parametro_ID { get; set; }
        public  string Url_Imagen { get; set; }
        public decimal Precio { get; set; }
        public string Correos_CC { get; set; }
        public string MensajeReservacion { get; set; }
    }
}
