﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Microsoft.EntityFrameworkCore.ValueGeneration.Internal;

namespace NucleoBuscoSalon.Models
{
  public  class DataContex : DbContext 
    {

        public DataContex(DbContextOptions<DataContex> opts)
             : base(opts) { }

        public DbSet<Tipos_Ambientes> Tipos_Ambientes { get; set; }
        public DbSet<Colonias> Colonias { get; set; }
        public DbSet<Ciudades> Ciudades { get; set; }
        public DbSet<Estados> Estados { get; set; }
        public DbSet<Paises> Paises { get; set; }
        public DbSet<Socios> Socios { get; set; }
        public DbSet<Tipos_Socio> Tipos_Socio { get; set; }
        public DbSet<Parametros> Parametros { get; set; }
        public DbSet<Anuncios> Anuncios { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var converter = new ValueConverter<int, long>(
                  v => v,
                  v => (int)v,
                  new ConverterMappingHints(valueGeneratorFactory: (p, t) => new TemporaryIntValueGenerator())
                );

            modelBuilder.Entity<Colonias>()
                .Property(p => p.Colonia_ID)
                .HasConversion(converter);

            modelBuilder.Entity<Ciudades>()
                .Property(p => p.Ciudad_ID)
                .HasConversion(converter);

            modelBuilder.Entity<Socios>()
                .Property(p => p.Socio_ID)
                .HasConversion(converter);

            modelBuilder.Entity<Anuncios>()
                .Property(p => p.Anuncio_ID)
                .HasConversion(converter);

          
                
        }

    }
}
