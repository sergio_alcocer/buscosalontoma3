﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace NucleoBuscoSalon.Models
{
    [Table("Tipos_Ambientes")]
    public class Tipos_Ambientes
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Ambiente_ID { get; set; }
        public string Nombre { get; set; }
        public int Estatus { get; set; }
    }
}
