﻿using NucleoBuscoSalon.Contratos;
using NucleoBuscoSalon.Models;
using NucleoBuscoSalon.Tareas;
using NucleoBuscoSalon.Utilerias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;

namespace NucleoBuscoSalon.Implementaciones
{
    public class Implementaciones : IUsuarios, ISocio, IInmuebles, IReservaciones, IParametros
    {
        #region Usuarios

        public Usuario Login(string Nick, string Password)
        {
            TareasUsuarios task = null;
            Usuario usuarios = new Usuario();
            try
            {
                task = new TareasUsuarios();
                usuarios = task.Login(Nick, Password);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se puede loggear el usuario. Expeción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return usuarios;
        }

        public Resultado CrearUsuario(Usuario p_usuario)
        {
            TareasUsuarios task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasUsuarios();
                respuesta = task.CrearUsuario(p_usuario);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden crear el usuario. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public Resultado ModificarUsuario(Usuario p_usuario)
        {
            TareasUsuarios task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasUsuarios();
                respuesta = task.ModificarUsuario(p_usuario);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden modificar el usuario. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public Resultado ModificarUsuarioPerfil(Usuario p_usuario)
        {
            TareasUsuarios task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasUsuarios();
                respuesta = task.ModificarUsuarioPerfil(p_usuario);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden modificar el usuario. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }


        

        public Usuario ConsultarUsuarioPorID(long idUsuario)
        {
            TareasUsuarios task = null;
            Usuario usuarios = new Usuario();
            try
            {
                task = new TareasUsuarios();
                usuarios = task.ConsultarUsuarioPorID(idUsuario);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar el usuario. v: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return usuarios;
        }

        public List<Usuario> existenickname(string NickName)
        {
            TareasUsuarios task = null;
            List<Usuario> usuarios = new List<Usuario>();
            try
            {
                task = new TareasUsuarios();
                usuarios = task.ConsultaNickEmail(NickName, "");
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar el usuario. v: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return usuarios;
        }

        public List<Usuario> existenicknameusuario(Usuario p_usuario)
        {
            TareasUsuarios task = null;
            List<Usuario> usuarios = new List<Usuario>();
            try
            {
                task = new TareasUsuarios();
                usuarios = task.ConsultaNickEmailUsuario(p_usuario);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar el usuario. v: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return usuarios;
        }
        public List<Socio> existenicknamesocio(Socio p_usuario)
        {
            TareasSocios task = null;
            List<Socio> usuarios = new List<Socio>();
            try
            {
                task = new TareasSocios();
                usuarios = task.ConsultaNickEmailSocio(p_usuario);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar el usuario. v: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return usuarios;
        }

        public List<Usuario> existeemail(string Email)
        {
            TareasUsuarios task = null;
            List<Usuario> usuarios = new List<Usuario>();
            try
            {
                task = new TareasUsuarios();
                usuarios = task.ConsultaNickEmail("", Email);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar el usuario. v: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return usuarios;
        }
        public string consultaremail(string Email)
        {
            TareasUsuarios task = null;
            string email;
            try
            {
                task = new TareasUsuarios();
                email = task.ConsultaNickEmail("", Email).FirstOrDefault().Email;
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar el usuario. v: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return email;
        }

        public List<Usuario> ConsultarUsuarios(string filtroBusqueda)
        {
            TareasUsuarios task = null;
            List<Usuario> usuarios = new List<Usuario>();
            try
            {
                task = new TareasUsuarios();
                usuarios = task.ConsultarUsuarios(filtroBusqueda);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar los usuarios. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return usuarios;
        }

        public Resultado EliminarUsuario(long idUsuario)
        {
            TareasUsuarios task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasUsuarios();
                respuesta = task.EliminarUsuario(idUsuario);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden eliminar el usuario. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public Resultado RestablecerPasswordUsuario(long idUsuario)
        {
            TareasUsuarios task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasUsuarios();
                respuesta = task.RestablecerPassword(idUsuario);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se puede reestablecer la contraseña del usuario. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public Resultado ActualizaPasswordUsuario(ActualizaPassword actualiza)
        {
            TareasUsuarios task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasUsuarios();
                respuesta = task.ActualizaPassword(actualiza);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se puede actualizar la contraseña del usuario. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public Resultado CodigoRecuperacion(string Email)
        {
            TareasUsuarios task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasUsuarios();
                respuesta = task.CodigoRecuperacion(Email);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se puedo enviar el codigo de recuperacion. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public Resultado actualizapasspublico(Usuario p_usuario)
        {
            TareasUsuarios task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasUsuarios();
                respuesta = task.actualizapasspublico(p_usuario);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden crear el usuario. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        #endregion Usuarios

        #region Socios

        public Resultado CrearSocio(Socio p_socio)
        {
            TareasSocios task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasSocios();
                respuesta = task.CrearSocio(p_socio);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden crear el socio. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public Resultado ModificarSocio(Socio p_socio)
        {
            TareasSocios task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasSocios();
                respuesta = task.ModificarSocio(p_socio);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden modificar el socio. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }
        public Resultado ModificarSocioPerfil(Socio p_socio)
        {
            TareasSocios task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasSocios();
                respuesta = task.ModificarSocioPerfil(p_socio);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden modificar el perfil del socio. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }


        
        public Resultado ActivarSocio(Socio p_socio)
        {
            TareasSocios task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasSocios();
                respuesta = task.ActivarSocio(p_socio);
                if (respuesta.ID > 0)
                {
                    EnvioCorreos.CorreoActivacion(p_socio.Email, p_socio.Nombre + " " + p_socio.Paterno + " " + p_socio.Materno, p_socio.NoReferencia);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden modificar el socio. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public Socio ConsultarSocioPorID(long idSocio)
        {
            TareasSocios task = null;
            Socio socio = new Socio();
            try
            {
                task = new TareasSocios();
                socio = task.ConsultarSocioPorID(idSocio);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar el socio. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return socio;
        }

        public List<Socio> ConsultarSocios(string filtroBusqueda, int tipo)
        {
            TareasSocios task = null;
            List<Socio> socios = new List<Socio>();
            try
            {
                task = new TareasSocios();
                socios = task.ConsultarSocios(filtroBusqueda, tipo);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar los socios. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return socios;
        }
        public List<Socio> ConsultarSociosSinActivar(string filtroBusqueda, int tipo)
        {
            TareasSocios task = null;
            List<Socio> socios = new List<Socio>();
            try
            {
                task = new TareasSocios();
                socios = task.ConsultarSociosSinActivar(filtroBusqueda, tipo);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar los socios. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return socios;
        }

        public Resultado EliminarSocio(long idSocio)
        {
            TareasSocios task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasSocios();
                respuesta = task.EliminarSocio(idSocio);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden eliminar el socio. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public Resultado RestablecerPasswordSocio(long idSocio)
        {
            TareasSocios task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasSocios();
                respuesta = task.RestablecerPassword(idSocio);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se puede reestablcer la contraseña del socio. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }
        public Resultado ActualizaPasswordSocio(ActualizaPassword actualiza)
        {
            TareasSocios task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasSocios();
                respuesta = task.ActualizaPassword(actualiza);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se puede actualizar la contraseña del usuario. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }


        public List<Socio> consultarnicknameSocio(string NickName)
        {
            return null;
        }

        #endregion Socios

        #region Ambos Socios Usuario
        public Resultado ActualizaPasswordToken(Recuperacion p_recupera)
        {
            TareasSocios task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasSocios();
                respuesta = task.ActualizaPasswordToken(p_recupera);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se puede reestablcer la contraseña del socio. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }
        #endregion

        #region Inmuebles

        public Resultado CrearInmueble(Inmueble p_inmueble)
        {
            TareasInmuebles task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasInmuebles();
                respuesta = task.CrearInmueble(p_inmueble);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden crear el inmueble. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public Resultado ModificarInmueble(Inmueble p_inmueble)
        {
            TareasInmuebles task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasInmuebles();
                respuesta = task.ModificarInmueble(p_inmueble);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden modificar el inmueble. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public Inmueble ConsultarInmueblePorID(long idInmueble)
        {
            TareasInmuebles task = null;
            Inmueble inmueble = new Inmueble();
            try
            {
                task = new TareasInmuebles();
                inmueble = task.ConsultarInmueblePorID(idInmueble);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar el inmueble. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return inmueble;
        }

        public List<Inmueble> ConsultarInmuebles(string filtroBusqueda, long idSocio)
        {
            TareasInmuebles task = null;
            List<Inmueble> inmuebles = new List<Inmueble>();
            try
            {
                task = new TareasInmuebles();
                inmuebles = task.ConsultarInmuebles(filtroBusqueda, idSocio);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar los inmuebles. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return inmuebles;
        }

        public Resultado EliminarInmueble(long idInmueble)
        {
            TareasInmuebles task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasInmuebles();
                respuesta = task.EliminarInmueble(idInmueble);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden eliminar el inmueble. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public Resultado ValidarNuevoInmueblePorIDSocio(long idSocio)
        {
            TareasInmuebles task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasInmuebles();
                respuesta = task.ValidarNuevoInmueblePorIDSocio(idSocio);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar el total de  inmuebles permitidos. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        #endregion Inmuebles

        #region Catalogos

        public List<Colonia> ConsultarColoniasPorCP(string codigopostal)
        {
            TareasInmuebles task = null;
            List<Colonia> respuesta = new List<Colonia>();
            try
            {
                task = new TareasInmuebles();
                respuesta = task.ConsultarColoniasPorCP(codigopostal);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar los tipos de colonias. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public List<Ambiente> ConsultarTiposAmbientes()
        {
            TareasInmuebles task = null;
            List<Ambiente> respuesta = new List<Ambiente>();
            try
            {
                task = new TareasInmuebles();
                respuesta = task.ConsultarTiposAmbientes();
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar los tipos ambientes. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public List<Estado> ConsultarEstadosPorPais(long ID)
        {
            TareasInmuebles task = null;
            List<Estado> respuesta = new List<Estado>();
            try
            {
                task = new TareasInmuebles();
                respuesta = task.ConsultarEstadosPorPais(ID);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar los estados. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public List<Ciudad> ConsultarCiudadesPorEstado(long ID)
        {
            TareasInmuebles task = null;
            List<Ciudad> respuesta = new List<Ciudad>();
            try
            {
                task = new TareasInmuebles();
                respuesta = task.ConsultarCiudadesPorEstado(ID);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar los estados. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public List<Colonia> ConsultarColoniasPorCiudad(long ID)
        {
            TareasInmuebles task = null;
            List<Colonia> respuesta = new List<Colonia>();
            try
            {
                task = new TareasInmuebles();
                respuesta = task.ConsultarColoniasPorCiudad(ID);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar los estados. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        #endregion Catalogos

        #region Buscador

        public List<Inmueble> BuscarInmuebles(Parametro_Busqueda parametro)
        {
            TareasInmuebles task = null;
            List<Inmueble> inmuebles = new List<Inmueble>();
            try
            {
                task = new TareasInmuebles();
                inmuebles = task.BuscarInmuebles(parametro);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar los inmuebles. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return inmuebles;
        }

        #endregion Buscador

        #region Paquetes

        public Resultado CrearPaquete(Paquete p_Paquete)
        {
            TareasInmuebles task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasInmuebles();
                respuesta = task.CrearPaquete(p_Paquete);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden crear el paquete. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public Resultado ModificarPaquete(Paquete p_Paquete)
        {
            TareasInmuebles task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasInmuebles();
                respuesta = task.ModificarPaquete(p_Paquete);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden modificar el paquete. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public Paquete ConsultarPaquetePorID(long idPaquete)
        {
            TareasInmuebles task = null;
            Paquete paquete = new Paquete();
            try
            {
                task = new TareasInmuebles();
                paquete = task.ConsultarPaquetePorID(idPaquete);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar el paquete. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return paquete;
        }

        public List<Paquete> ConsultarPaquetes(long idPaquete)
        {
            TareasInmuebles task = null;
            List<Paquete> paquetes = new List<Paquete>();
            try
            {
                task = new TareasInmuebles();
                paquetes = task.ConsultarPaquetes(idPaquete);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar los paquetes. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return paquetes;
        }

        public Resultado EliminarPaquete(long idPaquete)
        {
            TareasInmuebles task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasInmuebles();
                respuesta = task.EliminarPaquete(idPaquete);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden eliminar el paquete. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        #endregion Paquetes

        #region Imagenes

        public Resultado CrearImagen(Imagen p_imagen)
        {
            TareasInmuebles task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasInmuebles();
                respuesta = task.CrearImagen(p_imagen);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden crear la imagen. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public List<Imagen> ConsultarImagenes(long idInmueble)
        {
            TareasInmuebles task = null;
            List<Imagen> imagenes = new List<Imagen>();
            try
            {
                task = new TareasInmuebles();
                imagenes = task.ConsultarImagenes(idInmueble);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar las imagenes. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return imagenes;
        }

        public Imagen ConsultarImagenMiniatura(long idInmueble)
        {
            TareasInmuebles task = null;
            Imagen imagen = new Imagen();
            try
            {
                task = new TareasInmuebles();
                imagen = task.ObtenerImagenParaMiniatura(idInmueble);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar la imagen. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return imagen;
        }

        public Resultado EliminarImagen(long idImagen)
        {
            TareasInmuebles task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasInmuebles();
                respuesta = task.EliminarImagen(idImagen);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden eliminar la imagen. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        #endregion Imagenes

        #region Reservaciones

        public Resultado CrearReservacion(Reservacion p_reservacion)
        {
            TareasReservaciones task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasReservaciones();
                respuesta = task.CrearReservacion(p_reservacion);
                if (respuesta.ID > 0)
                {
                    TareasInmuebles taskMail = new TareasInmuebles();
                    var Resultado = taskMail.ConsultarInmueblePorID(p_reservacion.InmuebleID);
                    EnvioCorreos.CorreoReservacionSocio(Resultado.Socio.Email, Resultado.Socio.Nombre, Resultado.Nombre, Resultado.Direccion + ", " + Resultado.DescripcionColonia + " en " + Resultado.DescripcionCiudad + ", " + Resultado.DescripcionEstado);

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden crear la reservación. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public Resultado AutorizarReservacion(long idReservacion)
        {
            TareasReservaciones task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasReservaciones();
                respuesta = task.AutorizarReservacion(idReservacion);
                if (respuesta.ID > 0)
                {
                    task = new TareasReservaciones();
                    var resultado = task.ConsultarReservaciones(idReservacion, 4).FirstOrDefault();
                    EnvioCorreos.CorreoAutorizacionReservacion(resultado.SocioEmail, resultado.SocioNombre, resultado.InmuebleNombre, resultado.FechaReservacionInicia.ToString("dd/MM/yyyy"));
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden autorizar la reservación. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public Resultado CancelarReservacion(long idReservacion)
        {
            TareasReservaciones task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasReservaciones();
                respuesta = task.CancelarReservacion(idReservacion);
                if (respuesta.ID > 0)
                {
                    task = new TareasReservaciones();
                    var resultado = task.ConsultarReservaciones(idReservacion, 4).FirstOrDefault();
                    EnvioCorreos.CorreoCancelacionReservacion(resultado.SocioEmail, resultado.SocioNombre, resultado.InmuebleNombre, resultado.FechaReservacionInicia.ToString("dd/MM/yyyy"));
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden cancelar la reservación. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public Resultado EliminarReservacion(long noReservacion)
        {
            TareasReservaciones task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasReservaciones();
                respuesta = task.EliminarReservacion(noReservacion);
                //if (respuesta.ID > 0)
                //{
                //    task = new TareasReservaciones();
                //    var resultado = task.ConsultarReservaciones(idReservacion, 4).FirstOrDefault();
                //    EnvioCorreos.CorreoCancelacionReservacion(resultado.SocioEmail, resultado.SocioNombre, resultado.InmuebleNombre, resultado.FechaReservacionInicia.ToString("dd/MM/yyyy"));
                //}
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden cancelar la reservación. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public List<Reservacion> ConsultarReservaciones(long id, int tipo)
        {
            TareasReservaciones task = null;
            List<Reservacion> respuesta = new List<Reservacion>();
            try
            {
                task = new TareasReservaciones();
                respuesta = task.ConsultarReservaciones(id, tipo);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar las reservaciones. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public Resultado CalificarReservacion(long idReservacion, int cantidad, string comentarios)
        {
            TareasReservaciones task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasReservaciones();
                respuesta = task.CalificarReservacion(idReservacion, cantidad, comentarios);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden autorizar la reservación. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public List<Calificacion> ObtenerCalificacionesInmueble(long idInmueble)
        {
            TareasReservaciones task = null;
            List<Calificacion> respuesta = new List<Calificacion>();
            try
            {
                task = new TareasReservaciones();
                respuesta = task.ConsultarCalificaciones(idInmueble);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden obtener las calificaciones. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public List<Reservacion> ConsultarFechasInmuebles(long idInmueble)
        {
            TareasReservaciones task = null;
            List<Reservacion> respuesta = new List<Reservacion>();
            try
            {
                task = new TareasReservaciones();
                respuesta = task.ConsultarFechasInmuebles(idInmueble);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden obtener las calificaciones. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }


        #endregion Reservaciones

        #region Parametros

        public Resultado ModificarParametros(Parametros p_parametro)
        {
            TareasParametros task = null;
            Resultado respuesta = new Resultado();
            try
            {
                task = new TareasParametros();
                respuesta = task.ModificarParametros(p_parametro);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se puede modificar los parametros. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        public Parametros ConsultarParametros()
        {
            TareasParametros task = null;
            Parametros respuesta = new Parametros();
            try
            {
                task = new TareasParametros();
                respuesta = task.ConsultarParametros();
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrió una excepción, no se pueden consultar los parametros. Excepción: " + ex.Message);
            }
            finally
            {
                task.Dispose();
            }
            return respuesta;
        }

        #endregion Parametros
    }
}
