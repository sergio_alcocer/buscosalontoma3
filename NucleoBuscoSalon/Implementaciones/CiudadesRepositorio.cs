﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NucleoBuscoSalon.Contratos;
using NucleoBuscoSalon.Models;

namespace NucleoBuscoSalon.Implementaciones
{
    public class CiudadesRepositorio : ICiudadRepositorio
    {
        public DataContex Contex;

        public CiudadesRepositorio(DataContex contex) {
            Contex = contex;
        }

        public IQueryable<Ciudades> Ciudades => Contex.Ciudades;

        public void editar(Ciudades ciudades)
        {
            Ciudades ciudadBuscar = Contex.Ciudades.Find(ciudades.Ciudad_ID);
            ciudadBuscar.Estado_ID = ciudades.Estado_ID;
            ciudadBuscar.Ciudad_Nombre = ciudades.Ciudad_Nombre;
            ciudadBuscar.Ciudad_Estatus = ciudades.Ciudad_Estatus;

            Contex.Update(ciudadBuscar);
            Contex.SaveChanges();
        }

        public void guardar(Ciudades ciudades)
        {
            Contex.Ciudades.Add(ciudades);
            Contex.SaveChanges();
        }
    }
}
