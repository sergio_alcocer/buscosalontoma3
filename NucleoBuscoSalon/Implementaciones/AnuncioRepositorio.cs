﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NucleoBuscoSalon.Contratos;
using NucleoBuscoSalon.Models;

namespace NucleoBuscoSalon.Implementaciones
{
   public class AnuncioRepositorio : IAnuncioRepositorio
    {
        public DataContex Contex;

        public AnuncioRepositorio(DataContex contex) {
            Contex = contex;
        }

        public IQueryable<Anuncios> Anuncios => Contex.Anuncios;

        public void activarImagen(int anuncio_id)
        {
            Anuncios anunciosBuscar = Contex.Anuncios.Find(anuncio_id);
            anunciosBuscar.Estatus = 1;
            Contex.Update(anunciosBuscar);
            Contex.SaveChanges();
        }

        public void editar(Anuncios anuncios, bool editoFoto)
        {
            Anuncios anunciosBuscar = Contex.Anuncios.Find(anuncios.Anuncio_ID);
            anunciosBuscar.Socio_ID = anuncios.Socio_ID;
            anunciosBuscar.Fecha_Final = anuncios.Fecha_Final;
            anunciosBuscar.Estatus = anuncios.Estatus;
            anunciosBuscar.Fecha_Inicio = anuncios.Fecha_Inicio;
            anunciosBuscar.Dias = anuncios.Dias;
            anunciosBuscar.Total_Costo = anuncios.Total_Costo;

            if (editoFoto)
            {
                anunciosBuscar.Archivo = anuncios.Archivo;
                anunciosBuscar.Url = anuncios.Url;
            }
            Contex.Update(anunciosBuscar);
            Contex.SaveChanges();

        }

        public void guardar(Anuncios anuncios)
        {
            Contex.Anuncios.Add(anuncios);
            Contex.SaveChanges();
        }
    }
}
