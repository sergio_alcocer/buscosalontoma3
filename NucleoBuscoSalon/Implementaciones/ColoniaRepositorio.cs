﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NucleoBuscoSalon.Contratos;
using NucleoBuscoSalon.Models;


namespace NucleoBuscoSalon.Implementaciones
{
    public class ColoniaRepositorio : IColoniaRepositorio
    {
        public DataContex Contex;

        public ColoniaRepositorio(DataContex contex)
        {
            Contex = contex;
        }

        public IQueryable<Colonias> Colonia => Contex.Colonias;

        public void editar(Colonias colonia)
        {
            Colonias coloniasBuscar = Contex.Colonias.Find(colonia.Colonia_ID);
            coloniasBuscar.Ciudad_ID = colonia.Ciudad_ID;
            coloniasBuscar.Codigo_Postal = colonia.Codigo_Postal;
            coloniasBuscar.Estatus = colonia.Estatus;
            coloniasBuscar.Nombre = colonia.Nombre;
            coloniasBuscar.Tipo = colonia.Tipo;

            Contex.Update(coloniasBuscar);
            Contex.SaveChanges();
        }

        public void guardar(Colonias colonia)
        {
            Contex.Colonias.Add(colonia);
            Contex.SaveChanges();
        }
    }
}