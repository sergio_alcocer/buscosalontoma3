﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NucleoBuscoSalon.Contratos;
using NucleoBuscoSalon.Models;

namespace NucleoBuscoSalon.Implementaciones
{
  public  class EstadoRepositorio : IEstadosRepositorio
    {
        public DataContex Contex;

        public EstadoRepositorio(DataContex contex) {
            Contex = contex;
        }

        public IQueryable<Estados> Estados => Contex.Estados;
    }
}
