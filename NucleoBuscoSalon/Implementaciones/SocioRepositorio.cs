﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NucleoBuscoSalon.Contratos;
using NucleoBuscoSalon.Models;


namespace NucleoBuscoSalon.Implementaciones
{
   public class SocioRepositorio : ISociosRepositorio
    {
        public DataContex Contex;

        public SocioRepositorio(DataContex contex)
        {
            Contex = contex;
        }

        public IQueryable<Socios> Socios => Contex.Socios;
    }
}
