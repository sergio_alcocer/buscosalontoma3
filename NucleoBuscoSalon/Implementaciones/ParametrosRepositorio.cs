﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NucleoBuscoSalon.Contratos;
using NucleoBuscoSalon.Models;

namespace NucleoBuscoSalon.Implementaciones
{
   public class ParametrosRepositorio : IParametrosRepositorio
    {
        public DataContex Contex;

        public ParametrosRepositorio(DataContex contex) {
            Contex = contex;
        }

        public IQueryable<Parametros> Parametros => Contex.Parametros;
    }
}
