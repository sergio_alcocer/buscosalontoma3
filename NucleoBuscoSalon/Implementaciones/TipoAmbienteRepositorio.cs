﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NucleoBuscoSalon.Contratos;
using NucleoBuscoSalon.Models;


namespace NucleoBuscoSalon.Implementaciones
{
   public class TipoAmbienteRepositorio : ITipoAmbienteRepositorio
    {

        public DataContex Contex;

        public TipoAmbienteRepositorio(DataContex contex) {
            Contex = contex;
        }

        public IQueryable<Tipos_Ambientes> Tipos_Ambientes => Contex.Tipos_Ambientes;

        public void editar(Tipos_Ambientes tipos_Ambientes)
        {
            Tipos_Ambientes tipoAmbienteBuscar = Contex.Tipos_Ambientes.Find(tipos_Ambientes.Ambiente_ID);
            tipoAmbienteBuscar.Nombre = tipos_Ambientes.Nombre;
            tipoAmbienteBuscar.Estatus = tipos_Ambientes.Estatus;
            Contex.Update(tipoAmbienteBuscar);
            Contex.SaveChanges();
        }

        public void guadar(Tipos_Ambientes tipos_Ambientes)
        {
            Contex.Tipos_Ambientes.Add(tipos_Ambientes);
            Contex.SaveChanges();
        }
    }
}
