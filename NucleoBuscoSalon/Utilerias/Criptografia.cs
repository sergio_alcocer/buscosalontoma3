﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace NucleoBuscoSalon.Utilerias
{
    public class EncripcionMD5
    {
        public string Md5Hash(string input)
        {
            // Creamos una nueva instancias
            MD5 md5Hasher = MD5.Create();

            // le sacamos los byte a la cadea
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            //Creamos un string builder para aterrizar la cadena
            StringBuilder sBuilder = new StringBuilder();

            // recorremos byte por byte hasta que se transforme toda en una cadena hex
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // la regresamos
            return sBuilder.ToString();
        }
        //Verificamos de Nuestros hash
        public bool VerificarMd5Hash(string input, string hash)
        {
            //Lo que vamos a comparar
            string hashOfInput = input;

            // Creamos un comparador decadenas
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public class EncripcionSHA256
    {
        public string SALTHash(string input)
        {
            //Cramos un codificador
            UnicodeEncoding uEncode = new UnicodeEncoding();
            //Obtenemos los bytes de la cadena
            byte[] Cadena = uEncode.GetBytes(input);
            // Creamos una nueva instancias
            System.Security.Cryptography.SHA256Managed sha = new System.Security.Cryptography.SHA256Managed();
            byte[] hash = sha.ComputeHash(Cadena);
            return Convert.ToBase64String(hash);
        }

        //Verificamos de Nuestros hash
        public bool VerificarSALTHash(string input, string hash)
        {
            //Lo que vamos a SALTHash
            string hashOfInput = SALTHash(input);

            // Creamos un comparador decadenas
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public class Hexadecimal
    {
        public string CovertirHex(string Cadena)
        {
            string hex = "";
            foreach (char c in Cadena)
            {
                int tmp = c;
                hex += String.Format("{0:x2}", (uint)System.Convert.ToUInt32(tmp.ToString()));
            }
            return hex;
        }

        public string ConvertirCadena(string Hex)
        {
            string StrValue = "";
            while (Hex.Length > 0)
            {
                StrValue += System.Convert.ToChar(System.Convert.ToUInt32(Hex.Substring(0, 2), 16)).ToString();
                Hex = Hex.Substring(2, Hex.Length - 2);
            }
            return StrValue;
        }
    }
}
