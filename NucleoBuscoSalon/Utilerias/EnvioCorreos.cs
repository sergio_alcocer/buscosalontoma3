﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using NucleoBuscoSalon.Tareas;


namespace NucleoBuscoSalon.Utilerias
{
    class EnvioCorreos
    {

        private static string email = "noreply@buscosalon.com";
        private static string psw = "5uPPort3u$co54";
        private static string ssmtp = "mail.negox.com";
        private static int port = 587;
        private static string path = "https://buscosalon.com";
        private static string name = "BuscoSalon.com";

        public static void CorreoRecuperacion(string Email, string Token)
        {
            MailMessage mail = new MailMessage();
            SmtpClient smtp = new SmtpClient(ssmtp);
            string url = path + "/nuevopassword/" + Token;        
            try
            {

                mail.From = new MailAddress(email, name);
                mail.Subject = "Recuperacion de Contraseña";
                //mail.Body = "<b>Estimado suscriptor:</b></br></br>Favor de dar click en el siguiente enlace para recuperar su contraseña.</br></br></br><a href='" + url + "'>Click para recuperar contraseña</a>";
                mail.Body = LlenarCuerpoMail("Estimado suscriptor!", "Favor de dar click en el siguiente enlace para recuperar su contraseña.", url);
                mail.To.Add(Email);
                mail.IsBodyHtml = true;

                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Port = port;
                smtp.Credentials = new System.Net.NetworkCredential(email, psw);

                smtp.Send(mail);
            }
            catch(Exception ex)
            {

            }
        }
        public static void CorreoBienvenida(string Email, string Usuario)
        {
            MailMessage mail = new MailMessage();
            SmtpClient smtp = new SmtpClient(ssmtp);
            TareasParametros parametros = new TareasParametros();

            try
            {
                mail.From = new MailAddress(email, name);
                mail.Subject = "Te damos la bienvenida a BuscoSalon";
                mail.Body = LlenarCuerpoMail("Hola " + Usuario + "!", "BuscoSalon.com es una comunidad donde podras reservar, publicar salones incluso tenemos nuestra seccion de anuncios donde podras publicar tus productos.");

                var resultado = parametros.ConsultarParametros();
                string[] correos = resultado.Correos_CC.Split(';');

                foreach (var Renglon in correos)
                {
                    mail.Bcc.Add(Renglon.Replace(";", ""));
                }

                mail.To.Add(Email);
                mail.IsBodyHtml = true;

                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Port = port;
                smtp.Credentials = new System.Net.NetworkCredential(email, psw);

                smtp.Send(mail);
            }
            catch(Exception Ex)
            {

            }
        }
        public static void CorreoActivacion(string Email, string Usuario, string NoReferencia)
        {
            MailMessage mail = new MailMessage();
            SmtpClient smtp = new SmtpClient(ssmtp);
            TareasParametros parametros = new TareasParametros();

            try
            {

                mail.From = new MailAddress(email, name);
                mail.Subject = "Activacion BuscoSalon";
                mail.Body = LlenarCuerpoMail("Hola " + Usuario + "!", "Te comunicamos que has sido dado de alta para poder publicar tus Salones. Acontinuacion te mandados tu No de Referencia: "+ NoReferencia);
                //mail.Body = "<b>Hola, " + Usuario + ":</b></br></br>Te comunicamos que has sido dado de alta para poder publicar tus Salones. Acontinuacion te mandados tu No de Referencia: "+ NoReferencia + ".</br></br></br><a href='" + url + "'>Comenzar</a>";

                var resultado = parametros.ConsultarParametros();
                string[] correos = resultado.Correos_CC.Split(';');

                foreach (var Renglon in correos)
                {
                    mail.Bcc.Add(Renglon.Replace(";", ""));
                }

                mail.To.Add(Email);
                mail.IsBodyHtml = true;

                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Port = port;
                smtp.Credentials = new System.Net.NetworkCredential(email, psw);

                smtp.Send(mail);
            }
            catch (Exception Ex)
            {

            }
        }
        public static void CorreoReservacionSocio(string Email,string Socio,string Salon, string DomicilioSalon)
        {
            MailMessage mail = new MailMessage();
            SmtpClient smtp = new SmtpClient(ssmtp);
            TareasParametros parametros = new TareasParametros();
            try
            {

                mail.From = new MailAddress(email, name);
                mail.Subject = "Usted tiene una reservacion por autorizar BuscoSalon";
                mail.Body = LlenarCuerpoMail("Hola " + Socio + "!", "Usted tiene una reservacion de su Salon " + Salon + ", " + DomicilioSalon + ". Es necesario que entre al portal para confirmar o cancelar la reservacion.");
                //mail.Body = "<b>Hola, " + Socio + ":</b></br></br>Usted tiene una reservacion de su Salon "+ Salon + ", "+ DomicilioSalon + ". Es necesario que entre al portal para confirmar o cancelar la reservacion.</br></br></br><a href='" + url + "'>Comenzar</a>";
                var resultado = parametros.ConsultarParametros();
                string[] correos = resultado.Correos_CC.Split(';');

                foreach (var Renglon in correos)
                {
                    mail.Bcc.Add(Renglon.Replace(";", ""));
                }
                mail.To.Add(Email);
                mail.IsBodyHtml = true;

                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Port = port;
                smtp.Credentials = new System.Net.NetworkCredential(email, psw);

                smtp.Send(mail);
            }
            catch (Exception Ex)
            {

            }
        }
        public static void CorreoAutorizacionReservacion(string Email, string Socio, string Salon, string FechaRerva)
        {
            MailMessage mail = new MailMessage();
            SmtpClient smtp = new SmtpClient(ssmtp);
            TareasParametros parametros = new TareasParametros();
            try
            {

                mail.From = new MailAddress(email, name);
                mail.Subject = "Su reservacion ha sido autorizada BuscoSalon";
                mail.Body = LlenarCuerpoMail("Hola " + Socio + "!", "Le notificamos que su reservación al Salon " + Salon + " el día " + FechaRerva + " ha sido autorizada. Para mas detalles consulte nuestro portal.");
                //mail.Body = "<b>Hola, " + Socio + ":</b></br></br>Le notificamos que su reservación al Salon " + Salon + " el día " + FechaRerva + " ha sido autorizada. Para mas detalles consulte nuestro portal.</br></br></br><a href='" + url + "'>Comenzar</a>";
                var resultado = parametros.ConsultarParametros();
                string[] correos = resultado.Correos_CC.Split(';');

                foreach (var Renglon in correos)
                {
                    mail.Bcc.Add(Renglon.Replace(";", ""));
                }
                mail.To.Add(Email);
                mail.IsBodyHtml = true;

                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Port = port;
                smtp.Credentials = new System.Net.NetworkCredential(email, psw);

                smtp.Send(mail);
            }
            catch (Exception Ex)
            {

            }
        }
        public static void CorreoCancelacionReservacion(string Email, string Socio, string Salon, string FechaRerva)
        {
            MailMessage mail = new MailMessage();
            SmtpClient smtp = new SmtpClient(ssmtp);
            TareasParametros parametros = new TareasParametros();
            try
            {

                mail.From = new MailAddress(email, name);
                mail.Subject = "Su reservacion ha sido cancelada BuscoSalon";
                mail.Body = LlenarCuerpoMail("Hola " + Socio + "!", "Lamentamos notificarle que su reservación al Salon " + Salon + " el día " + FechaRerva + " ha sido rechazada. Para mas detalles consulte nuestro portal.");
                //mail.Body = "<b>Hola, " + Socio + ":</b></br></br>Lamentamos notificarle que su reservación al Salon " + Salon + " el día " + FechaRerva + " ha sido rechazada. Para mas detalles consulte nuestro portal.</br></br></br><a href='" + url + "'>Comenzar</a>";
                var resultado = parametros.ConsultarParametros();
                string[] correos = resultado.Correos_CC.Split(';');

                foreach (var Renglon in correos)
                {
                    mail.Bcc.Add(Renglon.Replace(";", ""));
                }
                mail.To.Add(Email);
                mail.IsBodyHtml = true;

                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Port = port;
                smtp.Credentials = new System.Net.NetworkCredential(email, psw);

                smtp.Send(mail);
            }
            catch (Exception Ex)
            {

            }
        }

        public static string LlenarCuerpoMail(string Titulo, string Description, string link="")
        {
            string StrBody = string.Empty;
            if(String.IsNullOrEmpty(link))
            {
                link = "https://buscosalon.com";
            }

            var folderName = Path.Combine("Recursos", "Formatos");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
            var fullPath = Path.Combine(pathToSave, "CorreoParametros.html");


            using (StreamReader reader = new StreamReader(fullPath))
            {
                StrBody = reader.ReadToEnd();
            }

            StrBody = StrBody.Replace("{link}", link);
            StrBody = StrBody.Replace("{Titulo}", Titulo);
            StrBody = StrBody.Replace("{Description}", Description);

            return StrBody;
        }

    }
}
