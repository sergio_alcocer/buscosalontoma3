﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.IO;

namespace NucleoBuscoSalon.Utilerias
{
    public class SqlConexion : IDisposable
    {
        #region Variables

        private SqlConnection _conn = null;        
        private SqlTransaction _trans = null;
		bool _Conectado = false;
        string _NombreProcedimiento = "";
        List<SqlParameter> _Parametros = new List<SqlParameter>();		
        bool _Preparado = false;

        #endregion Variables

        #region Carga Inicial

        public SqlConexion()
        {

        }

        public bool AutoDesconexion { get; set; }

        public bool Conectar(string ConnectionString)
        {
            bool _Rsp = false;
            _conn = new SqlConnection(ConnectionString);
            try
            {
                _conn.Open();
                _Conectado = true;
                _Rsp = true;				
            }
            catch
            {
                _Rsp = false;
            }
            return _Rsp;
        }

        public void Desconectar()
        {
            _conn.Close();            
        }

        public void PrepararProcedimiento(string NombreProcedimiento, List<SqlParameter> Parametros)
        {
            if (_Conectado)
            {
                _NombreProcedimiento = "";
                _Parametros.Clear();

                _NombreProcedimiento = NombreProcedimiento;
                _Parametros = Parametros;
                _Preparado = true;
            }
            else
            {
                throw new Exception("No hay conexion con la bd");
            }
        }

        #endregion Carga Inicial

        #region Ejecuciones

        public int EjecutarProcedimiento()
        {
            if (_Preparado)
            {
				SqlCommand cmm;
				cmm = new SqlCommand(_NombreProcedimiento, _conn, _trans);
                cmm.CommandType = System.Data.CommandType.StoredProcedure;
                cmm.Parameters.AddRange(_Parametros.ToArray());
                _Preparado = false;
                return cmm.ExecuteNonQuery();
            }
            else
            {
                _Preparado = false;
                throw new Exception("Procedimiento no preparado");
            }
        }
		
		public SqlDataReader EjecutarReader(System.Data.CommandType TipoComando)
        {
            if (_Preparado)
            {
				SqlCommand cmm ;                
				cmm = new SqlCommand(_NombreProcedimiento, _conn,_trans);
                cmm.CommandType = TipoComando;
                cmm.Parameters.AddRange(_Parametros.ToArray());
                _Preparado = false;
                return cmm.ExecuteReader();
            }
            else
            {
                _Preparado = false;
                throw new Exception("Procedimiento no preparado");
            }
		}
        
        public DataTableReader EjecutarTableReader(System.Data.CommandType TipoComando)
        {
            if (_Preparado)
            {
                DataTable dt = new DataTable();
				SqlCommand cmm;
				cmm = new SqlCommand(_NombreProcedimiento, _conn, _trans);
                cmm.CommandType = TipoComando;
                cmm.Parameters.AddRange(_Parametros.ToArray());
                cmm.CommandTimeout = 90000; 
                SqlDataAdapter adt = new SqlDataAdapter(cmm);
                adt.Fill(dt);
                _Preparado = false;
                return dt.CreateDataReader();
            }
            else
            {
                _Preparado = false;
                throw new Exception("Procedimiento no preparado");
            }
        }
        
        public XmlTextReader EjecutarXmlReader()
        {
            object valor = null;
            if (_Preparado)
            {
				SqlCommand cmm;
				cmm = new SqlCommand(_NombreProcedimiento, _conn, _trans);
                cmm.CommandType = System.Data.CommandType.StoredProcedure;
                cmm.Parameters.AddRange(_Parametros.ToArray());
                _Preparado = false;
                cmm.ExecuteScalar();

                foreach (SqlParameter sp in cmm.Parameters)
                {
                    if (sp.Direction == ParameterDirection.ReturnValue || sp.Direction == ParameterDirection.Output)
                    {
                        valor = sp.Value;
                        break;
                    }
                }

                if (valor != null)
                {

                    XmlParserContext context = new XmlParserContext(null, null, string.Empty, XmlSpace.None, Encoding.UTF8);
                    XmlTextReader xmr = new XmlTextReader(new MemoryStream(UTF8Encoding.UTF8.GetBytes(valor.ToString())), XmlNodeType.Document, context);
                    return xmr;
                }
                else
                {
                    _Preparado = false;
                    throw new Exception("No es un xml valido");
                }
            }
            else
            {
                _Preparado = false;
                throw new Exception("Procedimiento no preparado");
            }
        }
        
        public XmlTextReader EjecutarXmlReader(System.Data.CommandType tipo)
        {
            object valor = null;
            if (_Preparado)
            {
				SqlCommand cmm;
				cmm = new SqlCommand(_NombreProcedimiento, _conn, _trans);
                cmm.CommandType = tipo;
                cmm.Parameters.AddRange(_Parametros.ToArray());
                _Preparado = false;
                cmm.ExecuteScalar();

                foreach (SqlParameter sp in cmm.Parameters)
                {
                    if (sp.Direction == ParameterDirection.ReturnValue || sp.Direction == ParameterDirection.Output)
                    {
                        valor = sp.Value;
                        break;
                    }
                }

                if (valor != null)
                {

                    XmlParserContext context = new XmlParserContext(null, null, string.Empty, XmlSpace.None, Encoding.UTF8);
                    XmlTextReader xmr = new XmlTextReader(new MemoryStream(UTF8Encoding.UTF8.GetBytes(valor.ToString())), XmlNodeType.Document, context);
                    return xmr;
                }
                else
                {
                    _Preparado = false;
                    throw new Exception("No es un xml valido");
                }
            }
            else
            {
                _Preparado = false;
                throw new Exception("Procedimiento no preparado");
            }
        }

        public object EjecutarProcedimientoOutput()
        {
            object valor = null;
            if (_Preparado)
            {
				SqlCommand cmm;
				cmm = new SqlCommand(_NombreProcedimiento, _conn, _trans);
                cmm.CommandType = System.Data.CommandType.StoredProcedure;
                cmm.Parameters.AddRange(_Parametros.ToArray());
                _Preparado = false;
                cmm.ExecuteScalar();

                foreach (SqlParameter sp in cmm.Parameters)
                {
                    if (sp.Direction == ParameterDirection.ReturnValue || sp.Direction == ParameterDirection.Output)
                    {
                        valor = sp.Value;
                        break;
                    }
                }
            }
            else
            {
                _Preparado = false;
                throw new Exception("Procedimiento no preparado");
            }
            return valor;
        }

        public object EjecutarScalar()
        {
            if (_Preparado)
            {
				SqlCommand cmm;
				cmm = new SqlCommand(_NombreProcedimiento, _conn, _trans);
                cmm.CommandType = System.Data.CommandType.StoredProcedure;
                cmm.Parameters.AddRange(_Parametros.ToArray());
                _Preparado = false;
                return cmm.ExecuteScalar();
            }
            else
            {
                _Preparado = false;
                throw new Exception("Procedimiento no preparado");
            }
        }

        public DataTable EjecutarTable()
        {
            if (_Preparado)
            {
                DataTable dt = new DataTable();
				SqlCommand cmm;
				if (_trans == null)
					cmm = new SqlCommand(_NombreProcedimiento, _conn);
				else
					cmm = new SqlCommand(_NombreProcedimiento, _conn, _trans);
                cmm.CommandType = System.Data.CommandType.StoredProcedure;
                cmm.Parameters.AddRange(_Parametros.ToArray());
                SqlDataAdapter adt = new SqlDataAdapter(cmm);
                adt.Fill(dt);
                _Preparado = false;
                return dt.Copy();
            }
            else
            {
                _Preparado = false;
                throw new Exception("Procedimiento no preparado");
            }
        }

		public DataTable EjecutarTable(System.Data.CommandType tipoComando)
        {
            if (_Preparado)
            {
                DataTable dt = new DataTable();
				SqlCommand cmm;
				cmm = new SqlCommand(_NombreProcedimiento, _conn, _trans);
                cmm.CommandType = tipoComando;
                cmm.Parameters.AddRange(_Parametros.ToArray());
                SqlDataAdapter adt = new SqlDataAdapter(cmm);
                adt.Fill(dt);
                _Preparado = false;
                return dt.Copy();
            }
            else
            {
                _Preparado = false;
                throw new Exception("Procedimiento no preparado");
            }
        }

		public DataSet EjecutarDataSet()
		{
			return EjecutarDataSet(CommandType.StoredProcedure);
		}

		public DataSet EjecutarDataSet(System.Data.CommandType tipoComando)
		{
			if (_Preparado)
			{
				DataSet ds = new DataSet();
				SqlCommand cmm;
				cmm = new SqlCommand(_NombreProcedimiento, _conn, _trans);
				cmm.CommandTimeout = 90;
				cmm.CommandType = tipoComando;
				cmm.Parameters.AddRange(_Parametros.ToArray());
				SqlDataAdapter adt = new SqlDataAdapter(cmm);
				adt.Fill(ds);
				_Preparado = false;
				return ds;
			}
			else
			{
				_Preparado = false;
				throw new Exception("Procedimiento no preparado");
			}
		}

        public object[] EjecutarObject()
        {
            if (_Preparado)
            {
                object[] Resp = new object[0];
				SqlCommand cmm;
				cmm = new SqlCommand(_NombreProcedimiento, _conn, _trans);
                cmm.CommandType = System.Data.CommandType.StoredProcedure;
                cmm.Parameters.AddRange(_Parametros.ToArray());
                _Preparado = false;
                SqlDataReader dtr = cmm.ExecuteReader();
                dtr.Read();
                dtr.GetValues(Resp);
                return Resp;
            }
            else
            {
                _Preparado = false;
                throw new Exception("Procedimiento no preparado");
            }
        }

        #endregion Ejecuciones

        #region Transaction

        internal string ConexionString
		{
			get { return _conn.ConnectionString; }
		}

		internal SqlTransaction Transaccion 
		{
			get { return _trans; }
			set { _trans = value; }
		}

		public void StartTransaccion()
		{
			_trans =_conn.BeginTransaction();
			if (BeginTranstion != null)
				BeginTranstion(this, _trans);		
		}

		public void StartTransaccion(IsolationLevel level)
		{
			_trans = _conn.BeginTransaction(level);
			if (BeginTranstion != null)
				BeginTranstion(this, _trans);
		}

		public void StartTransaccion(string name)
		{
			_trans = _conn.BeginTransaction(name);
			if (BeginTranstion != null)
				BeginTranstion(this,_trans);
		}

		internal delegate void EventBeginTransaction(SqlConexion Con,SqlTransaction Trans);

		internal event EventBeginTransaction BeginTranstion;

        #endregion Transaction

        #region IDisposable Members

        public void Dispose()
        {
            try
            {
                if (_conn != null) _conn.Dispose();
                _Parametros.Clear();
                _Preparado = false;
                GC.Collect();
            }
            catch { }
        }

        #endregion
    }
}
