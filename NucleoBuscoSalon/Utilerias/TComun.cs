﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace NucleoBuscoSalon.Utilerias
{
    ///Autor: Luis Enrique Rios Villanueva
    public class TComun : IDisposable
    {
        #region Conexion
        SqlConexion _conexion = new SqlConexion();
        public TComun(bool ConSqlConexion)
        {
            if(ConSqlConexion)
				CrearConexion();
        }

        public TComun(string ConSqlConexion)
        {
            CrearConexion(ConSqlConexion);
        }

        public TComun() { CrearConexion(); }
        public TComun(SqlConexion SqlConexion)
        {
            _conexion = SqlConexion;			
        }
		public TComun(SqlConexion SqlConexion,bool NewConection)
        {
            if(NewConection)
			{
				CrearConexion(SqlConexion.ConexionString);
				Conexion.Transaccion = SqlConexion.Transaccion;
				SqlConexion.BeginTranstion += (c,t) => 
				{
					_conexion.Transaccion = t;
				};
			}
			else
				_conexion = SqlConexion;			
        }
        public TComun(TComun tcomun)
        {
            _conexion = tcomun.Conexion;			
        }
		public TComun(TComun tcomun,bool NewConection)
        {
			if(NewConection)
			{
				tcomun.CrearConexion(tcomun.Conexion.ConexionString);
				tcomun.Conexion.Transaccion = tcomun.Conexion.Transaccion;			
				tcomun.Conexion.BeginTranstion += (c,t) => 
				{
					_conexion.Transaccion = t;
				};
			}
			_conexion = tcomun.Conexion;
        }
        private void CrearConexion()
        {
            _conexion = new SqlConexion();
            if (!_conexion.Conectar(Genericos.ConectionString))
            {
                throw new Exception("Error al conectar");
            }

        }
		private void CrearConexion(string Conextionstring)
        {
            _conexion = new SqlConexion();
            if (!_conexion.Conectar(Conextionstring))
            {
                throw new Exception("Error al conectar");
            }

        }
        public void Dispose()
        {
            _conexion.Desconectar();
            _conexion.Dispose();
        }
        public SqlConexion Conexion { get { return _conexion; } }
        #endregion

		#region Transacciones
		public void StartTransaction()
		{
			Conexion.StartTransaccion();
		}

		public void Commit()
		{
			if(Conexion.Transaccion != null)
				this.Conexion.Transaccion.Commit();
		}
		public void RollBack()
		{
			if (Conexion.Transaccion != null)
				this.Conexion.Transaccion.Rollback();
		}
		#endregion

    }
}
