﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NucleoBuscoSalon.Models;

namespace NucleoBuscoSalon.Contratos
{
   public interface IEstadosRepositorio
    {
        IQueryable<Estados> Estados { get; }
    }
}
