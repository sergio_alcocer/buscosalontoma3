﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NucleoBuscoSalon.Models;

namespace NucleoBuscoSalon.Contratos
{
   public interface IAnuncioRepositorio
    {
        IQueryable<Anuncios> Anuncios { get;}

        void guardar(Anuncios anuncios);
        void editar(Anuncios anuncios, bool editoFoto);
        void activarImagen(int anuncio_id);
    }
}
