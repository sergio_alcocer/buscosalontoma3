﻿using System;
using System.Runtime.Serialization;

namespace NucleoBuscoSalon.Contratos
{
    public class Resultado
    {
        public long ID { get; set; }
        public string Descripcion { get; set; }
        public string MensajeError { get; set; }
    }

    public class Usuario
    {
        public long ID { get; set; }
        public string Nick { get; set; }
        public string Password { get; set; }
        public string Nombre { get; set; }
        public string Email { get; set; }
        public int Tipo { get; set; }
        public DateTime Creacion { get; set; }
        public long Realizo { get; set; }
        public int Estatus { get; set; }
        public string Token { get; set; }
    }

    public class Tipo_Cliente
    {
        public int Tipo_ID { get; set; }
        public string Nombre { get; set; }
        public int Estatus { get; set; }
    }

    public class Socio
    {
        public long ID { get; set; }
        public int? Tipo { get; set; }
        public string Nick { get; set; }
        public string Password { get; set; }
        public string Nombre { get; set; }
        public string Paterno { get; set; }
        public string Materno { get; set; }
        public int? Sexo { get; set; }
        public string Email { get; set; }
        public int? Acepta_Pago_Tarjeta { get; set; }
        public string Cuenta_Clabe { get; set; }
        public int? Numero_Lugares { get; set; }
        public DateTime? Fecha_Alta { get; set; }
        public int? Estatus { get; set; }
        public string Token { get; set; }
        public string NoReferencia { get; set; }
        public int? NoSalones { get; set; }
        public string WhatsApp { get; set; }
    }

    public class Inmueble
    {
        public long ID { get; set; }
        public long SocioID { get; set; }
        public string DescripcionSocio { get; set; }
        public long CiudadID { get; set; }
        public string DescripcionCiudad { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public long ColoniaID { get; set; }
        public string DescripcionColonia { get; set; }
        public long EstadoID { get; set; }
        public string DescripcionEstado { get; set; }
        public string Referencias { get; set; }
        public int AmbienteID { get; set; }
        public string DescripcionAmbiente { get; set; }
        public int Capacidad { get; set; }
        public decimal Costo { get; set; }
        public float CalificacionPromedio { get; set; }
        public int Estatus { get; set; }
        public string Usuario { get; set; }
        public string Src { get; set; }
        public DateTime Fecha { get; set; }
        public string Horario { get; set; }
        public string CorreoElectronico { get; set; }
        public Socio Socio { get; set; }
        public string Telefono { get; set; }
        public string Localizacion { get; set; }
        public decimal Latitud { get; set; }
        public decimal Longitud { get; set; }
        public string Whatsapp { get; set; }
        public string Facebook { get; set; }
        public string SitioWeb { get; set; }
        public string WhatsAppSocio { get; set; }
    }

    public class Paquete
    {
        public long ID { get; set; }
        public string Nombre { get; set; }
        public long InmuebleID { get; set; }
        public string InmuebleNombre { get; set; }
        public float Costo { get; set; }
        public int IncluyeMeseros { get; set; }
        public int IncluyeMesas { get; set; }
        public int IncluyeManteleria { get; set; }
        public string OtrosDatos { get; set; }
        public int Estatus { get; set; }
    }

    public class Colonia
    {
        public long ColoniaID { get; set; }
        public string NombreColonia { get; set; }
        public long CiudadID { get; set; }
        public string NombreCiudad { get; set; }
        public long EstadoID { get; set; }
        public string NombreEstado { get; set; }
    }

    public class Ciudad
    {
        public long ID { get; set; }
        public string Nombre { get; set; }
    }

    public class Estado
    {
        public long ID { get; set; }
        public string Nombre { get; set; }
    }

    public class Ambiente
    {
        public long ID { get; set; }
        public string Nombre { get; set; }
    }

    public class Parametro_Busqueda
    {
        public string FitroEstado { get; set; }
        public string FiltroGeneral { get; set; }
        public string TipoAmbiente { get; set; }
        public string PrecioMaximo { get; set; }
        public string IncluirMesas { get; set; }
        public string IncluirMeseros { get; set; }
        public string IncluirManteles { get; set; }
        public string OrderBy { get; set; }
        public string Pagina { get; set; }
    }

    public class Imagen
    {
        public long ID { get; set; }
        public long InmuebleID { get; set; }
        public string Nombre { get; set; }
        public string Src { get; set; }
        public int Estatus { get; set; }
    }

    public class Recuperacion
    {
        public string Token { get; set; }
        public string Password { get; set; }
    }

    public class Reservacion
    {
        public long NoReservacion { get; set; }
        public long SocioID { get; set; }
        public string SocioNombre { get; set; }
        public string SocioEmail { get; set; }
        public long InmuebleID { get; set; }
        public string InmuebleNombre { get; set; }
        public long PaqueteID { get; set; }
        public string PaqueteNombre { get; set; }
        public DateTime FechaReservacionInicia { get; set; }
        public DateTime FechaReservacionTermina { get; set; }
        public decimal Costo { get; set; }
        public int Estatus { get; set; }
    }

    public class ActualizaPassword
    {
        public long ID { get; set; }
        public string ActualPassword { get; set; }
        public string NuevoPassword { get; set; }
    }
    public class Calificacion
    {
        public long NoCalificacion { get; set; }
        public long NoReservacion { get; set; }
        public long SocioID { get; set; }
        public string SocioNombre { get; set; }
        public long InmuebleID { get; set; }
        public string InmuebleNombre { get; set; }
        public int ValorCalificacion { get; set; }
        public string Comentario { get; set; }
        public DateTime Fecha { get; set; }
    }

}
