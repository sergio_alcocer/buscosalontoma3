﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NucleoBuscoSalon.Models;

namespace NucleoBuscoSalon.Contratos
{
   public interface ICiudadRepositorio
    {
        IQueryable<Ciudades> Ciudades { get; }

        void guardar(Ciudades ciudades);
        void editar(Ciudades ciudades);
    }
}
