﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NucleoBuscoSalon.Models;

namespace NucleoBuscoSalon.Contratos
{
   public interface IColoniaRepositorio
    {
        IQueryable<Colonias> Colonia { get; }


        void guardar(Colonias colonia);
        void editar(Colonias colonia);
    }
}
