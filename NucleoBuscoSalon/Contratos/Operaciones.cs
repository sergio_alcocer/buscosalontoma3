﻿using NucleoBuscoSalon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace NucleoBuscoSalon.Contratos
{
    public interface IUsuarios
    {
        Resultado CrearUsuario(Usuario p_usuario);
        Resultado ModificarUsuario(Usuario p_usuario);
        Resultado ModificarUsuarioPerfil(Usuario p_usuario);        
        Usuario ConsultarUsuarioPorID(long idUsuario);
        List<Usuario> ConsultarUsuarios(string filtroBusqueda);
        Resultado EliminarUsuario(long idUsuario);
        Resultado RestablecerPasswordUsuario(long idUsuario);
        Resultado CodigoRecuperacion(string Email);
        Resultado actualizapasspublico(Usuario p_usuario);
        Resultado ActualizaPasswordUsuario(ActualizaPassword actualiza);
    }

    public interface ISocio
    {
        Resultado CrearSocio(Socio p_socio);
        Resultado ModificarSocio(Socio p_socio);
        Resultado ModificarSocioPerfil(Socio p_socio);
        Resultado ActivarSocio(Socio p_socio);
        Socio ConsultarSocioPorID(long idSocio);
        List<Socio> ConsultarSocios(string filtroBusqueda, int tipo);
        List<Socio> ConsultarSociosSinActivar(string filtroBusqueda, int tipo);
        Resultado EliminarSocio(long idSocio);
        Resultado RestablecerPasswordSocio(long idSocio);
        Resultado ActualizaPasswordToken(Recuperacion p_recupera);
        Resultado ActualizaPasswordSocio(ActualizaPassword actualiza);
    }

    public interface IInmuebles
    {
        Resultado CrearInmueble(Inmueble p_inmueble);
        Resultado ModificarInmueble(Inmueble p_inmueble);
        Inmueble ConsultarInmueblePorID(long idInmueble);
        List<Inmueble> ConsultarInmuebles(string filtroBusqueda, long idSocio);
        Resultado EliminarInmueble(long idInmueble);
        List<Colonia> ConsultarColoniasPorCP(string codigopostal);
        List<Ambiente> ConsultarTiposAmbientes();
        List<Estado> ConsultarEstadosPorPais(long ID);
        List<Ciudad> ConsultarCiudadesPorEstado(long ID);
        List<Colonia> ConsultarColoniasPorCiudad(long ID);
        List<Inmueble> BuscarInmuebles(Parametro_Busqueda parametro);    
        Resultado CrearPaquete(Paquete p_paquete);
        Resultado ModificarPaquete(Paquete p_paquete);
        Paquete ConsultarPaquetePorID(long idPaquete);
        List<Paquete> ConsultarPaquetes(long idInmueble);
        Resultado EliminarPaquete(long idInmueble);
        Resultado CrearImagen(Imagen p_imagen);
        List<Imagen> ConsultarImagenes(long idInmueble);
        Resultado EliminarImagen(long idImagen);
        Resultado ValidarNuevoInmueblePorIDSocio(long idSocio);
        Imagen ConsultarImagenMiniatura(long idInmueble);
    }

    public interface IReservaciones
    {
        Resultado CrearReservacion(Reservacion p_reservacion);
        Resultado AutorizarReservacion(long idReservacion);
        Resultado CancelarReservacion(long idReservacion);
        Resultado EliminarReservacion(long idReservacion);
        List<Reservacion> ConsultarReservaciones(long id, int tipo);
    }

    public interface IParametros
    {
        Resultado ModificarParametros(Parametros p_parametro);
        Parametros ConsultarParametros();
    }
}
