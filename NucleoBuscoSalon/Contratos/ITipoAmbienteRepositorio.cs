﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NucleoBuscoSalon.Models;

namespace NucleoBuscoSalon.Contratos
{
   public interface ITipoAmbienteRepositorio
    {
        IQueryable<Tipos_Ambientes> Tipos_Ambientes { get; }

        void guadar(Tipos_Ambientes tipos_Ambientes);
        void editar(Tipos_Ambientes tipos_Ambientes);
    }
}
