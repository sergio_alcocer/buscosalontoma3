﻿using NucleoBuscoSalon.Contratos;
using NucleoBuscoSalon.Utilerias;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NucleoBuscoSalon.Tareas
{
    internal class TareasUsuarios : TComun
    {
        #region Constructores

        public TareasUsuarios() : base() { }
        public TareasUsuarios(SqlConexion sql) : base(sql) { }
        public TareasUsuarios(Boolean ConSqlConexion) : base(ConSqlConexion) { }
        public TareasUsuarios(TComun tcomun) : base(tcomun) { }

        #endregion

        #region Operaciones

        public List<Usuario> ConsultarUsuarios(string filtroBusqueda)
        {
            if (filtroBusqueda == "---sindatos---") filtroBusqueda = "";
            List<Usuario> lstUsuarios = new List<Usuario>();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@FiltroBusqueda", filtroBusqueda));
            Conexion.PrepararProcedimiento("[dbo].[usuarios.Consultar]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                Usuario usuario = new Usuario();
                if (dataReader["Usuario_ID"] != DBNull.Value) usuario.ID = long.Parse(dataReader["Usuario_ID"].ToString());
                if (dataReader["Usuario_Nick"] != DBNull.Value) usuario.Nick = dataReader["Usuario_Nick"].ToString();
                if (dataReader["Usuario_Password"] != DBNull.Value) usuario.Password = dataReader["Usuario_Password"].ToString();
                if (dataReader["Usuario_Nombre"] != DBNull.Value) usuario.Nombre = dataReader["Usuario_Nombre"].ToString();
                if (dataReader["Usuario_Email"] != DBNull.Value) usuario.Email = dataReader["Usuario_Email"].ToString();
                if (dataReader["Usuario_Tipo"] != DBNull.Value) usuario.Tipo = int.Parse(dataReader["Usuario_Tipo"].ToString());
                if (dataReader["Usuario_Creacion"] != DBNull.Value) usuario.Creacion = (DateTime)dataReader["Usuario_Creacion"];
                if (dataReader["Usuario_Estatus"] != DBNull.Value) usuario.Estatus = int.Parse(dataReader["Usuario_Estatus"].ToString());
                lstUsuarios.Add(usuario);
            }
            return lstUsuarios;
        }

        public List<Usuario> ConsultaNickEmail(string Nick, string Email)
        {
            List<Usuario> lstUsuarios = new List<Usuario>();
            List<SqlParameter> _Parametros = new List<SqlParameter>();

            if(!String.IsNullOrEmpty(Nick))
                _Parametros.Add(new SqlParameter("@Nick", Nick));

            if (!String.IsNullOrEmpty(Email))
                _Parametros.Add(new SqlParameter("@Email", Email));

            Conexion.PrepararProcedimiento("[dbo].[Ambos.ConsultaNickEmail]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                Usuario usuario = new Usuario();
                if (dataReader["ID"] != DBNull.Value) usuario.ID = long.Parse(dataReader["ID"].ToString());
                if (dataReader["Nick"] != DBNull.Value) usuario.Nick = dataReader["Nick"].ToString();
                if (dataReader["Password"] != DBNull.Value) usuario.Password = dataReader["Password"].ToString();
                if (dataReader["Nombre"] != DBNull.Value) usuario.Nombre = dataReader["Nombre"].ToString();
                if (dataReader["Email"] != DBNull.Value) usuario.Email = dataReader["Email"].ToString();
                if (dataReader["Tipo"] != DBNull.Value) usuario.Tipo = int.Parse(dataReader["Tipo"].ToString());
                if (dataReader["Estatus"] != DBNull.Value) usuario.Estatus = int.Parse(dataReader["Estatus"].ToString());
                lstUsuarios.Add(usuario);
            }
            return lstUsuarios;
        }
        public List<Usuario> ConsultaNickEmailUsuario(Usuario p_usuario)
        {
            List<Usuario> lstUsuarios = new List<Usuario>();
            List<SqlParameter> _Parametros = new List<SqlParameter>();

            _Parametros.Add(new SqlParameter("@Id", p_usuario.ID));

            if (!String.IsNullOrEmpty(p_usuario.Nick))
                _Parametros.Add(new SqlParameter("@Nick", p_usuario.Nick));

            if (!String.IsNullOrEmpty(p_usuario.Email))
                _Parametros.Add(new SqlParameter("@Email", p_usuario.Email));

            Conexion.PrepararProcedimiento("[dbo].[usuarios.ConsultaNickEmail]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                Usuario usuario = new Usuario();
                if (dataReader["ID"] != DBNull.Value) usuario.ID = long.Parse(dataReader["ID"].ToString());
                if (dataReader["Nick"] != DBNull.Value) usuario.Nick = dataReader["Nick"].ToString();
                if (dataReader["Password"] != DBNull.Value) usuario.Password = dataReader["Password"].ToString();
                if (dataReader["Nombre"] != DBNull.Value) usuario.Nombre = dataReader["Nombre"].ToString();
                if (dataReader["Email"] != DBNull.Value) usuario.Email = dataReader["Email"].ToString();
                if (dataReader["Tipo"] != DBNull.Value) usuario.Tipo = int.Parse(dataReader["Tipo"].ToString());
                if (dataReader["Estatus"] != DBNull.Value) usuario.Estatus = int.Parse(dataReader["Estatus"].ToString());
                lstUsuarios.Add(usuario);
            }
            return lstUsuarios;
        }

        public Usuario ConsultarUsuarioPorID(long idUsuario)
        {
            Usuario usuario = new Usuario();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@ID", idUsuario));
            Conexion.PrepararProcedimiento("[dbo].[usuarios.ConsultarPorID]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                if (dataReader["Usuario_ID"] != DBNull.Value) usuario.ID = long.Parse(dataReader["Usuario_ID"].ToString());
                if (dataReader["Usuario_Nick"] != DBNull.Value) usuario.Nick = dataReader["Usuario_Nick"].ToString();
                if (dataReader["Usuario_Password"] != DBNull.Value) usuario.Password = dataReader["Usuario_Password"].ToString();
                if (dataReader["Usuario_Nombre"] != DBNull.Value) usuario.Nombre = dataReader["Usuario_Nombre"].ToString();
                if (dataReader["Usuario_Email"] != DBNull.Value) usuario.Email = dataReader["Usuario_Email"].ToString();
                if (dataReader["Usuario_Tipo"] != DBNull.Value) usuario.Tipo = int.Parse(dataReader["Usuario_Tipo"].ToString());
                if (dataReader["Usuario_Creacion"] != DBNull.Value) usuario.Creacion = (DateTime)dataReader["Usuario_Creacion"];
                if (dataReader["Usuario_Estatus"] != DBNull.Value) usuario.Estatus = int.Parse(dataReader["Usuario_Estatus"].ToString());
            }
            return usuario;
        }
        
        public Resultado CrearUsuario(Usuario p_usuario)
        {
            string password = string.Empty;
            EncripcionMD5 _MD5 = new EncripcionMD5();
            password = _MD5.Md5Hash(password);
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@Nick", p_usuario.Nick));
            _Parametros.Add(new SqlParameter("@Password", password));
            _Parametros.Add(new SqlParameter("@Nombre", p_usuario.Nombre));
            _Parametros.Add(new SqlParameter("@Email", p_usuario.Email));
            _Parametros.Add(new SqlParameter("@Tipo", p_usuario.Tipo));
            _Parametros.Add(new SqlParameter("@Estatus", p_usuario.Estatus));
            Conexion.PrepararProcedimiento("[dbo].[usuarios.Crear]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = long.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        public Resultado ModificarUsuario(Usuario p_usuario)
        {
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@ID", p_usuario.ID));
            _Parametros.Add(new SqlParameter("@Nick", p_usuario.Nick));
            _Parametros.Add(new SqlParameter("@Nombre", p_usuario.Nombre));
            _Parametros.Add(new SqlParameter("@Tipo", p_usuario.Tipo));
            _Parametros.Add(new SqlParameter("@Email", p_usuario.Email));
            _Parametros.Add(new SqlParameter("@Estatus", p_usuario.Estatus));
            Conexion.PrepararProcedimiento("[dbo].[usuarios.Modificar]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);

                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = int.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        public Resultado ModificarUsuarioPerfil(Usuario p_usuario)
        {
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@ID", p_usuario.ID));
            _Parametros.Add(new SqlParameter("@Nick", p_usuario.Nick));
            _Parametros.Add(new SqlParameter("@Nombre", p_usuario.Nombre));
            _Parametros.Add(new SqlParameter("@Email", p_usuario.Email));
            Conexion.PrepararProcedimiento("[dbo].[usuarios.ModificarPerfil]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);

                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = int.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        public Resultado CodigoRecuperacion(string Email)
        {
            EncripcionSHA256 Cripto = new EncripcionSHA256();
            string token = Cripto.SALTHash(Guid.NewGuid().ToString());
            token = token.Replace("/", "");
            token = token.Replace(@"\", "");
            token = token.Replace("=", "");

            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@Email", Email));
            _Parametros.Add(new SqlParameter("@Token", token));
            Conexion.PrepararProcedimiento("[dbo].[usuarios.Creartoken]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);

                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = int.Parse(dataReader["Resultado"].ToString());
                    if (resultado.ID > 0)
                    {
                        EnvioCorreos.CorreoRecuperacion(Email, token);
                    }
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        public Resultado EliminarUsuario(long idUsuario)
        {
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@ID", idUsuario));
            Conexion.PrepararProcedimiento("[dbo].[usuarios.Eliminar]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = int.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        public Resultado RestablecerPassword(long idUsuario)
        {
            string password = string.Empty;
            EncripcionMD5 _MD5 = new EncripcionMD5();
            password = _MD5.Md5Hash("qwerty");
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@ID", idUsuario));
            _Parametros.Add(new SqlParameter("@Password", password));
            Conexion.PrepararProcedimiento("[dbo].[usuarios.RestablecerPassword]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = int.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        public Resultado ActualizaPassword(ActualizaPassword actualiza)
        {
            string password = string.Empty;
            EncripcionMD5 _MD5 = new EncripcionMD5();
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@ID", actualiza.ID));
            _Parametros.Add(new SqlParameter("@ActualPassword", _MD5.Md5Hash(actualiza.ActualPassword)));
            _Parametros.Add(new SqlParameter("@NuevoPassword", _MD5.Md5Hash(actualiza.NuevoPassword)));

            Conexion.PrepararProcedimiento("[dbo].[usuarios.ActualizaPassword]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = int.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        public Usuario Login(string Nick, string Password)
        {
            Usuario usuario = new Usuario();
            EncripcionMD5 _MD5 = new EncripcionMD5();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@Nick", Nick));
            _Parametros.Add(new SqlParameter("@Password", _MD5.Md5Hash(Password)));
            Conexion.PrepararProcedimiento("[dbo].[usuarios.Login]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                if (dataReader["Usuario_ID"] != DBNull.Value) usuario.ID = long.Parse(dataReader["Usuario_ID"].ToString());
                if (dataReader["Usuario_Nick"] != DBNull.Value) usuario.Nick = dataReader["Usuario_Nick"].ToString();
                if (dataReader["Usuario_Password"] != DBNull.Value) usuario.Password = dataReader["Usuario_Password"].ToString();
                if (dataReader["Usuario_Nombre"] != DBNull.Value) usuario.Nombre = dataReader["Usuario_Nombre"].ToString();
                if (dataReader["Usuario_Email"] != DBNull.Value) usuario.Email = dataReader["Usuario_Email"].ToString();
                if (dataReader["Usuario_Creacion"] != DBNull.Value) usuario.Creacion = (DateTime)dataReader["Usuario_Creacion"];
                if (dataReader["Usuario_Estatus"] != DBNull.Value) usuario.Estatus = int.Parse(dataReader["Usuario_Estatus"].ToString());
                if (dataReader["Tipo_Usuario"] != DBNull.Value) usuario.Tipo = int.Parse(dataReader["Tipo_Usuario"].ToString());
                
            }
            return usuario;
        }

        public Resultado actualizapasspublico(Usuario p_usuario)
        {
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@Password", p_usuario.Password));
            _Parametros.Add(new SqlParameter("@Token", p_usuario.Token));
            Conexion.PrepararProcedimiento("[dbo].[usuarios.actualizapasspublico]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = long.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        #endregion Operaciones

        #region IDisposable Members

        public new void Dispose()
        {
            try
            {
                GC.Collect();
            }
            catch { }
        }

        #endregion
    }
}
