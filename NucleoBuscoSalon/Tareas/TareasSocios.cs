﻿using NucleoBuscoSalon.Contratos;
using NucleoBuscoSalon.Utilerias;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NucleoBuscoSalon.Tareas
{
    class TareasSocios : TComun
    {
        #region Constructores

        public TareasSocios() : base() { }
        public TareasSocios(SqlConexion sql) : base(sql) { }
        public TareasSocios(Boolean ConSqlConexion) : base(ConSqlConexion) { }
        public TareasSocios(TComun tcomun) : base(tcomun) { }

        #endregion

        #region Operaciones

        public List<Socio> ConsultarSocios(string filtroBusqueda, int tipo)
        {
            if (filtroBusqueda == "---sindatos---") filtroBusqueda = "";
            List<Socio> lstSocios = new List<Socio>();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@FiltroBusqueda", filtroBusqueda));
            _Parametros.Add(new SqlParameter("@TipoSocio", tipo));
            Conexion.PrepararProcedimiento("[dbo].[socios.Consultar]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                Socio socio = new Socio();
                if (dataReader["Socio_ID"] != DBNull.Value) socio.ID = long.Parse(dataReader["Socio_ID"].ToString());
                if (dataReader["Tipos_Socio_ID"] != DBNull.Value) socio.Tipo = int.Parse(dataReader["Tipos_Socio_ID"].ToString());
                if (dataReader["Socio_Nick"] != DBNull.Value) socio.Nick = dataReader["Socio_Nick"].ToString();
                if (dataReader["Socio_Password"] != DBNull.Value) socio.Password = dataReader["Socio_Password"].ToString();
                if (dataReader["Socio_Nombre"] != DBNull.Value) socio.Nombre = dataReader["Socio_Nombre"].ToString();
                if (dataReader["Socio_Paterno"] != DBNull.Value) socio.Paterno = dataReader["Socio_Paterno"].ToString();
                if (dataReader["Socio_Materno"] != DBNull.Value) socio.Materno = dataReader["Socio_Materno"].ToString();
                if (dataReader["Socio_Sexo"] != DBNull.Value) socio.Sexo = int.Parse(dataReader["Socio_Sexo"].ToString());
                if (dataReader["Socio_Email"] != DBNull.Value) socio.Email = dataReader["Socio_Email"].ToString();
                if (dataReader["Socio_Acepta_Pago_Tarjeta"] != DBNull.Value) socio.Acepta_Pago_Tarjeta = int.Parse(dataReader["Socio_Acepta_Pago_Tarjeta"].ToString());
                if (dataReader["Socio_Cuenta_Clabe"] != DBNull.Value) socio.Cuenta_Clabe = dataReader["Socio_Cuenta_Clabe"].ToString();
                if (dataReader["Socio_Fecha_Alta"] != DBNull.Value) socio.Fecha_Alta = DateTime.Parse(dataReader["Socio_Fecha_Alta"].ToString());
                if (dataReader["Socio_Numero_Lugares"] != DBNull.Value) socio.Numero_Lugares = int.Parse(dataReader["Socio_Numero_Lugares"].ToString());
                if (dataReader["Socio_WhatsApp"] != DBNull.Value) socio.WhatsApp = dataReader["Socio_WhatsApp"].ToString();
                if (dataReader["Socio_Estatus"] != DBNull.Value) socio.Estatus = int.Parse(dataReader["Socio_Estatus"].ToString());
                lstSocios.Add(socio);
            }
            return lstSocios;
        }
        public List<Socio> ConsultarSociosSinActivar(string filtroBusqueda, int tipo)
        {
            if (filtroBusqueda == "---sindatos---") filtroBusqueda = "";
            List<Socio> lstSocios = new List<Socio>();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@FiltroBusqueda", filtroBusqueda));
            _Parametros.Add(new SqlParameter("@TipoSocio", tipo));
            Conexion.PrepararProcedimiento("[dbo].[socios.ConsultarSinActivar]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                Socio socio = new Socio();
                if (dataReader["Socio_ID"] != DBNull.Value) socio.ID = long.Parse(dataReader["Socio_ID"].ToString());
                if (dataReader["Tipos_Socio_ID"] != DBNull.Value) socio.Tipo = int.Parse(dataReader["Tipos_Socio_ID"].ToString());
                if (dataReader["Socio_Nick"] != DBNull.Value) socio.Nick = dataReader["Socio_Nick"].ToString();
                if (dataReader["Socio_Password"] != DBNull.Value) socio.Password = dataReader["Socio_Password"].ToString();
                if (dataReader["Socio_Nombre"] != DBNull.Value) socio.Nombre = dataReader["Socio_Nombre"].ToString();
                if (dataReader["Socio_Paterno"] != DBNull.Value) socio.Paterno = dataReader["Socio_Paterno"].ToString();
                if (dataReader["Socio_Materno"] != DBNull.Value) socio.Materno = dataReader["Socio_Materno"].ToString();
                if (dataReader["Socio_Sexo"] != DBNull.Value) socio.Sexo = int.Parse(dataReader["Socio_Sexo"].ToString());
                if (dataReader["Socio_Email"] != DBNull.Value) socio.Email = dataReader["Socio_Email"].ToString();
                if (dataReader["Socio_Acepta_Pago_Tarjeta"] != DBNull.Value) socio.Acepta_Pago_Tarjeta = int.Parse(dataReader["Socio_Acepta_Pago_Tarjeta"].ToString());
                if (dataReader["Socio_Cuenta_Clabe"] != DBNull.Value) socio.Cuenta_Clabe = dataReader["Socio_Cuenta_Clabe"].ToString();
                if (dataReader["Socio_Fecha_Alta"] != DBNull.Value) socio.Fecha_Alta = DateTime.Parse(dataReader["Socio_Fecha_Alta"].ToString());
                if (dataReader["Socio_Numero_Lugares"] != DBNull.Value) socio.Numero_Lugares = int.Parse(dataReader["Socio_Numero_Lugares"].ToString());
                if (dataReader["Socio_WhatsApp"] != DBNull.Value) socio.WhatsApp = dataReader["Socio_WhatsApp"].ToString();
                if (dataReader["Socio_Estatus"] != DBNull.Value) socio.Estatus = int.Parse(dataReader["Socio_Estatus"].ToString());
                lstSocios.Add(socio);
            }
            return lstSocios;
        }

        public Socio ConsultarSocioPorID(long idSocio)
        {
            Socio socio = new Socio();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@ID", idSocio));
            Conexion.PrepararProcedimiento("[dbo].[socios.ConsultarPorID]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                if (dataReader["Socio_ID"] != DBNull.Value) socio.ID = long.Parse(dataReader["Socio_ID"].ToString());
                if (dataReader["Tipos_Socio_ID"] != DBNull.Value) socio.Tipo = int.Parse(dataReader["Tipos_Socio_ID"].ToString());
                if (dataReader["Socio_Nick"] != DBNull.Value) socio.Nick = dataReader["Socio_Nick"].ToString();
                if (dataReader["Socio_Password"] != DBNull.Value) socio.Password = dataReader["Socio_Password"].ToString();
                if (dataReader["Socio_Nombre"] != DBNull.Value) socio.Nombre = dataReader["Socio_Nombre"].ToString();
                if (dataReader["Socio_Paterno"] != DBNull.Value) socio.Paterno = dataReader["Socio_Paterno"].ToString();
                if (dataReader["Socio_Materno"] != DBNull.Value) socio.Materno = dataReader["Socio_Materno"].ToString();
                if (dataReader["Socio_Sexo"] != DBNull.Value) socio.Sexo = int.Parse(dataReader["Socio_Sexo"].ToString());
                if (dataReader["Socio_Email"] != DBNull.Value) socio.Email = dataReader["Socio_Email"].ToString();
                if (dataReader["Socio_Acepta_Pago_Tarjeta"] != DBNull.Value) socio.Acepta_Pago_Tarjeta = int.Parse(dataReader["Socio_Acepta_Pago_Tarjeta"].ToString());
                if (dataReader["Socio_Cuenta_Clabe"] != DBNull.Value) socio.Cuenta_Clabe = dataReader["Socio_Cuenta_Clabe"].ToString();
                if (dataReader["Socio_Fecha_Alta"] != DBNull.Value) socio.Fecha_Alta = DateTime.Parse(dataReader["Socio_Fecha_Alta"].ToString());
                if (dataReader["Socio_Numero_Lugares"] != DBNull.Value) socio.Numero_Lugares = int.Parse(dataReader["Socio_Numero_Lugares"].ToString());
                if (dataReader["Socio_Estatus"] != DBNull.Value) socio.Estatus = int.Parse(dataReader["Socio_Estatus"].ToString());
                if (dataReader["Socio_NoReferencia"] != DBNull.Value) socio.NoReferencia = dataReader["Socio_NoReferencia"].ToString();
                if (dataReader["Socio_NoSalones"] != DBNull.Value) socio.NoSalones = int.Parse(dataReader["Socio_NoSalones"].ToString());
                if (dataReader["Socio_WhatsApp"] != DBNull.Value) socio.WhatsApp = dataReader["Socio_WhatsApp"].ToString();

            }
            return socio;
        }

        public List<Socio> ConsultaNickEmailSocio(Socio p_socio)
        {
            List<Socio> lstSocios = new List<Socio>();
            List<SqlParameter> _Parametros = new List<SqlParameter>();

            _Parametros.Add(new SqlParameter("@Id", p_socio.ID));

            if (!String.IsNullOrEmpty(p_socio.Nick))
                _Parametros.Add(new SqlParameter("@Nick", p_socio.Nick));

            if (!String.IsNullOrEmpty(p_socio.Email))
                _Parametros.Add(new SqlParameter("@Email", p_socio.Email));

            Conexion.PrepararProcedimiento("[dbo].[socios.ConsultaNickEmail]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                Socio Renglon = new Socio();
                if (dataReader["ID"] != DBNull.Value) Renglon.ID = long.Parse(dataReader["ID"].ToString());
                if (dataReader["Nick"] != DBNull.Value) Renglon.Nick = dataReader["Nick"].ToString();
                if (dataReader["Password"] != DBNull.Value) Renglon.Password = dataReader["Password"].ToString();
                if (dataReader["Nombre"] != DBNull.Value) Renglon.Nombre = dataReader["Nombre"].ToString();
                if (dataReader["Email"] != DBNull.Value) Renglon.Email = dataReader["Email"].ToString();
                if (dataReader["Tipo"] != DBNull.Value) Renglon.Tipo = int.Parse(dataReader["Tipo"].ToString());
                if (dataReader["Estatus"] != DBNull.Value) Renglon.Estatus = int.Parse(dataReader["Estatus"].ToString());
                lstSocios.Add(Renglon);
            }
            return lstSocios;
        }

        public Resultado CrearSocio(Socio p_socio)
        {
            string password = string.Empty;
            EncripcionMD5 _MD5 = new EncripcionMD5();
            password = _MD5.Md5Hash(p_socio.Password.ToString());
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@Tipos_Socio_ID", p_socio.Tipo));
            _Parametros.Add(new SqlParameter("@Socio_Nick", p_socio.Nick));
            _Parametros.Add(new SqlParameter("@Socio_Password", password));
            _Parametros.Add(new SqlParameter("@Socio_Nombre", p_socio.Nombre));
            _Parametros.Add(new SqlParameter("@Socio_Paterno", p_socio.Paterno));
            _Parametros.Add(new SqlParameter("@Socio_Materno", p_socio.Materno));
            _Parametros.Add(new SqlParameter("@Socio_Sexo", p_socio.Sexo));
            _Parametros.Add(new SqlParameter("@Socio_Email", p_socio.Email));
            _Parametros.Add(new SqlParameter("@Socio_Acepta_Pago_Tarjeta", p_socio.Acepta_Pago_Tarjeta));
            _Parametros.Add(new SqlParameter("@Socio_Cuenta_Clabe", p_socio.Cuenta_Clabe));
            _Parametros.Add(new SqlParameter("@Socio_Fecha_Alta", DateTime.Now));
            _Parametros.Add(new SqlParameter("@Socio_Numero_Lugares", p_socio.Numero_Lugares));
            _Parametros.Add(new SqlParameter("@Socio_Estatus", p_socio.Estatus));
            _Parametros.Add(new SqlParameter("@Socio_NoSalones", p_socio.NoSalones));
            _Parametros.Add(new SqlParameter("@Socio_WhatsApp", p_socio.WhatsApp));
            Conexion.PrepararProcedimiento("[dbo].[socios.Crear]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value)
                    {
                        resultado.ID = long.Parse(dataReader["Resultado"].ToString());
                        EnvioCorreos.CorreoBienvenida(p_socio.Email, p_socio.Nombre);
                    }
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        public Resultado ModificarSocio(Socio p_socio)
        {
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@ID", p_socio.ID));
            _Parametros.Add(new SqlParameter("@Tipos_Socio_ID", p_socio.Tipo));
            _Parametros.Add(new SqlParameter("@Socio_Nick", p_socio.Nick));
            _Parametros.Add(new SqlParameter("@Socio_Nombre", p_socio.Nombre));
            _Parametros.Add(new SqlParameter("@Socio_Paterno", p_socio.Paterno));
            _Parametros.Add(new SqlParameter("@Socio_Materno", p_socio.Materno));
            _Parametros.Add(new SqlParameter("@Socio_Sexo", p_socio.Sexo));
            _Parametros.Add(new SqlParameter("@Socio_Email", p_socio.Email));
            _Parametros.Add(new SqlParameter("@Socio_Acepta_Pago_Tarjeta", p_socio.Acepta_Pago_Tarjeta));
            _Parametros.Add(new SqlParameter("@Socio_Cuenta_Clabe", p_socio.Cuenta_Clabe));
            _Parametros.Add(new SqlParameter("@Socio_WhatsApp", p_socio.WhatsApp));
            _Parametros.Add(new SqlParameter("@Socio_Estatus", p_socio.Estatus));
            Conexion.PrepararProcedimiento("[dbo].[socios.Modificar]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);

                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = int.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }
        public Resultado ModificarSocioPerfil(Socio p_socio)
        {
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@ID", p_socio.ID));
            _Parametros.Add(new SqlParameter("@Socio_Nick", p_socio.Nick));
            _Parametros.Add(new SqlParameter("@Socio_Nombre", p_socio.Nombre));
            _Parametros.Add(new SqlParameter("@Socio_Paterno", p_socio.Paterno));
            _Parametros.Add(new SqlParameter("@Socio_Materno", p_socio.Materno));
            _Parametros.Add(new SqlParameter("@Socio_Email", p_socio.Email));
            //_Parametros.Add(new SqlParameter("@Socio_Acepta_Pago_Tarjeta", p_socio.Acepta_Pago_Tarjeta));
            _Parametros.Add(new SqlParameter("@Socio_Cuenta_Clabe", p_socio.Cuenta_Clabe));
            _Parametros.Add(new SqlParameter("@Socio_NoSalones", p_socio.NoSalones));
            _Parametros.Add(new SqlParameter("@Socio_WhatsApp", p_socio.WhatsApp));
            Conexion.PrepararProcedimiento("[dbo].[socios.ModificarPerfil]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);

                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = int.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }
        public Resultado ActivarSocio(Socio p_socio)
        {
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@ID", p_socio.ID));
            _Parametros.Add(new SqlParameter("@Socio_Estatus", 1));
            _Parametros.Add(new SqlParameter("@NoReferencia", p_socio.NoReferencia));
            _Parametros.Add(new SqlParameter("@WhatsApp", p_socio.WhatsApp));
            Conexion.PrepararProcedimiento("[dbo].[socios.Activar]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);

                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = int.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        public Resultado EliminarSocio(long idSocio)
        {
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@ID", idSocio));
            Conexion.PrepararProcedimiento("[dbo].[socios.Eliminar]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = int.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        public Resultado RestablecerPassword(long idSocio)
        {
            string password = string.Empty;
            EncripcionMD5 _MD5 = new EncripcionMD5();
            password = _MD5.Md5Hash("qwerty");
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@ID", idSocio));
            _Parametros.Add(new SqlParameter("@Password", password));
            Conexion.PrepararProcedimiento("[dbo].[socios.RestablecerPassword]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = int.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        public Resultado ActualizaPassword(ActualizaPassword actualiza)
        {
            string password = string.Empty;
            EncripcionMD5 _MD5 = new EncripcionMD5();
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@ID", actualiza.ID));
            _Parametros.Add(new SqlParameter("@ActualPassword", _MD5.Md5Hash(actualiza.ActualPassword)));
            _Parametros.Add(new SqlParameter("@NuevoPassword", _MD5.Md5Hash(actualiza.NuevoPassword)));

            Conexion.PrepararProcedimiento("[dbo].[socios.ActualizaPassword]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = int.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }
        public Resultado ActualizaPasswordToken(Recuperacion p_recupera)
        {
            string password = string.Empty;
            EncripcionMD5 _MD5 = new EncripcionMD5();
            password = _MD5.Md5Hash(p_recupera.Password.ToString());
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@Token", p_recupera.Token));
            _Parametros.Add(new SqlParameter("@Password", password));
            Conexion.PrepararProcedimiento("[dbo].[socios.ActualizaPasswordToken]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value)
                    {
                        resultado.ID = long.Parse(dataReader["Resultado"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        public Socio Login(string Nick, string Password)
        {
            Socio socio = new Socio();
            EncripcionMD5 _MD5 = new EncripcionMD5();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@Nick", Nick));
            _Parametros.Add(new SqlParameter("@Password", _MD5.Md5Hash(Password)));
            Conexion.PrepararProcedimiento("[dbo].[socios.Login]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                if (dataReader["Socio_ID"] != DBNull.Value) socio.ID = long.Parse(dataReader["Socio_ID"].ToString());
                if (dataReader["Tipos_Socio_ID"] != DBNull.Value) socio.Tipo = int.Parse(dataReader["Tipos_Socio_ID"].ToString());
                if (dataReader["Socio_Nick"] != DBNull.Value) socio.Nick = dataReader["Socio_Nick"].ToString();
                if (dataReader["Socio_Password"] != DBNull.Value) socio.Password = dataReader["Socio_Password"].ToString();
                if (dataReader["Socio_Nombre"] != DBNull.Value) socio.Nombre = dataReader["Socio_Nombre"].ToString();
                if (dataReader["Socio_Paterno"] != DBNull.Value) socio.Paterno = dataReader["Socio_Paterno"].ToString();
                if (dataReader["Socio_Materno"] != DBNull.Value) socio.Materno = dataReader["Socio_Materno"].ToString();
                if (dataReader["Socio_Sexo"] != DBNull.Value) socio.Sexo = int.Parse(dataReader["Socio_Sexo"].ToString());
                if (dataReader["Socio_Email"] != DBNull.Value) socio.Email = dataReader["Socio_Email"].ToString();
                if (dataReader["Socio_Acepta_Pago_Tarjeta"] != DBNull.Value) socio.Acepta_Pago_Tarjeta = int.Parse(dataReader["Socio_Acepta_Pago_Tarjeta"].ToString());
                if (dataReader["Socio_Cuenta_Clabe"] != DBNull.Value) socio.Cuenta_Clabe = dataReader["Socio_Cuenta_Clabe"].ToString();
                if (dataReader["Socio_Fecha_Alta"] != DBNull.Value) socio.Fecha_Alta = DateTime.Parse(dataReader["Socio_Fecha_Alta"].ToString());
                if (dataReader["Socio_Numero_Lugares"] != DBNull.Value) socio.Numero_Lugares = int.Parse(dataReader["Socio_Numero_Lugares"].ToString());
                if (dataReader["Socio_WhatsApp"] != DBNull.Value) socio.WhatsApp = dataReader["Socio_WhatsApp"].ToString();
                if (dataReader["Socio_Estatus"] != DBNull.Value) socio.Estatus = int.Parse(dataReader["Socio_Estatus"].ToString());
            }
            return socio;
        }

        #endregion Operaciones

        #region IDisposable Members

        public new void Dispose()
        {
            try
            {
                GC.Collect();
            }
            catch { }
        }

        #endregion
    }
}
