﻿using NucleoBuscoSalon.Contratos;
using NucleoBuscoSalon.Models;
using NucleoBuscoSalon.Utilerias;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NucleoBuscoSalon.Tareas
{
    class TareasInmuebles : TComun
    {
        #region Constructores

        public TareasInmuebles() : base() { }
        public TareasInmuebles(SqlConexion sql) : base(sql) { }
        public TareasInmuebles(Boolean ConSqlConexion) : base(ConSqlConexion) { }
        public TareasInmuebles(TComun tcomun) : base(tcomun) { }

        #endregion

        #region Operaciones

        public List<Inmueble> ConsultarInmuebles(string filtroBusqueda, long idSocio)
        {
            if (filtroBusqueda == "---sindatos---") filtroBusqueda = "";
            List<Inmueble> lstInmuebles = new List<Inmueble>();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@SocioID", idSocio));
            _Parametros.Add(new SqlParameter("@FiltroBusqueda", filtroBusqueda));
            Conexion.PrepararProcedimiento("[dbo].[inmuebles.Consultar]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                Inmueble inmueble = new Inmueble();
                if (dataReader["Inmueble_ID"] != DBNull.Value) inmueble.ID = long.Parse(dataReader["Inmueble_ID"].ToString());
                if (dataReader["Ciudad_ID"] != DBNull.Value) inmueble.CiudadID = long.Parse(dataReader["Ciudad_ID"].ToString());
                if (dataReader["Ciudad_Nombre"] != DBNull.Value) inmueble.DescripcionCiudad = dataReader["Ciudad_Nombre"].ToString();
                if (dataReader["Estado_ID"] != DBNull.Value) inmueble.EstadoID = long.Parse(dataReader["Estado_ID"].ToString());
                if (dataReader["Estado_Nombre"] != DBNull.Value) inmueble.DescripcionEstado = dataReader["Estado_Nombre"].ToString();
                if (dataReader["Socio_ID"] != DBNull.Value) inmueble.SocioID = long.Parse(dataReader["Socio_ID"].ToString());
                if (dataReader["Nombre"] != DBNull.Value) inmueble.Nombre = dataReader["Nombre"].ToString();
                if (dataReader["Direccion"] != DBNull.Value) inmueble.Direccion = dataReader["Direccion"].ToString();
                if (dataReader["Colonia_ID"] != DBNull.Value) inmueble.ColoniaID = long.Parse(dataReader["Colonia_ID"].ToString());
                if (dataReader["Colonia"] != DBNull.Value) inmueble.DescripcionColonia = dataReader["Colonia"].ToString();
                if (dataReader["Referencias"] != DBNull.Value) inmueble.Referencias = dataReader["Referencias"].ToString();
                if (dataReader["Ambiente_ID"] != DBNull.Value) inmueble.AmbienteID = int.Parse(dataReader["Ambiente_ID"].ToString());
                if (dataReader["TipoAmbienteNombre"] != DBNull.Value) inmueble.DescripcionAmbiente = dataReader["TipoAmbienteNombre"].ToString();
                if (dataReader["Capacidad"] != DBNull.Value) inmueble.Capacidad = int.Parse(dataReader["Capacidad"].ToString());
                if (dataReader["Costo"] != DBNull.Value) inmueble.Costo = decimal.Parse(dataReader["Costo"].ToString());
                if (dataReader["Calificacion_Promedio"] != DBNull.Value) inmueble.CalificacionPromedio = float.Parse(dataReader["Calificacion_Promedio"].ToString());
                if (dataReader["Estatus"] != DBNull.Value) inmueble.Estatus = int.Parse(dataReader["Estatus"].ToString());
                if (dataReader["Horario"] != DBNull.Value) inmueble.Horario = dataReader["Horario"].ToString();
                if (dataReader["Correo_Electronico"] != DBNull.Value) inmueble.CorreoElectronico = dataReader["Correo_Electronico"].ToString();
                if (dataReader["Telefonos"] != DBNull.Value) inmueble.Telefono = dataReader["Telefonos"].ToString();
                if (dataReader["Localizacion"] != DBNull.Value) inmueble.Localizacion = dataReader["Localizacion"].ToString();
                lstInmuebles.Add(inmueble);
            }
            return lstInmuebles;
        }

        public Inmueble ConsultarInmueblePorID(long idInmueble)
        {
            Inmueble inmueble = new Inmueble();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@ID", idInmueble));
            Conexion.PrepararProcedimiento("[dbo].[inmuebles.ConsultarPorID]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                if (dataReader["Inmueble_ID"] != DBNull.Value) inmueble.ID = long.Parse(dataReader["Inmueble_ID"].ToString());
                if (dataReader["Ciudad_ID"] != DBNull.Value) inmueble.CiudadID = long.Parse(dataReader["Ciudad_ID"].ToString());
                if (dataReader["Ciudad_Nombre"] != DBNull.Value) inmueble.DescripcionCiudad = dataReader["Ciudad_Nombre"].ToString();
                if (dataReader["Estado_ID"] != DBNull.Value) inmueble.EstadoID = long.Parse(dataReader["Estado_ID"].ToString());
                if (dataReader["Estado_Nombre"] != DBNull.Value) inmueble.DescripcionEstado = dataReader["Estado_Nombre"].ToString();
                if (dataReader["Socio_ID"] != DBNull.Value) inmueble.SocioID = long.Parse(dataReader["Socio_ID"].ToString());
                if (dataReader["Nombre"] != DBNull.Value) inmueble.Nombre = dataReader["Nombre"].ToString();
                if (dataReader["Direccion"] != DBNull.Value) inmueble.Direccion = dataReader["Direccion"].ToString();
                if (dataReader["Colonia_ID"] != DBNull.Value) inmueble.ColoniaID = long.Parse(dataReader["Colonia_ID"].ToString());
                if (dataReader["Colonia"] != DBNull.Value) inmueble.DescripcionColonia = dataReader["Colonia"].ToString();
                if (dataReader["Referencias"] != DBNull.Value) inmueble.Referencias = dataReader["Referencias"].ToString();
                if (dataReader["Ambiente_ID"] != DBNull.Value) inmueble.AmbienteID = int.Parse(dataReader["Ambiente_ID"].ToString());
                if (dataReader["Colonia"] != DBNull.Value) inmueble.DescripcionAmbiente = dataReader["Ambiente_Nombre"].ToString();
                if (dataReader["Capacidad"] != DBNull.Value) inmueble.Capacidad = int.Parse(dataReader["Capacidad"].ToString());
                if (dataReader["Costo"] != DBNull.Value) inmueble.Costo = decimal.Parse(dataReader["Costo"].ToString());
                if (dataReader["Calificacion_Promedio"] != DBNull.Value) inmueble.CalificacionPromedio = float.Parse(dataReader["Calificacion_Promedio"].ToString());
                if (dataReader["Estatus"] != DBNull.Value) inmueble.Estatus = int.Parse(dataReader["Estatus"].ToString());
                if (dataReader["Horario"] != DBNull.Value) inmueble.Horario = dataReader["Horario"].ToString();
<<<<<<< HEAD
                if (dataReader["Correo_Electronico"] != DBNull.Value) inmueble.CorreoElectronico = dataReader["Correo_Electronico"].ToString();
                if (dataReader["Telefonos"] != DBNull.Value) inmueble.Telefono = dataReader["Telefonos"].ToString();
                if (dataReader["Localizacion"] != DBNull.Value) inmueble.Localizacion = dataReader["Localizacion"].ToString();
=======
                inmueble.Socio = new Socio();
                
                if (dataReader["Socio_ID"] != DBNull.Value) inmueble.Socio.ID = long.Parse(dataReader["Socio_ID"].ToString());
                if (dataReader["Email"] != DBNull.Value) inmueble.Socio.Email = dataReader["Email"].ToString();
                if (dataReader["Socio"] != DBNull.Value) inmueble.Socio.Nombre = dataReader["Socio"].ToString();
>>>>>>> c11f0433e84b1e4328d92c5fdfb1cd2198976424
            }
            return inmueble;
        }

        public Resultado CrearInmueble(Inmueble p_inmueble)
        {
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@CiudadID", p_inmueble.CiudadID));
            _Parametros.Add(new SqlParameter("@SocioID", p_inmueble.SocioID));
            _Parametros.Add(new SqlParameter("@Nombre", p_inmueble.Nombre));
            _Parametros.Add(new SqlParameter("@Direccion", p_inmueble.Direccion));
            _Parametros.Add(new SqlParameter("@ColoniaID", p_inmueble.ColoniaID));
            _Parametros.Add(new SqlParameter("@Colonia", p_inmueble.DescripcionColonia));
            _Parametros.Add(new SqlParameter("@Referencias", p_inmueble.Referencias));
            _Parametros.Add(new SqlParameter("@AmbienteID", p_inmueble.AmbienteID));
            _Parametros.Add(new SqlParameter("@Capacidad", p_inmueble.Capacidad));
            _Parametros.Add(new SqlParameter("@Horario", p_inmueble.Horario));
            _Parametros.Add(new SqlParameter("@Costo", p_inmueble.Costo));
            _Parametros.Add(new SqlParameter("@Correo_Electronico", p_inmueble.CorreoElectronico));
            _Parametros.Add(new SqlParameter("@Telefonos", p_inmueble.Telefono));
            _Parametros.Add(new SqlParameter("@Localizacion", p_inmueble.Localizacion));
            _Parametros.Add(new SqlParameter("@Estatus", p_inmueble.Estatus));
            _Parametros.Add(new SqlParameter("@UsuarioCreo", p_inmueble.Usuario));
            Conexion.PrepararProcedimiento("[dbo].[inmuebles.Crear]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = long.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        public Resultado ModificarInmueble(Inmueble p_inmueble)
        {
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@ID", p_inmueble.ID));
            _Parametros.Add(new SqlParameter("@CiudadID", p_inmueble.CiudadID));
            _Parametros.Add(new SqlParameter("@Nombre", p_inmueble.Nombre));
            _Parametros.Add(new SqlParameter("@Direccion", p_inmueble.Direccion));
            _Parametros.Add(new SqlParameter("@ColoniaID", p_inmueble.ColoniaID));
            _Parametros.Add(new SqlParameter("@Colonia", p_inmueble.DescripcionColonia));
            _Parametros.Add(new SqlParameter("@Referencias", p_inmueble.Referencias));
            _Parametros.Add(new SqlParameter("@AmbienteID", p_inmueble.AmbienteID));
            _Parametros.Add(new SqlParameter("@Capacidad", p_inmueble.Capacidad));
            _Parametros.Add(new SqlParameter("@Horario", p_inmueble.Horario));
            _Parametros.Add(new SqlParameter("@Costo", p_inmueble.Costo));
            _Parametros.Add(new SqlParameter("@Correo_Electronico", p_inmueble.CorreoElectronico));
            _Parametros.Add(new SqlParameter("@Telefonos", p_inmueble.Telefono));
            _Parametros.Add(new SqlParameter("@Localizacion", p_inmueble.Localizacion));
            _Parametros.Add(new SqlParameter("@Estatus", p_inmueble.Estatus));
            _Parametros.Add(new SqlParameter("@UsuarioModifico", p_inmueble.Usuario));
            Conexion.PrepararProcedimiento("[dbo].[inmuebles.Modificar]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);

                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = int.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        public Resultado EliminarInmueble(long idSocio)
        {
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@ID", idSocio));
            Conexion.PrepararProcedimiento("[dbo].[inmuebles.Eliminar]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = int.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        #endregion Operaciones

        #region Catalogos

        public List<Colonia> ConsultarColoniasPorCP(string codigopostal)
        {
            if (codigopostal == "---sindatos---") codigopostal = "";
            List<Colonia> lstColonias = new List<Colonia>();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@CodigoPostal", codigopostal));
            Conexion.PrepararProcedimiento("[dbo].[catalogos.ObteneColoniasPorCP]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                Colonia colonia = new Colonia();
                if (dataReader["ColoniaID"] != DBNull.Value) colonia.ColoniaID = long.Parse(dataReader["ColoniaID"].ToString());
                if (dataReader["ColoniaNombre"] != DBNull.Value) colonia.NombreColonia = dataReader["ColoniaNombre"].ToString();
                if (dataReader["CiudadID"] != DBNull.Value) colonia.CiudadID = long.Parse(dataReader["CiudadID"].ToString());
                if (dataReader["CiudadNombre"] != DBNull.Value) colonia.NombreCiudad = dataReader["CiudadNombre"].ToString();
                if (dataReader["EstadoID"] != DBNull.Value) colonia.EstadoID = long.Parse(dataReader["EstadoID"].ToString());
                if (dataReader["EstadoNombre"] != DBNull.Value) colonia.NombreEstado = dataReader["EstadoNombre"].ToString();
                lstColonias.Add(colonia);
            }
            return lstColonias;
        }
        public List<Ambiente> ConsultarTiposAmbientes()
        {
            List<Ambiente> lstTiposAmbientes = new List<Ambiente>();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            Conexion.PrepararProcedimiento("[catalogos.ObtenerTiposAmbientes]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                Ambiente tipoAmbiente = new Ambiente();
                if (dataReader["Ambiente_ID"] != DBNull.Value) tipoAmbiente.ID = int.Parse(dataReader["Ambiente_ID"].ToString());
                if (dataReader["Nombre"] != DBNull.Value) tipoAmbiente.Nombre = dataReader["Nombre"].ToString();
                lstTiposAmbientes.Add(tipoAmbiente);
            }
            return lstTiposAmbientes;
        }
        public List<Estado> ConsultarEstadosPorPais(long ID)
        {
            List<Estado> lstEstados = new List<Estado>();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@PaisID", ID));
            Conexion.PrepararProcedimiento("[dbo].[catalogos.ObtenerEstadosPorPais]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                Estado estado = new Estado();
                if (dataReader["Estado_ID"] != DBNull.Value) estado.ID = int.Parse(dataReader["Estado_ID"].ToString());
                if (dataReader["Estado_Nombre"] != DBNull.Value) estado.Nombre = dataReader["Estado_Nombre"].ToString();
                lstEstados.Add(estado);
            }
            return lstEstados;
        }
        public List<Ciudad> ConsultarCiudadesPorEstado(long ID)
        {
            List<Ciudad> lstCiudades = new List<Ciudad>();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@EstadoID", ID));
            Conexion.PrepararProcedimiento("[dbo].[catalogos.ObtenerCiudadesPorEstado]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                Ciudad ciudad = new Ciudad();
                if (dataReader["Ciudad_ID"] != DBNull.Value) ciudad.ID = int.Parse(dataReader["Ciudad_ID"].ToString());
                if (dataReader["Ciudad_Nombre"] != DBNull.Value) ciudad.Nombre = dataReader["Ciudad_Nombre"].ToString();
                lstCiudades.Add(ciudad);
            }
            return lstCiudades;
        }
        public List<Colonia> ConsultarColoniasPorCiudad(long ID)
        {
            List<Colonia> lstColonias = new List<Colonia>();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@CiudadID", ID));
            Conexion.PrepararProcedimiento("[dbo].[catalogos.ObtenerColoniasPorCiudad]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                Colonia colonia = new Colonia();
                if (dataReader["Colonia_ID"] != DBNull.Value) colonia.ColoniaID = int.Parse(dataReader["Colonia_ID"].ToString());
                if (dataReader["Nombre"] != DBNull.Value) colonia.NombreColonia = ((dataReader["Tipo"] != DBNull.Value) ? dataReader["Tipo"].ToString() : "") + " " + dataReader["Nombre"].ToString();
                lstColonias.Add(colonia);
            }
            return lstColonias;
        }

        #endregion Catalogos

        #region Paquetes

        public List<Paquete> ConsultarPaquetes(long idInmueble)
        {
            List<Paquete> lstInmuebles = new List<Paquete>();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@InmuebleID", idInmueble));
            Conexion.PrepararProcedimiento("[dbo].[paquetes.Consultar]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                Paquete paquete = new Paquete();
                if (dataReader["Paquete_ID"] != DBNull.Value) paquete.ID = long.Parse(dataReader["Paquete_ID"].ToString());
                if (dataReader["Nombre"] != DBNull.Value) paquete.Nombre = dataReader["Nombre"].ToString();
                if (dataReader["Inmueble_ID"] != DBNull.Value) paquete.InmuebleID = long.Parse(dataReader["Inmueble_ID"].ToString());
                if (dataReader["Inmueble_Nombre"] != DBNull.Value) paquete.InmuebleNombre = dataReader["Inmueble_Nombre"].ToString();
                if (dataReader["Costo"] != DBNull.Value) paquete.Costo = float.Parse(dataReader["Costo"].ToString());
                if (dataReader["Incluye_Meseros"] != DBNull.Value) paquete.IncluyeMeseros = int.Parse(dataReader["Incluye_Meseros"].ToString());
                if (dataReader["Incluye_Mesas"] != DBNull.Value) paquete.IncluyeMesas = int.Parse(dataReader["Incluye_Mesas"].ToString());
                if (dataReader["Incluye_Manteleria"] != DBNull.Value) paquete.IncluyeManteleria = int.Parse(dataReader["Incluye_Manteleria"].ToString());
                if (dataReader["Otros_Datos"] != DBNull.Value) paquete.OtrosDatos = dataReader["Otros_Datos"].ToString();
                if (dataReader["Estatus"] != DBNull.Value) paquete.Estatus = int.Parse(dataReader["Estatus"].ToString());
                lstInmuebles.Add(paquete);
            }
            return lstInmuebles;
        }

        public Paquete ConsultarPaquetePorID(long idPaquete)
        {
            Paquete paquete = new Paquete();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@ID", idPaquete));
            Conexion.PrepararProcedimiento("[dbo].[paquetes.ConsultarPorID]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                if (dataReader["Paquete_ID"] != DBNull.Value) paquete.ID = long.Parse(dataReader["Paquete_ID"].ToString());
                if (dataReader["Nombre"] != DBNull.Value) paquete.Nombre = dataReader["Nombre"].ToString();
                if (dataReader["Inmueble_ID"] != DBNull.Value) paquete.InmuebleID = long.Parse(dataReader["Inmueble_ID"].ToString());
                if (dataReader["Inmueble_Nombre"] != DBNull.Value) paquete.InmuebleNombre = dataReader["Inmueble_Nombre"].ToString();
                if (dataReader["Costo"] != DBNull.Value) paquete.Costo = float.Parse(dataReader["Costo"].ToString());
                if (dataReader["Incluye_Meseros"] != DBNull.Value) paquete.IncluyeMeseros = int.Parse(dataReader["Incluye_Meseros"].ToString());
                if (dataReader["Incluye_Mesas"] != DBNull.Value) paquete.IncluyeMesas = int.Parse(dataReader["Incluye_Mesas"].ToString());
                if (dataReader["Incluye_Manteleria"] != DBNull.Value) paquete.IncluyeManteleria = int.Parse(dataReader["Incluye_Manteleria"].ToString());
                if (dataReader["Otros_Datos"] != DBNull.Value) paquete.OtrosDatos = dataReader["Otros_Datos"].ToString();
                if (dataReader["Estatus"] != DBNull.Value) paquete.Estatus = int.Parse(dataReader["Estatus"].ToString());
            }
            return paquete;
        }

        public Resultado CrearPaquete(Paquete p_Paquete)
        {
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@InmuebleID", p_Paquete.InmuebleID));
            _Parametros.Add(new SqlParameter("@Nombre", p_Paquete.Nombre));
            _Parametros.Add(new SqlParameter("@Costo", p_Paquete.Costo));
            _Parametros.Add(new SqlParameter("@Incluye_Meseros", p_Paquete.IncluyeMeseros));
            _Parametros.Add(new SqlParameter("@Incluye_Mesas", p_Paquete.IncluyeMesas));
            _Parametros.Add(new SqlParameter("@Incluye_Manteleria", p_Paquete.IncluyeManteleria));
            _Parametros.Add(new SqlParameter("@Otros_Datos", p_Paquete.OtrosDatos));
            _Parametros.Add(new SqlParameter("@Estatus", p_Paquete.Estatus));
            Conexion.PrepararProcedimiento("[dbo].[paquetes.Crear]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = long.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        public Resultado ModificarPaquete(Paquete p_Paquete)
        {
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@PaqueteID", p_Paquete.ID));
            _Parametros.Add(new SqlParameter("@InmuebleID", p_Paquete.InmuebleID));
            _Parametros.Add(new SqlParameter("@Nombre", p_Paquete.Nombre));
            _Parametros.Add(new SqlParameter("@Costo", p_Paquete.Costo));
            _Parametros.Add(new SqlParameter("@Incluye_Meseros", p_Paquete.IncluyeMeseros));
            _Parametros.Add(new SqlParameter("@Incluye_Mesas", p_Paquete.IncluyeMesas));
            _Parametros.Add(new SqlParameter("@Incluye_Manteleria", p_Paquete.IncluyeManteleria));
            _Parametros.Add(new SqlParameter("@Otros_Datos", p_Paquete.OtrosDatos));
            _Parametros.Add(new SqlParameter("@Estatus", p_Paquete.Estatus));
            Conexion.PrepararProcedimiento("[dbo].[paquetes.Modificar]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);

                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = int.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        public Resultado EliminarPaquete(long idPaquete)
        {
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@ID", idPaquete));
            Conexion.PrepararProcedimiento("[dbo].[paquetes.Eliminar]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = int.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        #endregion

        #region Imagenes

        public Resultado CrearImagen(Imagen p_imagen)
        {
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@InmuebleID", p_imagen.InmuebleID));
            _Parametros.Add(new SqlParameter("@Nombre", p_imagen.Nombre));
            _Parametros.Add(new SqlParameter("@Src", p_imagen.Src));;
            _Parametros.Add(new SqlParameter("@Estatus", p_imagen.Estatus));
            Conexion.PrepararProcedimiento("[dbo].[imagenes.Crear]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = long.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        public List<Imagen> ConsultarImagenes(long idInmueble)
        {
            List<Imagen> lstImagenes = new List<Imagen>();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@InmuebleID", idInmueble));
            Conexion.PrepararProcedimiento("[dbo].[imagenes.Consultar]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                Imagen imagen = new Imagen();
                if (dataReader["ImagenID"] != DBNull.Value) imagen.ID = long.Parse(dataReader["ImagenID"].ToString());
                if (dataReader["Inmueble_ID"] != DBNull.Value) imagen.InmuebleID = long.Parse(dataReader["Inmueble_ID"].ToString());
                if (dataReader["Nombre"] != DBNull.Value) imagen.Nombre = dataReader["Nombre"].ToString();
                if (dataReader["SRC"] != DBNull.Value) imagen.Src = dataReader["SRC"].ToString();
                if (dataReader["Estatus"] != DBNull.Value) imagen.Estatus = int.Parse(dataReader["Estatus"].ToString());
                lstImagenes.Add(imagen);
            }
            return lstImagenes;
        }

        public Resultado EliminarImagen(long idImagen)
        {
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@ID", idImagen));
            Conexion.PrepararProcedimiento("[dbo].[imagenes.Eliminar]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = int.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }
    
        #endregion

        #region Buscador

        public List<Inmueble> BuscarInmuebles(Parametro_Busqueda parametro)
        {
            List<Inmueble> lstInmuebles = new List<Inmueble>();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@FiltroEstado", parametro.FitroEstado));
            _Parametros.Add(new SqlParameter("@FiltroGeneral", parametro.FiltroGeneral));
            _Parametros.Add(new SqlParameter("@TipoAmbiente", parametro.TipoAmbiente));
            Conexion.PrepararProcedimiento("[dbo].[inmuebles.Buscador]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                Inmueble inmueble = new Inmueble();
                if (dataReader["Inmueble_ID"] != DBNull.Value) inmueble.ID = long.Parse(dataReader["Inmueble_ID"].ToString());
                if (dataReader["Ciudad_ID"] != DBNull.Value) inmueble.CiudadID = long.Parse(dataReader["Ciudad_ID"].ToString());
                if (dataReader["Ciudad_Nombre"] != DBNull.Value) inmueble.DescripcionCiudad = dataReader["Ciudad_Nombre"].ToString();
                if (dataReader["Estado_ID"] != DBNull.Value) inmueble.EstadoID = long.Parse(dataReader["Estado_ID"].ToString());
                if (dataReader["Estado_Nombre"] != DBNull.Value) inmueble.DescripcionEstado = dataReader["Estado_Nombre"].ToString();
                if (dataReader["Socio_ID"] != DBNull.Value) inmueble.SocioID = long.Parse(dataReader["Socio_ID"].ToString());
                if (dataReader["Nombre"] != DBNull.Value) inmueble.Nombre = dataReader["Nombre"].ToString();
                if (dataReader["Direccion"] != DBNull.Value) inmueble.Direccion = dataReader["Direccion"].ToString();
                if (dataReader["Colonia_ID"] != DBNull.Value) inmueble.ColoniaID = long.Parse(dataReader["Colonia_ID"].ToString());
                if (dataReader["Colonia"] != DBNull.Value) inmueble.DescripcionColonia = dataReader["Colonia"].ToString();
                if (dataReader["Referencias"] != DBNull.Value) inmueble.Referencias = dataReader["Referencias"].ToString();
                if (dataReader["Ambiente_ID"] != DBNull.Value) inmueble.AmbienteID = int.Parse(dataReader["Ambiente_ID"].ToString());
                if (dataReader["TipoAmbienteNombre"] != DBNull.Value) inmueble.DescripcionAmbiente = dataReader["TipoAmbienteNombre"].ToString();
                if (dataReader["Capacidad"] != DBNull.Value) inmueble.Capacidad = int.Parse(dataReader["Capacidad"].ToString());
                if (dataReader["Costo"] != DBNull.Value) inmueble.Costo = decimal.Parse(dataReader["Costo"].ToString());
                if(dataReader["Calificacion_Promedio"]!= DBNull.Value) inmueble.CalificacionPromedio = float.Parse(dataReader["Calificacion_Promedio"].ToString());
                if (dataReader["Estatus"] != DBNull.Value) inmueble.Estatus = int.Parse(dataReader["Estatus"].ToString());
                if (dataReader["Src"] != DBNull.Value) inmueble.Src = dataReader["Src"].ToString();
                if (dataReader["Horario"] != DBNull.Value) inmueble.Horario = dataReader["Horario"].ToString();
                if (dataReader["Correo_Electronico"] != DBNull.Value) inmueble.CorreoElectronico = dataReader["Correo_Electronico"].ToString();
                if (dataReader["Telefonos"] != DBNull.Value) inmueble.Telefono = dataReader["Telefonos"].ToString();
                if (dataReader["Localizacion"] != DBNull.Value) inmueble.Localizacion = dataReader["Localizacion"].ToString();
                lstInmuebles.Add(inmueble);
            }
            return lstInmuebles;
        }        

        #endregion

        #region IDisposable Members

        public new void Dispose()
        {
            try
            {
                GC.Collect();
            }
            catch { }
        }

        #endregion
    }
}

