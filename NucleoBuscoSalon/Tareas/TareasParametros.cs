﻿using NucleoBuscoSalon.Contratos;
using NucleoBuscoSalon.Models;
using NucleoBuscoSalon.Utilerias;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NucleoBuscoSalon.Tareas
{
    class TareasParametros : TComun
    {
        #region Constructores

        public TareasParametros() : base() { }
        public TareasParametros(SqlConexion sql) : base(sql) { }
        public TareasParametros(Boolean ConSqlConexion) : base(ConSqlConexion) { }
        public TareasParametros(TComun tcomun) : base(tcomun) { }

        #endregion

        #region Operaciones

        public Parametros ConsultarParametros()
        {
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            Parametros Parametro = new Parametros();

            Conexion.PrepararProcedimiento("[dbo].[parametros.Consultar]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                if (dataReader["Url_Imagen"] != DBNull.Value) Parametro.Url_Imagen =dataReader["Url_Imagen"].ToString();
                if (dataReader["Precio"] != DBNull.Value) Parametro.Precio = Convert.ToDecimal(dataReader["Precio"]);
                if (dataReader["Correos_CC"] != DBNull.Value) Parametro.Correos_CC = dataReader["Correos_CC"].ToString();
                if (dataReader["MensajeReservacion"] != DBNull.Value) Parametro.MensajeReservacion = dataReader["MensajeReservacion"].ToString();
            }
            return Parametro;
        }
        public Resultado ModificarParametros(Parametros p_parametro)
        {
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            
            _Parametros.Add(new SqlParameter("@Url_Imagen", p_parametro.Url_Imagen));
            _Parametros.Add(new SqlParameter("@Precio", p_parametro.Precio));
            _Parametros.Add(new SqlParameter("@Correos_CC", p_parametro.Correos_CC));
            _Parametros.Add(new SqlParameter("@MensajeReservacion", p_parametro.MensajeReservacion));
            Conexion.PrepararProcedimiento("[dbo].[parametros.Modificar]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);

                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = int.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }
        
        #endregion Operaciones

        #region IDisposable Members

        public new void Dispose()
        {
            try
            {
                GC.Collect();
            }
            catch { }
        }

        #endregion
    }
}
