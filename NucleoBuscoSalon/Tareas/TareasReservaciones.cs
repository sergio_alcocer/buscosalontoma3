﻿using NucleoBuscoSalon.Contratos;
using NucleoBuscoSalon.Utilerias;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NucleoBuscoSalon.Tareas
{
    class TareasReservaciones : TComun
    {
        #region Constructores

        public TareasReservaciones() : base() { }
        public TareasReservaciones(SqlConexion sql) : base(sql) { }
        public TareasReservaciones(Boolean ConSqlConexion) : base(ConSqlConexion) { }
        public TareasReservaciones(TComun tcomun) : base(tcomun) { }

        #endregion

        #region Operaciones

        public Resultado CrearReservacion(Reservacion p_reservacion)
        {
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@SocioID", p_reservacion.SocioID));
            _Parametros.Add(new SqlParameter("@Inmueble_ID", p_reservacion.InmuebleID));
            _Parametros.Add(new SqlParameter("@Paquete_ID", p_reservacion.PaqueteID));
            _Parametros.Add(new SqlParameter("@Fecha_Reservacion_Inicia", p_reservacion.FechaReservacionInicia));
            _Parametros.Add(new SqlParameter("@Fecha_Reservacion_Termina", p_reservacion.FechaReservacionTermina));
            _Parametros.Add(new SqlParameter("@Costo", p_reservacion.Costo));
            _Parametros.Add(new SqlParameter("@Estatus", p_reservacion.Estatus));
            _Parametros.Add(new SqlParameter("@SocioNombre", p_reservacion.SocioNombre));
            _Parametros.Add(new SqlParameter("@PaqueteNombre", p_reservacion.PaqueteNombre));
            Conexion.PrepararProcedimiento("[dbo].[reservaciones.Crear]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value)
                    {
                        resultado.ID = long.Parse(dataReader["Resultado"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        public Resultado AutorizarReservacion(long idReservacion)
        {
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@NoReservacion", idReservacion));
            Conexion.PrepararProcedimiento("[dbo].[reservaciones.Autorizar]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);

                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = int.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        public Resultado CalificarReservacion(long idReservacion, int cantidad, string comentarios)
        {
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@NoReservacion", idReservacion));
            _Parametros.Add(new SqlParameter("@Calificacion", cantidad));
            _Parametros.Add(new SqlParameter("@Comentarios", comentarios));
            Conexion.PrepararProcedimiento("[dbo].[reservaciones.Calificar]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);

                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = int.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        public Resultado CancelarReservacion(long idReservacion)
        {
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@NoReservacion", idReservacion));
            Conexion.PrepararProcedimiento("[dbo].[reservaciones.Cancelar]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);

                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = int.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }

        public Resultado EliminarReservacion(long noReservacion)
        {
            Resultado resultado = new Resultado();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@NoReservacion", noReservacion));
            Conexion.PrepararProcedimiento("[dbo].[reservaciones.Eliminar]", _Parametros);
            try
            {
                var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);

                while (dataReader.Read())
                {
                    if (dataReader["Resultado"] != DBNull.Value) resultado.ID = int.Parse(dataReader["Resultado"].ToString());
                }
            }
            catch (Exception ex)
            {
                resultado.MensajeError = ex.Message;
            }
            return resultado;
        }


        public List<Reservacion> ConsultarReservaciones(long id, int tipo)
        {
            string spEjecutar = string.Empty;
            if (tipo == 1) spEjecutar = "[dbo].[reservaciones.ConsultarPorActivar]";
            else if (tipo == 2) spEjecutar = "[dbo].[reservaciones.ConsultarPorInmuebleID]";
            else if (tipo == 3) spEjecutar = "[dbo].[reservaciones.ConsultarPorSocioID]";
            else if (tipo == 4) spEjecutar = "[dbo].[reservaciones.ConsultarPorID]";
            else if (tipo == 5) spEjecutar = "[dbo].[reservaciones.ConsultarMisReservaciones]";
            else if (tipo == 6) spEjecutar = "[dbo].[reservaciones.ConsultarPorCalificar]";

            List<Reservacion> lstReservaciones = new List<Reservacion>();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@ID", id));
            Conexion.PrepararProcedimiento(spEjecutar, _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                Reservacion reservacion = new Reservacion();
                if (dataReader["No_Reservacion"] != DBNull.Value) reservacion.NoReservacion = long.Parse(dataReader["No_Reservacion"].ToString());
                if (dataReader["Socio_ID"] != DBNull.Value) reservacion.SocioID = long.Parse(dataReader["Socio_ID"].ToString());
                if (dataReader["Socio_Nombre"] != DBNull.Value) reservacion.SocioNombre = dataReader["Socio_Nombre"].ToString();
                if (dataReader["Socio_Email"] != DBNull.Value) reservacion.SocioEmail = dataReader["Socio_Email"].ToString();
                if (dataReader["Inmueble_ID"] != DBNull.Value) reservacion.InmuebleID = long.Parse(dataReader["Inmueble_ID"].ToString());
                if (dataReader["Inmueble_Nombre"] != DBNull.Value) reservacion.InmuebleNombre = dataReader["Inmueble_Nombre"].ToString();
                if (dataReader["Paquete_ID"] != DBNull.Value) reservacion.PaqueteID = long.Parse(dataReader["Paquete_ID"].ToString());
                if (dataReader["Paquete_Nombre"] != DBNull.Value) reservacion.PaqueteNombre = dataReader["Paquete_Nombre"].ToString();
                if (dataReader["Fecha_Reservacion_Inicia"] != DBNull.Value) reservacion.FechaReservacionInicia = DateTime.Parse(dataReader["Fecha_Reservacion_Inicia"].ToString());
                if (dataReader["Fecha_Reservacion_Termina"] != DBNull.Value) reservacion.FechaReservacionTermina = DateTime.Parse(dataReader["Fecha_Reservacion_Termina"].ToString());
                if (dataReader["Costo"] != DBNull.Value) reservacion.Costo = decimal.Parse(dataReader["Costo"].ToString());
                if (dataReader["Estatus"] != DBNull.Value) reservacion.Estatus = int.Parse(dataReader["Estatus"].ToString());
                lstReservaciones.Add(reservacion);
            }
            return lstReservaciones;
        }

        public List<Calificacion> ConsultarCalificaciones(long id)
        {
            List<Calificacion> lstCalificaciones = new List<Calificacion>();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@ID", id));
            Conexion.PrepararProcedimiento("[dbo].[calificaciones.ConsultarPorInmueble]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                Calificacion calificacion = new Calificacion();
                if (dataReader["No_Calificacion"] != DBNull.Value) calificacion.NoCalificacion = long.Parse(dataReader["No_Calificacion"].ToString());
                if (dataReader["No_Reservacion"] != DBNull.Value) calificacion.NoReservacion = long.Parse(dataReader["No_Reservacion"].ToString());
                if (dataReader["Socio_ID"] != DBNull.Value) calificacion.SocioID = long.Parse(dataReader["Socio_ID"].ToString());
                if (dataReader["Socio_Nombre"] != DBNull.Value) calificacion.SocioNombre = dataReader["Socio_Nombre"].ToString();
                if (dataReader["Inmueble_ID"] != DBNull.Value) calificacion.InmuebleID = long.Parse(dataReader["Inmueble_ID"].ToString());
                if (dataReader["Inmueble_Nombre"] != DBNull.Value) calificacion.InmuebleNombre = dataReader["Inmueble_Nombre"].ToString();
                if (dataReader["Fecha_Creo"] != DBNull.Value) calificacion.Fecha = DateTime.Parse(dataReader["Fecha_Creo"].ToString());
                if (dataReader["Calificacion"] != DBNull.Value) calificacion.ValorCalificacion = int.Parse(dataReader["Calificacion"].ToString());
                if (dataReader["Comentario"] != DBNull.Value) calificacion.Comentario = dataReader["Comentario"].ToString();
                lstCalificaciones.Add(calificacion);
            }
            return lstCalificaciones;
        }

        public List<Reservacion> ConsultarFechasInmuebles(long id)
        {
            List<Reservacion> lstReservaciones = new List<Reservacion>();
            List<SqlParameter> _Parametros = new List<SqlParameter>();
            _Parametros.Add(new SqlParameter("@ID", id));
            Conexion.PrepararProcedimiento("[dbo].[inmuebles.ConsultarFechas]", _Parametros);
            var dataReader = Conexion.EjecutarReader(CommandType.StoredProcedure);
            while (dataReader.Read())
            {
                Reservacion reservacion = new Reservacion();
                if (dataReader["No_Reservacion"] != DBNull.Value) reservacion.NoReservacion = long.Parse(dataReader["No_Reservacion"].ToString());
                if (dataReader["Socio_ID"] != DBNull.Value) reservacion.SocioID = long.Parse(dataReader["Socio_ID"].ToString());
                if (dataReader["Socio_Nombre"] != DBNull.Value) reservacion.SocioNombre = dataReader["Socio_Nombre"].ToString();
                if (dataReader["Socio_Email"] != DBNull.Value) reservacion.SocioEmail = dataReader["Socio_Email"].ToString();
                if (dataReader["Inmueble_ID"] != DBNull.Value) reservacion.InmuebleID = long.Parse(dataReader["Inmueble_ID"].ToString());
                if (dataReader["Inmueble_Nombre"] != DBNull.Value) reservacion.InmuebleNombre = dataReader["Inmueble_Nombre"].ToString();
                if (dataReader["Paquete_ID"] != DBNull.Value) reservacion.PaqueteID = long.Parse(dataReader["Paquete_ID"].ToString());
                if (dataReader["Paquete_Nombre"] != DBNull.Value) reservacion.PaqueteNombre = dataReader["Paquete_Nombre"].ToString();
                if (dataReader["Fecha_Reservacion_Inicia"] != DBNull.Value) reservacion.FechaReservacionInicia = DateTime.Parse(dataReader["Fecha_Reservacion_Inicia"].ToString());
                if (dataReader["Fecha_Reservacion_Termina"] != DBNull.Value) reservacion.FechaReservacionTermina = DateTime.Parse(dataReader["Fecha_Reservacion_Termina"].ToString());
                if (dataReader["Costo"] != DBNull.Value) reservacion.Costo = decimal.Parse(dataReader["Costo"].ToString());
                if (dataReader["Estatus"] != DBNull.Value) reservacion.Estatus = int.Parse(dataReader["Estatus"].ToString());
                lstReservaciones.Add(reservacion);
            }
            return lstReservaciones;
        }

        #endregion Operaciones

        #region IDisposable Members

        public new void Dispose()
        {
            try
            {
                GC.Collect();
            }
            catch { }
        }
        #endregion
    }
}
